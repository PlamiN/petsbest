//
//  LoadingScreen.swift
//  PetsBest
//
//  Created by Plamena on 27.10.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoadingScreen: UIView {
    @IBOutlet weak var activityIndicatorScreen: NVActivityIndicatorView!
    @IBOutlet weak var labelLoadingText: UILabel!
    @IBOutlet weak var activityIndicatorHeight: NSLayoutConstraint!
    @IBOutlet weak var activityIndicatorWidth: NSLayoutConstraint!
}
