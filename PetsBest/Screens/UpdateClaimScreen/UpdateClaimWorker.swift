//
//  UpdateClaimWorker.swift
//  PetsBest
//
//  Created by Plamena on 29.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation
import KeychainSwift

typealias OnSuccessCompletionUpdateClaim = (Result<UpdateClaimModel>) -> ()
typealias OnErrorHandlerUpdateClaim = (Result<String>) -> ()

class UpdateClaimWorker {
    private let defaults = UserDefaults.standard
    private let keychain = KeychainSwift()
    
    func updateClaim(withClaimId: Int, withCheckedFraud: Bool, withCheckedDeclarations: Bool, withFiles: [[String : Any]], withFileName: String, completionHandler: @escaping OnSuccessCompletionUpdateClaim,
                   onErrorHandler: @escaping OnErrorHandlerUpdateClaim) {
        if var url = try? APIRouter.update_claim.asURLRequest() {
            let parameterDictionary = ["ClaimId": withClaimId,
                                       "FraudWarningChecked": withCheckedFraud,
                                       "DeclarationsChecked": withCheckedDeclarations,
                                       "Files": withFiles] as [String : Any]
            url.httpMethod = HTTPMethod.post.rawValue
            
            url.timeoutInterval = 180
            let appToken = keychain.get("appToken")
            
            guard let token = appToken?.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil) else { return }
            url.setValue(token, forHTTPHeaderField: "AuthToken")
            url.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
                return
            }
            url.httpBody = httpBody
            
            URLSession.shared.dataTask(with: url) { data, response, error in
                
                if let error = error {
                    return onErrorHandler(.failure(error))
                }
                
                guard let content = data else {
                    print("No data")
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    let statusCode = httpResponse.statusCode
                    
                    if (statusCode != 200) {
                        guard
                            let errorString = String(data: content, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                            else { return }
                        print(errorString)
                        return onErrorHandler(.success(errorString))
                    } else {
                        
                        let response = try? JSONDecoder().decode(UpdateClaimModel.self, from: content)
                        guard
                            let successString = String(data: content, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                            else { return }
                        print(successString)
                        if let response = response {
                            if response.Success == true {
                                DispatchQueue.main.async {
                                    completionHandler(.success(response))
                                }
                            } else {
                                DispatchQueue.main.async {
                                    onErrorHandler(.success(response.Message!))
                                }
                            }
                        }
                    }
                }
            }.resume()
        }
    }
}
