//
//  UpdateClaimViewController.swift
//  PetsBest
//
//  Created by Plamena on 15.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import WeScan
import MobileCoreServices
import NVActivityIndicatorView
import FirebaseAnalytics
import FirebaseFirestore
import FirebaseAuth
import KeychainSwift

extension Int {
    var byteSize: String {
        return ByteCountFormatter().string(fromByteCount: Int64(self))
    }
}

class UpdateClaimViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var activityIndicatorScreen: NVActivityIndicatorView!
    
    var claimId: Int?
    var petName: String = ""
    private let worker = ClaimDetailsWorker()
    private let workerUpdate = UpdateClaimWorker()
    private var claim: ClaimDetails?
    private var picker = UIImagePickerController()
    private var imageDocument: UIImage?
    private var imageSecondDocument: UIImage?
    private var imageThirdDocument: UIImage?
    private var imageFourthDocument: UIImage?
    
    private var isAgreedChecked = false
    private var isPolicyChecked = false
    private var isFraudChecked = false
    private var isUploadedDocument = false
    
    private var loadingScreenView = LoadingScreen()
    private var submittedClaimView = SubmittedClaimView()
    
    private var isPDF = [false, false, false, false]
    var fileFirstString:String = ""
    var fileSecondString:String = ""
    var fileThirdString:String = ""
    var fileFourthString:String = ""
    
    private var policyNumber:String = ""
    
    private let db = Firestore.firestore()
    private let keychain = KeychainSwift()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        networkingClaimDetails()
        configTableViewProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
    }
    
    //Firebase Analytics
    override func viewDidAppear(_ animated: Bool) {
        Analytics.logEvent(Constants.FirebaseEvents.updateClaim, parameters: nil)
    }
    
    private func setViews() {
        self.navigationItem.title = Constants.titlesScreens.updateClaim
        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
        
        loadingScreenView = (Bundle.main.loadNibNamed("LoadingScreen", owner: self, options: nil)?.first as? LoadingScreen)!
        loadingScreenView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(loadingScreenView)
        loadingScreenView.labelLoadingText.textColor = UIColor.init(hex: Constants.Colors.labels)
        loadingScreenView.labelLoadingText.text = Constants.titlesLabels.loadingUpdateClaimText
        loadingScreenView.isHidden = true
        
        if UIDevice().userInterfaceIdiom == .pad {
            
            loadingScreenView.labelLoadingText.font = UIFont(name: "Proxima Nova Bold", size: 47)
            loadingScreenView.activityIndicatorHeight.constant = 240
            loadingScreenView.activityIndicatorWidth.constant = 240
            loadingScreenView.labelLoadingText.font = UIFont(name: "Proxima Nova Bold", size: 47)
            loadingScreenView.activityIndicatorHeight.constant = 240
            loadingScreenView.activityIndicatorWidth.constant = 240
        }
        
        //view after submitting claim
        submittedClaimView = (Bundle.main.loadNibNamed("SubmittedClaimView", owner: self, options: nil)?.first as? SubmittedClaimView)!
        submittedClaimView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(submittedClaimView)
        
        submittedClaimView.buttonSubmitClaim.setTitle("Submit New Claim", for: .normal)
        submittedClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        submittedClaimView.buttonSubmitClaim.setTitleColor(UIColor.white, for: .normal)
        submittedClaimView.buttonSubmitClaim.layer.cornerRadius = 20
        submittedClaimView.isHidden = true
        submittedClaimView.labelDone.text = "DONE"
        submittedClaimView.labelDone.textColor = UIColor.init(hex: Constants.Colors.orangeButton)
        let tapDoneClaim = UITapGestureRecognizer(target: self, action:  #selector(doneClaim))
        submittedClaimView.labelDone.isUserInteractionEnabled = true
        submittedClaimView.labelDone.addGestureRecognizer(tapDoneClaim)
        submittedClaimView.labelTitle.text = Constants.titlesLabels.labelClaimUpdate
        submittedClaimView.buttonSubmitClaim.setTitle(Constants.titlesLabels.buttonClaimUpdate, for: .normal)
        let tapSubmitNewClaim = UITapGestureRecognizer(target: self, action:  #selector(updateNewClaim))
        submittedClaimView.buttonSubmitClaim.addGestureRecognizer(tapSubmitNewClaim)
        
        submittedClaimView.isHidden = true
        drawDottedLine(start: CGPoint(x: submittedClaimView.secondView.bounds.minX, y: submittedClaimView.secondView.bounds.minY), end: CGPoint(x: submittedClaimView.secondView.bounds.maxX, y: submittedClaimView.secondView.bounds.minY), view: submittedClaimView.secondView)
        
        DispatchQueue.main.async {
            Spinner.start()
        }
    }
    
    @objc func doneClaim() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func updateNewClaim() {
        Analytics.logEvent(Constants.FirebaseEvents.submitClaim, parameters: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [7, 3] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    @objc private func back(){
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Set tableView
    
    private func configTableViewProperties() {
        tableView.registerCell(UpdateClaimTableViewCell.self)
        tableView.registerCell(TitleTableViewCell.self)
        tableView.registerCell(UploadClaimTableViewCell.self)
        tableView.registerCell(IncludedDocumentsTableViewCell.self)
        tableView.registerCell(AgreeCheckboxTableViewCell.self)
        tableView.registerCell(FraudCheckboxTableViewCell.self)
        tableView.registerCell(ViewExampleTableViewCell.self)
        tableView.registerCell(SubmitTableViewCell.self)
        tableView.registerCell(UploadedDocumentTableViewCell.self)
        tableView.registerCell(UploadClaimIPadTableViewCell.self)
        tableView.separatorStyle = .none
    }
    
    private func networkingClaimDetails() {
        guard let id = claimId else { return }
        worker.getClaimDetails(withClaimId: id, completionHandler: { [weak self] resultUser in
            switch resultUser {
            case .success(let resultUser):
                self?.claim = resultUser
                guard let number = self?.claim?.PolicyNumber else {
                    return
                }
                self?.policyNumber = "\(number)"
                self?.tableView.reloadData()
                DispatchQueue.main.async {
                    Spinner.stop()
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            }
        }) { [weak self] error  in
            switch error {
            case .success(let error):
                DispatchQueue.main.async {
                    self?.showAlert(withTitle: "", message: error, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            }
        }
    }
    
}

extension UpdateClaimViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if imageDocument != nil && imageSecondDocument != nil {
            return 8
        } else {
            return 7
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            if UIDevice().userInterfaceIdiom == .pad {
                return 400.0
            } else {
                return 260.0
            }
        }
        if indexPath.row == 1 {
            if UIDevice().userInterfaceIdiom == .pad {
                return 150.0
            } else {
                return 90.0
            }
        }
        if indexPath.row == 2 {
            if UIDevice().userInterfaceIdiom == .pad {
                return 220.0
            } else {
                return 140.0
            }
        }
        else {
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument == nil && indexPath.row == 4 {
                if UIDevice().userInterfaceIdiom == .pad {
                    return 400.0
                } else {
                    return 170.0
                }
            }
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && indexPath.row == 4 {
                if UIDevice().userInterfaceIdiom == .pad {
                    return 400.0
                } else {
                    return 200.0
                }
            } else {
                if indexPath.row == 3 && imageDocument != nil {
                    if UIDevice().userInterfaceIdiom == .pad {
                        return 400.0
                    } else {
                        return 200.0
                    }
                }
                if indexPath.row == 3 {
                    if UIDevice().userInterfaceIdiom == .pad {
                        return 400.0
                    } else {
                        return 170.0
                    }
                } else {
                    if UIDevice().userInterfaceIdiom == .pad {
                        return 110.0
                    } else {
                        return 70.0
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UpdateClaimTableViewCell = tableView.dequeueCell(UpdateClaimTableViewCell.self)
        let cell1: TitleTableViewCell = tableView.dequeueCell(TitleTableViewCell.self)
        let cell2: UploadClaimTableViewCell = tableView.dequeueCell(UploadClaimTableViewCell.self)
        let cell3: IncludedDocumentsTableViewCell = tableView.dequeueCell(IncludedDocumentsTableViewCell.self)
        let cell4: AgreeCheckboxTableViewCell = tableView.dequeueCell(AgreeCheckboxTableViewCell.self)
        let cell5: FraudCheckboxTableViewCell = tableView.dequeueCell(FraudCheckboxTableViewCell.self)
        let cell7: SubmitTableViewCell = tableView.dequeueCell(SubmitTableViewCell.self)
        let cell6: UploadedDocumentTableViewCell = tableView.dequeueCell(UploadedDocumentTableViewCell.self)
        let cell8: UploadClaimIPadTableViewCell = tableView.dequeueCell(UploadClaimIPadTableViewCell.self)
        
        cell.selectionStyle = .none
        cell1.selectionStyle = .none
        cell2.selectionStyle = .none
        cell3.selectionStyle = .none
        cell4.selectionStyle = .none
        cell5.selectionStyle = .none
        cell6.selectionStyle = .none
        cell7.selectionStyle = .none
        cell8.selectionStyle = .none
        
        cell2.delegate = self
        cell4.delegate = self
        cell5.delegate = self
        cell6.delegate = self
        cell6.delegate1 = self
        cell8.delegate = self
        
        if indexPath.row == 0 {
            if let claimDetails = claim {
                let dateIncident = claimDetails.IncidentDate.toDate()
                
                if let dateUnwrappedIncident = dateIncident {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM/dd/YYYY"
                    cell.configureCell(withClaimNumber: claimDetails.ClaimId, withClaimType: "\(claimDetails.ClaimType)", withCondition: claimDetails.Description, withDate: "\(formatter.string(from: dateUnwrappedIncident))", withName: claimDetails.PetInfo.PetName, withPolicyNumber: "\(claimDetails.PolicyNumber)", withIndex: indexPath.row, withClaimStatus: claimDetails.StatusName)
                }
            }
            return cell
        }
        if indexPath.row == 1 {
            cell1.configureCell(withTitle: Constants.titlesLabels.addInvoice,withTitle2: "", withTitle3: "")
            return cell1
        }
        if indexPath.row == 2 {
            cell3.configureCell(withFirstTitleInvoice: Constants.titlesLabels.firstDocuments, withSecondTitleInvoice: Constants.titlesLabels.secondDocuments, withFirstTitleReceipt: Constants.titlesLabels.thirdDocuments, withSecondTitleReceipt: Constants.titlesLabels.fourthsDocuments, withThirdTitleReceipt: Constants.titlesLabels.fifthsDocuments, withTitle: Constants.titlesLabels.titleDocuments)
            return cell3
        }
        if indexPath.row == 3 {
            if imageDocument != nil {
                if imageSecondDocument != nil {
                    if UIDevice().userInterfaceIdiom == .pad {
                    }
                    cell6.configureCell(withImage:imageDocument!, withSecondImage: imageSecondDocument, withDetails: "", atIndex: indexPath.row)
                } else {
                    cell6.configureCell(withImage: imageDocument!, withSecondImage: nil, withDetails: "", atIndex: indexPath.row)
                }
                return cell6
            } else {
                if isUploadedDocument == false {
                    if UIDevice().userInterfaceIdiom == .pad {
                        return cell8
                    } else {
                        return cell2
                    }
                } else {
                    return cell6
                }
            }
        }
        if indexPath.row == 4 {
            if imageDocument != nil && imageSecondDocument != nil {
                if imageThirdDocument != nil {
                    if imageFourthDocument != nil {
                        cell6.configureCell(withImage: imageThirdDocument!, withSecondImage: imageFourthDocument!, withDetails: "", atIndex: indexPath.row)
                        return cell6
                    } else {
                        cell6.configureCell(withImage: imageThirdDocument!, withSecondImage: nil, withDetails: "", atIndex: indexPath.row)
                        return cell6
                    }
                } else {
                    if UIDevice().userInterfaceIdiom == .pad {
                        return cell8
                    } else {
                        return cell2
                    }
                }
            } else {
                cell4.configureCell(withFirstTitle: Constants.titlesLabels.firstPolicy, withSecondTitle: Constants.titlesLabels.secondPolicy, withIndex: indexPath.row, withChecked: isPolicyChecked)
                return cell4
            }
        }
        if indexPath.row == 5 {
            if imageDocument != nil && imageSecondDocument != nil {
                cell4.configureCell(withFirstTitle: Constants.titlesLabels.firstPolicy, withSecondTitle: Constants.titlesLabels.secondPolicy, withIndex: indexPath.row, withChecked: isPolicyChecked)
                return cell4
            } else {
                cell5.configureCell(withFirstTitle: Constants.titlesLabels.firstFraud, withSecondTitle: Constants.titlesLabels.secondFraud, withThirdTitle: Constants.titlesLabels.thirdFraud, withIndex: indexPath.row, withChecked: isFraudChecked)
                return cell5
            }
        } else {
            if imageDocument != nil && imageSecondDocument != nil && indexPath.row == 6 {
                cell5.configureCell(withFirstTitle: Constants.titlesLabels.firstFraud, withSecondTitle: Constants.titlesLabels.secondFraud, withThirdTitle: Constants.titlesLabels.thirdFraud, withIndex: indexPath.row, withChecked: isFraudChecked)
                return cell5
            } else {
                if isPolicyChecked == true && isFraudChecked == true && imageDocument != nil {
                    cell7.configureCell(withButtonColor: UIColor.init(hex: Constants.Colors.orangeButton)!)
                    return cell7
                } else {
                    cell7.configureCell(withButtonColor: UIColor.gray)
                    return cell7
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let systemVersion = UIDevice.current.systemVersion
        let usernameLocal = self.keychain.get("username")
        
        let localDateFormatter = DateFormatter()
        localDateFormatter.dateStyle = .medium
        localDateFormatter.timeStyle = .medium

        // Printing a Date
        let dateTime = Date()
        //print(localDateFormatter.string(from: dateTime))

        let currentDateTime = localDateFormatter.string(from: dateTime)
        
        if indexPath.row == 6 && isPolicyChecked == true && isFraudChecked == true {
            let group = DispatchGroup()
            
            //1 document
            if imageDocument != nil && imageSecondDocument == nil && imageThirdDocument == nil && imageFourthDocument == nil {
                
                DispatchQueue.main.async {
                    self.loadingScreenView.isHidden = false
                    self.startLoadingWholeScreen()
                    self.navigationItem.leftBarButtonItem = nil
                }
                
                let nameImage = "Invoice1"
                let imageData:Data = imageDocument!.jpegData(compressionQuality: 0.6)!
                var strBase64: String?
                
                if self.isPDF[0] == false {
                    strBase64 = imageData.base64EncodedString(options: [])
                } else {
                    strBase64 = self.fileFirstString
                }
                
                let fileDictionary = [["Filename": nameImage, "ContentType": "image/jpeg",
                                      "Base64FileData": strBase64]] as [[String : Any]]
                
                //print(uiImage.getSizeIn(.megabyte))
                self.workerUpdate.updateClaim(withClaimId: self.claimId!, withCheckedFraud: self.isFraudChecked, withCheckedDeclarations: self.isPolicyChecked, withFiles: fileDictionary, withFileName: nameImage) { [weak self] resultUser in
                    switch resultUser {
                    case .success(let resultUser):
                        DispatchQueue.main.async {
                            self?.stopLoadingWholeScreen()
                            self?.loadingScreenView.isHidden = true
                            self?.submittedClaimView.isHidden = false
                            self?.submittedClaimView.labelDetails.text = "\(self!.petName)'s claim #\(resultUser.ClaimId) has been successfully updated and is now being reviewed. You will be notified once your claim has been processed."
                            self?.navigationController?.navigationBar.isHidden = true
                            
//                            Auth.auth().signInAnonymously { authResult, error in
//                                if self?.isPDF[0] == true {
//                                    self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                        "app_version": appVersion ?? "",
//                                        "claim#": "\(resultUser.ClaimId)",
//                                        "date_time": currentDateTime,
//                                        "device_info": UIDevice.modelName,
//                                        "os_version": systemVersion,
//                                        "policy#": self!.policyNumber,
//                                        "user": usernameLocal ?? "",
//                                        "filenames": [
//                                            "0": "\(nameImage).pdf"
//                                        ]
//                                    ]) { err in
//                                        if let err = err {
//                                            print("Error writing document: \(err)")
//                                        } else {
//                                            print("Document successfully written!")
//                                        }
//                                    }
//                                } else {
//                                    self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                        "app_version": appVersion ?? "",
//                                        "claim#": "\(resultUser.ClaimId)",
//                                        "date_time": currentDateTime,
//                                        "device_info": UIDevice.modelName,
//                                        "os_version": systemVersion,
//                                        "policy#": self!.policyNumber,
//                                        "user": usernameLocal ?? "",
//                                        "filenames": [
//                                            "0": "\(nameImage).jpg"
//                                        ]
//                                    ]) { err in
//                                        if let err = err {
//                                            print("Error writing document: \(err)")
//                                        } else {
//                                            print("Document successfully written!")
//                                        }
//                                    }
//                                }
//                            }
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.loadingScreenView.isHidden = true
                            self?.stopLoadingWholeScreen()
                            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                            self?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self?.back))
                        }
                    }
                } onErrorHandler: { [weak self] error  in
                    switch error {
                    case .success(_):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.loadingScreenView.isHidden = true
                            self?.stopLoadingWholeScreen()
                            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                            self?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self?.back))
                        }
                    case .failure(_):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.loadingScreenView.isHidden = true
                            self?.stopLoadingWholeScreen()
                            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                            self?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self?.back))
                        }
                    }
                }
                group.notify(queue: DispatchQueue.main) { // this here is the key
                    print("Finished")
                }
            }
        }
        
        if indexPath.row == 7 && isPolicyChecked == true && isFraudChecked == true {
            // Create a dispatch group
            let group = DispatchGroup()
            
            //2 documents
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil {
                
                DispatchQueue.main.async {
                    self.loadingScreenView.isHidden = false
                    self.startLoadingWholeScreen()
                    self.navigationItem.leftBarButtonItem = nil
                }
                
                let nameImage = "Invoice1"
                let imageData:Data = self.imageDocument!.jpegData(compressionQuality: 0.6)!
                var strBase64: String?
                
                if self.isPDF[0] == false {
                    strBase64 = imageData.base64EncodedString(options: [])
                } else {
                    strBase64 = self.fileFirstString
                }
                
                let nameImageSecond = "Invoice2"
                let imageDataSecond:Data = self.imageSecondDocument!.jpegData(compressionQuality: 0.6)!
                var strBase64Second: String?
                
                if self.isPDF[1] == false {
                    strBase64Second = imageDataSecond.base64EncodedString(options: [])
                } else {
                    strBase64Second = self.fileSecondString
                }
                
                let fileDictionary = [["Filename": nameImage, "ContentType": "image/jpeg",
                                      "Base64FileData": strBase64],["Filename": nameImageSecond, "ContentType": "image/jpeg","Base64FileData": strBase64Second] ] as [[String : Any]]
                
                self.workerUpdate.updateClaim(withClaimId: self.claimId!, withCheckedFraud: self.isFraudChecked, withCheckedDeclarations: self.isPolicyChecked, withFiles: fileDictionary, withFileName: nameImage) { [weak self] resultUser in
                    switch resultUser {
                    case .success(let resultUser):
                        DispatchQueue.main.async {
                            self?.stopLoadingWholeScreen()
                            self?.loadingScreenView.isHidden = true
                            self?.submittedClaimView.isHidden = false
                            self?.submittedClaimView.labelDetails.text = "\(self!.petName)'s claim #\(resultUser.ClaimId) has been successfully updated and is now being reviewed. You will be notified once your claim has been processed."
                            self?.navigationController?.navigationBar.isHidden = true
                            
//                            Auth.auth().signInAnonymously { authResult, error in
//                                if self?.isPDF[0] == true {
//                                    if self?.isPDF[1] == true {
//                                        self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                            "app_version": appVersion ?? "",
//                                            "claim#": "\(resultUser.ClaimId)",
//                                            "date_time": currentDateTime,
//                                            "device_info": UIDevice.modelName,
//                                            "os_version": systemVersion,
//                                            "policy#": self!.policyNumber,
//                                            "user": usernameLocal ?? "",
//                                            "filenames": [
//                                                "0": "\(nameImage).pdf",
//                                                "1": "\(nameImageSecond).pdf"
//                                            ]
//                                        ]) { err in
//                                            if let err = err {
//                                                print("Error writing document: \(err)")
//                                            } else {
//                                                print("Document successfully written!")
//                                            }
//                                        }
//                                    } else {
//                                        self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                            "app_version": appVersion ?? "",
//                                            "claim#": "\(resultUser.ClaimId)",
//                                            "date_time": currentDateTime,
//                                            "device_info": UIDevice.modelName,
//                                            "os_version": systemVersion,
//                                            "policy#": self!.policyNumber,
//                                            "user": usernameLocal ?? "",
//                                            "filenames": [
//                                                "0": "\(nameImage).pdf",
//                                                "1": "\(nameImageSecond).img"
//                                            ]
//                                        ]) { err in
//                                            if let err = err {
//                                                print("Error writing document: \(err)")
//                                            } else {
//                                                print("Document successfully written!")
//                                            }
//                                        }
//                                    }
//                                } else {
//                                    if self?.isPDF[1] == true {
//                                        self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                            "app_version": appVersion ?? "",
//                                            "claim#": "\(resultUser.ClaimId)",
//                                            "date_time": currentDateTime,
//                                            "device_info": UIDevice.modelName,
//                                            "os_version": systemVersion,
//                                            "policy#": self!.policyNumber,
//                                            "user": usernameLocal ?? "",
//                                            "filenames": [
//                                                "0": "\(nameImage).img",
//                                                "1": "\(nameImageSecond).pdf"
//                                            ]
//                                        ]) { err in
//                                            if let err = err {
//                                                print("Error writing document: \(err)")
//                                            } else {
//                                                print("Document successfully written!")
//                                            }
//                                        }
//                                    } else {
//                                        self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                            "app_version": appVersion ?? "",
//                                            "claim#": "\(resultUser.ClaimId)",
//                                            "date_time": currentDateTime,
//                                            "device_info": UIDevice.modelName,
//                                            "os_version": systemVersion,
//                                            "policy#": self!.policyNumber,
//                                            "user": usernameLocal ?? "",
//                                            "filenames": [
//                                                "0": "\(nameImage).img",
//                                                "1": "\(nameImageSecond).img"
//                                            ]
//                                        ]) { err in
//                                            if let err = err {
//                                                print("Error writing document: \(err)")
//                                            } else {
//                                                print("Document successfully written!")
//                                            }
//                                        }
//                                    }
//                                }
//                            }
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.loadingScreenView.isHidden = true
                            self?.stopLoadingWholeScreen()
                            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                            self?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self?.back))
                        }
                    }
                } onErrorHandler: { [weak self] error  in
                    switch error {
                    case .success(_):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.loadingScreenView.isHidden = true
                            self?.stopLoadingWholeScreen()
                            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                            self?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self?.back))
                        }
                    case .failure(_):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.loadingScreenView.isHidden = true
                            self?.stopLoadingWholeScreen()
                            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                            self?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self?.back))
                        }
                    }
                }
                group.notify(queue: DispatchQueue.main) { // this here is the key
                    print("Finished")
                }
            }
            //3 documents
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                
                DispatchQueue.main.async {
                    self.loadingScreenView.isHidden = false
                    self.startLoadingWholeScreen()
                    self.navigationItem.leftBarButtonItem = nil
                }
                
                let nameImage = "Invoice1"
                let imageData:Data = self.imageDocument!.jpegData(compressionQuality: 0.6)!
                var strBase64: String?
                
                if self.isPDF[0] == false {
                    strBase64 = imageData.base64EncodedString(options: [])
                } else {
                    strBase64 = self.fileFirstString
                }
                
                let nameImageSecond = "Invoice2"
                let imageDataSecond:Data = self.imageSecondDocument!.jpegData(compressionQuality: 0.6)!
                var strBase64Second: String?
                
                if self.isPDF[1] == false {
                    strBase64Second = imageDataSecond.base64EncodedString(options: [])
                } else {
                    strBase64Second = self.fileSecondString
                }
                
                let nameImageThird = "Invoice3"
                let imageDataThird:Data = self.imageThirdDocument!.jpegData(compressionQuality: 0.6)!

                var strBase64Third: String?
                
                if self.isPDF[2] == false {
                    strBase64Third = imageDataThird.base64EncodedString(options: [])
                } else {
                    strBase64Third = self.fileThirdString
                }
                
                let fileDictionary = [["Filename": nameImage, "ContentType": "image/jpeg",
                                      "Base64FileData": strBase64],["Filename": nameImageSecond, "ContentType": "image/jpeg","Base64FileData": strBase64Second],["Filename": nameImageThird, "ContentType": "image/jpeg","Base64FileData": strBase64Third]] as [[String : Any]]
                self.workerUpdate.updateClaim(withClaimId: self.claimId!, withCheckedFraud: self.isFraudChecked, withCheckedDeclarations: self.isPolicyChecked, withFiles: fileDictionary, withFileName: nameImage) { [weak self] resultUser in
                    switch resultUser {
                    case .success(let resultUser):
                        DispatchQueue.main.async {
                            self?.stopLoadingWholeScreen()
                            self?.loadingScreenView.isHidden = true
                            self?.submittedClaimView.isHidden = false
                            self?.submittedClaimView.labelDetails.text = "\(self!.petName)'s claim #\(resultUser.ClaimId) has been successfully updated and is now being reviewed. You will be notified once your claim has been processed."
                            self?.navigationController?.navigationBar.isHidden = true
                            
//                            Auth.auth().signInAnonymously { authResult, error in
//                                if self?.isPDF[0] == true {
//                                    if self?.isPDF[1] == true {
//                                        if self?.isPDF[2] == true {
//                                            //1-pdf
//                                            //2-pdf
//                                            //3-pdf
//                                            self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                "app_version": appVersion ?? "",
//                                                "claim#": "\(resultUser.ClaimId)",
//                                                "date_time": currentDateTime,
//                                                "device_info": UIDevice.modelName,
//                                                "os_version": systemVersion,
//                                                "policy#": self!.policyNumber,
//                                                "user": usernameLocal ?? "",
//                                                "filenames": [
//                                                    "0": "\(nameImage).pdf",
//                                                    "1": "\(nameImageSecond).pdf",
//                                                    "2": "\(nameImageThird).pdf"
//                                                ]
//                                            ]) { err in
//                                                if let err = err {
//                                                    print("Error writing document: \(err)")
//                                                } else {
//                                                    print("Document successfully written!")
//                                                }
//                                            }
//                                        } else {
//                                            //1-pdf
//                                            //2-pdf
//                                            //3-img
//                                            self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                "app_version": appVersion ?? "",
//                                                "claim#": "\(resultUser.ClaimId)",
//                                                "date_time": currentDateTime,
//                                                "device_info": UIDevice.modelName,
//                                                "os_version": systemVersion,
//                                                "policy#": self!.policyNumber,
//                                                "user": usernameLocal ?? "",
//                                                "filenames": [
//                                                    "0": "\(nameImage).pdf",
//                                                    "1": "\(nameImageSecond).pdf",
//                                                    "2": "\(nameImageThird).img"
//                                                ]
//                                            ]) { err in
//                                                if let err = err {
//                                                    print("Error writing document: \(err)")
//                                                } else {
//                                                    print("Document successfully written!")
//                                                }
//                                            }
//                                        }
//                                    } else {
//                                        //1-pdf
//                                        //2-img
//                                        //3-pdf
//                                        if self?.isPDF[2] == true {
//                                            self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                "app_version": appVersion ?? "",
//                                                "claim#": "\(resultUser.ClaimId)",
//                                                "date_time": currentDateTime,
//                                                "device_info": UIDevice.modelName,
//                                                "os_version": systemVersion,
//                                                "policy#": self!.policyNumber,
//                                                "user": usernameLocal ?? "",
//                                                "filenames": [
//                                                    "0": "\(nameImage).pdf",
//                                                    "1": "\(nameImageSecond).img",
//                                                    "2": "\(nameImageThird).pdf"
//                                                ]
//                                            ]) { err in
//                                                if let err = err {
//                                                    print("Error writing document: \(err)")
//                                                } else {
//                                                    print("Document successfully written!")
//                                                }
//                                            }
//                                        } else {
//                                            //1-pdf
//                                            //2-img
//                                            //3-img
//                                            self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                "app_version": appVersion ?? "",
//                                                "claim#": "\(resultUser.ClaimId)",
//                                                "date_time": currentDateTime,
//                                                "device_info": UIDevice.modelName,
//                                                "os_version": systemVersion,
//                                                "policy#": self!.policyNumber,
//                                                "user": usernameLocal ?? "",
//                                                "filenames": [
//                                                    "0": "\(nameImage).pdf",
//                                                    "1": "\(nameImageSecond).img",
//                                                    "2": "\(nameImageThird).img"
//                                                ]
//                                            ]) { err in
//                                                if let err = err {
//                                                    print("Error writing document: \(err)")
//                                                } else {
//                                                    print("Document successfully written!")
//                                                }
//                                            }
//                                        }
//                                    }
//                                } else {
//                                    if self?.isPDF[1] == true {
//                                        if self?.isPDF[2] == true {
//                                            //1-img
//                                            //2-pdf
//                                            //3-pdf
//                                            self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                "app_version": appVersion ?? "",
//                                                "claim#": "\(resultUser.ClaimId)",
//                                                "date_time": currentDateTime,
//                                                "device_info": UIDevice.modelName,
//                                                "os_version": systemVersion,
//                                                "policy#": self!.policyNumber,
//                                                "user": usernameLocal ?? "",
//                                                "filenames": [
//                                                    "0": "\(nameImage).img",
//                                                    "1": "\(nameImageSecond).pdf",
//                                                    "2": "\(nameImageThird).pdf"
//                                                ]
//                                            ]) { err in
//                                                if let err = err {
//                                                    print("Error writing document: \(err)")
//                                                } else {
//                                                    print("Document successfully written!")
//                                                }
//                                            }
//                                        } else {
//                                            //1-img
//                                            //2-pdf
//                                            //3-img
//                                            self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                "app_version": appVersion ?? "",
//                                                "claim#": "\(resultUser.ClaimId)",
//                                                "date_time": currentDateTime,
//                                                "device_info": UIDevice.modelName,
//                                                "os_version": systemVersion,
//                                                "policy#": self!.policyNumber,
//                                                "user": usernameLocal ?? "",
//                                                "filenames": [
//                                                    "0": "\(nameImage).img",
//                                                    "1": "\(nameImageSecond).pdf",
//                                                    "2": "\(nameImageThird).img"
//                                                ]
//                                            ]) { err in
//                                                if let err = err {
//                                                    print("Error writing document: \(err)")
//                                                } else {
//                                                    print("Document successfully written!")
//                                                }
//                                            }
//                                        }
//                                    } else {
//                                        //1-img
//                                        //2-img
//                                        //3-pdf
//                                        if self?.isPDF[2] == true {
//                                            self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                "app_version": appVersion ?? "",
//                                                "claim#": "\(resultUser.ClaimId)",
//                                                "date_time": currentDateTime,
//                                                "device_info": UIDevice.modelName,
//                                                "os_version": systemVersion,
//                                                "policy#": self!.policyNumber,
//                                                "user": usernameLocal ?? "",
//                                                "filenames": [
//                                                    "0": "\(nameImage).img",
//                                                    "1": "\(nameImageSecond).img",
//                                                    "2": "\(nameImageThird).pdf"
//                                                ]
//                                            ]) { err in
//                                                if let err = err {
//                                                    print("Error writing document: \(err)")
//                                                } else {
//                                                    print("Document successfully written!")
//                                                }
//                                            }
//                                        } else {
//                                            //1-img
//                                            //2-img
//                                            //3-img
//                                            self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                "app_version": appVersion ?? "",
//                                                "claim#": "\(resultUser.ClaimId)",
//                                                "date_time": currentDateTime,
//                                                "device_info": UIDevice.modelName,
//                                                "os_version": systemVersion,
//                                                "policy#": self!.policyNumber,
//                                                "user": usernameLocal ?? "",
//                                                "filenames": [
//                                                    "0": "\(nameImage).img",
//                                                    "1": "\(nameImageSecond).img",
//                                                    "2": "\(nameImageThird).img"
//                                                ]
//                                            ]) { err in
//                                                if let err = err {
//                                                    print("Error writing document: \(err)")
//                                                } else {
//                                                    print("Document successfully written!")
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.loadingScreenView.isHidden = true
                            self?.stopLoadingWholeScreen()
                            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                            self?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self?.back))
                        }
                    }
                } onErrorHandler: { [weak self] error  in
                    switch error {
                    case .success(_):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.loadingScreenView.isHidden = true
                            self?.stopLoadingWholeScreen()
                            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                            self?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self?.back))
                        }
                    case .failure(_):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.loadingScreenView.isHidden = true
                            self?.stopLoadingWholeScreen()
                            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                            self?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self?.back))
                        }
                    }
                }
                group.notify(queue: DispatchQueue.main) { // this here is the key
                    print("Finished")
                }
            }
            //documents
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument != nil {
                
                DispatchQueue.main.async {
                    self.loadingScreenView.isHidden = false
                    self.startLoadingWholeScreen()
                    self.navigationItem.leftBarButtonItem = nil
                }
                
                let nameImage = "Invoice1"
                let imageData:Data = self.imageDocument!.jpegData(compressionQuality: 0.6)!
                var strBase64: String?
                
                if self.isPDF[0] == false {
                    strBase64 = imageData.base64EncodedString(options: [])
                } else {
                    strBase64 = self.fileFirstString
                }
                
                let nameImageSecond = "Invoice2"
                let imageDataSecond:Data = self.imageSecondDocument!.jpegData(compressionQuality: 0.6)!
                var strBase64Second: String?
                
                if self.isPDF[1] == false {
                    strBase64Second = imageDataSecond.base64EncodedString(options: [])
                } else {
                    strBase64Second = self.fileSecondString
                }
                
                let nameImageThird = "Invoice3"
                let imageDataThird:Data = self.imageThirdDocument!.jpegData(compressionQuality: 0.6)!

                var strBase64Third: String?
                
                if self.isPDF[2] == false {
                    strBase64Third = imageDataThird.base64EncodedString(options: [])
                } else {
                    strBase64Third = self.fileThirdString
                }
                
                let nameImageFourth = "Invoice4"
                let imageDataFourth:Data = self.imageFourthDocument!.jpegData(compressionQuality: 0.6)!
                var strBase64Fourth: String?
                
                if self.isPDF[3] == false {
                    strBase64Fourth = imageDataFourth.base64EncodedString(options: [])
                } else {
                    strBase64Fourth = self.fileFourthString
                }
                
                let fileDictionary = [["Filename": nameImage, "ContentType": "image/jpeg",
                                      "Base64FileData": strBase64],["Filename": nameImageSecond, "ContentType": "image/jpeg","Base64FileData": strBase64Second],["Filename": nameImageThird, "ContentType": "image/jpeg","Base64FileData": strBase64Third],["Filename": nameImageFourth, "ContentType": "image/jpeg","Base64FileData": strBase64Fourth]] as [[String : Any]]
                
                self.workerUpdate.updateClaim(withClaimId: self.claimId!, withCheckedFraud: self.isFraudChecked, withCheckedDeclarations: self.isPolicyChecked, withFiles: fileDictionary, withFileName: nameImage) { [weak self] resultUser in
                    switch resultUser {
                    case .success(let resultUser):
                        DispatchQueue.main.async {
                            self?.stopLoadingWholeScreen()
                            self?.loadingScreenView.isHidden = true
                            self?.submittedClaimView.isHidden = false
                            self?.submittedClaimView.labelDetails.text = "\(self!.petName)'s claim #\(resultUser.ClaimId) has been successfully updated and is now being reviewed. You will be notified once your claim has been processed."
                            self?.navigationController?.navigationBar.isHidden = true
                            
//                            Auth.auth().signInAnonymously { authResult, error in
//                                if self?.isPDF[0] == true {
//                                    if self?.isPDF[1] == true {
//                                        if self?.isPDF[2] == true {
//                                            if self?.isPDF[3] == true {
//                                                //1-pdf
//                                                //2-pdf
//                                                //3-pdf
//                                                //4-pdf
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).pdf",
//                                                        "1": "\(nameImageSecond).pdf",
//                                                        "2": "\(nameImageThird).pdf",
//                                                        "3": "\(nameImageFourth).pdf"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            } else {
//                                                //1-pdf
//                                                //2-pdf
//                                                //3-pdf
//                                                //4-img
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).pdf",
//                                                        "1": "\(nameImageSecond).pdf",
//                                                        "2": "\(nameImageThird).pdf",
//                                                        "3": "\(nameImageThird).pdf",
//                                                        "3": "\(nameImageFourth).img"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            }
//                                        } else {
//                                            //1-pdf
//                                            //2-pdf
//                                            //3-img
//                                            //4-pdf
//                                            if self?.isPDF[3] == true {
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).pdf",
//                                                        "1": "\(nameImageSecond).pdf",
//                                                        "2": "\(nameImageThird).img",
//                                                        "3": "\(nameImageFourth).pdf"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            } else {
//                                                //1-pdf
//                                                //2-pdf
//                                                //3-img
//                                                //4-img
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).pdf",
//                                                        "1": "\(nameImageSecond).pdf",
//                                                        "2": "\(nameImageThird).pdf",
//                                                        "3": "\(nameImageThird).img",
//                                                        "3": "\(nameImageFourth).img"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    } else {
//                                        if self?.isPDF[2] == true {
//                                            if self?.isPDF[3] == true {
//                                                //1-pdf
//                                                //2-img
//                                                //3-pdf
//                                                //4-pdf
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).pdf",
//                                                        "1": "\(nameImageSecond).img",
//                                                        "2": "\(nameImageThird).pdf",
//                                                        "3": "\(nameImageFourth).pdf"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            } else {
//                                                //1-pdf
//                                                //2-img
//                                                //3-pdf
//                                                //4-img
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).pdf",
//                                                        "1": "\(nameImageSecond).img",
//                                                        "2": "\(nameImageThird).pdf",
//                                                        "3": "\(nameImageThird).img"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            }
//                                        } else {
//                                            //1-pdf
//                                            //2-img
//                                            //3-img
//                                            //4-pdf
//                                            if self?.isPDF[3] == true {
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).pdf",
//                                                        "1": "\(nameImageSecond).img",
//                                                        "2": "\(nameImageThird).img",
//                                                        "3": "\(nameImageFourth).pdf"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            } else {
//                                                //1-pdf
//                                                //2-img
//                                                //3-img
//                                                //4-img
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).pdf",
//                                                        "1": "\(nameImageSecond).img",
//                                                        "2": "\(nameImageThird).img",
//                                                        "3": "\(nameImageThird).img"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
//                                } else {
//                                    if self?.isPDF[1] == true {
//                                        if self?.isPDF[2] == true {
//                                            if self?.isPDF[3] == true {
//                                                //1-img
//                                                //2-pdf
//                                                //3-pdf
//                                                //4-pdf
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).img",
//                                                        "1": "\(nameImageSecond).pdf",
//                                                        "2": "\(nameImageThird).pdf",
//                                                        "3": "\(nameImageFourth).pdf"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            } else {
//                                                //1-img
//                                                //2-pdf
//                                                //3-pdf
//                                                //4-img
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).img",
//                                                        "1": "\(nameImageSecond).pdf",
//                                                        "2": "\(nameImageThird).pdf",
//                                                        "3": "\(nameImageThird).img"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            }
//                                        } else {
//                                            //1-img
//                                            //2-pdf
//                                            //3-img
//                                            //4-pdf
//                                            if self?.isPDF[3] == true {
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).img",
//                                                        "1": "\(nameImageSecond).pdf",
//                                                        "2": "\(nameImageThird).img",
//                                                        "3": "\(nameImageFourth).pdf"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            } else {
//                                                //1-img
//                                                //2-pdf
//                                                //3-img
//                                                //4-img
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).img",
//                                                        "1": "\(nameImageSecond).pdf",
//                                                        "2": "\(nameImageThird).img",
//                                                        "3": "\(nameImageThird).img"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    } else {
//                                        if self?.isPDF[2] == true {
//                                            if self?.isPDF[3] == true {
//                                                //1-img
//                                                //2-img
//                                                //3-pdf
//                                                //4-pdf
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).img",
//                                                        "1": "\(nameImageSecond).img",
//                                                        "2": "\(nameImageThird).pdf",
//                                                        "3": "\(nameImageFourth).pdf"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            } else {
//                                                //1-img
//                                                //2-img
//                                                //3-pdf
//                                                //4-img
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).img",
//                                                        "1": "\(nameImageSecond).img",
//                                                        "2": "\(nameImageThird).pdf",
//                                                        "3": "\(nameImageThird).img"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            }
//                                        } else {
//                                            //1-img
//                                            //2-img
//                                            //3-img
//                                            //4-pdf
//                                            if self?.isPDF[3] == true {
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).img",
//                                                        "1": "\(nameImageSecond).img",
//                                                        "2": "\(nameImageThird).img",
//                                                        "3": "\(nameImageFourth).pdf"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            } else {
//                                                //1-img
//                                                //2-img
//                                                //3-img
//                                                //4-img
//                                                self?.db.collection("user_claims_info").document("\(resultUser.ClaimId)").setData([
//                                                    "app_version": appVersion ?? "",
//                                                    "claim#": "\(resultUser.ClaimId)",
//                                                    "date_time": currentDateTime,
//                                                    "device_info": UIDevice.modelName,
//                                                    "os_version": systemVersion,
//                                                    "policy#": self!.policyNumber,
//                                                    "user": usernameLocal ?? "",
//                                                    "filenames": [
//                                                        "0": "\(nameImage).img",
//                                                        "1": "\(nameImageSecond).img",
//                                                        "2": "\(nameImageThird).img",
//                                                        "3": "\(nameImageThird).img"
//                                                    ]
//                                                ]) { err in
//                                                    if let err = err {
//                                                        print("Error writing document: \(err)")
//                                                    } else {
//                                                        print("Document successfully written!")
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.loadingScreenView.isHidden = true
                            self?.stopLoadingWholeScreen()
                            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                            self?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self?.back))
                        }
                    }
                } onErrorHandler: { [weak self] error  in
                    switch error {
                    case .success(_):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.loadingScreenView.isHidden = true
                            self?.stopLoadingWholeScreen()
                            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                            self?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self?.back))
                        }
                    case .failure(_):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.loadingScreenView.isHidden = true
                            self?.stopLoadingWholeScreen()
                            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                            self?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self?.back))
                        }
                    }
                }
                group.notify(queue: DispatchQueue.main) { // this here is the key
                    print("Finished")
                }
            }
        } else {
            if indexPath.row == 7  {
                DispatchQueue.main.async {
                    self.showAlert(withTitle: "", message: "Please add your vet invoice(s) or proof of payment before submitting", buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    
                }
            }
        }
    }
    
    func reloadTableView() {
        
        let alert:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)

        let cameraAction = UIAlertAction(title: "Take a photo", style: UIAlertAction.Style.default) {
            UIAlertAction in
            let scannerViewController = ImageScannerController()
            scannerViewController.imageScannerDelegate = self
            self.present(scannerViewController, animated: true)
        }
        let gallaryAction = UIAlertAction(title: "Photo Library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallary()
        }
        let browseAction = UIAlertAction(title: "Browse", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openICloud()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(browseAction)
        alert.addAction(cancelAction)
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func startLoadingWholeScreen() {
        loadingScreenView.activityIndicatorScreen?.startAnimating()
    }
    
    private func stopLoadingWholeScreen() {
        loadingScreenView.activityIndicatorScreen?.stopAnimating()
    }
    
    private func startLoadingScreen() {
        activityIndicatorScreen.startAnimating()
    }
    
    private func stopLoadingScreen() {
        activityIndicatorScreen.stopAnimating()
    }
    
    func openGallary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            picker.delegate = self
            let barApperance = UINavigationBar.appearance()
            barApperance.tintColor = .systemBlue
            picker.sourceType = .savedPhotosAlbum
            picker.allowsEditing = false
            
            present(picker, animated: true, completion: nil)
            startLoadingScreen()
        }
    }
    
    func openICloud() {
        if #available(iOS 11.0, *) {
            UINavigationBar.appearance(whenContainedInInstancesOf: [UIDocumentBrowserViewController.self]).tintColor = nil
        }
        let documentsPicker = UIDocumentPickerViewController(documentTypes: ["public.image", "public.jpeg", "public.png", String(kUTTypePDF)], in: .open)
        documentsPicker.delegate = self
        documentsPicker.allowsMultipleSelection = false
        documentsPicker.modalPresentationStyle = .fullScreen
        self.present(documentsPicker, animated: true, completion: nil)
    }
}

extension UpdateClaimViewController: AgreeCheckboxDelegate, UploadClaimDelegate {
    func isCheckBoxPolicyCheck(checked: Bool, atIndex: Int) {
        isPolicyChecked = checked
        let indexPath = IndexPath(item: atIndex+2, section: 0)
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
    
    func openPolicyHolder() {
        let vc = PolicyHolderViewController()
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
}

extension UpdateClaimViewController: FraudCheckboxDelegate {
    func isCheckBoxCheck(checked: Bool, atIndex: Int) {
        isFraudChecked = checked
        let indexPath = IndexPath(item: atIndex+1, section: 0)
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
    
    func openFraudWarning() {
        let vc = FraudWarningViewController()
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
}

extension UpdateClaimViewController: ImageScannerControllerDelegate, UIDocumentPickerDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    func imageScannerController(_ scanner: ImageScannerController, didFinishScanningWithResults results: ImageScannerResults) {
        //print(results.croppedScan.image)
        startLoadingScreen()
        if imageDocument != nil && imageSecondDocument == nil && imageThirdDocument == nil && imageFourthDocument == nil {
            imageSecondDocument = results.croppedScan.image
            imageSecondDocument = imageSecondDocument!.resizeImage(image: imageSecondDocument!, newWidth: 1300)
            
            isPDF[1] = false
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.stopLoadingScreen()
            }
        } else {
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil {
                imageThirdDocument = results.croppedScan.image
                imageThirdDocument = imageThirdDocument!.resizeImage(image: imageThirdDocument!, newWidth: 1300)
                
                isPDF[2] = false
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.stopLoadingScreen()
                }
            } else {
                if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                    imageFourthDocument = results.croppedScan.image
                    imageFourthDocument = imageFourthDocument!.resizeImage(image: imageFourthDocument!, newWidth: 1300)
                    
                    isPDF[3] = false
                    
                    DispatchQueue.main.async {
                        let indexPath = IndexPath(item: 4, section: 0)
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                        self.stopLoadingScreen()
                    }
                    
                } else {
                    imageDocument = results.croppedScan.image
                    imageDocument = imageDocument!.resizeImage(image: imageDocument!, newWidth: 1300)
                    
                    isPDF[0] = false
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.stopLoadingScreen()
                    }
                }
            }
        }
        //1300px width, maintaining aspect ratio, and saved as a jpeg with 95% quality
        scanner.dismiss(animated: true)
    }
    
    func imageScannerControllerDidCancel(_ scanner: ImageScannerController) {
        scanner.dismiss(animated: true)
    }
    
    func imageScannerController(_ scanner: ImageScannerController, didFailWithError error: Error) {
        print(error)
    }
    
    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }

        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)

            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)

            ctx.cgContext.drawPDFPage(page)
        }

        return img
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard controller.documentPickerMode == .open, let url = urls.first, url.startAccessingSecurityScopedResource() else { return }
        defer {
            DispatchQueue.main.async {
                url.stopAccessingSecurityScopedResource()
            }
        }

        startLoadingScreen()
        if url.pathExtension == "pdf" {
            let url = URL(fileURLWithPath: url.path)
            guard let image = drawPDFfromURL(url: url) else { return }
            
            if imageDocument != nil && imageSecondDocument == nil && imageThirdDocument == nil && imageFourthDocument == nil {
                imageSecondDocument = image
                imageSecondDocument = imageSecondDocument!.resizeImage(image: imageSecondDocument!, newWidth: 1300)
                
                let fileData = try? Data.init(contentsOf: url)
                fileSecondString = (fileData?.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0)))!
                
                isPDF[1] = true
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.stopLoadingScreen()
                }
            } else {
                if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil {
                    imageThirdDocument = image
                    imageThirdDocument = imageThirdDocument!.resizeImage(image: imageThirdDocument!, newWidth: 1300)
                    
                    let fileData = try? Data.init(contentsOf: url)
                    fileThirdString = (fileData?.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0)))!
                    
                    isPDF[2] = true
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.stopLoadingScreen()
                    }
                } else {
                    if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                        imageFourthDocument = image
                        imageFourthDocument = imageFourthDocument!.resizeImage(image: imageFourthDocument!, newWidth: 1300)
                        
                        let fileData = try? Data.init(contentsOf: url)
                        fileFourthString = (fileData?.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0)))!
                        
                        isPDF[3] = true
                        
                        DispatchQueue.main.async {
                            let indexPath = IndexPath(item: 4, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .fade)
                            self.stopLoadingScreen()
                        }
                    } else {
                        imageDocument = image
                        imageDocument = imageDocument!.resizeImage(image: imageDocument!, newWidth: 1300)

                        let fileData = try? Data.init(contentsOf: url)
                        fileFirstString = (fileData?.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0)))!
                        
                        isPDF[0] = true
                        
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            self.stopLoadingScreen()
                        }
                    }
                }
            }
            
        } else {
            guard let image = UIImage(contentsOfFile: url.path) else { return }
            if imageDocument != nil && imageSecondDocument == nil && imageThirdDocument == nil && imageFourthDocument == nil {
                imageSecondDocument = image
                imageSecondDocument = imageSecondDocument!.resizeImage(image: imageSecondDocument!, newWidth: 1300)
                
                isPDF[1] = false
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.stopLoadingScreen()
                }
            } else {
                if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil {
                    imageThirdDocument = image
                    imageThirdDocument = imageThirdDocument!.resizeImage(image: imageThirdDocument!, newWidth: 1300)
                    
                    isPDF[2] = false
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.stopLoadingScreen()
                    }
                } else {
                    if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                        imageFourthDocument = image
                        imageFourthDocument = imageFourthDocument!.resizeImage(image: imageFourthDocument!, newWidth: 1300)
                        
                        isPDF[3] = false
                        
                        DispatchQueue.main.async {
                            let indexPath = IndexPath(item: 4, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .fade)
                            self.stopLoadingScreen()
                        }
                        
                    } else {
                        imageDocument = image
                        imageDocument = imageDocument!.resizeImage(image: imageDocument!, newWidth: 1300)
                        
                        isPDF[0] = false
                        
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            self.stopLoadingScreen()
                        }
                    }
                }
            }
        }
        controller.dismiss(animated: true)
    }

    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true)
    }
    
    // For Swift 4.2+
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        if imageDocument != nil && imageSecondDocument == nil && imageThirdDocument == nil && imageFourthDocument == nil {
            imageSecondDocument = image
            imageSecondDocument = imageSecondDocument!.resizeImage(image: imageSecondDocument!, newWidth: 1300)
            
            isPDF[1] = false
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.stopLoadingScreen()
            }
        } else {
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil {
                imageThirdDocument = image
                imageThirdDocument = imageThirdDocument!.resizeImage(image: imageThirdDocument!, newWidth: 1300)
                
                isPDF[2] = false
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.stopLoadingScreen()
                }
            } else {
                if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                    imageFourthDocument = image
                    imageFourthDocument = imageFourthDocument!.resizeImage(image: imageFourthDocument!, newWidth: 1300)
                    
                    isPDF[3] = false
                    
                    DispatchQueue.main.async {
                        let indexPath = IndexPath(item: 4, section: 0)
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                        self.stopLoadingScreen()
                    }
                    
                } else {
                    imageDocument = image
                    imageDocument = imageDocument!.resizeImage(image: imageDocument!, newWidth: 1300)
                    
                    isPDF[0] = false
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.stopLoadingScreen()
                    }
                }
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }
}

extension UpdateClaimViewController: ViewExampleDelegate {
    func navigateToEOB() {
        
    }
}

extension UpdateClaimViewController: UploadedDocumentDelegate {
    func reloadTableView(atIndex: Int, atRow: Int) {
        if atRow == 3 && atIndex == 0 {
            if imageDocument != nil && imageSecondDocument == nil && imageThirdDocument == nil && imageFourthDocument == nil {
                imageDocument = nil
                isUploadedDocument = false
            }
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil{
                imageDocument = imageSecondDocument
                imageSecondDocument = nil
            }
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                imageDocument = imageSecondDocument
                imageSecondDocument = imageThirdDocument
                imageThirdDocument = nil
            }
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument != nil{
                imageDocument = imageSecondDocument
                imageSecondDocument = imageThirdDocument
                imageThirdDocument = imageFourthDocument
                imageFourthDocument = nil
            }
            tableView.reloadData()
        }
        if atRow == 3 && atIndex == 1 {
            if imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil {
                imageSecondDocument = nil
            }
            if imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                imageSecondDocument = imageThirdDocument
                imageThirdDocument = nil
            }
            if imageSecondDocument != nil && imageFourthDocument != nil && imageFourthDocument != nil  {
                imageSecondDocument = imageThirdDocument
                imageThirdDocument = imageFourthDocument
                imageFourthDocument = nil
            }
            tableView.reloadData()
        }
        if atRow == 4 && atIndex == 0 {
            if imageFourthDocument != nil {
                imageThirdDocument = imageFourthDocument
                imageFourthDocument = nil
            } else {
                imageThirdDocument = nil
            }
            let indexPath = IndexPath(item: atRow, section: 0)
            tableView.reloadRows(at: [indexPath], with: .fade)
        }
        if atRow == 4 && atIndex == 1 {
            imageFourthDocument = nil
            let indexPath = IndexPath(item: atRow, section: 0)
            tableView.reloadRows(at: [indexPath], with: .fade)
        }
    }
}
