//
//  FraudCheckboxTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 15.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol FraudCheckboxDelegate {
    func openFraudWarning()
    func isCheckBoxCheck(checked: Bool, atIndex: Int)
}

class FraudCheckboxTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var imageViewCheckbox: UIImageView!
    @IBOutlet private weak var labelFirst: UILabel!
    @IBOutlet private weak var labelSecond: UILabel!
    @IBOutlet private weak var labelThird: UnderlinedLabel!
    @IBOutlet private weak var imageHeigth: NSLayoutConstraint!
    @IBOutlet private weak var imageWidth: NSLayoutConstraint!
    
    var delegate: FraudCheckboxDelegate?
    var index: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelThird.textColor = UIColor.init(hex: Constants.Colors.main)
        imageViewCheckbox.image = UIImage(named: "empty")
        let tapCheckAgree = UITapGestureRecognizer(target: self, action:  #selector(checkAgreeFraud))
        imageViewCheckbox.isUserInteractionEnabled = true
        imageViewCheckbox.addGestureRecognizer(tapCheckAgree)
        
        let tapDownloadFraudWarning = UITapGestureRecognizer(target: self, action:  #selector(openFraud))
        labelThird.isUserInteractionEnabled = true
        labelThird.addGestureRecognizer(tapDownloadFraudWarning)
        
        if UIDevice().userInterfaceIdiom == .pad {
            labelFirst.font = UIFont(name: "Proxima Nova", size: 20)
            labelSecond.font = UIFont(name: "Proxima Nova", size: 20)
            labelThird.font = UIFont(name: "Proxima Nova", size: 20)
            imageWidth.constant = 60
            imageHeigth.constant = 60
        }
    }
    
    func configureCell(withFirstTitle: String, withSecondTitle: String, withThirdTitle: String, withIndex: Int, withChecked: Bool) {
        labelFirst.text = withFirstTitle
        labelSecond.text = withSecondTitle
        labelThird.text = withThirdTitle
        index = withIndex
        if withChecked == true {
            imageViewCheckbox.image = UIImage(named: "ticked")
        } else {
            imageViewCheckbox.image = UIImage(named: "empty")
        }
    }
    
    @objc func checkAgreeFraud() {
        if imageViewCheckbox.image == UIImage(named: "empty") {
            imageViewCheckbox.image = UIImage(named: "ticked")
            delegate?.isCheckBoxCheck(checked: true, atIndex: index!)
        } else {
            imageViewCheckbox.image = UIImage(named: "empty")
            delegate?.isCheckBoxCheck(checked: false, atIndex: index!)
        }
    }
    
    @objc private func openFraud() {
        delegate?.openFraudWarning()
    }
}
