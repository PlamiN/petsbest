//
//  IncludedDocumentsTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 21.10.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class IncludedDocumentsTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var checkBoxInvoice: UIImageView!
    @IBOutlet private weak var checkboxReceipt: UIImageView!
    @IBOutlet private weak var labelInvoiceFirst: UILabel!
    @IBOutlet private weak var labelInvoiceSecond: UILabel!
    @IBOutlet private weak var labelReceiptFirst: UILabel!
    @IBOutlet private weak var labelReceiptSecond: UILabel!
    @IBOutlet private weak var labelReceiptThird: UILabel!
    @IBOutlet private weak var checkBoxInvoiceHeight: NSLayoutConstraint!
    @IBOutlet private weak var checkBoxInvoiceWidth: NSLayoutConstraint!
    @IBOutlet private weak var checkboxReceiptHeight: NSLayoutConstraint!
    @IBOutlet private weak var checkboxReceiptWidth: NSLayoutConstraint!
    @IBOutlet private weak var stackViewInvoiceWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice().userInterfaceIdiom == .pad {
            labelInvoiceFirst.font = UIFont(name: "Proxima Nova", size: 25)
            labelInvoiceSecond.font = UIFont(name: "Proxima Nova", size: 25)
            labelReceiptFirst.font = UIFont(name: "Proxima Nova", size: 25)
            labelReceiptSecond.font = UIFont(name: "Proxima Nova", size: 25)
            labelReceiptThird.font = UIFont(name: "Proxima Nova", size: 25)
            labelTitle.font = UIFont(name: "Proxima Nova", size: 25)
            checkBoxInvoiceHeight.constant = 50
            checkBoxInvoiceWidth.constant = 50
            checkboxReceiptHeight.constant = 50
            checkboxReceiptWidth.constant = 50
            stackViewInvoiceWidth.constant = 500
        }
    }
    
    func configureCell(withFirstTitleInvoice: String, withSecondTitleInvoice: String,withFirstTitleReceipt: String, withSecondTitleReceipt: String, withThirdTitleReceipt: String, withTitle: String) {
        labelTitle.text = withTitle
        labelInvoiceFirst.text = withFirstTitleInvoice
        labelInvoiceSecond.text = withSecondTitleInvoice
        labelReceiptFirst.text = withFirstTitleReceipt
        labelReceiptSecond.text = withSecondTitleReceipt
        labelReceiptThird.text = withThirdTitleReceipt
    }
}
