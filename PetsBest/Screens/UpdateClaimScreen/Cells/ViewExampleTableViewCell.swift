//
//  ViewExampleTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 15.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol ViewExampleDelegate {
    func navigateToEOB()
}

class ViewExampleTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var labelViewExample: UnderlinedLabel!
    
    var delegate: ViewExampleDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelViewExample.textColor = UIColor.init(hex: Constants.Colors.main)
        
        let tapOpenEOB = UITapGestureRecognizer(target: self, action:  #selector(openEOB))
        labelViewExample.isUserInteractionEnabled = true
        labelViewExample.addGestureRecognizer(tapOpenEOB)
        
    }
    
    func configureCell(withFirstTitle: String, withSecondTitle: String) {
        labelTitle.text = withFirstTitle
        labelViewExample.text = withSecondTitle
    }
    
    @objc func openEOB() {
        delegate?.navigateToEOB()
    }
}
