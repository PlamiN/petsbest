//
//  SubmitTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 15.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class SubmitTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var buttonSubmitClaim: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        buttonSubmitClaim.setTitle("Submit Claim", for: .normal)
        buttonSubmitClaim.backgroundColor = UIColor.gray
        buttonSubmitClaim.isEnabled = false
        buttonSubmitClaim.setTitleColor(UIColor.white, for: .normal)
        buttonSubmitClaim.layer.cornerRadius = 25
        
        if UIDevice().userInterfaceIdiom == .pad {
            buttonSubmitClaim.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 30)
            buttonSubmitClaim.layer.cornerRadius = 45
        }
    }
    
    func configureCell(withButtonColor: UIColor) {
        buttonSubmitClaim.backgroundColor = withButtonColor
    }
}
