//
//  CheckBoxTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 15.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol AgreeDocumentsCheckboxDelegate {
    func isCheckBoxAgreedCheck(checked: Bool, atIndex: Int)
}

class CheckBoxTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var imageViewCheckbox: UIImageView!
    @IBOutlet private weak var labelFirst: UILabel!
    @IBOutlet private weak var labelSecond: UILabel!
    @IBOutlet private weak var imageHeigth: NSLayoutConstraint!
    @IBOutlet private weak var imageWidth: NSLayoutConstraint!
    
    var delegate: AgreeDocumentsCheckboxDelegate?
    var index: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tapCheckAgree = UITapGestureRecognizer(target: self, action:  #selector(checkAgreeDocument))
        imageViewCheckbox.isUserInteractionEnabled = true
        imageViewCheckbox.addGestureRecognizer(tapCheckAgree)
        
        if UIDevice().userInterfaceIdiom == .pad {
            labelFirst.font = UIFont(name: "Proxima Nova Bold", size: 30)
            labelSecond.font = UIFont(name: "Proxima Nova Bold", size: 30)
            imageWidth.constant = 62
            imageHeigth.constant = 62
        }
    }
    
    @objc func checkAgreeDocument() {
        if imageViewCheckbox.image == UIImage(named: "empty") {
            imageViewCheckbox.image = UIImage(named: "ticked")
            delegate?.isCheckBoxAgreedCheck(checked: true, atIndex: index!)
        } else {
            imageViewCheckbox.image = UIImage(named: "empty")
            delegate?.isCheckBoxAgreedCheck(checked: false, atIndex: index!)
        }
    }
    
    func configureCell(withFirstTitle: String, withSecondTitle: String, withIndex: Int, withChecked: Bool) {
        labelFirst.text = withFirstTitle
        labelSecond.text = withSecondTitle
        index = withIndex
        if withChecked == true {
            imageViewCheckbox.image = UIImage(named: "ticked")
        } else {
            imageViewCheckbox.image = UIImage(named: "empty")
        }
    }
}
