//
//  AgreeCheckboxTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 15.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol AgreeCheckboxDelegate {
    func openPolicyHolder()
    func isCheckBoxPolicyCheck(checked: Bool, atIndex: Int)
}

class AgreeCheckboxTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var imageViewCheckbox: UIImageView!
    @IBOutlet private weak var labelFirst: UILabel!
    @IBOutlet private weak var labelSecond: UnderlinedLabel!
    @IBOutlet private weak var imageHeigth: NSLayoutConstraint!
    @IBOutlet private weak var imageWidth: NSLayoutConstraint!
    @IBOutlet private weak var labelFirstWidth: NSLayoutConstraint!
    
    var delegate: AgreeCheckboxDelegate?
    var index: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        labelSecond.textColor = UIColor.init(hex: Constants.Colors.main)
        imageViewCheckbox.image = UIImage(named: "empty")
        let tapCheckAgree = UITapGestureRecognizer(target: self, action:  #selector(checkAgree))
        imageViewCheckbox.isUserInteractionEnabled = true
        imageViewCheckbox.addGestureRecognizer(tapCheckAgree)
        
        let tapDownloadPolicy = UITapGestureRecognizer(target: self, action:  #selector(openPolicyDeclaration))
        labelSecond.isUserInteractionEnabled = true
        labelSecond.addGestureRecognizer(tapDownloadPolicy)
        
        if UIDevice().userInterfaceIdiom == .pad {
            labelFirst.font = UIFont(name: "Proxima Nova", size: 20)
            labelSecond.font = UIFont(name: "Proxima Nova", size: 20)
            imageWidth.constant = 60
            imageHeigth.constant = 60
            labelFirstWidth.constant = 120
        }
    }
    
    @objc func checkAgree() {
        if imageViewCheckbox.image == UIImage(named: "empty") {
            delegate?.isCheckBoxPolicyCheck(checked: true, atIndex: index!)
            imageViewCheckbox.image = UIImage(named: "ticked")
        } else {
            imageViewCheckbox.image = UIImage(named: "empty")
            delegate?.isCheckBoxPolicyCheck(checked: false, atIndex: index!)
        }
    }
    
    func configureCell(withFirstTitle: String, withSecondTitle: String, withIndex: Int, withChecked: Bool) {
        labelFirst.text = withFirstTitle
        labelSecond.text = withSecondTitle
        index = withIndex
        if withChecked == true {
            imageViewCheckbox.image = UIImage(named: "ticked")
        } else {
            imageViewCheckbox.image = UIImage(named: "empty")
        }
    }
    
    @objc private func openPolicyDeclaration() {
        delegate?.openPolicyHolder()
    }
}
