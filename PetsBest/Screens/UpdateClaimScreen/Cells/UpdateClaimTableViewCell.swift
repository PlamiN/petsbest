//
//  UpdateClaimTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 15.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class UpdateClaimTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var viewAllElements: UIView!
    @IBOutlet private weak var labelClaim: UILabel!
    @IBOutlet private weak var labelPolicy: UILabel!
    @IBOutlet private weak var labelServiceDate: UILabel!
    @IBOutlet private weak var labelServiceDateDetails: UILabel!
    @IBOutlet private weak var labelClaimType: UILabel!
    @IBOutlet private weak var labelClaimTypeDetails: UILabel!
    @IBOutlet private weak var labelCondition: UILabel!
    @IBOutlet private weak var labelConditionDetails: UILabel!
    @IBOutlet private weak var labelClaimStatus: UILabel!
    @IBOutlet private weak var labelClaimStatusDetails: UILabel!
    @IBOutlet private weak var labelStatus: UILabel!
    @IBOutlet private weak var labelStatusDetails: UILabel!
    @IBOutlet private weak var topStack: UIStackView!
    @IBOutlet private weak var viewGreyTop: UIView!
    @IBOutlet private weak var viewTitleHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelTitleHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelServiceDateHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelClaimTypeHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelStatusHeight: NSLayoutConstraint!
    
    private let worker = ClaimDetailsWorker()
    var delegate: NotFinishedClaimDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        viewAllElements.layer.cornerRadius = 10.0
        
        viewGreyTop.clipsToBounds = true
        viewGreyTop.layer.cornerRadius = 10
        viewGreyTop.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        viewGreyTop.backgroundColor =  UIColor.systemGray6
        
        if self.traitCollection.userInterfaceStyle == .dark{
            labelClaim.textColor = UIColor.white
            labelClaim.textColor = UIColor.white
            //labelPolicy.textColor = UIColor.white
            labelServiceDate.textColor = UIColor.white
            labelServiceDateDetails.textColor = UIColor.white
            labelClaimType.textColor = UIColor.white
            labelClaimTypeDetails.textColor = UIColor.white
            labelCondition.textColor = UIColor.white
            labelConditionDetails.textColor = UIColor.white
            labelClaimStatus.textColor = UIColor.white
            labelClaimStatusDetails.textColor = UIColor.white
        } else{
            labelClaim.textColor = UIColor.init(hex: Constants.Colors.labels)!
            labelPolicy.textColor = UIColor.init(hex: Constants.Colors.labels)!
            labelServiceDate.textColor = UIColor.init(hex: Constants.Colors.labels)!
            labelServiceDateDetails.textColor = UIColor.darkGray
            labelClaimType.textColor = UIColor.init(hex: Constants.Colors.labels)!
            labelClaimTypeDetails.textColor = UIColor.darkGray
            labelCondition.textColor = UIColor.init(hex: Constants.Colors.labels)!
            labelConditionDetails.textColor = UIColor.darkGray
            labelClaimStatus.textColor = UIColor.init(hex: Constants.Colors.labels)!
            labelClaimStatusDetails.textColor = UIColor.darkGray
            labelStatus.textColor = UIColor.init(hex: Constants.Colors.labels)!
            labelStatusDetails.textColor = UIColor.darkGray
        }
        
        contentView.layer.borderWidth = 1
        contentView.layer.cornerRadius = 5
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true
        contentView.layer.shadowOpacity = 0.18
        contentView.layer.shadowOffset = CGSize(width: 0, height: 2)
        contentView.layer.shadowRadius = 2
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.masksToBounds = false
        
        if UIDevice().userInterfaceIdiom == .pad {
            labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 30)
            //labelPolicy.font = UIFont(name: "Proxima Nova Bold", size: 30)
            labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelCondition.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelClaimStatus.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 25)
            labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 25)
            labelConditionDetails.font = UIFont(name: "Proxima Nova", size: 25)
            labelClaimStatusDetails.font = UIFont(name: "Proxima Nova", size: 25)
            labelStatus.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelStatusDetails.font = UIFont(name: "Proxima Nova", size: 25)
            viewTitleHeight.constant = 70
            labelTitleHeight.constant = 70
            labelServiceDateHeight.constant = 60
            labelClaimTypeHeight.constant = 60
            labelStatusHeight.constant = 60
        }
    }
    
    func configureCell(withClaimNumber: Int,withClaimType: String, withCondition: String, withDate: String, withName: String, withPolicyNumber: String, withIndex: Int, withClaimStatus: String) {
        
        DispatchQueue.main.async {
            self.labelClaim.text = Constants.titlesLabels.labelClaim + "\(withClaimNumber)"
            self.labelClaimStatus.text = "Pet Name"
            self.labelClaimStatusDetails.text = withName
            self.labelConditionDetails.text = withCondition
            self.labelServiceDateDetails.text = withDate
            self.labelStatus.text = Constants.titlesLabels.labelStatusClaim
            self.labelStatusDetails.text = withClaimStatus
            self.labelPolicy.text = Constants.titlesLabels.labelPolicyNumber + withPolicyNumber
        }
        
        worker.getClaimDetails(withClaimId: withClaimNumber, completionHandler: { [weak self] resultUser in
            switch resultUser {
            case .success(let resultUser):
                DispatchQueue.main.async {
                    if resultUser.ClaimType == 0 {
                        self?.labelClaimTypeDetails.text = Constants.ClaimTypes.accident
                    }
                    if resultUser.ClaimType == 1 {
                        self?.labelClaimTypeDetails.text = Constants.ClaimTypes.ilness
                    }
                    if resultUser.ClaimType == 2 {
                        self?.labelClaimTypeDetails.text = Constants.ClaimTypes.wellness
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.delegate?.addErroMessage(withError: error.localizedDescription)
                }
            }
        }) { [weak self] error  in
            DispatchQueue.main.async {
                self?.delegate?.addGeneralError(withError: Constants.errorMessages.generalHomeError)
            }
        }
    }
}
