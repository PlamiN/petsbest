//
//  ProfileView.swift
//  PetsBest
//
//  Created by Plamena on 21.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ProfileView: UIView {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelNoResults: UILabel!
    @IBOutlet weak var imageFilter: UIView!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonFilter: UIButton!
    @IBOutlet weak var imageFilterHeight: NSLayoutConstraint!
    @IBOutlet weak var labelNoResultsConstant: NSLayoutConstraint!
    @IBOutlet weak var topTableViewConstraint: NSLayoutConstraint!
}
