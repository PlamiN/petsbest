//
//  FilterTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 3.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell, Identifiable, ReusableView {
    
    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "downArrow"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(textField.frame.size.width - 30), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        textField.rightView = button
        textField.rightViewMode = .always
        
        let tapOpenStatuses = UITapGestureRecognizer(target: self, action:  #selector(openStatuses))
        button.isUserInteractionEnabled = true
        button.addGestureRecognizer(tapOpenStatuses)
    }
    
    @objc private func openStatuses() {
    }
    
    func configureCell(withStatus: String) {
        textField?.placeholder = withStatus
    }
}
