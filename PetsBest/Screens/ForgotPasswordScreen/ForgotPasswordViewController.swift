//
//  ForgotPasswordViewController.swift
//  PetsBest
//
//  Created by Plamena on 13.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var interfaceSegmented: CustomSegmentedControl!{
        didSet{
            interfaceSegmented.setButtonTitles(buttonTitles: [Constants.textFieldsPlaceholder.email, Constants.titlesLabels.phoneNumberZip])
            interfaceSegmented.selectorViewColor = UIColor.init(hex: Constants.Colors.main)!
            interfaceSegmented.selectorTextColor = UIColor.gray
        }
    }
    
    @IBOutlet private weak var labelResetPassword: UILabel!
    @IBOutlet private weak var textFieldUsername: SkyFloatingLabelTextField!
    @IBOutlet private weak var buttonReset: UIButton!
    @IBOutlet private weak var labelCancel: UnderlinedLabel!
    @IBOutlet private weak var labelAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
    }
    
    private func setViews() {
        if self.traitCollection.userInterfaceStyle == .dark{
            self.view.backgroundColor = UIColor.black
            self.textFieldUsername.textColor = UIColor.white
            labelResetPassword.textColor = UIColor.white
            labelAddress.textColor = UIColor.white
            interfaceSegmented.textColor = UIColor.white
        } else{
            self.view.backgroundColor = UIColor.white
            self.textFieldUsername.textColor = UIColor.gray
            labelResetPassword.textColor = UIColor.init(hex: Constants.Colors.labels)
            labelAddress.textColor = UIColor.white
            interfaceSegmented.textColor = UIColor.init(hex: Constants.Colors.main)!
        }
        setBackgroundImage("backgroundImage", contentMode: UIView.ContentMode.scaleAspectFill)
        buttonReset.setTitle(Constants.titlesButtons.recoveryPassword, for: .normal)
        buttonReset.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        buttonReset.setTitleColor(UIColor.white, for: .normal)
        buttonReset.layer.cornerRadius = 15
        
        labelResetPassword.text = Constants.titlesLabels.forgotPasswordDetails
        textFieldUsername.placeholder = Constants.textFieldsPlaceholder.emailForgotPassword
        
        textFieldUsername.delegate = self
        
        labelCancel.text = Constants.titlesLabels.cancel
        labelCancel.textColor = UIColor.init(hex: Constants.Colors.main)
        let tapCancel = UITapGestureRecognizer(target: self, action:  #selector(cancel))
        labelCancel.isUserInteractionEnabled = true
        labelCancel.addGestureRecognizer(tapCancel)

        
        // call the 'keyboardWillShow' function when the view controller receive notification that keyboard is going to be shown
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        // call the 'keyboardWillHide' function when the view controlelr receive notification that keyboard is going to be hidden
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        hideKeyboardWhenTappedAround()
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            // if keyboard size is not available for some reason, dont do anything
            return
        }
        
        // move the root view up by the distance of keyboard height
        self.view.frame.origin.y = 0
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        // move back the root view origin to zero
        self.view.frame.origin.y = 0
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyb))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyb() {
        view.endEditing(true)
    }
    
    @objc private func cancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionRecoverPassword(_ sender: UIButton) {
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func ForgotPasswordViewController(_ textField: UITextField) {
        if Validator.isEmpty(field: textField, value: "") {
            textFieldUsername.errorMessage = Constants.errorMessages.enterEmail
        }else {
            textFieldUsername.errorMessage = ""
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldUsername {
            textField.resignFirstResponder()
            textFieldUsername.becomeFirstResponder()
        }
        return true
    }
}
