//
//  HomeViewController.swift
//  PetsBest
//
//  Created by Plamena on 14.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import WebKit
import FSCalendar
import CoreData
import Foundation
import NVActivityIndicatorView
import Reachability
import UPCarouselFlowLayout
import FirebaseAnalytics
import KeychainSwift

class HomeViewController: UIViewController {
    
    init(withCoreDataStack: CoreDataStack) {
        super.init(nibName: nil, bundle: nil)
        print(withCoreDataStack.objectContext)
        moc = withCoreDataStack.objectContext
        if let moc = moc {
            petCoreDataService = PetsCoreDataService(moc: moc)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var coreDataStack = CoreDataStack()
    private var petCoreDataService: PetsCoreDataService?
    
    private let layout = UPCarouselFlowLayout()
    
    var moc: NSManagedObjectContext? {
        didSet {
            if let moc = moc {
                petCoreDataService = PetsCoreDataService(moc: moc)
            }
        }
    }
    
    @IBOutlet private weak var collectionViewPets: UICollectionView!{
        didSet {
            collectionViewPets.delegate = self
            collectionViewPets.dataSource = self
        }
    }
    
    fileprivate var pageSize: CGSize {
        let layout = self.collectionViewPets.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        } else {
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    }
    
    
    @IBOutlet private weak var activityIndicatorScreen: NVActivityIndicatorView!
    @IBOutlet private weak var logoHeight: NSLayoutConstraint!
    @IBOutlet private weak var titleHeight: NSLayoutConstraint!
    
    private var profileView = ProfileView()
    private var loadingIndicator: NVActivityIndicatorView?
    
    private let worker = PolicyWorker()
    private let workerClaims = ClaimsWorker()
    private let workerStatuses = AllStatusesWorker()
    private let loginWorker = LoginWorker()
    
    private let screenSize: CGRect = UIScreen.main.bounds
    
    private var selectedOption = 0
    private var imagePicker: ImagePicker!
    private var imageProfile: UIImage?
    private var imagePetCoreData: Data?
    private var allPets = [PetCoversList]()
    private var allClaimsForPet = [ClaimModel]()
    private var allClaimsForFiltering = [ClaimModel]()
    private var selectedPet: PetCoversList?
    
    private var filteredClaimsForPet = [ClaimModel]()
    
    private var localPets = [Pet]()
    
    private var claimStatusesAPI = [String]()
    
    private var isEnd = false
    private var notReload = false
    
    private var claimStatus: String?
    private var startDate: String?
    private var endDate: String?
    
    private var selectedStartDate:Date?
    private var selectedEndDate:Date?
    
    private var filterView = FilterView()
    private var isFiltered = false
    private var isNewPhotoTaken = false

    private var countPages = 0
    private var currentPage = 0
    
    // number of items to be fetched each time (i.e., database LIMIT)
    private let itemsPerBatch = 10

    // a flag for when all database items have already been loaded
    private var reachedEndOfItems = false
    
    private var indexForReload = 0
    
    var reachability: Reachability!
    
    private var username: String?
    private var password: String?
    private let defaults = UserDefaults.standard
    
    private var allPetsCoreDate = [Pet]()
    
    private let keychain = KeychainSwift()
    
    @IBOutlet private weak var collectionViewConstraint: NSLayoutConstraint!
    
    var registrationObserver: AnyObject?
    
    let arrayCannineBasicWelnessPlan = [Constants.cannineBasicWelnessPlan.first,
                                        Constants.cannineBasicWelnessPlan.second,
                                        Constants.cannineBasicWelnessPlan.third,
                                        Constants.cannineBasicWelnessPlan.fourth,
                                        Constants.cannineBasicWelnessPlan.fifth,
                                        Constants.cannineBasicWelnessPlan.sixth,
                                        Constants.cannineBasicWelnessPlan.seventh,
                                        Constants.cannineBasicWelnessPlan.eight,
                                        Constants.cannineBasicWelnessPlan.ninth,
                                        Constants.cannineBasicWelnessPlan.tenth,
                                        Constants.cannineBasicWelnessPlan.eleventh,
                                        Constants.cannineBasicWelnessPlan.twelfth,Constants.cannineBasicWelnessPlan.thirtheenth]

    let arrayCannineBasicWelnessPlanPrices = [Constants.cannineBasicWelnessPlan.firstValue,
                                              Constants.cannineBasicWelnessPlan.secondValue,
                                              Constants.cannineBasicWelnessPlan.thirdValue,
                                              Constants.cannineBasicWelnessPlan.fourthValue,
                                              Constants.cannineBasicWelnessPlan.fifthValue,
                                              Constants.cannineBasicWelnessPlan.sixthValue,
                                              Constants.cannineBasicWelnessPlan.seventhValue,
                                              Constants.cannineBasicWelnessPlan.eightValue,
                                              Constants.cannineBasicWelnessPlan.ninthValue,
                                              Constants.cannineBasicWelnessPlan.tenthValue,
                                              Constants.cannineBasicWelnessPlan.eleventhValue,
                                              Constants.cannineBasicWelnessPlan.twelfthValue,
                                              Constants.cannineBasicWelnessPlan.thirtheenthValue]
    
    let arrayCannineBestWelnessPlan = [Constants.cannineBestWelnessPlan.first,
                                       Constants.cannineBestWelnessPlan.second,
                                       Constants.cannineBestWelnessPlan.third,
                                       Constants.cannineBestWelnessPlan.fourth,
                                       Constants.cannineBestWelnessPlan.fifth,
                                       Constants.cannineBestWelnessPlan.sixth,
                                       Constants.cannineBestWelnessPlan.seventh,
                                       Constants.cannineBestWelnessPlan.eight,
                                       Constants.cannineBestWelnessPlan.ninth,
                                       Constants.cannineBestWelnessPlan.tenth,
                                       Constants.cannineBestWelnessPlan.eleventh,
                                       Constants.cannineBestWelnessPlan.twelfth,
                                       Constants.cannineBestWelnessPlan.thirtheenth,
                                       Constants.cannineBestWelnessPlan.fourtheenth,
                                       Constants.cannineBestWelnessPlan.fiftheenth,
                                       Constants.cannineBestWelnessPlan.sixteenth]

    let arrayCannineBestWelnessPlanPrices = [Constants.cannineBestWelnessPlan.firstValue,
                                             Constants.cannineBestWelnessPlan.secondValue,
                                             Constants.cannineBestWelnessPlan.thirdValue,
                                             Constants.cannineBestWelnessPlan.fourthValue,
                                             Constants.cannineBestWelnessPlan.fifthValue,
                                             Constants.cannineBestWelnessPlan.sixthValue,
                                             Constants.cannineBestWelnessPlan.seventhValue,
                                             Constants.cannineBestWelnessPlan.eightValue,
                                             Constants.cannineBestWelnessPlan.ninthValue,
                                             Constants.cannineBestWelnessPlan.tenthValue,
                                             Constants.cannineBestWelnessPlan.eleventhValue,
                                             Constants.cannineBestWelnessPlan.twelfthValue,
                                             Constants.cannineBestWelnessPlan.thirtheenthValue,
                                             Constants.cannineBestWelnessPlan.fourtheenthValue,
                                             Constants.cannineBestWelnessPlan.fiftheenthValue,
                                             Constants.cannineBestWelnessPlan.sixteenthValue]
    
    let arrayFelineBasicWelnessPlan = [Constants.felineBasicWelnessPlan.first,
                                       Constants.felineBasicWelnessPlan.second,
                                       Constants.felineBasicWelnessPlan.third,
                                       Constants.felineBasicWelnessPlan.fourth,
                                       Constants.felineBasicWelnessPlan.fifth,
                                       Constants.felineBasicWelnessPlan.sixth,
                                       Constants.felineBasicWelnessPlan.seventh,
                                       Constants.felineBasicWelnessPlan.eight,
                                       Constants.felineBasicWelnessPlan.ninth,
                                       Constants.felineBasicWelnessPlan.tenth,
                                       Constants.felineBasicWelnessPlan.eleventh]
    
    let arrayFelineBasicWelnessPlanPrices = [Constants.felineBasicWelnessPlan.firstValue,
                                      Constants.felineBasicWelnessPlan.secondValue,
                                      Constants.felineBasicWelnessPlan.thirdValue,
                                      Constants.felineBasicWelnessPlan.fourthValue,
                                      Constants.felineBasicWelnessPlan.fifthValue,
                                      Constants.felineBasicWelnessPlan.sixthValue,
                                      Constants.felineBasicWelnessPlan.seventhValue,
                                      Constants.felineBasicWelnessPlan.eightValue,
                                      Constants.felineBasicWelnessPlan.ninthValue,
                                      Constants.felineBasicWelnessPlan.tenthValue,
                                      Constants.felineBasicWelnessPlan.eleventhValue]
    
    let arrayFelineBestWelnessPlan = [Constants.felineBestWelnessPlan.first,
                                       Constants.felineBestWelnessPlan.second,
                                       Constants.felineBestWelnessPlan.third,
                                       Constants.felineBestWelnessPlan.fourth,
                                       Constants.felineBestWelnessPlan.fifth,
                                       Constants.felineBestWelnessPlan.sixth,
                                       Constants.felineBestWelnessPlan.seventh,
                                       Constants.felineBestWelnessPlan.eight,
                                       Constants.felineBestWelnessPlan.ninth,
                                       Constants.felineBestWelnessPlan.tenth,
                                       Constants.felineBestWelnessPlan.eleventh,
                                       Constants.felineBestWelnessPlan.twelfth,
                                       Constants.felineBestWelnessPlan.thirtheenth,
                                       Constants.felineBestWelnessPlan.fourtheenth,
                                       Constants.felineBestWelnessPlan.fiftheenth]

    let arrayFelineBestWelnessPlanPrices = [Constants.felineBestWelnessPlan.firstValue,
                                             Constants.felineBestWelnessPlan.secondValue,
                                             Constants.felineBestWelnessPlan.thirdValue,
                                             Constants.felineBestWelnessPlan.fourthValue,
                                             Constants.felineBestWelnessPlan.fifthValue,
                                             Constants.felineBestWelnessPlan.sixthValue,
                                             Constants.felineBestWelnessPlan.seventhValue,
                                             Constants.felineBestWelnessPlan.eightValue,
                                             Constants.felineBestWelnessPlan.ninthValue,
                                             Constants.felineBestWelnessPlan.tenthValue,
                                             Constants.felineBestWelnessPlan.eleventhValue,
                                             Constants.felineBestWelnessPlan.twelfthValue,
                                             Constants.felineBestWelnessPlan.thirtheenthValue,
                                             Constants.felineBestWelnessPlan.fourtheenthValue,
                                             Constants.felineBestWelnessPlan.fiftheenthValue]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: Constants.CLAIMS_UPDATE_NOTIFICAITON), object: nil)
        
        self.setViews()
        self.configCollectionViewProperties()
        self.configTableViewProperties()
        
        allPets = []
        localPets = []
        createHiddenLogin()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Firebase Analytics
        Analytics.logEvent(AnalyticsEventScreenView,
                           parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.home])
        
        //UserDefaults.standard.set(nil, forKey: "isAppLogout")
        reachability = try! Reachability()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChangedHomeScreen(_:)), name: Notification.Name.reachabilityChangedHome, object: reachability)

        do {
            try reachability?.startNotifier()
        } catch {
            print("This is not working.")
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: "", message: Constants.errorMessages.noInternetError, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: Constants.errorMessages.ok, style: .default, handler: { action in
                    print("Tap action")
                    let loginViewController = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: false)
                    if let navController = self.tabBarController?.viewControllers?[0] as? UINavigationController {
                        navController.pushViewController(loginViewController, animated: true)
                    }
                }))

                self.present(alertController, animated: true, completion: nil)

            }
            return
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        AppUtility.lockOrientation(.portrait)
        
        DispatchQueue.main.async {
            if self.isNewPhotoTaken == false {
                self.profileView.isHidden = true
                self.tabBarController?.tabBar.isHidden = false
                self.navigationController?.navigationBar.isHidden = true
            }
        }
        
        allPetsCoreDate = (petCoreDataService?.getAppPets()) ?? [Pet]()
        self.navigationController?.navigationBar.isHidden = true
        
        if allPetsCoreDate.count >= 0{
            self.collectionViewPets.reloadData()
        }
        
        let isOpenedFromMore = keychain.get("isOpenedFromMore")
        if isOpenedFromMore != nil {
            if isOpenedFromMore == "true"{
                self.profileView.isHidden = false
                keychain.delete("isOpenedFromMore")
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        //NotificationCenter.default.removeObserver(self)
    }

    @objc func reachabilityChangedHomeScreen(_ note: NSNotification) {

        let reachability = note.object as! Reachability

        if reachability.connection != .unavailable {
            if reachability.connection == .wifi {

            } else {

            }
        } else {
            DispatchQueue.main.async {
                self.stopLoadingWholeScreen()
            }
            print("Not reachable")
        }
    }
    
    @objc func loadList(){
        //load data here
        print("Reloading...")
        networkingClaims()
    }
    
    // MARK: - Set UI
    private func setViews() {
        
        navigationController?.navigationBar.isHidden = true
        profileView = (Bundle.main.loadNibNamed("ProfileView", owner: self, options: nil)?.first as? ProfileView)!
        profileView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        self.view.addSubview(profileView)
        
        DispatchQueue.main.async {
            self.startLoadingWholeScreen()
            self.addFilterView()
            self.addNavBarImage()
            self.setBackgroundImage("backgroundImage", contentMode: UIView.ContentMode.scaleAspectFill)
            self.profileView.isHidden = true
        }
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.profileView.addGestureRecognizer(swipeLeft)

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.profileView.addGestureRecognizer(swipeRight)

        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        profileView.buttonFilter.setTitle(Constants.titlesLabels.addFilter, for: .normal)
        profileView.buttonFilter.layer.cornerRadius = 15
        let tapFilter = UITapGestureRecognizer(target: self, action:  #selector(filterClaimsHideUnhide))
        profileView.buttonFilter.addGestureRecognizer(tapFilter)
        
        DispatchQueue.main.async {
            if UIDevice().userInterfaceIdiom == .pad {
                self.profileView.imageFilterHeight.constant = 70
                self.profileView.buttonFilter.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 35)
                self.profileView.buttonFilter.layer.cornerRadius = 27
                self.profileView.tableView.contentInset = UIEdgeInsets(top: -30, left: 0, bottom: 0, right: 0)
                self.profileView.labelNoResultsConstant.constant = 250
                self.profileView.labelNoResults.font = UIFont(name: "Proxima Nova Bold", size: 30)
            }
        }
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2532:
                print("iPhone 12, iPhone 12 Pro, iPhone 13, iPhone 13 Pro")
                profileView.topTableViewConstraint.constant = -48

            case 2778:
                print("iPhone 13 Pro Max")
                profileView.topTableViewConstraint.constant = -48

            default:
                print("Unknown")
            }
        }
        
        self.tabBarController?.delegate = self

        self.currentPage = 0
    }

    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == .right {
            UserDefaults.standard.set(nil, forKey: "chosenOption")
            if selectedOption > 0 {
                if selectedOption == 3{
                    selectedOption = 1
                } else {
                    selectedOption -= 1
                }
            }

            if selectedOption == 2 {
                profileView.imageFilter.isHidden = false
                DispatchQueue.main.async {
                    self.profileView.tableView.reloadData()
                }
            }
            if selectedOption == 1 || selectedOption == 0 {
                profileView.imageFilter.isHidden = true
                self.profileView.labelNoResults.isHidden = true
                DispatchQueue.main.async {
                    self.profileView.tableView.reloadData()
                }
            }
        }
        else if gesture.direction == .left {
            UserDefaults.standard.set(nil, forKey: "chosenOption")
            if selectedOption <= 2 {
                selectedOption += 1
            }
            if selectedOption == 2 {
                profileView.imageFilter.isHidden = false
                DispatchQueue.main.async {
                    self.profileView.tableView.reloadData()
                }
            }
            if selectedOption == 1 {
                profileView.imageFilter.isHidden = true
                self.profileView.labelNoResults.isHidden = true
                DispatchQueue.main.async {
                    self.profileView.tableView.reloadData()
                }
            }
        }
    }

    private func addNavBarImage() {
        let image = UIImage(named: "petsBestLogo") //Your logo url here
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
    }
    
    func addFilterView() {
        filterView = (Bundle.main.loadNibNamed("FilterView", owner: self, options: nil)?.first as? FilterView)!
        filterView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        filterView.viewFilter.clipsToBounds = true
        filterView.viewFilter.layer.cornerRadius = 15
        filterView.viewFilter.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        filterView.viewFilter.layer.borderWidth = 1
        filterView.viewFilter.layer.borderColor = UIColor.clear.cgColor
        filterView.viewFilter.layer.masksToBounds = true
        filterView.viewFilter.layer.shadowOpacity = 0.8
        filterView.viewFilter.layer.shadowOffset = CGSize(width: 50, height: 2)
        filterView.viewFilter.layer.shadowRadius = 50
        filterView.viewFilter.layer.shadowColor = UIColor.black.cgColor
        filterView.viewFilter.layer.masksToBounds = false
        filterView.textFieldStatus.placeholder = Constants.textFieldsPlaceholder.status
        filterView.textFieldStartDate.placeholder = Constants.textFieldsPlaceholder.startDate
        filterView.textFieldEndDate.placeholder = Constants.textFieldsPlaceholder.endDate
        
        filterView.buttonFilter.setTitle(Constants.titlesButtons.filter, for: .normal)
        filterView.buttonFilter.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        filterView.buttonFilter.setTitleColor(UIColor.white, for: .normal)
        filterView.buttonFilter.layer.cornerRadius = 25
        filterView.labelFilterClaims.text = Constants.titlesLabels.filterClaims
        
        let tapFilterResult = UITapGestureRecognizer(target: self, action:  #selector(actionFilterClaims))
        filterView.buttonFilter.isUserInteractionEnabled = true
        filterView.buttonFilter.addGestureRecognizer(tapFilterResult)
        
        filterView.buttonClearResults.setTitle(Constants.titlesButtons.clear, for: .normal)
        filterView.buttonClearResults.backgroundColor = UIColor.init(hex: Constants.Colors.gray)
        filterView.buttonClearResults.setTitleColor(UIColor.darkGray, for: .normal)
        filterView.buttonClearResults.layer.cornerRadius = 25
        
        let tapFilterClearResult = UITapGestureRecognizer(target: self, action:  #selector(actionFilterClearClaims))
        filterView.buttonClearResults.isUserInteractionEnabled = true
        filterView.buttonClearResults.addGestureRecognizer(tapFilterClearResult)
        
        filterView.labelFilterClaims.clipsToBounds = true
        filterView.labelFilterClaims.layer.cornerRadius = 10
        filterView.labelFilterClaims.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        filterView.labelFilterClaims.backgroundColor = UIColor.systemGray6
        
        let tapClose = UITapGestureRecognizer(target: self, action:  #selector(closeFilteringView))
        filterView.imageClose.isUserInteractionEnabled = true
        filterView.imageClose.addGestureRecognizer(tapClose)
        
        let tapOpenStatusesTextField = UITapGestureRecognizer(target: self, action:  #selector(openStatuses))
        filterView.textFieldStatus.isUserInteractionEnabled = true
        filterView.textFieldStatus.addGestureRecognizer(tapOpenStatusesTextField)
        
        filterView.textFieldEndDate.isUserInteractionEnabled = true
        let tapOpenCalendarEndTextField = UITapGestureRecognizer(target: self, action:  #selector(openCalendarEnd))
        filterView.textFieldEndDate.addGestureRecognizer(tapOpenCalendarEndTextField)
        
        filterView.textFieldEndDate.delegate = self
        filterView.textFieldStartDate.delegate = self
        filterView.textFieldStatus.delegate = self
        
        self.view.addSubview(filterView)
        filterView.isHidden = true
        
        self.labelsAndViewsIPadIphone()
    }
    
    private func labelsAndViewsIPadIphone() {
        if UIDevice().userInterfaceIdiom == .pad {
            filterView.labelFilterClaims.font = UIFont(name: "Proxima Nova Bold", size: 35)
            filterView.textFieldEndDate.font = UIFont(name: "Proxima Nova", size: 25)
            filterView.textFieldStartDate.font = UIFont(name: "Proxima Nova", size: 25)
            filterView.textFieldStatus.font = UIFont(name: "Proxima Nova", size: 25)
            filterView.buttonFilter.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 25)
            filterView.buttonFilter.layer.cornerRadius = 35
            filterView.buttonClearResults.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 25)
            filterView.buttonClearResults.layer.cornerRadius = 35
            filterView.buttonFilterHeight.constant = 70
            filterView.buttonClearResultsHeight.constant = 70
            filterView.labelFilterHeight.constant = 80
            filterView.imageCloseWidth.constant = 60
            filterView.imageCloseHeight.constant = 60
            filterView.textFieldStatusHeight.constant = 60
            filterView.textFieldStartDateHeight.constant = 60
            filterView.textFieldEndDateHeight.constant = 60
            
            let button = UIButton(type: .custom)
            button.setImage(UIImage(named: "downArrow"), for: .normal)
            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
            button.frame = CGRect(x: CGFloat(filterView.textFieldStatus.frame.size.width - 30), y: CGFloat(5), width: CGFloat(75), height: CGFloat(35))
            filterView.textFieldStatus.rightView = button
            filterView.textFieldStatus.rightViewMode = .always
            
            let tapOpenStatuses = UITapGestureRecognizer(target: self, action:  #selector(openStatuses))
            button.isUserInteractionEnabled = true
            button.addGestureRecognizer(tapOpenStatuses)
            
            let button1 = UIButton(type: .custom)
            button1.setImage(UIImage(named: "downArrow"), for: .normal)
            button1.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
            button1.frame = CGRect(x: CGFloat(filterView.textFieldStartDate.frame.size.width - 30), y: CGFloat(5), width: CGFloat(75), height: CGFloat(35))
            filterView.textFieldStartDate.rightView = button1
            filterView.textFieldStartDate.rightViewMode = .always
            
            let tapOpenCalendarStart = UITapGestureRecognizer(target: self, action:  #selector(openCalendarStart))
            button1.isUserInteractionEnabled = true
            button1.addGestureRecognizer(tapOpenCalendarStart)
            
            let tapOpenCalendarStartTextField = UITapGestureRecognizer(target: self, action:  #selector(openCalendarStart))
            filterView.textFieldStartDate.isUserInteractionEnabled = true
            filterView.textFieldStartDate.addGestureRecognizer(tapOpenCalendarStartTextField)
            
            let button2 = UIButton(type: .custom)
            button2.setImage(UIImage(named: "downArrow"), for: .normal)
            button2.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
            button2.frame = CGRect(x: CGFloat(filterView.textFieldStartDate.frame.size.width - 30), y: CGFloat(5), width: CGFloat(75), height: CGFloat(35))
            
            filterView.textFieldEndDate.rightView = button2
            filterView.textFieldEndDate.rightViewMode = .always
            
            let tapOpenCalendarEnd = UITapGestureRecognizer(target: self, action:  #selector(openCalendarEnd))
            button2.isUserInteractionEnabled = true
            button2.addGestureRecognizer(tapOpenCalendarEnd)
            
        } else {
            let button = UIButton(type: .custom)
            button.setImage(UIImage(named: "downArrow"), for: .normal)
            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
            button.frame = CGRect(x: CGFloat(filterView.textFieldStatus.frame.size.width - 30), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
            filterView.textFieldStatus.rightView = button
            filterView.textFieldStatus.rightViewMode = .always
            
            let tapOpenStatuses = UITapGestureRecognizer(target: self, action:  #selector(openStatuses))
            button.isUserInteractionEnabled = true
            button.addGestureRecognizer(tapOpenStatuses)
            
            let button1 = UIButton(type: .custom)
            button1.setImage(UIImage(named: "downArrow"), for: .normal)
            button1.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
            button1.frame = CGRect(x: CGFloat(filterView.textFieldStartDate.frame.size.width - 30), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
            filterView.textFieldStartDate.rightView = button1
            filterView.textFieldStartDate.rightViewMode = .always
            
            let tapOpenCalendarStart = UITapGestureRecognizer(target: self, action:  #selector(openCalendarStart))
            button1.isUserInteractionEnabled = true
            button1.addGestureRecognizer(tapOpenCalendarStart)
            
            let tapOpenCalendarStartTextField = UITapGestureRecognizer(target: self, action:  #selector(openCalendarStart))
            filterView.textFieldStartDate.isUserInteractionEnabled = true
            filterView.textFieldStartDate.addGestureRecognizer(tapOpenCalendarStartTextField)
            
            let button2 = UIButton(type: .custom)
            button2.setImage(UIImage(named: "downArrow"), for: .normal)
            button2.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
            button2.frame = CGRect(x: CGFloat(filterView.textFieldStartDate.frame.size.width - 30), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
            
            filterView.textFieldEndDate.rightView = button2
            filterView.textFieldEndDate.rightViewMode = .always
            
            let tapOpenCalendarEnd = UITapGestureRecognizer(target: self, action:  #selector(openCalendarEnd))
            button2.isUserInteractionEnabled = true
            button2.addGestureRecognizer(tapOpenCalendarEnd)
        }
    }
    
    // MARK: - Activity Indicators
    private func startLoadingWholeScreen() {
        DispatchQueue.main.async {
            self.activityIndicatorScreen?.startAnimating()
        }
    }
    
    private func stopLoadingWholeScreen() {
        DispatchQueue.main.async {
            self.activityIndicatorScreen?.stopAnimating()
        }
    }
    
    private func startLoadingProfile() {
        DispatchQueue.main.async {
            self.profileView.activityIndicator.startAnimating()
        }
    }
    
    private func stopLoadingProfile() {
        DispatchQueue.main.async {
            self.profileView.activityIndicator.stopAnimating()
        }
    }
    
    
    private func startLoading() {
        DispatchQueue.main.async {
            if self.loadingIndicator == nil {
                let frame = CGRect(x: 0, y: 0, width: 60, height: 60)
                let backgroundColor = UIColor(white: 0.0, alpha: 0.5)
                let foregroundColor = UIColor.init(hex: Constants.Colors.main)
                self.loadingIndicator = NVActivityIndicatorView(frame: frame, type: .ballSpinFadeLoader, color: foregroundColor, padding: 10)
                self.loadingIndicator?.layer.cornerRadius = 5
                self.loadingIndicator?.backgroundColor = backgroundColor
                self.loadingIndicator?.center = CGPoint(x: self.profileView.tableView.bounds.width / 2, y: self.profileView.tableView.bounds.height / 2)
                self.profileView.addSubview(self.loadingIndicator!)
            }
            self.profileView.tableView.isUserInteractionEnabled = false
            self.loadingIndicator?.startAnimating()
        }
    }
    
    private func stopLoading() {
        DispatchQueue.main.async {
            self.loadingIndicator?.stopAnimating()
        }
        profileView.tableView.isUserInteractionEnabled = true
    }
    
    // MARK: - Set collectionView
    
    private func configCollectionViewProperties() {
        collectionViewPets.collectionViewLayout = layout
        if UIDevice().userInterfaceIdiom == .pad {
            layout.itemSize = CGSize(width: 600, height: 700)
            layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 100)
        }
        if UIDevice().userInterfaceIdiom == .phone {
            if UIScreen.main.nativeBounds.height == 1334 {
                layout.itemSize = CGSize(width: 220, height: 400)
                layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 40)
            } else {
                layout.itemSize = CGSize(width: 280, height: 400)
                layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 30)
            }
        }
        layout.scrollDirection = .horizontal
        collectionViewPets.registerCellNib(PetsCollectionViewCell.self)
    }
    
    // MARK: - Set tableView
    
    private func configTableViewProperties() {
        profileView.tableView.registerCell(ProgressBarTableViewCell.self)
        profileView.tableView.registerCell(ProfileDetailsTableViewCell.self)
        profileView.tableView.registerCell(ProfileTableViewCell.self)
        profileView.tableView.registerCell(ClaimHistoryTableViewCell.self)
        profileView.tableView.registerCell(ProfileImageTableViewCell.self)
        profileView.tableView.registerCell(ProcessedTableViewCell.self)
        profileView.tableView.registerCell(NotFinishedClaimTableViewCell.self)
        profileView.tableView.registerCell(WaitingOnCustomerTableViewCell.self)
        profileView.tableView.registerCell(ClosedWaitingOnCustomerTableViewCell.self)
        profileView.tableView.registerCell(ReceivedClosedTableViewCell.self)
        profileView.tableView.registerCell(LastProcessedTableViewCell.self)
        profileView.tableView.registerCell(LastNotFinishedTableViewCell.self)
        profileView.tableView.registerCell(LastReceivedTableViewCell.self)
        profileView.tableView.registerCell(LastClaimHistoryTableViewCell.self)
        profileView.tableView.registerCell(LastWaitingOnCustomerTableViewCell.self)
        profileView.tableView.registerCell(LastClosedWaitingOnCustomerTableViewCell.self)
        profileView.tableView.registerCell(LastReceivedClosedTableViewCell.self)
        profileView.tableView.registerCell(ReceivedUpdatableTableViewCell.self)
        profileView.tableView.registerCell(WellnessCoverageTableViewCell.self)
        
        profileView.tableView.registerCell(FirstClaimHistoryTableViewCell.self)
        profileView.tableView.registerCell(FirstProcessedTableViewCell.self)
        profileView.tableView.registerCell(FirstNotFinishedClaimTableViewCell.self)
        profileView.tableView.registerCell(FirstReceivedTableViewCell.self)
        profileView.tableView.registerCell(FirstReceivedUpdatableTableViewCell.self)
        profileView.tableView.registerCell(FirstWaitingOnCustomerTableViewCell.self)
        profileView.tableView.registerCell(FirstClosedWaitingOnCustomerTableViewCell.self)
        profileView.tableView.registerCell(FirstReceivedClosedTableViewCell.self)
        
        profileView.tableView.separatorStyle = .none
        
        if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
            case 1136:
                //print("iPhone 5 or 5S or 5C")
                profileView.tableView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
            case 1334:
                //print("iPhone 6/6S/7/8")
                profileView.tableView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
            case 1920, 2208:
                //print("iPhone 6+/6S+/7+/8+")
                profileView.tableView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
            case 2436:
                //print("iPhone X/XS/11 Pro")
                profileView.tableView.contentInset = UIEdgeInsets(top: -50, left: 0, bottom: 0, right: 0)
            case 2688:
                //print("iPhone XS Max/11 Pro Max")
                profileView.tableView.contentInset = UIEdgeInsets(top: -50, left: 0, bottom: 0, right: 0)
            case 1792:
                //print("iPhone XR/11")
                profileView.tableView.contentInset = UIEdgeInsets(top: -50, left: 0, bottom: 0, right: 0)
            default:
                print("Unknown")
            }
        } else {
            logoHeight.constant = 100
            titleHeight.constant = 200
        }
        
    }
    
    // MARK: - Local storage via CoreData
    private func updatePet(pet: PetCoversList) {
        isNewPhotoTaken = true
        guard let imageData = imagePetCoreData else { return }
        self.petCoreDataService?.addImageToPet(policyNumber: "\(pet.PolicyLineNum)",image: imageData, completion: { (success, pets)  in
            DispatchQueue.main.async {
                self.localPets = (self.petCoreDataService?.getAppPets()) ?? [Pet]()
                self.collectionViewPets.reloadData()
            }
        })
    }
    
    // MARK: - Networking
//    private func networkingGetAllStatuses() {
//        if ReachabilityClass.isConnectedToNetwork(){
//            workerStatuses.getAllStatuses(completionHandler: { [weak self] resultUser in
//                switch resultUser {
//                case .success(let resultUser):
//                    self?.claimStatusesAPI = resultUser
//                case .failure( _):
//                    print("Error")
//                }
//            }) { [weak self] error  in
//                switch error {
//                case .success( _):
//                    print("Error")
//                case .failure( _):
//                    print("Error")
//                }
//
//            }
//        } else {
//            DispatchQueue.main.async {
//                let alertController = UIAlertController(title: "", message: Constants.errorMessages.noInternetError, preferredStyle: .alert)
//                alertController.addAction(UIAlertAction(title: Constants.errorMessages.ok, style: .default, handler: { action in
//                    let loginViewController = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: false)
//                    loginViewController.modalPresentationStyle = .overFullScreen
//                    self.present(loginViewController, animated: true, completion: nil)
//                }))
//
//                self.present(alertController, animated: true, completion: nil)
//                UserDefaults.standard.set("true", forKey: "isAppLogout")
//                Spinner.stop()
//
//            }
//        }
//    }
    
    private func createHiddenLogin() {
        guard let username = self.keychain.get("username"),
                let password = self.keychain.get("password")
        else { return }

        self.allPets = []
        self.localPets = []
        self.loginWorker.loginUser(withUsername: username, withPassword: password, mobileUserId: self.keychain.get("mobileUserId") ?? "", channelId: self.keychain.get("channelId") ?? "", appKey: Constants.productionAcousticSDKKey, completionHandler: { [weak self] resultUser in
            switch resultUser {
            case .success(_):
                self?.networkingPolicyDetails()
                DispatchQueue.main.async {
                    self?.stopLoadingWholeScreen()
                    self?.profileView.tableView.reloadData()
                    self?.collectionViewPets.reloadData()
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.stopLoadingWholeScreen()
                    self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    
                }
            }
        }) { [weak self] error in
            switch error {
            case .success(let value):
                DispatchQueue.main.async {
                    self?.stopLoadingWholeScreen()
                    let alertController = UIAlertController(title: "", message: Constants.errorMessages.generalHomeError, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: Constants.errorMessages.ok, style: .default, handler: { action in
                        let loginViewController = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: false)
                        if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                            navController.pushViewController(loginViewController, animated: true)
                        }
                    }))

                    self?.present(alertController, animated: true, completion: nil)
                    UserDefaults.standard.set("true", forKey: "isAppLogout")
                }
            case .failure(_):
                DispatchQueue.main.async {
                    self?.stopLoadingWholeScreen()
                    self?.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                }
            }
        }
    }
    
    @objc private func openStatuses() {
        if ReachabilityClass.isConnectedToNetwork(){
            workerStatuses.getAllStatuses(completionHandler: { [weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    self?.showActionSheetWithCancel(title: resultUser) { [weak self] (value) in
                        self?.filterView.textFieldStatus.text = value
                        self?.profileView.buttonFilter.setTitle(Constants.titlesLabels.removeFilter, for: .normal)
                        if value == "Cancel" {
                            self?.claimStatus = ""
                            self?.filterView.textFieldStatus.text = ""
                            self?.filteredClaimsForPet = []
                            self?.profileView.labelNoResults.isHidden = true
                            self?.isFiltered = false
                            DispatchQueue.main.async {
                                self?.profileView.tableView.reloadSections(IndexSet(integersIn: 1...1), with: .none)
                                self?.profileView.buttonFilter.setTitle(Constants.titlesLabels.addFilter, for: .normal)
                            }
                            self?.filterView.isHidden = true
                        } else {
                            self?.isFiltered = true
                            self?.claimStatus = value
                        }
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: {
                            let loginViewController = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: false)
                            if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                                navController.pushViewController(loginViewController, animated: true)
                            }
                        })
                        Spinner.stop()
                    }
                }
            }) { [weak self] error  in
                switch error {
                case .success(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: {
                            let loginViewController = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: false)
                            if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                                navController.pushViewController(loginViewController, animated: true)
                            }
                        })
                        self?.stopLoading()
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: {
                            let loginViewController = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: false)
                            if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                                navController.pushViewController(loginViewController, animated: true)
                            }
                        })
                        self?.stopLoading()
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.noInternetError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
               Spinner.stop()
            }
        }
    }
    
    private func networkingPolicyDetails() {

        if ReachabilityClass.isConnectedToNetwork(){
            //Testing with login every time
            allPets = []
            localPets = []
            
            if allPetsCoreDate.count != 0 {
                localPets = allPetsCoreDate ?? [Pet]()
                
                worker.getPolicyDetails(completionHandler: { [
                    weak self] resultUser in
                    switch resultUser {
                    case .success(let resultUser):
                        
                        DispatchQueue.main.async {
                            self?.stopLoadingWholeScreen()
                        }
                        for pet in resultUser {
                            if pet.IsCanceled == false {
                                self?.allPets.append(pet)

                                DispatchQueue.main.async {
                                    self?.collectionViewPets.reloadData()
                                }
                            }
                        }
                        
                    case .failure(_):
                        DispatchQueue.main.async {
                            self?.collectionViewPets.reloadData()
                        }
                    }
                }) { [weak self] error  in
                    switch error {
                    case .success(_):
                        DispatchQueue.main.async {
                            self?.collectionViewPets.reloadData()
                        }
                    case .failure(_):
                        DispatchQueue.main.async {
                            self?.collectionViewPets.reloadData()
                        }
                    }
                }
                
            } else {
                worker.getPolicyDetails(completionHandler: { [weak self] resultUser in
                    switch resultUser {
                    case .success(let resultUser):
                        DispatchQueue.main.async {
                            self?.stopLoadingWholeScreen()
                        }
                        for pet in resultUser {
                            if pet.IsCanceled == false {
                                self?.allPets.append(pet)
                                if self?.allPetsCoreDate.count == 0 {
                                    //The CoreData saving is done on the background thread
                                    self?.petCoreDataService?.addPet(policyNumber: "\(pet.PolicyLineNum)", petName: pet.PetName, breedName: pet.BreedName, image: Data(), sex: pet.Sex, age: pet.Age, species: pet.Species, planDescription: pet.PetCovers[0].PlanDescription, planType: pet.PetCovers[0].PlanName, deductible: "\(pet.PetCovers[0].Deductible)", reimbursement: "\(pet.PetCovers[0].Reimbursement)", completion: { (success, pets) in
                                        if success {
                                            //print("Success")
                                            self?.localPets = pets
                                        }
                                    })
                                } else if (self?.allPetsCoreDate.count)! < (self?.allPets.count)! {
                                    //The CoreData saving is done on the background thread
                                    self?.petCoreDataService?.addPet(policyNumber: "\(pet.PolicyLineNum)", petName: pet.PetName, breedName: pet.BreedName, image: Data(), sex: pet.Sex, age: pet.Age, species: pet.Species, planDescription: pet.PetCovers[0].PlanDescription, planType: pet.PetCovers[0].PlanTypeDescription, deductible: "\(pet.PetCovers[0].Deductible)", reimbursement: "\(pet.PetCovers[0].Reimbursement)", completion: { (success, pets) in
                                        if success {
                                            //print("Success")
                                            self?.localPets = pets
                                        }
                                    })
                                } else {
                                    self?.localPets = self!.allPetsCoreDate
                                }
                                DispatchQueue.main.async {
                                    self?.collectionViewPets.reloadData()
                                }
                            }
                        }
                        
                    case .failure(_):
                        DispatchQueue.main.async {
                            self?.collectionViewPets.reloadData()
                        }
                    }
                }) { [weak self] error  in
                    switch error {
                    case .success(_):
                        DispatchQueue.main.async {
                            self?.collectionViewPets.reloadData()
                        }
                    case .failure(_):
                        DispatchQueue.main.async {
                            self?.collectionViewPets.reloadData()
                        }
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.noInternetError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                self.stopLoadingWholeScreen()
            }
        }
    }
    
    @objc private func networkingClaims() {
        guard let pet = self.selectedPet else { return }
        allClaimsForPet = []
        filteredClaimsForPet = []
        startLoadingProfile()
        if ReachabilityClass.isConnectedToNetwork(){
            workerClaims.getClaims(completionHandler: { [weak self] claimsResult in
                switch claimsResult {
                case .success(let claims):
                    let count = Double((claims.count))/Double(self!.itemsPerBatch)
                    self?.countPages = Int(count.rounded())
                    for (_,claim) in claims.enumerated() {
                        if claim.PetName == pet.PetName {
                            self?.allClaimsForFiltering.append(claim)
                            if (self?.allClaimsForPet.count)! <= self!.itemsPerBatch {
                                self?.allClaimsForPet.append(claim)
                            }
                        }
                    }
                    if self!.allClaimsForPet.isEmpty {
                        if self!.selectedOption == 2 {
                            self?.profileView.labelNoResults.isHidden = false
                        } else {
                            self?.profileView.labelNoResults.isHidden = true
                        }
                    } else {
                        self?.profileView.labelNoResults.isHidden = true
                    }
                    DispatchQueue.main.async {
                        self?.stopLoadingProfile()
                        self?.profileView.tableView.reloadSections(IndexSet(integersIn: 1...1), with: .none)
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: {
                            let loginViewController = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: false)
                            if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                                navController.pushViewController(loginViewController, animated: true)
                            }
                        })
                        self?.stopLoadingProfile()
                    }
                }
            })  { [weak self] error  in
                switch error {
                case .success(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: {
                            let loginViewController = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: false)
                            if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                                navController.pushViewController(loginViewController, animated: true)
                            }
                        })
                        self?.stopLoadingProfile()
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: {
                            let loginViewController = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: false)
                            if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                                navController.pushViewController(loginViewController, animated: true)
                            }
                        })
                        self?.stopLoadingProfile()
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.noInternetError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                self.stopLoadingProfile()
            }
        }
    }

    func loadMoreClaims() {
        DispatchQueue.global(qos: .background).async {
            guard let pet = self.selectedPet else { return }
            if ReachabilityClass.isConnectedToNetwork(){
                self.workerClaims.getClaims(completionHandler: { [weak self] claimsResult in
                    switch claimsResult {
                    case .success(let claims):
                        self?.indexForReload = self!.indexForReload + self!.itemsPerBatch
                        for (index,claim) in claims.enumerated() {
                            if claim.PetName == pet.PetName {
                                if self!.indexForReload == self!.itemsPerBatch {
                                    if index > self!.indexForReload && index < self!.indexForReload + self!.itemsPerBatch{
                                        self?.allClaimsForPet.append(claim)
                                    }
                                } else {
                                    if index > self!.indexForReload && index <= self!.indexForReload + self!.itemsPerBatch {
                                        self?.allClaimsForPet.append(claim)
                                    }
                                }
                            }
                        }
                        // check if this was the last of the data
                        if (self?.allClaimsForPet.count)! == claims.count {
                            self?.reachedEndOfItems = true
                        }
                    
                        DispatchQueue.main.async {
                            self?.stopLoading()
                            self?.profileView.tableView.reloadSections(IndexSet(integersIn: 1...1), with: .none)
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.stopLoading()
                        }
                    }
                })  { [weak self] error  in
                    switch error {
                    case .success(let error):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: error, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.stopLoading()
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.stopLoading()
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(withTitle: "", message: Constants.errorMessages.noInternetError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    self.stopLoading()
                }
            }
        }
    }
    
    // MARK: - Filtering claims
    @objc private func filterClaimsHideUnhide() {
        if self.profileView.buttonFilter.titleLabel?.text == Constants.titlesLabels.removeFilter {
            filterView.isHidden = false
        } else {
            filterView.isHidden = false
        }
    }
    
    @objc private func closeFilteringView() {
        filterView.isHidden = true
        profileView.labelNoResults.isHidden = true
        isFiltered = false
        DispatchQueue.main.async {
            self.profileView.tableView.reloadSections(IndexSet(integersIn: 0...1), with: .none)
        }
    }
    
    @objc private func openCalendarStart() {
        let vc = CalendarViewController()
        vc.delegate = self
        vc.isEnd = false
        let navigationController = NavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .overFullScreen
        present(navigationController, animated: true, completion: nil)
    }
    
    @objc private func openCalendarEnd() {
        let vc = CalendarViewController()
        vc.delegate = self
        vc.isEnd = true
        let navigationController = NavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .overFullScreen
        present(navigationController, animated: true, completion: nil)
    }
    
    @objc private func actionFilterClearClaims() {
        self.filterView.textFieldStatus.text = ""
        self.filterView.textFieldStartDate.text = ""
        self.filterView.textFieldEndDate.text = ""
        filterView.isHidden = true
        self.isFiltered = false
        profileView.labelNoResults.isHidden = true
        self.profileView.buttonFilter.setTitle(Constants.titlesLabels.addFilter, for: .normal)
        filteredClaimsForPet = []
        
        DispatchQueue.main.async {
            self.profileView.tableView.reloadSections(IndexSet(integersIn: 1...1), with: .none)
        }
    }
    
    @objc private func actionFilterClaims() {
        filteredClaimsForPet = []
        startDate = filterView.textFieldStartDate.text
        endDate = filterView.textFieldEndDate.text

        guard let start = startDate,
            let end = endDate
        else {
            return }

        for claim in allClaimsForFiltering {
            let dateOfIncident = claim.DateReceived.toDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/YYYY"
            let date = "\(formatter.string(from: dateOfIncident!))"
            
            if claimStatus != "" && claimStatus != nil {
                if claimStatus! == claim.StatusName {
                    if start.isEmpty && end.isEmpty {
                        filteredClaimsForPet.append(claim)
                    }
                    if start == date || end == date {
                        filteredClaimsForPet.append(claim)
                    }
                    if dateOfIncident! > selectedStartDate ?? Date()  && dateOfIncident! < selectedEndDate ?? Date() {
                        filteredClaimsForPet.append(claim)
                    }
                }
            } else {
                if start != ""{
                    if end != "" && start != ""{
                        if selectedStartDate! > selectedEndDate! {
                            DispatchQueue.main.async {
                                self.showAlert(withTitle: "", message: "The Start Date cannot be bigger than the End Date", buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            }
                        } else {
                            if dateOfIncident!.isBetween(selectedStartDate!,and: selectedEndDate!) == true {
                                filteredClaimsForPet.append(claim)
                            }
                            isFiltered = true
                        }
                    } else {
                        if selectedStartDate! <= dateOfIncident! {
                            filteredClaimsForPet.append(claim)
                        }
                        isFiltered = true
                    }
                }
                if end != "" {
                    if end != "" && start != ""{
                        if dateOfIncident!.isBetween(selectedStartDate!,and: selectedEndDate!) == true {
                            filteredClaimsForPet.append(claim)
                        }
                        filteredClaimsForPet = filteredClaimsForPet.unique{$0.ClaimId}
                        isFiltered = true
                    } else {
                        if selectedEndDate! <= dateOfIncident! {
                            filteredClaimsForPet.append(claim)
                        }
                        isFiltered = true
                    }
                }
            }
        }
        
        //When we clear the filtering or doesn't filter at all
        if filteredClaimsForPet.isEmpty && isFiltered == false {
            profileView.labelNoResults.isHidden = false
            DispatchQueue.main.async {
                self.profileView.tableView.reloadSections(IndexSet(integersIn: 1...1), with: .none)
            }
            self.profileView.buttonFilter.setTitle(Constants.titlesLabels.removeFilter, for: .normal)
         }
        //When we are filtering, but there are no results
        if filteredClaimsForPet.isEmpty && isFiltered == true {
            profileView.labelNoResults.isHidden = false
            DispatchQueue.main.async {
                self.profileView.tableView.reloadSections(IndexSet(integersIn: 1...1), with: .none)
            }
            self.profileView.buttonFilter.setTitle(Constants.titlesLabels.removeFilter, for: .normal)
         }
        //When we are filtering and there are results
        if !filteredClaimsForPet.isEmpty && isFiltered == true {
            profileView.labelNoResults.isHidden = true
            DispatchQueue.main.async {
                self.profileView.tableView.reloadSections(IndexSet(integersIn: 1...1), with: .none)
            }
            self.profileView.buttonFilter.setTitle(Constants.titlesLabels.removeFilter, for: .normal)
         }
        filterView.isHidden = true
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if allPets.count != 0 {
            return allPets.count + 1
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PetsCollectionViewCell = collectionView.dequeueCell(at: indexPath)
        cell.delegate = self

        if indexPath.row < allPets.count {
            if indexPath.row < localPets.count {
                if localPets[indexPath.row].image != nil {
                    let imagePet = UIImage(data:localPets[indexPath.row].image ?? Data())
                    cell.configureCell(withName: localPets[indexPath.row].petName ?? "", withImage: imagePet, withIndex: indexPath.row)
                    return cell
                } else {
                    cell.configureCell(withName: localPets[indexPath.row].petName ?? "", withImage: nil, withIndex: indexPath.row)
                    return cell
                }
            } else {
                cell.configureCell(withName: allPets[indexPath.row].PetName, withImage: nil, withIndex: indexPath.row)
                return cell
            }
        } else {
            cell.configureCell(withName: "", withImage: UIImage(named: "plus"), withIndex: indexPath.row)
            return cell
        }
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let layout = self.collectionViewPets.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
    }
}

extension HomeViewController: PetsCollectionViewCellDelegate{
    func openMyPetsScreen(index: Int) {
        allClaimsForFiltering = []
        
        if index <= allPets.count-1 {
            profileView.isHidden = false
            selectedPet = allPets[index]
            self.profileView.labelNoResults.isHidden = true
            networkingClaims()
            
            if selectedOption == 2 {
                profileView.imageFilter.isHidden = false
            } else {
                profileView.imageFilter.isHidden = true
            }
            

            if index <= localPets.count-1 {
                if localPets[index].image != nil {
                    DispatchQueue.main.async {
                        self.imageProfile = UIImage(data:self.localPets[index].image!)
                    }
                } else {
                    self.imageProfile = nil
                }
            }
            
            DispatchQueue.main.async {
                self.profileView.tableView.reloadData()
            }
            self.profileView.buttonFilter.setTitle(Constants.titlesLabels.addFilter, for: .normal)
        } else {
            let vc = AddPetViewController()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        if section == 1 {
            if selectedOption == 2 {
                if filteredClaimsForPet.isEmpty && isFiltered == true {
                    return 0
                }
                if filteredClaimsForPet.isEmpty && isFiltered == false {
                    return allClaimsForPet.count
                }
                else {
                    return filteredClaimsForPet.count
                }
            } else {
                if selectedOption == 1 {
                    if selectedPet != nil {
                        return (selectedPet?.PetCovers.count)!
                    } else {
                        return 1
                    }
                } else {
                    return 1
                }
            }
        } else {
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 && UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1334:
                    return 375.0
                    
                case 1792:
                    return 414.0
                    
                case 1920, 2208:
                    return 414.0
                    
                case 2436:
                    return 375.0
                    
                case 2532:
                    return 390.0
                    
                case 2688:
                    return 414.0
                    
                case 2778:
                    return 428.0
                default:
                    return 0.0
                }
            } else {
                if UIDevice().userInterfaceIdiom == .pad && indexPath.row == 0 {
                    return 580.0
                } else {
                    if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1334 {
                        return 40.0
                    } else {
                        return 50.0
                    }
                }
            }
        } else {
            if UIDevice().userInterfaceIdiom == .pad {
                if indexPath.section == 0 && indexPath.section == 1 {
                    return 50.0
                } else {
                    if selectedOption == 2 {
                        if indexPath.row == allClaimsForPet.count-1 {
                            return 590.0
                        } else {
                            if !filteredClaimsForPet.isEmpty {
                                if indexPath.row == filteredClaimsForPet.count-1 {
                                    return 590.0
                                }
                                if indexPath.row == 0 {
                                    if (filteredClaimsForPet[indexPath.row].StatusCode == "NO" && filteredClaimsForPet[indexPath.row].CanUpdate == true) || filteredClaimsForPet[indexPath.row].StatusCode == "IC" || filteredClaimsForPet[indexPath.row].StatusCode == "CL"{
                                        return 590.0
                                    }
                                    else {
                                        return 550.0
                                    }
                                }
                                if (filteredClaimsForPet[indexPath.row].StatusCode == "NO" && filteredClaimsForPet[indexPath.row].CanUpdate == true) || filteredClaimsForPet[indexPath.row].StatusCode == "IC" || filteredClaimsForPet[indexPath.row].StatusCode == "CL"{
                                    return 570.0
                                }
                                else {
                                    return 550.0
                                }
                            } else {
                                if indexPath.row == allClaimsForPet.count-1 {
                                    return 590.0
                                }
                                if indexPath.row == 0 {
                                    if (allClaimsForPet[indexPath.row].StatusCode == "NO" && allClaimsForPet[indexPath.row].CanUpdate == true) || allClaimsForPet[indexPath.row].StatusCode == "IC" || allClaimsForPet[indexPath.row].StatusCode == "CL"{
                                        return 590.0
                                    }
                                    else {
                                        return 550.0
                                    }
                                }
                                if (allClaimsForPet[indexPath.row].StatusCode == "NO" && allClaimsForPet[indexPath.row].CanUpdate == true) || allClaimsForPet[indexPath.row].StatusCode == "IC" || allClaimsForPet[indexPath.row].StatusCode == "CL" {
                                    return 570.0
                                }
                                else {
                                    return 550.0
                                }
                            }
                        }
                    }
                    if selectedOption == 1 {
                        return 340.0
                    } else {
                        return 340.0
                    }
                }
            } else {
                if indexPath.section == 0 && indexPath.section == 1  {
                    return 50.0
                } else {
                    if selectedOption == 2 {
                        if indexPath.row == allClaimsForPet.count-1 {
                            return 360.0
                        } else {
                            if !filteredClaimsForPet.isEmpty {
                                if indexPath.row == filteredClaimsForPet.count-1 {
                                    return 360.0
                                }
                                if indexPath.row == 0 {
                                    if (filteredClaimsForPet[indexPath.row].StatusCode == "NO" && filteredClaimsForPet[indexPath.row].CanUpdate == true) || filteredClaimsForPet[indexPath.row].StatusCode == "IC" || filteredClaimsForPet[indexPath.row].StatusCode == "CL" {
                                        return 370.0
                                    } else {
                                        return 350.0
                                    }
                                }
                                if (filteredClaimsForPet[indexPath.row].StatusCode == "NO" && filteredClaimsForPet[indexPath.row].CanUpdate == true) || filteredClaimsForPet[indexPath.row].StatusCode == "IC" || filteredClaimsForPet[indexPath.row].StatusCode == "CL" {
                                    return 370.0
                                } else {
                                    return 340.0
                                }
                            } else {
                                if indexPath.row == allClaimsForPet.count-1 {
                                    return 360.0
                                }
                                if indexPath.row == 0 {
                                    if (allClaimsForPet[indexPath.row].StatusCode == "NO" && allClaimsForPet[indexPath.row].CanUpdate == true) || allClaimsForPet[indexPath.row].StatusCode == "IC" || allClaimsForPet[indexPath.row].StatusCode == "CL"{
                                        return 370.0
                                    }
                                    else {
                                        return 350.0
                                    }
                                }
                                if (allClaimsForPet[indexPath.row].StatusCode == "NO" && allClaimsForPet[indexPath.row].CanUpdate == true) || allClaimsForPet[indexPath.row].StatusCode == "IC" || allClaimsForPet[indexPath.row].StatusCode == "CL" {
                                    return 370.0
                                }
                                else {
                                    return 340.0
                                }
                            }
                        }
                    }
                    if selectedOption == 1 {
                        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1334 {
                            return 220.0
                        } else {
                            return 230.0
                        }
                    } else {
                        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1334 {
                            return 220.0
                        } else {
                            return 230.0
                        }
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProgressBarTableViewCell = tableView.dequeueCell(ProgressBarTableViewCell.self)
        let cell1: ProfileDetailsTableViewCell = tableView.dequeueCell(ProfileDetailsTableViewCell.self)
        let cell2: ProfileTableViewCell = tableView.dequeueCell(ProfileTableViewCell.self)
        let cell3: ClaimHistoryTableViewCell = tableView.dequeueCell(ClaimHistoryTableViewCell.self)
        let cell4: ProfileImageTableViewCell = tableView.dequeueCell(ProfileImageTableViewCell.self)
        let cell5: ProcessedTableViewCell = tableView.dequeueCell(ProcessedTableViewCell.self)
        let cell6: NotFinishedClaimTableViewCell = tableView.dequeueCell(NotFinishedClaimTableViewCell.self)
        let cell7: ReceivedUpdatableTableViewCell = tableView.dequeueCell(ReceivedUpdatableTableViewCell.self)
        let cell8: WaitingOnCustomerTableViewCell = tableView.dequeueCell(WaitingOnCustomerTableViewCell.self)
        let cell9: ClosedWaitingOnCustomerTableViewCell = tableView.dequeueCell(ClosedWaitingOnCustomerTableViewCell.self)
        let cell10: ReceivedClosedTableViewCell = tableView.dequeueCell(ReceivedClosedTableViewCell.self)
        
        let cell18: ReceivedUpdatableTableViewCell = tableView.dequeueCell(ReceivedUpdatableTableViewCell.self)
        
        let cell11: LastProcessedTableViewCell = tableView.dequeueCell(LastProcessedTableViewCell.self)
        let cell12: LastNotFinishedTableViewCell = tableView.dequeueCell(LastNotFinishedTableViewCell.self)
        let cell13: LastReceivedTableViewCell = tableView.dequeueCell(LastReceivedTableViewCell.self)
        let cell14: LastClaimHistoryTableViewCell = tableView.dequeueCell(LastClaimHistoryTableViewCell.self)
        let cell15: LastWaitingOnCustomerTableViewCell = tableView.dequeueCell(LastWaitingOnCustomerTableViewCell.self)
        let cell16: LastClosedWaitingOnCustomerTableViewCell = tableView.dequeueCell(LastClosedWaitingOnCustomerTableViewCell.self)
        let cell17: LastReceivedClosedTableViewCell = tableView.dequeueCell(LastReceivedClosedTableViewCell.self)
        
        let cell20: WellnessCoverageTableViewCell = tableView.dequeueCell(WellnessCoverageTableViewCell.self)
        
        //First cells
        let cell21: FirstClaimHistoryTableViewCell = tableView.dequeueCell(FirstClaimHistoryTableViewCell.self)
        let cell22: FirstProcessedTableViewCell = tableView.dequeueCell(FirstProcessedTableViewCell.self)
        let cell23: FirstNotFinishedClaimTableViewCell = tableView.dequeueCell(FirstNotFinishedClaimTableViewCell.self)
        let cell25: FirstReceivedUpdatableTableViewCell = tableView.dequeueCell(FirstReceivedUpdatableTableViewCell.self)
        let cell26: FirstWaitingOnCustomerTableViewCell = tableView.dequeueCell(FirstWaitingOnCustomerTableViewCell.self)
        let cell27: FirstClosedWaitingOnCustomerTableViewCell = tableView.dequeueCell(FirstClosedWaitingOnCustomerTableViewCell.self)
        let cell28: FirstReceivedClosedTableViewCell = tableView.dequeueCell(FirstReceivedClosedTableViewCell.self)
        
        cell1.delegate = self
        cell.delegate = self
        cell20.delegate = self
        cell1.delegate1 = self
        cell3.delegate = self
        cell4.delegate = self
        cell5.delegate = self
        cell5.delegate1 = self
        cell6.delegate = self
        cell7.delegate = self
        cell8.delegate = self
        
        cell11.delegate = self
        cell12.delegate = self
        cell13.delegate = self
        
        cell21.delegate = self
        cell22.delegate = self
        cell22.delegate1 = self
        cell23.delegate = self
        cell25.delegate = self
        cell26.delegate = self
        cell27.delegate = self
        cell28.delegate = self
        
        if filteredClaimsForPet.isEmpty && isFiltered == false {
            self.profileView.labelNoResults.isHidden = true
        }
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                if imageProfile != nil {
                    cell4.configureCell(withName: selectedPet?.PetName ?? "", withImage: imageProfile!)
                    return cell4
                } else {
                    cell4.configureCell(withName: selectedPet?.PetName ?? "", withImage: nil)
                    return cell4
                }
            }
            if indexPath.row == 1 {
                cell1.configureCell(withSelectedIndex: selectedOption)
                return cell1
            }
        }
        if indexPath.section == 1 {
            if selectedOption == 1 {
                
//                user: 67411944125@test.com
//                password: 1234567
                if selectedPet?.PetCovers[indexPath.row].PlanId == 3 {
                    let titleArray = ["\(selectedPet!.PetCovers[indexPath.row].PlanDescription) Deductible:", "Reimbursement Level:"]
                    let planName = "  \(selectedPet!.PetCovers[indexPath.row].PlanName)"
                    if selectedPet!.PetCovers[indexPath.row].Limit == "Unlimited" {
                        let detailsArray = ["$\(selectedPet!.PetCovers[indexPath.row].Deductible)", "\(selectedPet!.PetCovers[indexPath.row].Reimbursement)%"]
                        let percentageArray: [CGFloat] = [0.0, CGFloat(selectedPet!.PetCovers[indexPath.row].Reimbursement)/100]
                        cell20.configureCell(withTitle: titleArray, withDetails: detailsArray, withPercentage: percentageArray,withPlan: planName, withRowNumber: indexPath.row)
                    } else {
                        let detailsArray = ["$\(selectedPet!.PetCovers[indexPath.row].Deductible)", "\(selectedPet!.PetCovers[indexPath.row].Reimbursement)%"]
                        let percentageArray: [CGFloat] = [0.0, CGFloat(selectedPet!.PetCovers[indexPath.row].Reimbursement)/100]
                        cell20.configureCell(withTitle: titleArray, withDetails: detailsArray, withPercentage: percentageArray,withPlan: planName, withRowNumber: indexPath.row)
                    }
                    DispatchQueue.main.async {
                        self.stopLoadingProfile()
                    }
                    return cell20
                }
                if selectedPet?.PetCovers[indexPath.row].PlanId == 4 {
                    let titleArray = ["\(selectedPet!.PetCovers[indexPath.row].PlanDescription) Deductible:", "Reimbursement Level:"]
                    let planName = "  \(selectedPet!.PetCovers[indexPath.row].PlanName)"
                    if selectedPet!.PetCovers[indexPath.row].Limit == "Unlimited" {
                        let detailsArray = ["$\(selectedPet!.PetCovers[indexPath.row].Deductible)", "\(selectedPet!.PetCovers[indexPath.row].Reimbursement)%"]
                        let percentageArray: [CGFloat] = [0.0, CGFloat(selectedPet!.PetCovers[indexPath.row].Reimbursement)/100]
                        cell20.configureCell(withTitle: titleArray, withDetails: detailsArray, withPercentage: percentageArray,withPlan: planName, withRowNumber: indexPath.row)
                    } else {
                        let detailsArray = ["$\(selectedPet!.PetCovers[indexPath.row].Deductible)", "\(selectedPet!.PetCovers[indexPath.row].Reimbursement)%"]
                        let percentageArray: [CGFloat] = [0.0, CGFloat(selectedPet!.PetCovers[indexPath.row].Reimbursement)/100]
                        cell20.configureCell(withTitle: titleArray, withDetails: detailsArray, withPercentage: percentageArray,withPlan: planName, withRowNumber: indexPath.row)
                    }
                    DispatchQueue.main.async {
                        self.stopLoadingProfile()
                    }
                    return cell20
                } else {
                    let titleArray = ["\(selectedPet!.PetCovers[indexPath.row].PlanDescription) Deductible:", "\(selectedPet!.PetCovers[indexPath.row].PlanDescription) Limit:", "Reimbursement Level:"]
                    let planName = "  \(selectedPet!.PetCovers[indexPath.row].PlanName)"
                    if selectedPet!.PetCovers[indexPath.row].Limit == "Unlimited" {
                        let detailsArray = ["$\(selectedPet!.PetCovers[indexPath.row].Deductible)", "\(selectedPet!.PetCovers[indexPath.row].Limit)", "\(selectedPet!.PetCovers[indexPath.row].Reimbursement)%"]
                        let percentageArray: [CGFloat] = [0.0, 1.0, CGFloat(selectedPet!.PetCovers[indexPath.row].Reimbursement)/100]
                        cell.configureCell(withTitle: titleArray, withDetails: detailsArray, withPercentage: percentageArray,withPlan: planName, withRowNumber: indexPath.row)
                    } else {
                        let detailsArray = ["$\(selectedPet!.PetCovers[indexPath.row].Deductible)", " \(selectedPet!.PetCovers[indexPath.row].Limit)", "\(selectedPet!.PetCovers[indexPath.row].Reimbursement)%"]
                        let percentageArray: [CGFloat] = [0.0, 0.0, CGFloat(selectedPet!.PetCovers[indexPath.row].Reimbursement)/100]
                        cell.configureCell(withTitle: titleArray, withDetails: detailsArray, withPercentage: percentageArray,withPlan: planName, withRowNumber: indexPath.row)
                    }
                    DispatchQueue.main.async {
                        self.stopLoadingProfile()
                    }
                    return cell
                }
            }

            if selectedOption == 2 {

                if filteredClaimsForPet.isEmpty && isFiltered == false && !allClaimsForPet.isEmpty {
                    if allClaimsForPet[indexPath.row].CanUpdate == true {
                        //IC
                        if allClaimsForPet[indexPath.row].StatusCode == "IC" {
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell12.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor: Constants.Colors.orangeButton, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell12
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell23.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor: Constants.Colors.orangeButton, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell23
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell6.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor: Constants.Colors.orangeButton, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell6
                            }
                        }
                        if allClaimsForPet[indexPath.row].StatusCode == "OP" {
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell13.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell13
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell25.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.viewClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell25
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell18.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell18
                            }
                        }
                        if allClaimsForPet[indexPath.row].StatusCode == "RO" {
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell13.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell13
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell25.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.viewClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell25
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell18.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell18
                            }
                        }
                        else {
                            //NO
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell13.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row,withButtonColor: UIColor.systemGray6, withButtonName: Constants.titlesButtons.viewClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell13
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell25.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.viewClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell25
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell7.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row,withButtonColor: UIColor.systemGray6, withButtonName: Constants.titlesButtons.viewClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell7
                            }
                        }
                    } else {
                        //NO
                        if allClaimsForPet[indexPath.row].StatusCode == "NO" {
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell14.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell14
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell21.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell21
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell10.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell10
                            }
                        }
                        if allClaimsForPet[indexPath.row].StatusCode == "CI" {
                            if indexPath.row == allClaimsForPet.count-1 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell16.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell16
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell27.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell27
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell9.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell9
                            }
                        }
                        if allClaimsForPet[indexPath.row].StatusCode == "MR"{
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell14.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell14
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell21.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell21
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell3.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell3
                            }
                        }
                        if allClaimsForPet[indexPath.row].StatusCode == "CU" {
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell15.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell15
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell26.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell26
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell8.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell8
                            }
                        }
                        if allClaimsForPet[indexPath.row].StatusCode == "VE" {
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell14.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell14
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell21.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell21
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell3.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell3
                            }
                        }
                        if allClaimsForPet[indexPath.row].StatusCode == "RO" {
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell17.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell17
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell28.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell28
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell10.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell10
                            }
                        }
                        if allClaimsForPet[indexPath.row].StatusCode == "CL" && allClaimsForPet[indexPath.row].PaymentNum != 0 {
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"

                                    cell11.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withAvailableEOB: true, withButtonColor: Constants.Colors.main, withButtonName: Constants.titlesButtons.viewDetails)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell11
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"

                                    cell22.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withAvailableEOB: true, withButtonColor: Constants.Colors.main, withButtonName: Constants.titlesButtons.viewDetails)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell22
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"

                                    cell5.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withAvailableEOB: true, withButtonColor: Constants.Colors.main, withButtonName: Constants.titlesButtons.viewDetails)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell5
                            }
                        }

                        if allClaimsForPet[indexPath.row].StatusCode == "CL" && allClaimsForPet[indexPath.row].PaymentNum == 0 {
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell17.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell17
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell28.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell28
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell10.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell10
                            }
                        }

                        if allClaimsForPet[indexPath.row].StatusCode == "IC"{
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell12.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor: Constants.Colors.orangeButton, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell12
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell23.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor: Constants.Colors.orangeButton, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell23
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell6.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor: Constants.Colors.orangeButton, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell6
                            }
                        } else {
                            //OP
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell17.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell17
                            }
                            if indexPath.row == 0 {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell28.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell28
                            } else {
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell10.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell10
                            }
                        }
                    }
                } else {
                    if filteredClaimsForPet[indexPath.row].CanUpdate == true {
                        //IC
                        if filteredClaimsForPet[indexPath.row].StatusCode == "IC" {
                            if indexPath.row == filteredClaimsForPet.count-1{
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell12.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor: Constants.Colors.orangeButton, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell12
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell23.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor: Constants.Colors.orangeButton, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell23
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell6.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor: Constants.Colors.orangeButton, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell6
                            }
                        }
                        if filteredClaimsForPet[indexPath.row].StatusCode == "OP" {
                            if indexPath.row == filteredClaimsForPet.count-1{
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell13.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell13
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell25.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.viewClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell25
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell18.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell18
                            }
                        }
                        if filteredClaimsForPet[indexPath.row].StatusCode == "RO" {
                            if indexPath.row == filteredClaimsForPet.count-1{
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell13.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell13
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell25.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.viewClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell25
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell18.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell18
                            }
                        }
                        else {
                            //NO
                            if indexPath.row == filteredClaimsForPet.count-1{
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell13.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row,withButtonColor: UIColor.systemGray6, withButtonName: Constants.titlesButtons.viewClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell13
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell25.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor:  UIColor.systemGray6, withButtonName: Constants.titlesButtons.viewClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell25
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell7.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row,withButtonColor: UIColor.systemGray6, withButtonName: Constants.titlesButtons.viewClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell7
                            }
                        }
                    } else {
                        //NO
                        if filteredClaimsForPet[indexPath.row].StatusCode == "NO" {
                            if indexPath.row == allClaimsForPet.count-1{
                                let date = allClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell14.configureCell(withClaimNumber: allClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: allClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: allClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell14
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell21.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell21
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell10.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell10
                            }
                        }
                        if filteredClaimsForPet[indexPath.row].StatusCode == "CI" {
                            if indexPath.row == filteredClaimsForPet.count-1 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell16.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell16
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell27.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell27
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell9.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell9
                            }
                        }
                        if filteredClaimsForPet[indexPath.row].StatusCode == "MR"{
                            if indexPath.row == filteredClaimsForPet.count-1{
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell14.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell14
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell21.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell21
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell3.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell3
                            }
                        }
                        if filteredClaimsForPet[indexPath.row].StatusCode == "CU" {
                            if indexPath.row == filteredClaimsForPet.count-1{
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell15.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell15
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell26.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell26
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell8.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell8
                            }
                        }
                        if filteredClaimsForPet[indexPath.row].StatusCode == "VE" {
                            if indexPath.row == filteredClaimsForPet.count-1{
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell14.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell14
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell21.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell21
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell3.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell3
                            }
                        }
                        if filteredClaimsForPet[indexPath.row].StatusCode == "RO" {
                            if indexPath.row == filteredClaimsForPet.count-1{
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell17.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell17
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell28.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell28
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell10.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell10
                            }
                        }
                        if filteredClaimsForPet[indexPath.row].StatusCode == "CL" && filteredClaimsForPet[indexPath.row].PaymentNum != 0 {
                            if indexPath.row == filteredClaimsForPet.count-1{
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"

                                    cell11.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withAvailableEOB: true, withButtonColor: Constants.Colors.main, withButtonName: Constants.titlesButtons.viewDetails)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell11
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"

                                    cell22.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withAvailableEOB: true, withButtonColor: Constants.Colors.main, withButtonName: Constants.titlesButtons.viewDetails)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell22
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"

                                    cell5.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withAvailableEOB: true, withButtonColor: Constants.Colors.main, withButtonName: Constants.titlesButtons.viewDetails)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell5
                            }
                        }

                        if filteredClaimsForPet[indexPath.row].StatusCode == "CL" && filteredClaimsForPet[indexPath.row].PaymentNum == 0 {
                            if indexPath.row == filteredClaimsForPet.count-1{
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell17.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell17
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell28.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell28
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell10.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell10
                            }
                        }

                        if filteredClaimsForPet[indexPath.row].StatusCode == "IC"{
                            if indexPath.row == filteredClaimsForPet.count-1{
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell12.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor: Constants.Colors.orangeButton, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell12
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell23.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor: Constants.Colors.orangeButton, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell23
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell6.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: allClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row, withButtonColor: Constants.Colors.orangeButton, withButtonName: Constants.titlesButtons.updateClaim)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell6
                            }
                        } else {
                            //OP
                            if indexPath.row == filteredClaimsForPet.count-1{
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell17.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell17
                            }
                            if indexPath.row == 0 {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell28.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell28
                            } else {
                                let date = filteredClaimsForPet[indexPath.row].DateReceived.toDate()
                                if let dateUnwrapped = date {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "MM/dd/YYYY"
                                    cell10.configureCell(withClaimNumber: filteredClaimsForPet[indexPath.row].ClaimId, withClaimType: filteredClaimsForPet[indexPath.row].StatusCode, withCondition: filteredClaimsForPet[indexPath.row].Description, withDate: formatter.string(from: dateUnwrapped), withClaimStatus: filteredClaimsForPet[indexPath.row].StatusName, withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withIndex: indexPath.row)
                                }
                                DispatchQueue.main.async {
                                    self.stopLoadingProfile()
                                }
                                return cell10
                            }
                        }
                    }
                }
            } else {
                var memberSinceYear: Int = 0
                let date = selectedPet?.FirstStartDate.toDate()
                if let dateUnwrapped = date {
                    let components = dateUnwrapped.get(.day, .month, .year)
                    memberSinceYear = components.year!
                }
                if selectedPet != nil {
                    if selectedPet!.Age.contains("old") {
                        cell2.configureCell(withName: selectedPet?.PetName ?? "", withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withDetails: "\(selectedPet?.BreedName ?? ""), " + "\(selectedPet?.Age ?? "")", withBreed: "\(Constants.titlesLabels.labelPolicyNumber) \(selectedPet?.PolicyNumber ?? 0)", withMembership: "Member since " + "\(String(describing: memberSinceYear))")
                    } else {
                        cell2.configureCell(withName: selectedPet?.PetName ?? "", withPolicyNumber: "\(selectedPet?.PolicyNumber ?? 0)", withDetails: "\(selectedPet?.BreedName ?? ""), " + "\(selectedPet?.Age ?? "")" + " old", withBreed: "\(Constants.titlesLabels.labelPolicyNumber) \(selectedPet?.PolicyNumber ?? 0)", withMembership: "Member since " + "\(String(describing: memberSinceYear))")
                    }
                }
                return cell2
            }
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath == self.profileView.tableView.indexPathsForVisibleRows?.last {
            if indexPath.row == allClaimsForPet.count-1 && currentPage < countPages && reachedEndOfItems == false
            {
                DispatchQueue.main.async {
                    self.startLoading()
                }
                currentPage = currentPage + 1
                loadMoreClaims()
            }
        }
    }
}

extension HomeViewController: ProfileDetailsDelegateReloadCell {
    
    func requestReloadTableViewCell(rowNumber: Int) {
        selectedOption = rowNumber
        DispatchQueue.main.async {
            self.profileView.tableView.reloadSections(IndexSet(integersIn: 1...1), with: .none)
        }
        if selectedOption == 2 {
            Analytics.logEvent(Constants.FirebaseEvents.myPetsClaimHistory, parameters: nil)
            Analytics.logEvent(AnalyticsEventScreenView,
                               parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.claims])
            profileView.imageFilter.isHidden = false
            if allClaimsForPet.isEmpty {
                self.profileView.labelNoResults.isHidden = false
            }
        } else {
            if selectedOption == 1 {
                Analytics.logEvent(Constants.FirebaseEvents.myPetsCoverage, parameters: nil)
                Analytics.logEvent(AnalyticsEventScreenView,
                                   parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.idCardCoverage])
                profileView.imageFilter.isHidden = true
                self.profileView.labelNoResults.isHidden = true
            } else {
                Analytics.logEvent(Constants.FirebaseEvents.myPetsIdCard, parameters: nil)
                Analytics.logEvent(AnalyticsEventScreenView,
                                   parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.idCardCoverage])
                profileView.imageFilter.isHidden = true
                self.profileView.labelNoResults.isHidden = true
            }
        }
    }
}

extension HomeViewController: ProfileImageDelegate {
    func closeView() {
        self.filterView.textFieldStatus.text = ""
        self.filterView.textFieldStartDate.text = ""
        self.filterView.textFieldEndDate.text = ""
        filterView.isHidden = true
        self.isFiltered = false
        self.profileView.buttonFilter.setTitle(Constants.titlesLabels.addFilter, for: .normal)
        
        filteredClaimsForPet = []
        self.reachedEndOfItems = false
        self.indexForReload = 0
        self.countPages = 0
        self.currentPage = 0
        profileView.isHidden = true
        if allClaimsForPet.isEmpty {
            self.profileView.labelNoResults.isHidden = false
        }
    }
    
    func openDeviceImages() {
        self.imagePicker.present(from: profileView)
    }
}

extension HomeViewController: ProcessedClaimDelegate {
    
    func navigateToDetails(index: Int, withEOB: Bool) {
        let vc = ClaimHistoryDetailsViewController()
        if filteredClaimsForPet.isEmpty {
            vc.paymentId = allClaimsForPet[index].PaymentNum
            vc.claimId = allClaimsForPet[index].ClaimId
        } else {
            vc.paymentId = filteredClaimsForPet[index].PaymentNum
            vc.claimId = filteredClaimsForPet[index].ClaimId
        }
        vc.withEOB = withEOB
        let navigationController = NavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .overFullScreen
        present(navigationController, animated: true, completion: nil)
    }
}

extension HomeViewController: NotFinishedClaimDelegate {
    func addGeneralError(withError: String) {
        DispatchQueue.main.async {
            self.showAlert(withTitle: "", message: withError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
        }
    }
    
    func addErroMessage(withError: String) {
        DispatchQueue.main.async {
            self.showAlert(withTitle: "", message: withError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
        }
    }
    
    func navigateToUpdateDetails(index: Int) {
        let vc = UpdateClaimViewController()
        vc.petName = selectedPet!.PetName
        if !filteredClaimsForPet.isEmpty {
            vc.claimId = filteredClaimsForPet[index].ClaimId
        } else {
            vc.claimId = allClaimsForPet[index].ClaimId
        }
        let navigationController = NavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .overFullScreen
        present(navigationController, animated: true, completion: nil)
    }
}

extension HomeViewController: ImagePickerDelegate {

    func didSelect(image: UIImage?) {
        imageProfile = image
        
        if image != nil {
            DispatchQueue.main.async {
                self.profileView.tableView.reloadData()
            }
            guard let img = image,
                  let selectPet = selectedPet else { return }
            imagePetCoreData = img.pngData()
            updatePet(pet: selectPet)
        }
    }
}

extension HomeViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 && filterView.textFieldStatus != nil {
            self.defaults.removeObject(forKey: "notNowActivatedLocationServices")
            self.filterView.textFieldStatus.text = ""
            self.filterView.textFieldStartDate.text = ""
            self.filterView.textFieldEndDate.text = ""
            self.isFiltered = false
            DispatchQueue.main.async {
                self.profileView.buttonFilter.setTitle(Constants.titlesLabels.addFilter, for: .normal)
            }
            
            filteredClaimsForPet = []
            self.reachedEndOfItems = false
            self.indexForReload = 0
            self.countPages = 0
            self.currentPage = 0
            profileView.isHidden = true
            if allClaimsForPet.isEmpty {
                self.profileView.labelNoResults.isHidden = false
            }
         }
        if tabBarIndex == 2 {
            self.defaults.removeObject(forKey: "notNowActivatedLocationServices")
        }
        if tabBarIndex == 3 {
            self.defaults.removeObject(forKey: "notNowActivatedLocationServices")
        }
    }
}

extension HomeViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}

extension HomeViewController: isAbleToReceiveData {
    func passStartNotStringDate(data: Date) {
        selectedStartDate = data
    }
    
    func passEndNotStringDate(data: Date) {
        selectedEndDate = data
    }
    
    func passEndDate(data: String) {
        filterView.textFieldEndDate.text = data
    }
    func passStartDate(data: String) {
        filterView.textFieldStartDate.text = data
    }
}

extension HomeViewController: ProgressBarTableViewCellDelegate {
    func seeCoreBenefits(rowNumber: Int) {
        
//        print(selectedPet?.PetCovers[rowNumber].PlanId)
//        print(selectedPet?.PetCovers[rowNumber].PlanName)
        
        if selectedPet?.PetCovers[rowNumber].PlanId == 3 {
            let vc = WelnessBenefitsViewController()
            vc.selectedPet = selectedPet?.PetCovers[rowNumber]
            vc.arrayBenefits = arrayCannineBasicWelnessPlan
            vc.arrayBenefitsPrices = arrayCannineBasicWelnessPlanPrices
            navigationController?.pushViewController(vc, animated: true)
        }
        if selectedPet?.PetCovers[rowNumber].PlanId == 4 {
            let vc = WelnessBenefitsViewController()
            vc.selectedPet = selectedPet?.PetCovers[rowNumber]
            vc.arrayBenefits = arrayFelineBestWelnessPlan
            vc.arrayBenefitsPrices = arrayFelineBestWelnessPlanPrices
            navigationController?.pushViewController(vc, animated: true)
        }
        if selectedPet?.PetCovers[rowNumber].PlanId == 18 {
            let vc = WelnessBenefitsViewController()
            vc.selectedPet = selectedPet?.PetCovers[rowNumber]
            vc.arrayBenefits = arrayFelineBasicWelnessPlan
            vc.arrayBenefitsPrices = arrayFelineBasicWelnessPlanPrices
            navigationController?.pushViewController(vc, animated: true)
        }
        if selectedPet?.PetCovers[rowNumber].PlanId == 30 { 
            let vc = CoreBenefitsViewController()
            vc.selectedPet = selectedPet?.PetCovers[rowNumber]
            vc.policyDocuments = selectedPet?.PolicyDocuments
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
