//
//  AddPetViewController.swift
//  PetsBest
//
//  Created by Plamena on 23.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import WebKit
import NVActivityIndicatorView
import KeychainSwift

class AddPetViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {

    @IBOutlet private weak var webViewAddPet: WKWebView!
    @IBOutlet private weak var activityIndicatorScreen: NVActivityIndicatorView!
    private let keychain = KeychainSwift()
    private let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        loadingWebView()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        if keychain.get("deleteCookies") != nil {
//            webViewAddPet.cleanAllCookies()
//            webViewAddPet.refreshCookies()
            self.keychain.delete("deleteCookies")
        }
    }
    
    private func setViews() {
        navigationController?.navigationBar.isHidden = false
        DispatchQueue.main.async {
            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self.back))
            self.webViewAddPet.navigationDelegate = self
            self.webViewAddPet.scrollView.bounces = false
            self.startLoadingWholeScreen()
        }
    }
    
    private func loadingWebView() {
        DispatchQueue.main.async {
            if let url = try? APIRouter.addPet.asURLRequestCustomerPortal() {
                self.webViewAddPet.load(url as URLRequest)
            }
        }
    }
    
    @objc private func back(){
        self.tabBarController?.selectedIndex = 0 //you can select another tab if needed
        let vc = HomeViewController(withCoreDataStack: CoreDataStack())
        if let navController = tabBarController?.viewControllers?[0] as? UINavigationController {
           navController.pushViewController(vc, animated: true)
        }
        self.tabBarController?.tabBar.isHidden = false
    }
    
    private func startLoadingWholeScreen() {
        activityIndicatorScreen?.startAnimating()
    }
    
    private func stopLoadingWholeScreen() {
        activityIndicatorScreen?.stopAnimating()
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
       //load cookie of current domain
        if let urlString = try? APIRouter.addPet.asURLRequestCustomerPortal() {
            webViewAddPet.loadDiskCookies(for: (urlString.url?.host)!){
                decisionHandler(.allow)
            }
        }
    }

    public func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
       //write cookie for current domain
        if let urlString = try? APIRouter.addPet.asURLRequestCustomerPortal() {
            webViewAddPet.writeDiskCookies(for: (urlString.url?.host)!){
                decisionHandler(.allow)
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error)
        webViewAddPet.refreshCookies()
    }
    
    internal func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        guard let jsFile = Bundle.main.url(forResource: "index", withExtension: "js") else {
            return
        }

        do {
            guard let username = self.keychain.get("username"),
                  let password = self.keychain.get("password")
            else { return }
            let injectJS = try String(contentsOf: jsFile)
            let formatted = String(format: injectJS, username, password)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                webView.evaluateJavaScript(formatted) { [self] (value, error) in
                    self.stopLoadingWholeScreen()
                }
            }
        } catch {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                self.stopLoadingWholeScreen()
            }
        }
    }
}
