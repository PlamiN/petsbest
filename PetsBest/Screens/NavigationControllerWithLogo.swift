//
//  NavigationControllerWithLogo.swift
//  PetsBest
//
//  Created by Plamena on 13.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

import UIKit

class NavigationControllerWithLogo: UINavigationController {

    // MARK: - Object lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
    }
    
    // MARK: - Navigation Settings
    // Note: Add more...
    private func setupNavigation() {
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = UIColor.white
            //self.navigationItem.titleView = imageView
            appearance.titleTextAttributes = [.foregroundColor: UIColor.init(hex: Constants.Colors.labels)!]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.init(hex: Constants.Colors.labels)!]
            UINavigationBar.appearance().tintColor = .white
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().compactAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        } else {
            UINavigationBar.appearance().tintColor = .black
            UINavigationBar.appearance().barTintColor = UIColor.clear
            UINavigationBar.appearance().isTranslucent = false
            self.navigationBar.prefersLargeTitles = true
            self.navigationItem.largeTitleDisplayMode = .automatic
        }
    }
}
