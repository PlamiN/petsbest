//
//  InformationViewController.swift
//  PetsBest
//
//  Created by Plamena on 20.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import KeychainSwift
import AVKit
import WebKit
import UserNotifications

class InformationViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet private weak var scrollView: UIScrollView!{
        didSet{
            scrollView.delegate = self
        }
    }
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var buttonNext: UIButton!
    @IBOutlet private weak var buttonNotNow: UIButton!
    @IBOutlet private weak var labelThingsYouCanDo: UILabel!
    @IBOutlet private weak var buttonNextHeight: NSLayoutConstraint!
    @IBOutlet private weak var stackViewDetails: UIStackView!
    @IBOutlet private weak var constantNextToDetails: NSLayoutConstraint!
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var heightLabelDetails: NSLayoutConstraint!
    @IBOutlet private weak var constraintBetweenButton: NSLayoutConstraint!
    @IBOutlet private weak var buttonNextHeighy: NSLayoutConstraint!
    @IBOutlet private weak var buttonNotNowHeight: NSLayoutConstraint!
    
    @IBOutlet private weak var titleConstraintTop: NSLayoutConstraint!
    @IBOutlet private weak var scrollHeight: NSLayoutConstraint!
    @IBOutlet private weak var scrollViewViewHeight: NSLayoutConstraint!
    @IBOutlet private weak var titleToScrollViewConstraint: NSLayoutConstraint!
    @IBOutlet private weak var constraintDetailsToScrollView: NSLayoutConstraint!
    
    private var slides:[SlideView] = []
    private var slidesIPad:[SlideViewiPad] = []
    private let keychain = KeychainSwift()
    private let defaults = UserDefaults.standard
    
    private let biometricLogin = BiometricIDAuth()
    
    var webview = WKWebView()
    
    var hasBiometricsActivates = true
    
    private var numberOfScrolling = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.setViews()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
    }
    
    private func setViews() {
        
        if UIDevice().userInterfaceIdiom == .pad {
            DispatchQueue.main.async {
                self.slidesIPad = self.createSlidesIPad()
                self.setupSlideScrollViewiPad(slides: self.slidesIPad)
            }
        } else {
            DispatchQueue.main.async {
                self.slides = self.createSlides()
                self.setupSlideScrollView(slides: self.slides)
            }
        }
        
        buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        buttonNext.setTitleColor(UIColor.white, for: .normal)
        buttonNext.layer.cornerRadius = 25
        labelThingsYouCanDo.textColor = UIColor.black
        labelTitle.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        
        if UIDevice().userInterfaceIdiom == .pad {
            if UIScreen.main.nativeBounds.height == 2048 || UIScreen.main.nativeBounds.height == 2266 {
                labelThingsYouCanDo.font = UIFont(name: "Proxima Nova", size: 30)
                buttonNext.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 40)
                labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 52)
                buttonNextHeight.constant = 80
                buttonNotNowHeight.constant = 80
                buttonNext.layer.cornerRadius = 40
                stackViewDetails.spacing = 50
                constantNextToDetails.constant = 25
                scrollHeight.constant = 450
                scrollViewViewHeight.constant = 300
                heightLabelDetails.constant = 250
                titleConstraintTop.constant = 16
                constraintDetailsToScrollView.constant = 0
                let yourAttributes: [NSAttributedString.Key: Any] = [
                    .font: UIFont(name: "Proxima Nova Bold", size: 40)!,
                    .foregroundColor: UIColor.init(hex: Constants.Colors.welcomeLabels)!,
                    .underlineStyle: NSUnderlineStyle.single.rawValue
                ] // .double.rawValue, .thick.rawValue
                
                let attributeString = NSMutableAttributedString(
                   string: Constants.titlesButtons.notNow,
                   attributes: yourAttributes
                )
                buttonNotNow.setAttributedTitle(attributeString, for: .normal)
            }
            if UIScreen.main.nativeBounds.height == 2732 {
                labelThingsYouCanDo.font = UIFont(name: "Proxima Nova", size: 35)
                buttonNext.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 45)
                labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 52)
                buttonNextHeight.constant = 80
                buttonNotNowHeight.constant = 80
                buttonNext.layer.cornerRadius = 40
                stackViewDetails.spacing = 50
                constantNextToDetails.constant = 25
                scrollHeight.constant = 630
                scrollViewViewHeight.constant = 400
                heightLabelDetails.constant = 280
                titleConstraintTop.constant = 16
                constraintDetailsToScrollView.constant = 0
                let yourAttributes: [NSAttributedString.Key: Any] = [
                    .font: UIFont(name: "Proxima Nova Bold", size: 40)!,
                    .foregroundColor: UIColor.init(hex: Constants.Colors.welcomeLabels)!,
                    .underlineStyle: NSUnderlineStyle.single.rawValue
                ] // .double.rawValue, .thick.rawValue
                
                let attributeString = NSMutableAttributedString(
                   string: Constants.titlesButtons.notNow,
                   attributes: yourAttributes
                )
                buttonNotNow.setAttributedTitle(attributeString, for: .normal)
            }
            if UIScreen.main.nativeBounds.height == 1024 {
                let yourAttributes: [NSAttributedString.Key: Any] = [
                    .font: UIFont(name: "Proxima Nova Bold", size: 30)!,
                    .foregroundColor: UIColor.init(hex: Constants.Colors.welcomeLabels)!,
                    .underlineStyle: NSUnderlineStyle.single.rawValue
                ] // .double.rawValue, .thick.rawValue
                
                let attributeString = NSMutableAttributedString(
                   string: Constants.titlesButtons.notNow,
                   attributes: yourAttributes
                )
                labelThingsYouCanDo.font = UIFont(name: "Proxima Nova", size: 30)
                buttonNotNow.setAttributedTitle(attributeString, for: .normal)
                labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 35)
                buttonNext.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 30)
                buttonNextHeight.constant = 60
                buttonNotNowHeight.constant = 60
                buttonNext.layer.cornerRadius = 30
                stackViewDetails.spacing = 20
                constantNextToDetails.constant = 25
                scrollHeight.constant = 450
                scrollViewViewHeight.constant = 300
                heightLabelDetails.constant = 200
                titleConstraintTop.constant = 16
                constraintDetailsToScrollView.constant = 0
            }
            else {
                labelThingsYouCanDo.font = UIFont(name: "Proxima Nova", size: 30)
                buttonNext.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 40)
                labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 52)
                buttonNextHeight.constant = 80
                buttonNotNowHeight.constant = 80
                buttonNext.layer.cornerRadius = 40
                stackViewDetails.spacing = 50
                constantNextToDetails.constant = 25
                scrollHeight.constant = 450
                scrollViewViewHeight.constant = 300
                heightLabelDetails.constant = 250
                
                let yourAttributes: [NSAttributedString.Key: Any] = [
                    .font: UIFont(name: "Proxima Nova Bold", size: 40)!,
                    .foregroundColor: UIColor.init(hex: Constants.Colors.welcomeLabels)!,
                    .underlineStyle: NSUnderlineStyle.single.rawValue
                ] // .double.rawValue, .thick.rawValue
                
                let attributeString = NSMutableAttributedString(
                   string: Constants.titlesButtons.notNow,
                   attributes: yourAttributes
                )
                buttonNotNow.setAttributedTitle(attributeString, for: .normal)
            }
        } else {
            if UIScreen.main.nativeBounds.height == 1792 || UIScreen.main.nativeBounds.height == 2436 || UIScreen.main.nativeBounds.height == 2532 || UIScreen.main.nativeBounds.height == 2688 || UIScreen.main.nativeBounds.height == 2778 {
                stackViewDetails.spacing = 20
            }
            
            if UIScreen.main.nativeBounds.height == 2532 {
                scrollHeight.constant = 250
            }
            
            scrollViewViewHeight.constant = 300
            let yourAttributes: [NSAttributedString.Key: Any] = [
                .font: UIFont(name: "Proxima Nova Bold", size: 18)!,
                .foregroundColor: UIColor.init(hex: Constants.Colors.welcomeLabels)!,
                .underlineStyle: NSUnderlineStyle.single.rawValue
            ] // .double.rawValue, .thick.rawValue
            
            let attributeString = NSMutableAttributedString(
               string: Constants.titlesButtons.notNow,
               attributes: yourAttributes
            )
            
            if labelThingsYouCanDo != nil {
                if UIScreen.main.nativeBounds.height == 1334 || UIScreen.main.nativeBounds.height == 1136 {
                    labelThingsYouCanDo.font = UIFont(name: "Proxima Nova", size: 17)
                } else {
                    labelThingsYouCanDo.font = UIFont(name: "Proxima Nova", size: 20)
                }
            }
            buttonNotNow.setAttributedTitle(attributeString, for: .normal)
            labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 25)
            buttonNext.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 18)
        }
    }
    
    private func getBiometricLogin() {
        switch biometricLogin.biometricType() {
        case .faceID:
            print("Face ID")
            biometricLogin.authenticateUser { [weak self] message in
                if message == "true" {
                    DispatchQueue.main.async {
                        self?.scrollToNextScreen()
                        self?.defaults.set("activate", forKey: "FaceID")
                    }
                } else {
                    DispatchQueue.main.async {
                        self?.defaults.set("deactivate", forKey: "FaceID")
//                        if let url = URL(string: "App-Prefs:root=TOUCHID_PASSCODE") {
//                            UIApplication.shared.openURL(url)
//                        }
                        
                        self?.scrollToNextScreen()
                    }
                }
            }
        case .touchID:
            print("Touch ID")
            biometricLogin.authenticateUser { [weak self] message in
                if message == "true" {
                    DispatchQueue.main.async {
                        self?.scrollToNextScreen()
                        self?.defaults.set("activate", forKey: "FaceID")
                    }
                } else {
                    DispatchQueue.main.async {
                        self?.defaults.set("deactivate", forKey: "FaceID")
                        
//                        if let url = URL(string: "App-Prefs:root=TOUCHID_PASSCODE") {
//                            UIApplication.shared.openURL(url)
//                        }
                        
                        self?.scrollToNextScreen()
                    }
                }
            }
        case .none:
            print("No ID")
            self.defaults.set("deactivate", forKey: "FaceID")
        default: break
        }
        
    }
    
    private func getPushNotificationsActivation() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()

            // iOS 10+ Push Message Registration, some versions of iOS have different options available
            let options: UNAuthorizationOptions = {
                if #available(iOS 12.0, *) {
                    return [.alert, .sound, .carPlay, .badge, .providesAppNotificationSettings]
                }
                return [.alert, .sound, .carPlay, .badge]
            }()
            
            center.requestAuthorization(options: options) { (granted, error) in
                if error == nil{
                    
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                        center.setNotificationCategories( self.notificationCategories() )
                        self.scrollToNextScreenWithoutPushNotifications()
                        self.checkPushNotifications()
                    }
                }
            }

        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
            self.scrollToNextScreenWithoutPushNotifications()
            self.checkPushNotifications()
        }
    }
    
    func checkPushNotifications(){
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { settings in

                switch settings.authorizationStatus {

                case .notDetermined:
                    // Authorization request has not been made yet
                    if self.defaults.string(forKey: "PushNotifications") == nil && self.defaults.string(forKey: "firstPushNotifications") == nil {
                        self.defaults.set("deactivate", forKey: "firstPushNotifications")
                        self.defaults.set("deactivate", forKey: "PushNotifications")
                    }
                case .denied:
                    // User has denied authorization.
                    // You could tell them to change this in Settings
                    if self.defaults.string(forKey: "PushNotifications") == nil && self.defaults.string(forKey: "firstPushNotifications") == nil {
                        self.defaults.set("deactivate", forKey: "firstPushNotifications")
                        self.defaults.set("deactivate", forKey: "PushNotifications")
                    }
                case .authorized:
                    // User has given authorization.
                    if self.defaults.string(forKey: "PushNotifications") == nil && self.defaults.string(forKey: "firstPushNotifications") == nil {
                        self.defaults.set("activate", forKey: "firstPushNotifications")
                        self.defaults.set("activate", forKey: "PushNotifications")
                    }
                case .provisional:
                    return
                case .ephemeral:
                    return
                @unknown default:
                    return
                }
            })
         } else {
             // Fallback on earlier versions
             if UIApplication.shared.isRegisteredForRemoteNotifications {
                 print("APNS-YES")
             } else {
                 print("APNS-NO")
             }
         }
    }
    
    func notificationCategories() -> Set<UNNotificationCategory> {
        // iOS 10+ Example static action category:
        let acceptAction = UNNotificationAction(identifier: "Accept", title: "Accept", options: [.foreground])
        let rejectAction = UNNotificationAction(identifier: "Reject", title: "Reject", options: [.destructive])
        let category = UNNotificationCategory(identifier: "example", actions: [acceptAction, rejectAction], intentIdentifiers: [], options: [.customDismissAction])
        
        return Set(arrayLiteral: category)
    }
    
    private func scrollToNextScreen() {
        
        if self.scrollView.contentOffset.x < (self.view.bounds.width) * CGFloat(self.slides.count - 1) || (self.scrollView.contentOffset.x) < self.view.bounds.width * CGFloat(self.slidesIPad.count - 1){
            self.scrollView.isScrollEnabled = true
            
            if numberOfScrolling == 0 {
                labelTitle.text = Constants.titlesButtons.stayUpToDate

                labelThingsYouCanDo.text = Constants.titlesButtons.detailsPushNotifications
                
                buttonNotNow.setTitle(Constants.titlesButtons.notNow, for: .normal)
                
                buttonNext.setTitle(Constants.titlesButtons.allowNotifications, for: .normal)
            }
            
            if numberOfScrolling == 1 {
                labelTitle.text = Constants.titlesButtons.thatsIt

                labelThingsYouCanDo.text = Constants.titlesButtons.videoDetails
                buttonNotNow.isHidden = true
                
                buttonNext.setTitle(Constants.titlesButtons.continueToApp, for: .normal)
            }
            self.numberOfScrolling += 1
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1136:
                    print("iPhone 5 or 5S or 5C")
                    self.scrollView.contentOffset.x +=  (self.view.bounds.width) - (self.view.bounds.width)/30
                    heightLabelDetails.constant = 90
                case 1334:
                    print("iPhone 6/6S/7/8, SE 2nd Generation")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width + (self.view.bounds.width)/9.5
                    heightLabelDetails.constant = 130
                case 1792:
                    print("iPhone XR/11")
                    self.scrollView.contentOffset.x +=  (self.view.bounds.width)
                    heightLabelDetails.constant = 200
                case 2208:
                    print("iPhone 6+/6S+/7+/8+")
                    self.scrollView.contentOffset.x +=  (self.view.bounds.width)
                    heightLabelDetails.constant = 150

                case 1920:
                    print("iPhone 6+/6S+/7+/8+")
                    self.scrollView.contentOffset.x +=  (self.view.bounds.width)
                    heightLabelDetails.constant = 150
                case 2436:
                    print("iPhone X/XS/11 Pro,iPhone 12 Mini")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width + self.view.bounds.width/9.5
                    heightLabelDetails.constant = 150

                case 2532:
                    print("iPhone 12, iPhone 12 Pro, iPhone 13, iPhone 13 Pro")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width + self.view.bounds.width/16
                    heightLabelDetails.constant = 200

                case 2688:
                    print("iPhone XS Max/11 Pro Max")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 150

                case 2340:
                    print("iPhone 13 mini")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 200

                case 2778:
                    print("iPhone 13 Pro Max")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 150

                default:
                    print("Unknown")
                }
            }

            if UIDevice().userInterfaceIdiom == .pad {
                switch UIScreen.main.nativeBounds.height {
                case 1024:
                    print("iPad 2, iPad 1st gen, iPad mini")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width - self.view.bounds.width/30
                    heightLabelDetails.constant = 400
                case 2266:
                    print("iPad mini")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width + self.view.bounds.width/10
                    heightLabelDetails.constant = 400
                case 2048:
                    print("iPad 3rd gen, iPad 4th gen, iPad Air,iPad Air 2, iPad mini 3, iPad mini 2, iPad mini 4,iPad Pro (1st gen 9.7”), iPad 5th gen, iPad 6th gen, iPad Mini (5th gen)")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width + 40
                    heightLabelDetails.constant = 400
                case 2160:
                    print("iPad 7th gen")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 250
                case 2224:
                    print("iPad Pro (2nd gen 10.5), iPad Air (3rd gen)")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 400
                case 2360:
                    print("iPad Air (4th generation)")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 400

                case 2388:
                    print("iPad Pro (3rd gen 11),iPad Pro (4th gen 11)")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 400
                case 2732:
                    print("iPad Pro (1st gen 12.9),iPad Pro (2nd gen 12.9),iPad Pro (3rd gen 12.9),iPad Pro (4th gen 12.9)")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 400

                default:
                    print("Unknown")
                }
            }
        } else {
            self.keychain.set("disabled", forKey: "isFirstLaunch")
            let vc = HomeViewController(withCoreDataStack: CoreDataStack())
            if let navController = self.tabBarController?.viewControllers?[0] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func scrollToNextScreenWithoutPushNotifications() {
        
        if self.scrollView.contentOffset.x < (self.view.bounds.width) * CGFloat(self.slides.count - 1) || (self.scrollView.contentOffset.x) < self.view.bounds.width * CGFloat(self.slidesIPad.count - 1){
            self.scrollView.isScrollEnabled = true
            if numberOfScrolling == 0 {
                
                labelTitle.text = Constants.titlesButtons.stayUpToDate
                
                labelThingsYouCanDo.text = Constants.titlesButtons.detailsPushNotifications

                buttonNotNow.setTitle(Constants.titlesButtons.notNow, for: .normal)
                
                buttonNext.setTitle(Constants.titlesButtons.allowNotifications, for: .normal)
                
                self.defaults.set("true", forKey: "notNowActivatedBiometrics")
            }
            if numberOfScrolling == 1 {
                labelTitle.text = Constants.titlesButtons.thatsIt

                labelThingsYouCanDo.text = Constants.titlesButtons.videoDetails
                buttonNotNow.isHidden = true
                
                buttonNext.setTitle(Constants.titlesButtons.continueToApp, for: .normal)
                
                self.defaults.set("true", forKey: "notNowActivatedPushNotifications")
            }
            self.numberOfScrolling += 1
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1136:
                    print("iPhone 5 or 5S or 5C")
                    self.scrollView.contentOffset.x +=  (self.view.bounds.width) - (self.view.bounds.width)/30
                    heightLabelDetails.constant = 70
                case 1334:
                    print("iPhone 6/6S/7/8, SE 2nd Generation")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width + (self.view.bounds.width)/9.5
                    heightLabelDetails.constant = 130

                case 1792:
                    print("iPhone XR/11")
                    self.scrollView.contentOffset.x +=  (self.view.bounds.width)
                    heightLabelDetails.constant = 180
                case 2208:
                    print("iPhone 6+/6S+/7+/8+")
                    self.scrollView.contentOffset.x +=  (self.view.bounds.width)
                    heightLabelDetails.constant = 150
                case 1920:
                    print("iPhone 6+/6S+/7+/8+")
                    self.scrollView.contentOffset.x +=  (self.view.bounds.width)
                    heightLabelDetails.constant = 150
                case 2436:
                    print("iPhone X/XS/11 Pro,iPhone 12 Mini")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width + self.view.bounds.width/9.5
                    heightLabelDetails.constant = 150

                case 2532:
                    print("iPhone 12, iPhone 12 Pro, iPhone 13, iPhone 13 Pro")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width + self.view.bounds.width/16
                    heightLabelDetails.constant = 200

                case 2688:
                    print("iPhone XS Max/11 Pro Max")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 150

                case 2340:
                    print("iPhone 13 mini")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 200

                case 2778:
                    print("iPhone 13 Pro Max")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 150

                default:
                    print("Unknown")
                }
            }

            if UIDevice().userInterfaceIdiom == .pad {
                switch UIScreen.main.nativeBounds.height {
                case 1024:
                    print("iPad 2, iPad 1st gen, iPad mini")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width - self.view.bounds.width/30
                    heightLabelDetails.constant = 150
                case 2266:
                    print("iPad mini")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width - self.view.bounds.width/30
                    heightLabelDetails.constant = 400
                case 2048:
                    print("iPad 3rd gen, iPad 4th gen, iPad Air,iPad Air 2, iPad mini 3, iPad mini 2, iPad mini 4,iPad Pro (1st gen 9.7”), iPad 5th gen, iPad 6th gen, iPad Mini (5th gen)")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width + 40
                    heightLabelDetails.constant = 400
                case 2160:
                    print("iPad 7th gen")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 250
                case 2224:
                    print("iPad Pro (2nd gen 10.5), iPad Air (3rd gen)")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 150
                case 2360:
                    print("iPad Air (4th generation)")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 400

                case 2388:
                    print("iPad Pro (3rd gen 11),iPad Pro (4th gen 11)")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 150
                case 2732:
                    print("iPad Pro (1st gen 12.9),iPad Pro (2nd gen 12.9),iPad Pro (3rd gen 12.9),iPad Pro (4th gen 12.9)")
                    self.scrollView.contentOffset.x +=  self.view.bounds.width
                    heightLabelDetails.constant = 150

                default:
                    print("Unknown")
                }
            }
        } else {
            self.keychain.set("disabled", forKey: "isFirstLaunch")
            let vc = HomeViewController(withCoreDataStack: CoreDataStack())
            if let navController = self.tabBarController?.viewControllers?[0] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func createSlides() -> [SlideView] {
        var videoLeadingConstraint = 0
        var videoPaddingSmallDevices = 0
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                videoLeadingConstraint = 8
                videoPaddingSmallDevices = 32
                constraintBetweenButton.constant = 8
                heightLabelDetails.constant = 100
            case 1334:
                videoLeadingConstraint = 8
                videoPaddingSmallDevices = 32
                constraintBetweenButton.constant = 8
                heightLabelDetails.constant = 130
            case 1792:
                videoLeadingConstraint = 8
                heightLabelDetails.constant = 200
            case 2208:
                videoLeadingConstraint = 4
                videoPaddingSmallDevices = 32
                heightLabelDetails.constant = 150
            case 1920:
                videoLeadingConstraint = 8
                constraintBetweenButton.constant = 8
                heightLabelDetails.constant = 150
            case 2436:
                videoLeadingConstraint = 8
                heightLabelDetails.constant = 150
            case 2532:
                videoLeadingConstraint = 8
                videoPaddingSmallDevices = 16
                heightLabelDetails.constant = 150
            case 2688:
                videoLeadingConstraint = 8
                heightLabelDetails.constant = 150
            case 2778:
                videoLeadingConstraint = 8
                heightLabelDetails.constant = 150
            case 2340:
                print("iPhone 13 mini")
                videoLeadingConstraint = 4
                videoPaddingSmallDevices = 32
                heightLabelDetails.constant = 150
            default:
                print("Unknown")
            }
        }
        
        if biometricLogin.biometricType() == .faceID {
            
            labelTitle.text = Constants.titlesLabels.labelEnableFaceID
            
            labelThingsYouCanDo.text = Constants.titlesLabels.labelSubtitleScrollViewFaceID
            
            buttonNext.setTitle(Constants.titlesButtons.allowFaceID, for: .normal)
            
            buttonNotNow.setTitle(Constants.titlesButtons.notNow, for: .normal)
            
            let slide2:SlideView = Bundle.main.loadNibNamed("SlideView", owner: self, options: nil)?.first as! SlideView
            slide2.imagePets.image = UIImage(named: "test2")

            let slide3:SlideView = Bundle.main.loadNibNamed("SlideView", owner: self, options: nil)?.first as! SlideView
            slide3.imagePets.image = UIImage(named: "test3")
            
            let slide4:SlideView = Bundle.main.loadNibNamed("SlideView", owner: self, options: nil)?.first as! SlideView
            slide4.imagePets.isHidden = true
            let configuration = WKWebViewConfiguration()
            configuration.allowsInlineMediaPlayback = true
            webview = WKWebView(frame: CGRect(x: CGFloat(videoLeadingConstraint), y: 0, width: slide4.bounds.width - CGFloat(videoPaddingSmallDevices), height: slide4.bounds.height), configuration: configuration)
            let myURL = URL(string: "https://fast.wistia.net/embed/iframe/wm5ur70pj2?seo=false&videoFoam=true4")
            let myRequest = URLRequest(url: myURL!)
            webview.load(myRequest)
            slide4.addSubview(webview)

            return [slide2, slide3, slide4]
        }
        if biometricLogin.biometricType() == .touchID {
            
            if hasBiometricsActivates == true {
                self.labelTitle.text = Constants.titlesLabels.labelEnableTouchID
                
                self.labelThingsYouCanDo.text = Constants.titlesLabels.labelSubtitleScrollViewTouchID
                
                self.buttonNext.setTitle(Constants.titlesButtons.allowTouchID, for: .normal)
                
                self.buttonNotNow.setTitle(Constants.titlesButtons.notNow, for: .normal)
                
                let slide1:SlideView = Bundle.main.loadNibNamed("SlideView", owner: self, options: nil)?.first as! SlideView
                slide1.imagePets.image = UIImage(named: "test1")

                let slide3:SlideView = Bundle.main.loadNibNamed("SlideView", owner: self, options: nil)?.first as! SlideView
                slide3.imagePets.image = UIImage(named: "test3")
                
                let slide4:SlideView = Bundle.main.loadNibNamed("SlideView", owner: self, options: nil)?.first as! SlideView
                let configuration = WKWebViewConfiguration()
                configuration.allowsInlineMediaPlayback = true
                self.webview = WKWebView(frame: CGRect(x: CGFloat(videoLeadingConstraint), y: 0, width: slide4.bounds.width - CGFloat(videoPaddingSmallDevices), height: slide4.bounds.height), configuration: configuration)
                let myURL = URL(string: "https://fast.wistia.net/embed/iframe/wm5ur70pj2?seo=false&videoFoam=true4")
                let myRequest = URLRequest(url: myURL!)
                self.webview.load(myRequest)
                slide4.addSubview(self.webview)
                
                return [slide1, slide3, slide4]
            } else {
                let slide3:SlideView = Bundle.main.loadNibNamed("SlideView", owner: self, options: nil)?.first as! SlideView
                slide3.imagePets.image = UIImage(named: "test3")
                
                let slide4:SlideView = Bundle.main.loadNibNamed("SlideView", owner: self, options: nil)?.first as! SlideView
                
                let configuration = WKWebViewConfiguration()
                configuration.allowsInlineMediaPlayback = true
                webview = WKWebView(frame: CGRect(x: CGFloat(videoLeadingConstraint), y: 0, width: slide4.bounds.width - CGFloat(videoPaddingSmallDevices), height: slide4.bounds.height), configuration: configuration)
                let myURL = URL(string: "https://fast.wistia.net/embed/iframe/wm5ur70pj2?seo=false&videoFoam=true4")
                let myRequest = URLRequest(url: myURL!)
                webview.load(myRequest)
                slide4.addSubview(webview)
                
                labelTitle.text = Constants.titlesButtons.stayUpToDate

                labelThingsYouCanDo.text = Constants.titlesButtons.detailsPushNotifications
                
                buttonNotNow.setTitle(Constants.titlesButtons.notNow, for: .normal)
                
                buttonNext.setTitle(Constants.titlesButtons.allowNotifications, for: .normal)
                
                numberOfScrolling = 1
                
                return [slide3, slide4]
            }
            
        } else {
            labelTitle.text = Constants.titlesLabels.labelEnableTouchID
            
            labelThingsYouCanDo.text = Constants.titlesLabels.labelSubtitleScrollViewTouchID
            
            buttonNext.setTitle(Constants.titlesButtons.allowTouchID, for: .normal)
            
            buttonNotNow.setTitle(Constants.titlesButtons.notNow, for: .normal)

            let slide3:SlideView = Bundle.main.loadNibNamed("SlideView", owner: self, options: nil)?.first as! SlideView
            slide3.imagePets.image = UIImage(named: "test3")
            
            let slide4:SlideView = Bundle.main.loadNibNamed("SlideView", owner: self, options: nil)?.first as! SlideView
            let configuration = WKWebViewConfiguration()
            configuration.allowsInlineMediaPlayback = true
            webview = WKWebView(frame: CGRect(x: CGFloat(videoLeadingConstraint), y: 0, width: slide4.bounds.width - CGFloat(videoPaddingSmallDevices), height: slide4.bounds.height), configuration: configuration)
            let myURL = URL(string: "https://fast.wistia.net/embed/iframe/wm5ur70pj2?seo=false&videoFoam=true4")
            let myRequest = URLRequest(url: myURL!)
            webview.load(myRequest)
            slide4.addSubview(webview)

            return [slide3, slide4]
        }
    }
    
    private func createSlidesIPad() -> [SlideViewiPad] {
        
        var videoLeadingConstraint = 0
        var videoPaddingSmallDevices = 0
        
        if UIDevice().userInterfaceIdiom == .pad {
            switch UIScreen.main.nativeBounds.height {
            case 1024:
                print("iPad 2, iPad 1st gen, iPad mini")
                videoLeadingConstraint = 8
                videoPaddingSmallDevices = 0
            case 2266:
                print("iPad mini")
                videoLeadingConstraint = -64
                videoPaddingSmallDevices = 96
            case 2048:
                print("iPad 3rd gen, iPad 4th gen, iPad Air,iPad Air 2, iPad mini 3, iPad mini 2, iPad mini 4,iPad Pro (1st gen 9.7”), iPad 5th gen, iPad 6th gen, iPad Mini (5th gen)")
                videoLeadingConstraint = 8
                videoPaddingSmallDevices = 64
            case 2160:
                print("iPad 7th gen")
                videoLeadingConstraint = 8
                videoPaddingSmallDevices = 16
            case 2224:
                print("iPad Pro (2nd gen 10.5), iPad Air (3rd gen)")
            case 2360:
                print("iPad Air (4th generation)")
                videoLeadingConstraint = 8
                videoPaddingSmallDevices = 16

            case 2388:
                print("iPad Pro (3rd gen 11),iPad Pro (4th gen 11)")
                videoLeadingConstraint = 8
            case 2732:
                print("iPad Pro (1st gen 12.9),iPad Pro (2nd gen 12.9),iPad Pro (3rd gen 12.9),iPad Pro (4th gen 12.9)")
                videoLeadingConstraint = 72
                videoPaddingSmallDevices = 0
            default:
                print("Unknown")
            }
        }
        if biometricLogin.biometricType() == .faceID {
            
            if hasBiometricsActivates == true {
                labelTitle.text = Constants.titlesLabels.labelEnableFaceID
                
                labelThingsYouCanDo.text = Constants.titlesLabels.labelSubtitleScrollViewFaceID

                let slide2:SlideViewiPad = Bundle.main.loadNibNamed("SlideViewiPad", owner: self, options: nil)?.first as! SlideViewiPad
                slide2.imagePets.image = UIImage(named: "test2")

                let slide3:SlideViewiPad = Bundle.main.loadNibNamed("SlideViewiPad", owner: self, options: nil)?.first as! SlideViewiPad
                slide3.imagePets.image = UIImage(named: "test3")
                
                let slide4:SlideViewiPad = Bundle.main.loadNibNamed("SlideViewiPad", owner: self, options: nil)?.first as! SlideViewiPad
                
                let configuration = WKWebViewConfiguration()
                configuration.allowsInlineMediaPlayback = true
                webview = WKWebView(frame: CGRect(x: CGFloat(videoLeadingConstraint), y: 0, width: slide4.bounds.width - CGFloat(videoPaddingSmallDevices), height: slide4.bounds.height), configuration: configuration)
                let myURL = URL(string: "https://fast.wistia.net/embed/iframe/wm5ur70pj2?seo=false&videoFoam=true4")
                let myRequest = URLRequest(url: myURL!)
                webview.load(myRequest)
                slide4.addSubview(webview)

                return [slide2, slide3, slide4]
            } else {
                labelTitle.text = Constants.titlesLabels.labelEnableFaceID
                
                labelThingsYouCanDo.text = Constants.titlesLabels.labelSubtitleScrollViewFaceID
                
                let slide3:SlideViewiPad = Bundle.main.loadNibNamed("SlideViewiPad", owner: self, options: nil)?.first as! SlideViewiPad
                slide3.imagePets.image = UIImage(named: "test3")
                
                let slide4:SlideViewiPad = Bundle.main.loadNibNamed("SlideViewiPad", owner: self, options: nil)?.first as! SlideViewiPad
                
                let configuration = WKWebViewConfiguration()
                configuration.allowsInlineMediaPlayback = true
                webview = WKWebView(frame: CGRect(x: CGFloat(videoLeadingConstraint), y: 0, width: slide4.bounds.width - CGFloat(videoPaddingSmallDevices), height: slide4.bounds.height), configuration: configuration)
                let myURL = URL(string: "https://fast.wistia.net/embed/iframe/wm5ur70pj2?seo=false&videoFoam=true4")
                let myRequest = URLRequest(url: myURL!)
                webview.load(myRequest)
                slide4.addSubview(webview)
                
                numberOfScrolling = 1
                
                return [slide3, slide4]
            }
        }
        if biometricLogin.biometricType() == .touchID {
            
            if hasBiometricsActivates == true {
                let slide1:SlideViewiPad = Bundle.main.loadNibNamed("SlideViewiPad", owner: self, options: nil)?.first as! SlideViewiPad
                slide1.imagePets.image = UIImage(named: "test1")

                let slide3:SlideViewiPad = Bundle.main.loadNibNamed("SlideViewiPad", owner: self, options: nil)?.first as! SlideViewiPad
                slide3.imagePets.image = UIImage(named: "test3")
                
                let slide4:SlideViewiPad = Bundle.main.loadNibNamed("SlideViewiPad", owner: self, options: nil)?.first as! SlideViewiPad
                let configuration = WKWebViewConfiguration()
                configuration.allowsInlineMediaPlayback = true
                self.webview = WKWebView(frame: CGRect(x: CGFloat(videoLeadingConstraint), y: 0, width: slide4.bounds.width - CGFloat(videoPaddingSmallDevices), height: slide4.bounds.height), configuration: configuration)
                let myURL = URL(string: "https://fast.wistia.net/embed/iframe/wm5ur70pj2?seo=false&videoFoam=true4")
                let myRequest = URLRequest(url: myURL!)
                self.webview.load(myRequest)
                slide4.addSubview(self.webview)
                
                self.labelTitle.text = Constants.titlesLabels.labelEnableTouchID
                
                self.labelThingsYouCanDo.text = Constants.titlesLabels.labelSubtitleScrollViewTouchID
                
                self.buttonNext.setTitle(Constants.titlesButtons.allowTouchID, for: .normal)
                
                self.buttonNotNow.setTitle(Constants.titlesButtons.notNow, for: .normal)
                
                return [slide1, slide3, slide4]
            } else {
                let slide3:SlideViewiPad = Bundle.main.loadNibNamed("SlideViewiPad", owner: self, options: nil)?.first as! SlideViewiPad
                slide3.imagePets.image = UIImage(named: "test3")
                
                let slide4:SlideViewiPad = Bundle.main.loadNibNamed("SlideViewiPad", owner: self, options: nil)?.first as! SlideViewiPad
                
                let configuration = WKWebViewConfiguration()
                configuration.allowsInlineMediaPlayback = true
                webview = WKWebView(frame: CGRect(x: CGFloat(videoLeadingConstraint), y: 0, width: slide4.bounds.width - CGFloat(videoPaddingSmallDevices), height: slide4.bounds.height), configuration: configuration)
                let myURL = URL(string: "https://fast.wistia.net/embed/iframe/wm5ur70pj2?seo=false&videoFoam=true4")
                let myRequest = URLRequest(url: myURL!)
                webview.load(myRequest)
                slide4.addSubview(webview)
                
                labelTitle.text = Constants.titlesButtons.stayUpToDate

                labelThingsYouCanDo.text = Constants.titlesButtons.detailsPushNotifications
                
                buttonNotNow.setTitle(Constants.titlesButtons.notNow, for: .normal)
                
                buttonNext.setTitle(Constants.titlesButtons.allowNotifications, for: .normal)
                
                numberOfScrolling = 1
                
                return [slide3, slide4]
            }
        } else {
            let slide4:SlideViewiPad = Bundle.main.loadNibNamed("SlideViewiPad", owner: self, options: nil)?.first as! SlideViewiPad

            slide4.imagePets.isHidden = true
            
            let configuration = WKWebViewConfiguration()
            configuration.mediaTypesRequiringUserActionForPlayback = .all
            webview = WKWebView(frame: CGRect(x: 8, y: 0, width: slide4.bounds.width, height: slide4.bounds.height), configuration: configuration)
            let myURL = URL(string: "https://fast.wistia.net/embed/iframe/wm5ur70pj2?seo=false&videoFoam=true4")
            let myRequest = URLRequest(url: myURL!)
            webview.load(myRequest)
            slide4.addSubview(webview)
            
            return [slide4]
        }
    }
    
    
    private func setupSlideScrollView(slides : [SlideView]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            if i == 0 {
                if UIScreen.main.nativeBounds.height == 2778 {
                    slides[i].frame = CGRect(x: 428.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }
                if UIScreen.main.nativeBounds.height == 2340 || UIScreen.main.nativeBounds.height == 2208 {
                    slides[i].frame = CGRect(x: 378.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                } else {
                    slides[i].frame = CGRect(x: 414.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }
            } else {

                if UIScreen.main.nativeBounds.height == 2778 {
                    slides[i].frame = CGRect(x: 428.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }
                if UIScreen.main.nativeBounds.height == 2340 || UIScreen.main.nativeBounds.height == 2208 {
                    slides[i].frame = CGRect(x: 378.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                } else {
                    slides[i].frame = CGRect(x: 414.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }
            }
            scrollView.addSubview(slides[i])
        }
    }
    
    private func setupSlideScrollViewiPad(slides : [SlideViewiPad]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrollView.isPagingEnabled = true
        
        print(UIScreen.main.nativeBounds.width)
        for i in 0 ..< slidesIPad.count {
            if UIScreen.main.nativeBounds.height == 2048 {
                if i == 0 {
                    slidesIPad[i].frame = CGRect(x: 810.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                } else {
                    slidesIPad[i].frame = CGRect(x: 810.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }
            }
            if UIScreen.main.nativeBounds.height == 2160 {
                if i == 0 {
                    slidesIPad[i].frame = CGRect(x: 810.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                } else {
                    slidesIPad[i].frame = CGRect(x: 810.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }
            }
            if UIScreen.main.nativeBounds.height == 2224 || UIScreen.main.nativeBounds.height == 2266 {
                if i == 0 {
                    slidesIPad[i].frame = CGRect(x: 810.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                } else {
                    slidesIPad[i].frame = CGRect(x: 810.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }
            }
            if UIScreen.main.nativeBounds.height == 2360{
                if i == 0 {
                    slidesIPad[i].frame = CGRect(x: 820.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                } else {
                    slidesIPad[i].frame = CGRect(x: 820.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }
            }
            if UIScreen.main.nativeBounds.height == 2388{
                if i == 0 {
                    slidesIPad[i].frame = CGRect(x: 835.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                } else {
                    slidesIPad[i].frame = CGRect(x: 835.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }
            }
            if UIScreen.main.nativeBounds.height == 2732{
                if i == 0 {
                    slidesIPad[i].imageViewHeight.constant = 700
                    slidesIPad[i].frame = CGRect(x: 1025.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                } else {
                    slidesIPad[i].imageViewHeight.constant = 700
                    slidesIPad[i].frame = CGRect(x: 1025.0 * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }
            }
            scrollView.addSubview(slidesIPad[i])
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.scrollView.isScrollEnabled = false
    }
    
    @IBAction func actionNotNow(_ sender: UIButton) {
        self.scrollToNextScreenWithoutPushNotifications()
    }
    
    @IBAction func actionMoveForward(_ sender: UIButton) {
        if numberOfScrolling == 0 {
            getBiometricLogin()
        }
        if numberOfScrolling == 1 {
            getPushNotificationsActivation()
        }
        if numberOfScrolling == 2 {
            scrollToNextScreen()
            webview.load(URLRequest(url: URL(string: "about:blank")!))
        }
    }
}
