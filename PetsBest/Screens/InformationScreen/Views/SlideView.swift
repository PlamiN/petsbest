//
//  SlideView.swift
//  PetsBest
//
//  Created by Plamena on 20.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class SlideView: UIView {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imagePets: UIImageView!
    @IBOutlet weak var labelDetailsFirst: UILabel!
    @IBOutlet weak var labelDetailsSecond: UILabel!
}
