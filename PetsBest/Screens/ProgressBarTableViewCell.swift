//
//  ProgressBarTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 21.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol ProgressBarTableViewCellDelegate {
    func seeCoreBenefits(rowNumber: Int)
}

class ProgressBarTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var viewAllElements: UIView!
    @IBOutlet private weak var labelMainTitle: UILabel!
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var labelDetails: UILabel!
    @IBOutlet private weak var labelTitle2: UILabel!
    @IBOutlet private weak var labelDetails2: UILabel!
    @IBOutlet private weak var labelTitle3: UILabel!
    @IBOutlet private weak var labelDetails3: UILabel!
    @IBOutlet private weak var labelTitle3Width: NSLayoutConstraint!
    @IBOutlet private weak var labelTitleHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelTitle2Width: NSLayoutConstraint!
    @IBOutlet private weak var buttonSeeCoreBenefits: UIButton!
    
    var delegate: ProgressBarTableViewCellDelegate?
    
    private var rowNumberValue = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        
        if self.traitCollection.userInterfaceStyle == .dark{
            labelDetails.textColor = UIColor.white
            labelMainTitle.textColor = UIColor.white
            labelTitle.textColor = UIColor.white
            labelTitle2.textColor = UIColor.white
            labelTitle3.textColor = UIColor.white
            labelDetails.textColor = UIColor.white
            labelDetails2.textColor = UIColor.white
            labelDetails3.textColor = UIColor.white
        } else{
        }
        viewAllElements.layer.cornerRadius = 10.0
        labelMainTitle.clipsToBounds = true
        labelMainTitle.layer.cornerRadius = 10
        labelMainTitle.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        labelTitle.text = Constants.titlesLabels.labelDeductible
        labelTitle2.text = Constants.titlesLabels.labelAnnualLimit
        labelTitle3.text = Constants.titlesLabels.labelReimbursementLevel
        
        labelMainTitle.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        labelTitle.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        labelTitle2.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        labelTitle3.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        buttonSeeCoreBenefits.setTitleColor( UIColor.init(hex: Constants.Colors.PBHelpline), for: .normal)
        buttonSeeCoreBenefits.backgroundColor = UIColor.init(hex: Constants.Colors.gray)
        buttonSeeCoreBenefits.clipsToBounds = true
        buttonSeeCoreBenefits.layer.cornerRadius = 20
        
        contentView.layer.borderWidth = 1
        contentView.layer.cornerRadius = 5
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true
        contentView.layer.shadowOpacity = 0.18
        contentView.layer.shadowOffset = CGSize(width: 0, height: 2)
        contentView.layer.shadowRadius = 2
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.masksToBounds = false
        
        selectionStyle = .none
        
        if UIDevice().userInterfaceIdiom == .pad {
            labelMainTitle.font = UIFont(name: "Proxima Nova Bold", size: 33)
            labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelTitle2.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelTitle3.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelDetails.font = UIFont(name: "Proxima Nova", size: 27)
            labelDetails2.font = UIFont(name: "Proxima Nova", size: 27)
            labelDetails3.font = UIFont(name: "Proxima Nova", size: 27)
            buttonSeeCoreBenefits.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 27)
            labelTitle3Width.constant = 300
            labelTitle2Width.constant = 300
            labelTitleHeight.constant = 80
        } else {
            labelMainTitle.font = UIFont(name: "Proxima Nova Bold", size: 21)
            labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 16)
            labelTitle2.font = UIFont(name: "Proxima Nova Bold", size: 16)
            labelTitle3.font = UIFont(name: "Proxima Nova Bold", size: 16)
            labelDetails.font = UIFont(name: "Proxima Nova", size: 18)
            labelDetails2.font = UIFont(name: "Proxima Nova", size: 18)
            labelDetails3.font = UIFont(name: "Proxima Nova", size: 18)
            buttonSeeCoreBenefits.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 18)
        }
        
        buttonSeeCoreBenefits.isHidden = true
    }

    func configureCell(withTitle: [String], withDetails: [String], withPercentage: [CGFloat], withPlan: String, withRowNumber: Int) {
        labelTitle.text = withTitle[0]
        labelDetails.text = withDetails[0]
        
        labelTitle2.text = withTitle[1]
        labelDetails2.text = withDetails[1]
        
        labelTitle3.text = withTitle[2]
        labelDetails3.text = withDetails[2]
        
        labelMainTitle.backgroundColor = UIColor.systemGray6
        labelMainTitle.textColor = UIColor.init(hex: Constants.Colors.labels)!
        
        labelMainTitle.text = withPlan + " Plan"
        rowNumberValue = withRowNumber
    }
    
    @IBAction func buttonOpenCoreBenefits(_ sender: UIButton) {
        delegate?.seeCoreBenefits(rowNumber: rowNumberValue)
    }
}
