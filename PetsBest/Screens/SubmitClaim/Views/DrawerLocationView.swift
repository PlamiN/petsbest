//
//  DrawerLocationView.swift
//  PetsBest
//
//  Created by Plamena on 4.02.22.
//  Copyright © 2022 Plamena. All rights reserved.
//

import UIKit

class DrawerLocationView: UIView {
    @IBOutlet weak var imageViewLocation: UIImageView!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonNotNow: UIButton!
    @IBOutlet weak var labelEnhanceExperience: UILabel!
}
