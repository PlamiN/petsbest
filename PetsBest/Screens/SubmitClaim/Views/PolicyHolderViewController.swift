//
//  PolicyHolderViewController.swift
//  PetsBest
//
//  Created by Plamena on 18.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class PolicyHolderViewController: UIViewController {

    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var textViewPolicyHolder: UITextView!
    @IBOutlet private weak var imageViewClose: UIImageView!
    @IBOutlet private weak var viewPolicyHolder: UIView!
    @IBOutlet private weak var viewPolicyHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelTitleHeight: NSLayoutConstraint!
    @IBOutlet private weak var imageCloseHeight: NSLayoutConstraint!
    @IBOutlet private weak var imageCloseWidth: NSLayoutConstraint!
    
    private let worker = PolicyHolderWorker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        networkingPolicyHolder()
    }
    
    private func setViews() {
        
        viewPolicyHolder.layer.borderWidth = 1
        viewPolicyHolder.layer.cornerRadius = 10
        viewPolicyHolder.layer.borderColor = UIColor.clear.cgColor
        viewPolicyHolder.layer.masksToBounds = true
        viewPolicyHolder.layer.shadowOpacity = 0.5
        viewPolicyHolder.layer.shadowOffset = CGSize(width: 0, height: 2)
        viewPolicyHolder.layer.shadowRadius = 10
        viewPolicyHolder.layer.shadowColor = UIColor.black.cgColor
        viewPolicyHolder.layer.masksToBounds = false
        
        labelTitle.text = Constants.titlesLabels.labelTitlePolicyHolderDeclaration
        let tapClose = UITapGestureRecognizer(target: self, action:  #selector(close))
        imageViewClose.isUserInteractionEnabled = true
        imageViewClose.addGestureRecognizer(tapClose)
        
        DispatchQueue.main.async {
            Spinner.start()
        }
        
        if UIDevice().userInterfaceIdiom == .pad {
            textViewPolicyHolder.font = UIFont(name: "Proxima Nova", size: 30)
            labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 40)
            labelTitleHeight.constant = 55
            imageCloseHeight.constant = 50
            imageCloseWidth.constant = 50
            viewPolicyHeight.constant = 500
        }
    }
    
    private func networkingPolicyHolder() {
        if ReachabilityClass.isConnectedToNetwork(){
            worker.getPolicyDetails(completionHandler: { [weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    DispatchQueue.main.async {
                        self?.textViewPolicyHolder.text = "I, \(resultUser.FirstName) \(resultUser.LastName), confirm to the best of my knowledge that all statements provided on this form are true. I hereby give Pets Best authorization to request any and all medical records or financial information for the claimed pet and authorization to discuss the details of this claim with the treating veterinarian or their authorized representative."
                        Spinner.stop()
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        Spinner.stop()
                    }
                }
            }) { error in
                DispatchQueue.main.async {
                    self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                Spinner.stop()
            }
        }
    }
    
    @objc private func close() {
        self.dismiss(animated: true, completion: nil)
    }
}
