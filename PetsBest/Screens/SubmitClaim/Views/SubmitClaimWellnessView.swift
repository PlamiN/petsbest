//
//  SubmitClaimWellnessView.swift
//  PetsBest
//
//  Created by Plamena on 12.02.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit

class SubmitClaimWellnessView: UIView {
    
    @IBOutlet weak var viewClaimTitle: UIView!
    @IBOutlet weak var viewClaim: UIView!
    @IBOutlet weak var labelClaim: UILabel!
    @IBOutlet weak var labelPet: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelServiceDate: UILabel!
    @IBOutlet weak var labelServiceDateDetails: UILabel!
    @IBOutlet weak var labelClaimType: UILabel!
    @IBOutlet weak var labelClaimTypeDetails: UILabel!
    @IBOutlet weak var labelVetClinic: UILabel!
    @IBOutlet weak var labelVetClinicDetails: UILabel!
    @IBOutlet weak var labelAdditionalInfo: UILabel!
    @IBOutlet weak var labelAdditionalInfoDetails: UILabel!
    @IBOutlet weak var imageViewCheckboxAgree: UIImageView!
    @IBOutlet weak var labelAgree: UILabel!
    @IBOutlet weak var imageViewCheckboxFraud: UIImageView!
    @IBOutlet weak var labelFraudWarning: UILabel!
    @IBOutlet weak var labelFraudWarningSecond: UILabel!
    @IBOutlet weak var buttonSubmitClaim: UIButton!
    @IBOutlet weak var labelPolicyHolderDeclarationLink: UnderlinedLabel!
    @IBOutlet weak var labelFraudWarningLink: UnderlinedLabel!
    @IBOutlet weak var labelClaimHeight: NSLayoutConstraint!
    @IBOutlet weak var labelPetHeight: NSLayoutConstraint!
    @IBOutlet weak var labelClaimTypeHeight: NSLayoutConstraint!
    @IBOutlet weak var labelServiceDateHeight: NSLayoutConstraint!
    @IBOutlet weak var labelVetClinicHeight: NSLayoutConstraint!
    @IBOutlet weak var labelAdditionalInfoHeight: NSLayoutConstraint!
    @IBOutlet weak var imageCheckboxPolicyHeight: NSLayoutConstraint!
    @IBOutlet weak var imageCheckboxPolicyWidth: NSLayoutConstraint!
    @IBOutlet weak var imageCheckboxFraudHeight: NSLayoutConstraint!
    @IBOutlet weak var imageCheckboxFraudWidth: NSLayoutConstraint!
    @IBOutlet weak var buttonSubmitClaimHeight: NSLayoutConstraint!
    @IBOutlet weak var labelAgreeWidth: NSLayoutConstraint!
    @IBOutlet weak var labelFraudWidth: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraintCheckbox: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraintCheckbox: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraintClaim: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraintClaim: NSLayoutConstraint!
}
