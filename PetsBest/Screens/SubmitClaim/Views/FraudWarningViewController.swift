//
//  FraudWarningViewController.swift
//  PetsBest
//
//  Created by Plamena on 18.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class FraudWarningViewController: UIViewController {

    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var textViewFraudWarning: UITextView!
    @IBOutlet private weak var imageViewClose: UIImageView!
    @IBOutlet private weak var viewFraudWarning: UIView!
    @IBOutlet private weak var viewFraudHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelTitleHeight: NSLayoutConstraint!
    @IBOutlet private weak var imageCloseHeight: NSLayoutConstraint!
    @IBOutlet private weak var imageCloseWidth: NSLayoutConstraint!
    
    private let worker = FraudWarningWorker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setViews()
        networkingFraudWarning()
    }
    
    @objc private func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //Firebase Analytics
    override func viewDidAppear(_ animated: Bool) {
        Analytics.logEvent(AnalyticsEventScreenView,
                           parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.fraudWarning])
    }
    
    private func setViews() {
        
        viewFraudWarning.layer.borderWidth = 1
        viewFraudWarning.layer.cornerRadius = 10
        viewFraudWarning.layer.borderColor = UIColor.clear.cgColor
        viewFraudWarning.layer.masksToBounds = true
        viewFraudWarning.layer.shadowOpacity = 0.5
        viewFraudWarning.layer.shadowOffset = CGSize(width: 0, height: 2)
        viewFraudWarning.layer.shadowRadius = 10
        viewFraudWarning.layer.shadowColor = UIColor.black.cgColor
        viewFraudWarning.layer.masksToBounds = false
        
        labelTitle.text = Constants.titlesLabels.labelTitleFraudWarning
        let tapClose = UITapGestureRecognizer(target: self, action:  #selector(close))
        imageViewClose.isUserInteractionEnabled = true
        imageViewClose.addGestureRecognizer(tapClose)
        DispatchQueue.main.async {
            Spinner.start()
        }
        if UIDevice().userInterfaceIdiom == .pad {
            textViewFraudWarning.font = UIFont(name: "Proxima Nova", size: 30)
            labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 40)
            labelTitleHeight.constant = 55
            imageCloseHeight.constant = 50
            imageCloseWidth.constant = 50
            viewFraudHeight.constant = 500
        }
    }
    
    private func networkingFraudWarning() {
        if ReachabilityClass.isConnectedToNetwork(){
            worker.getFraudWarning(completionHandler: { [weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    DispatchQueue.main.async {
                        let newText = resultUser.replacingOccurrences(of: "\"", with: "", options: .regularExpression, range: nil)
                        self?.textViewFraudWarning.text = newText
                        Spinner.stop()
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        Spinner.stop()
                    }
                }
            }) { [weak self] error  in
                DispatchQueue.main.async {
                    self?.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                Spinner.stop()
            }
        }
    }
}
