//
//  SubmittedClaimView.swift
//  PetsBest
//
//  Created by Plamena on 31.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class SubmittedClaimView: UIView {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageSubmittedClaim: UIImageView!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var labelDone: UnderlinedLabel!
    @IBOutlet weak var imageFirst: UIImageView!
    @IBOutlet weak var imageSecond: UIImageView!
    @IBOutlet weak var imageThird: UIImageView!
    @IBOutlet weak var buttonSubmitClaim: UIButton!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelReceived: UILabel!
    @IBOutlet weak var labelProcessing: UILabel!
    @IBOutlet weak var labelClaimed: UILabel!
    @IBOutlet weak var imageClaimWidth: NSLayoutConstraint!
    @IBOutlet weak var imageClaimHeight: NSLayoutConstraint!
    @IBOutlet weak var labelStatusDetailsHeight: NSLayoutConstraint!
    @IBOutlet weak var imageReceivedHeight: NSLayoutConstraint!
    @IBOutlet weak var imageReceivedWidth: NSLayoutConstraint!
    @IBOutlet weak var imageProcessingHeight: NSLayoutConstraint!
    @IBOutlet weak var imageProcessingWidth: NSLayoutConstraint!
    @IBOutlet weak var imageCompleteHeight: NSLayoutConstraint!
    @IBOutlet weak var imageCompleteWidth: NSLayoutConstraint!
    @IBOutlet weak var buttonSubmitHeight: NSLayoutConstraint!
    @IBOutlet weak var labelDoneHeght: NSLayoutConstraint!
    @IBOutlet weak var stackViewMain: UIStackView!
    @IBOutlet weak var stackViewButtons: UIStackView!
    @IBOutlet weak var firstStackWidth: NSLayoutConstraint!
    @IBOutlet weak var secondStackWidth: NSLayoutConstraint!
    @IBOutlet weak var ImageToTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var titleToStatusHeight: NSLayoutConstraint!
    @IBOutlet weak var statusToStatusImagesHeight: NSLayoutConstraint!
    @IBOutlet weak var statusToButtonsHeight: NSLayoutConstraint!
}

