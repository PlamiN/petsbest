//
//  SubmitClaimViewController.swift
//  PetsBest
//
//  Created by Plamena on 22.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import WeScan
import MobileCoreServices
import CoreLocation
import CoreData
import Reachability
import NVActivityIndicatorView
import PDFKit
import FirebaseAnalytics
import KeychainSwift
import StoreKit
import FirebaseFirestore
import FirebaseAuth


public class SynchronizedArray<T> {
    private var array: [T] = []
    private let accessQueue = DispatchQueue(label: "SynchronizedArrayAccess", attributes: .concurrent)
    
    public func append(newElement: T) {
        
        self.accessQueue.async(flags:.barrier) {
            self.array.append(newElement)
        }
    }
    
    public func removeAtIndex(index: Int) {
        
        self.accessQueue.async(flags:.barrier) {
            self.array.remove(at: index)
        }
    }
    
    public var count: Int {
        var count = 0
        
        self.accessQueue.sync {
            count = self.array.count
        }
        
        return count
    }
    
    public func first() -> T? {
        var element: T?
        
        self.accessQueue.sync {
            if !self.array.isEmpty {
                element = self.array[0]
            }
        }
        
        return element
    }
    
    public subscript(index: Int) -> T {
        set {
            self.accessQueue.async(flags:.barrier) {
                self.array[index] = newValue
            }
        }
        get {
            var element: T!
            self.accessQueue.sync {
                element = self.array[index]
            }
            
            return element
        }
    }
}


class SubmitClaimViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var buttonNext: UIButton!
    @IBOutlet private weak var activityIndicatorScreen: NVActivityIndicatorView!
    @IBOutlet private weak var buttonNextHeight: NSLayoutConstraint!
    
    //MARK: - Variables
    private var isService = false
    private var isPickDate = false
    private var isUploadFile = false
    private var isWellness = false
    private var isReasonWritten = false
    private var isCondition = false
    private var isVetClinic = false
    private var isDiagnosis = false
    private var isUploadedDocument = false
    private var twoUploadedDocuments = false
    private var canceledPolicies = false
    private var isPrimaryReasonSelected = false
    private var isConditionCodeText = false
    
    private var isPolicyChecked = false
    private var isFraudChecked = false
    
    private var isSearchedSelected = false
    private var isVetSavedSelected = false
    private var isOneLabelTitle = false
    
    private var isEmptySearch = false
    
    private var selectedPet: PetCoversList?
    
    private var dateFromDatePicker: String?
    
    private var imageDocument: UIImage?
    private var imageSecondDocument: UIImage?
    private var imageThirdDocument: UIImage?
    private var imageFourthDocument: UIImage?
    
    private var picker = UIImagePickerController()
    
    private var dateFromPicker: Date?
    
    private let typeServicesArray = [Constants.ClaimTypes.accident, Constants.ClaimTypes.ilness, Constants.ClaimTypes.wellness]
    private var selectedCondition = [String]()
    private var selectedOptionsForEndpoint = [String:Any]()
    
    private var selectedOptionsVet = [String:Any]()
    
    private var primaryReasonsArray = [String]()
    private var selectedReason = ""
    private var selectedConditionShow = ""
    private var conditionsArray = [String:[String]]()
    private var conditionsCodesArray = [Int:[Int]]()
    private var primaryReasonsCodeArray = [Int]()
    
    private let worker = PolicyWorker()
    private let workerEligibility = CheckEligibilityWorker()
    private let workerSubmitClaim = SubmitClaimWorker()
    private let testSubmitClaim = TestWorker()
    private let workerGetConditions = ConditionReasonWorker()
    private let workerGetSavedVets = SavetVetsWorker()
    private var allPets = [PetCoversList]()
    private var canceledPets = [PetCoversList]()
    
    private var selectedOptions = [0,0,0,0,0,0,0,0]
    private var selectedIndexes = [Int]()
    private var selectedIndexesReasons = [Int]()
    private var selectedIndexesConditions = [Int]()
    
    private var allVetsSearch = [String]()
    private var allVetsSearchCities = [String]()
    private var allVetsSearchIds = [String]()
    private var heightVetSearch: Double = 0.0
    
    private var allVetsSaved = [String]()
    private var allVetsSavedCities = [String]()
    private var allVetsSavedIds = [String]()
    private var isEmptySearchVet = false
    private var heightVetSaved: Double = 0.0
    
    private var submitClaimView = SubmitClaimView()
    private var submitClaimViewWellness = SubmitClaimWellnessView()
    private var submittedClaimView = SubmittedClaimView()
    
    private var usersLatitude: Double?
    private var usersLongitude: Double?
    
    private var isPDF = [false, false, false, false]
    
    private var fileFirstString:String = ""
    private var fileSecondString:String = ""
    private var fileThirdString:String = ""
    private var fileFourthString:String = ""
    
    private var policyNumberWhole:String = ""
    
    private var loadingScreenView = LoadingScreen()
    
    private var petCoreDataService: PetsCoreDataService?
    
    private let defaults = UserDefaults.standard
    
    private let keychain = KeychainSwift()
    
    private var reachability: Reachability!
    
    private let db = Firestore.firestore()
    
    private var allPetsCoreDate = [Pet]()
    
    private let loginWorker = LoginWorker()
   
    init(withCoreDataStack: CoreDataStack) {
        super.init(nibName: nil, bundle: nil)
        moc = withCoreDataStack.objectContext
        if let moc = moc {
            petCoreDataService = PetsCoreDataService(moc: moc)
        }
    }
    
    var moc: NSManagedObjectContext? {
        didSet {
            if let moc = moc {
                petCoreDataService = PetsCoreDataService(moc: moc)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setViews()
        
        configTableViewProperties()
//        networkingPolicyDetails()
    }
    
    
    //Firebase Analytics
    override func viewDidAppear(_ animated: Bool) {
        
        Analytics.logEvent(AnalyticsEventScreenView,
                           parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.startClaim])
        Analytics.logEvent(Constants.FirebaseEvents.startClaim, parameters: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        navigationItem.leftBarButtonItem = nil
        
        allPetsCoreDate = (petCoreDataService?.getAppPets()) ?? [Pet]()
        if allPetsCoreDate.count != allPets.count {
            self.tableView.reloadData()
            networkingPolicyDetails()
        }
        
        if !allPets.isEmpty {
            if allPetsCoreDate[0].policyNumber != "\(allPets[0].PolicyNumber)" {
                networkingPolicyDetails()
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        DispatchQueue.main.async {
            self.startLoadingScreen()
        }
        
        reachability = try! Reachability()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChangedSubmitClaim(_:)), name: Notification.Name.reachabilityChangedSubmitClaim, object: reachability)

        do {
            try reachability?.startNotifier()
        } catch {
            print("This is not working.")
            self.stopLoadingScreen()
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                self.stopLoadingWholeScreen()
                self.loadingScreenView.isHidden = true
            }
            return
        }
    }
    
    @objc func keyboardWillAppear() {
        //Do something here
    }

    @objc func keyboardWillDisappear() {
        if selectedOptionsVet["VetTypehead"] != nil {
            self.buttonNext.isEnabled = true
            self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)

//            NotificationCenter.default.removeObserver(self)
    }

    @objc func reachabilityChangedSubmitClaim(_ note: NSNotification) {

        let reachability = note.object as! Reachability

        if reachability.connection != .unavailable {
            if reachability.connection == .wifi {
                selectedCondition = [String]()
                networkingConditionReason()
                
                print("Reachable via WiFi")
            } else {
                selectedCondition = [String]()
                networkingConditionReason()
                print("Reachable via Cellular")
            }
        } else {
            DispatchQueue.main.async {
                self.stopLoadingScreen()
                self.showAlert(withTitle: "", message: Constants.errorMessages.noInternetError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                self.stopLoadingWholeScreen()
                self.loadingScreenView.isHidden = true
            }
            print("Not reachable")
        }
    }
    
    private func setViews() {
        
        self.tabBarController?.delegate = self
        
        self.navigationItem.title = Constants.titlesScreens.newClaim
        buttonNext.setTitle(Constants.titlesButtons.next, for: .normal)
        buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        buttonNext.setTitleColor(UIColor.white, for: .normal)
        buttonNext.layer.cornerRadius = 25
        buttonNext.isHidden = true
        
        //View before submitting claim
        submitClaimViewWellness = (Bundle.main.loadNibNamed("SubmitClaimWellnessView", owner: self, options: nil)?.first as? SubmitClaimWellnessView)!
        submitClaimViewWellness.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(submitClaimViewWellness)
        submitClaimViewWellness.isHidden = true
        
        submitClaimViewWellness.buttonSubmitClaim.setTitle("Submit Claim", for: .normal)
        submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.gray
        submitClaimViewWellness.buttonSubmitClaim.isEnabled = false
        submitClaimViewWellness.buttonSubmitClaim.setTitleColor(UIColor.white, for: .normal)
        submitClaimViewWellness.buttonSubmitClaim.layer.cornerRadius = 25
        submitClaimViewWellness.imageViewCheckboxAgree.image = UIImage(named: "empty")
        submitClaimViewWellness.imageViewCheckboxFraud.image = UIImage(named: "empty")

        submitClaimViewWellness.labelAgree.text = Constants.titlesLabels.labelAgree
        submitClaimViewWellness.labelPolicyHolderDeclarationLink.text = Constants.titlesLabels.labelAgreePolicyDeclaration
        submitClaimViewWellness.labelPolicyHolderDeclarationLink.textColor = UIColor.init(hex: Constants.Colors.main)
        submitClaimViewWellness.labelFraudWarning.text = Constants.titlesLabels.labelAgreeFraudWarning
        submitClaimViewWellness.labelFraudWarningSecond.text = Constants.titlesLabels.labelAgreeFraudWarningSecond
        submitClaimViewWellness.labelFraudWarningLink.text = Constants.titlesLabels.labelFraudWarning
        submitClaimViewWellness.labelFraudWarningLink.textColor = UIColor.init(hex: Constants.Colors.main)
        submitClaimViewWellness.viewClaimTitle.backgroundColor = UIColor.systemGray6
        submitClaimViewWellness.viewClaimTitle.clipsToBounds = true
        submitClaimViewWellness.viewClaimTitle.layer.cornerRadius = 10
        submitClaimViewWellness.viewClaimTitle.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        submitClaimViewWellness.labelClaim.textColor = UIColor.init(hex: Constants.Colors.labels)
        submitClaimViewWellness.labelPet.textColor = UIColor.init(hex: Constants.Colors.labels)
        submitClaimViewWellness.labelClaimType.textColor = UIColor.init(hex: Constants.Colors.labels)
        submitClaimViewWellness.labelServiceDate.textColor = UIColor.init(hex: Constants.Colors.labels)
        submitClaimViewWellness.labelVetClinic.textColor = UIColor.init(hex: Constants.Colors.labels)
        submitClaimViewWellness.labelAdditionalInfo.textColor = UIColor.init(hex: Constants.Colors.labels)
        
        if UIDevice().userInterfaceIdiom == .phone {
            submitClaimViewWellness.leadingConstraintCheckbox.constant = 40
            submitClaimViewWellness.trailingConstraintCheckbox.constant = 40
            submitClaimViewWellness.leadingConstraintClaim.constant = 32
            submitClaimViewWellness.trailingConstraintClaim.constant = 32
        }
        
        submitClaimViewWellness.viewClaim.layer.borderWidth = 1
        submitClaimViewWellness.viewClaim.layer.cornerRadius = 5
        submitClaimViewWellness.viewClaim.layer.borderColor = UIColor.clear.cgColor
        submitClaimViewWellness.viewClaim.layer.masksToBounds = true
        submitClaimViewWellness.viewClaim.layer.shadowOpacity = 0.18
        submitClaimViewWellness.viewClaim.layer.shadowOffset = CGSize(width: 0, height: 2)
        submitClaimViewWellness.viewClaim.layer.shadowRadius = 2
        submitClaimViewWellness.viewClaim.layer.shadowColor = UIColor.black.cgColor
        submitClaimViewWellness.viewClaim.layer.masksToBounds = false
        let tapCheckAgree1 = UITapGestureRecognizer(target: self, action:  #selector(checkAgree1))
        submitClaimViewWellness.imageViewCheckboxAgree.isUserInteractionEnabled = true
        submitClaimViewWellness.imageViewCheckboxAgree.addGestureRecognizer(tapCheckAgree1)
        
        let tapCheckFraud1 = UITapGestureRecognizer(target: self, action:  #selector(checkFraud1))
        submitClaimViewWellness.imageViewCheckboxFraud.isUserInteractionEnabled = true
        submitClaimViewWellness.imageViewCheckboxFraud.addGestureRecognizer(tapCheckFraud1)
        
        let tapSubmitClaim1 = UITapGestureRecognizer(target: self, action:  #selector(submitClaim))
        submitClaimViewWellness.buttonSubmitClaim.addGestureRecognizer(tapSubmitClaim1)
        
        let tapOpenFraudWarning1 = UITapGestureRecognizer(target: self, action:  #selector(openFraudWarning))
        submitClaimViewWellness.labelFraudWarningLink.isUserInteractionEnabled = true
        submitClaimViewWellness.labelFraudWarningLink.addGestureRecognizer(tapOpenFraudWarning1)
        
        let tapOpenPolicyDeclaration1 = UITapGestureRecognizer(target: self, action:  #selector(openPolicyDeclaration))
        submitClaimViewWellness.labelPolicyHolderDeclarationLink.isUserInteractionEnabled = true
        submitClaimViewWellness.labelPolicyHolderDeclarationLink.addGestureRecognizer(tapOpenPolicyDeclaration1)
        
        //View before submitting claim
        submitClaimView = (Bundle.main.loadNibNamed("SubmitClaimView", owner: self, options: nil)?.first as? SubmitClaimView)!
        submitClaimView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(submitClaimView)
        
        submitClaimView.buttonSubmitClaim.setTitle("Submit Claim", for: .normal)
        submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.gray
        submitClaimView.buttonSubmitClaim.isEnabled = false
        submitClaimView.buttonSubmitClaim.setTitleColor(UIColor.white, for: .normal)
        submitClaimView.buttonSubmitClaim.layer.cornerRadius = 25
        submitClaimView.imageViewCheckboxAgree.image = UIImage(named: "empty")
        submitClaimView.imageViewCheckboxFraud.image = UIImage(named: "empty")

        submitClaimView.labelAgree.text = Constants.titlesLabels.labelAgree
        submitClaimView.labelPolicyHolderDeclarationLink.text = Constants.titlesLabels.labelAgreePolicyDeclaration
        submitClaimView.labelPolicyHolderDeclarationLink.textColor = UIColor.init(hex: Constants.Colors.main)
        submitClaimView.labelFraudWarning.text = Constants.titlesLabels.labelAgreeFraudWarning
        submitClaimView.labelFraudWarningSecond.text = Constants.titlesLabels.labelAgreeFraudWarningSecond
        submitClaimView.labelFraudWarningLink.text = Constants.titlesLabels.labelFraudWarning
        submitClaimView.labelFraudWarningLink.textColor = UIColor.init(hex: Constants.Colors.main)
        submitClaimView.viewClaimTitle.backgroundColor = UIColor.systemGray6
        submitClaimView.viewClaimTitle.clipsToBounds = true
        submitClaimView.viewClaimTitle.layer.cornerRadius = 10
        submitClaimView.viewClaimTitle.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        submitClaimView.labelClaim.textColor = UIColor.init(hex: Constants.Colors.labels)
        submitClaimView.labelPet.textColor = UIColor.init(hex: Constants.Colors.labels)
        submitClaimView.labelClaimType.textColor = UIColor.init(hex: Constants.Colors.labels)
        submitClaimView.labelServiceDate.textColor = UIColor.init(hex: Constants.Colors.labels)
        submitClaimView.labelReasonForVisit.textColor = UIColor.init(hex: Constants.Colors.labels)
        submitClaimView.labelVetClinic.textColor = UIColor.init(hex: Constants.Colors.labels)
        submitClaimView.labelAdditionalInfo.textColor = UIColor.init(hex: Constants.Colors.labels)
        
        if UIDevice().userInterfaceIdiom == .phone {
            submitClaimView.leadingConstraintCheckbox.constant = 40
            submitClaimView.trailingConstraintCheckbox.constant = 40
            submitClaimView.leadingConstraintClaim.constant = 32
            submitClaimView.trailingConstraintClaim.constant = 32
        }
        
        submitClaimView.viewClaim.layer.borderWidth = 1
        submitClaimView.viewClaim.layer.cornerRadius = 5
        submitClaimView.viewClaim.layer.borderColor = UIColor.clear.cgColor
        submitClaimView.viewClaim.layer.masksToBounds = true
        submitClaimView.viewClaim.layer.shadowOpacity = 0.18
        submitClaimView.viewClaim.layer.shadowOffset = CGSize(width: 0, height: 2)
        submitClaimView.viewClaim.layer.shadowRadius = 2
        submitClaimView.viewClaim.layer.shadowColor = UIColor.black.cgColor
        submitClaimView.viewClaim.layer.masksToBounds = false
        let tapCheckAgree = UITapGestureRecognizer(target: self, action:  #selector(checkAgree))
        submitClaimView.imageViewCheckboxAgree.isUserInteractionEnabled = true
        submitClaimView.imageViewCheckboxAgree.addGestureRecognizer(tapCheckAgree)
        
        let tapCheckFraud = UITapGestureRecognizer(target: self, action:  #selector(checkFraud))
        submitClaimView.imageViewCheckboxFraud.isUserInteractionEnabled = true
        submitClaimView.imageViewCheckboxFraud.addGestureRecognizer(tapCheckFraud)
        
        let tapSubmitClaim = UITapGestureRecognizer(target: self, action:  #selector(submitClaim))
        submitClaimView.buttonSubmitClaim.addGestureRecognizer(tapSubmitClaim)
        
        let tapOpenFraudWarning = UITapGestureRecognizer(target: self, action:  #selector(openFraudWarning))
        submitClaimView.labelFraudWarningLink.isUserInteractionEnabled = true
        submitClaimView.labelFraudWarningLink.addGestureRecognizer(tapOpenFraudWarning)
        
        let tapOpenPolicyDeclaration = UITapGestureRecognizer(target: self, action:  #selector(openPolicyDeclaration))
        submitClaimView.labelPolicyHolderDeclarationLink.isUserInteractionEnabled = true
        submitClaimView.labelPolicyHolderDeclarationLink.addGestureRecognizer(tapOpenPolicyDeclaration)
        
        //view after submitting claim
        submittedClaimView = (Bundle.main.loadNibNamed("SubmittedClaimView", owner: self, options: nil)?.first as? SubmittedClaimView)!
        submittedClaimView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(submittedClaimView)
    
        submittedClaimView.buttonSubmitClaim.setTitle("Submit New Claim", for: .normal)
        submittedClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        submittedClaimView.buttonSubmitClaim.setTitleColor(UIColor.white, for: .normal)
        submittedClaimView.buttonSubmitClaim.layer.cornerRadius = 20
        submittedClaimView.isHidden = true
        submittedClaimView.labelDone.text = "DONE"
        submittedClaimView.labelDone.textColor = UIColor.init(hex: Constants.Colors.orangeButton)
        let tapDoneClaim = UITapGestureRecognizer(target: self, action:  #selector(doneClaim))
        submittedClaimView.labelDone.isUserInteractionEnabled = true
        submittedClaimView.labelDone.addGestureRecognizer(tapDoneClaim)
        
        submitClaimView.isHidden = true
        
        if (defaults.string(forKey: "submittedClaimPet") != nil) && ((defaults.string(forKey: "submittedClaimId")) != nil) {
            submittedClaimView.isHidden = false
            self.submittedClaimView.labelDetails.text = "\(String(describing: defaults.string(forKey: "submittedClaimPet")!))'s claim #\(String(describing: defaults.string(forKey: "submittedClaimId")!)) has been successfully submitted and is now being reviewed. You will be notified once your claim has been processed."
            self.navigationController?.navigationBar.isHidden = true
            let tapSubmitNewClaim = UITapGestureRecognizer(target: self, action:  #selector(self.submitNewClaim))
            self.submittedClaimView.buttonSubmitClaim.addGestureRecognizer(tapSubmitNewClaim)
        } else {
            submittedClaimView.isHidden = true
        }
        
        picker.delegate = self
        
        hideKeyboardWhenTappedAround()
        
        loadingScreenView = (Bundle.main.loadNibNamed("LoadingScreen", owner: self, options: nil)?.first as? LoadingScreen)!
        loadingScreenView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(loadingScreenView)
        loadingScreenView.labelLoadingText.textColor = UIColor.init(hex: Constants.Colors.labels)
        loadingScreenView.labelLoadingText.text = Constants.titlesLabels.loadingSubmitClaimText
        loadingScreenView.isHidden = true
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        
        labelsAndViewIPads()
        
        buttonNext.layer.borderColor = UIColor.clear.cgColor
        buttonNext.layer.masksToBounds = true
        buttonNext.layer.shadowOpacity = 0.18
        buttonNext.layer.shadowOffset = CGSize(width: 0, height: 2)
        buttonNext.layer.shadowRadius = 6
        buttonNext.layer.shadowColor = UIColor.black.cgColor
        buttonNext.layer.masksToBounds = false
        
        picker.allowsEditing = false
    }
    
    private func labelsAndViewIPads() {
        if UIDevice().userInterfaceIdiom == .pad {
            
            loadingScreenView.labelLoadingText.font = UIFont(name: "Proxima Nova Bold", size: 47)
            loadingScreenView.activityIndicatorHeight.constant = 240
            loadingScreenView.activityIndicatorWidth.constant = 240
            
            if UIScreen.main.nativeBounds.height == 2732 {
                buttonNextHeight.constant = 80
                buttonNext.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 40)
                buttonNext.layer.cornerRadius = 40
                submitClaimView.labelPet.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelAdditionalInfo.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelVetClinic.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelReasonForVisit.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelName.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimView.labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimView.labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimView.labelReasonForVisitDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimView.labelVetClinicDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimView.labelAdditionalInfoDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimView.buttonSubmitClaim.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 40)
                submitClaimView.labelAgree.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimView.labelFraudWarning.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimView.labelFraudWarningSecond.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimView.labelFraudWarningLink.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimView.labelPolicyHolderDeclarationLink.font = UIFont(name: "Proxima Nova", size: 20)
                
                submitClaimView.labelClaimHeight.constant = 70
                submitClaimView.labelPetHeight.constant = 50
                submitClaimView.labelClaimTypeHeight.constant = 50
                submitClaimView.labelServiceDateHeight.constant = 50
                submitClaimView.labelReasonHeight.constant = 70
                submitClaimView.labelVetClinicHeight.constant = 70
                submitClaimView.labelAdditionalInfoHeight.constant = 50
                submitClaimView.imageCheckboxPolicyHeight.constant = 60
                submitClaimView.imageCheckboxPolicyWidth.constant = 60
                submitClaimView.imageCheckboxFraudHeight.constant = 60
                submitClaimView.imageCheckboxFraudWidth.constant = 60
                submitClaimView.buttonSubmitClaimHeight.constant = 70
                submitClaimView.buttonSubmitClaim.layer.cornerRadius = 35
                submitClaimView.labelAgreeWidth.constant = 120
                submitClaimView.labelFraudWidth.constant = 130
                
                submitClaimViewWellness.labelPet.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimViewWellness.labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimViewWellness.labelAdditionalInfo.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimViewWellness.labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimViewWellness.labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimViewWellness.labelVetClinic.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimViewWellness.labelName.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimViewWellness.labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimViewWellness.labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimViewWellness.labelVetClinicDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimViewWellness.labelAdditionalInfoDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimViewWellness.buttonSubmitClaim.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 40)
                submitClaimViewWellness.labelAgree.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimViewWellness.labelFraudWarning.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimViewWellness.labelFraudWarningSecond.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimViewWellness.labelFraudWarningLink.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimViewWellness.labelPolicyHolderDeclarationLink.font = UIFont(name: "Proxima Nova", size: 20)
                
                submitClaimViewWellness.labelClaimHeight.constant = 70
                submitClaimViewWellness.labelPetHeight.constant = 50
                submitClaimViewWellness.labelClaimTypeHeight.constant = 50
                submitClaimViewWellness.labelServiceDateHeight.constant = 50
                submitClaimViewWellness.labelVetClinicHeight.constant = 70
                submitClaimViewWellness.labelAdditionalInfoHeight.constant = 50
                submitClaimViewWellness.imageCheckboxPolicyHeight.constant = 60
                submitClaimViewWellness.imageCheckboxPolicyWidth.constant = 60
                submitClaimViewWellness.imageCheckboxFraudHeight.constant = 60
                submitClaimViewWellness.imageCheckboxFraudWidth.constant = 60
                submitClaimViewWellness.buttonSubmitClaimHeight.constant = 70
                submitClaimViewWellness.buttonSubmitClaim.layer.cornerRadius = 35
                submitClaimViewWellness.labelAgreeWidth.constant = 120
                submitClaimViewWellness.labelFraudWidth.constant = 130
                
                submittedClaimView.imageClaimWidth.constant = 350
                submittedClaimView.imageClaimHeight.constant = 350
                submittedClaimView.imageReceivedHeight.constant = 50
                submittedClaimView.imageReceivedWidth.constant = 50
                submittedClaimView.imageProcessingWidth.constant = 50
                submittedClaimView.imageProcessingHeight.constant = 50
                submittedClaimView.imageCompleteHeight.constant = 50
                submittedClaimView.imageCompleteWidth.constant = 50
                submittedClaimView.buttonSubmitHeight.constant = 80
                submittedClaimView.buttonSubmitClaim.layer.cornerRadius = 40
                submittedClaimView.labelStatusDetailsHeight.constant = 60
                submittedClaimView.labelDoneHeght.constant = 50
                submittedClaimView.buttonSubmitClaim.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 40)
                submittedClaimView.labelDone.font = UIFont(name: "Proxima Nova Bold", size: 38)
                submittedClaimView.labelDetails.font = UIFont(name: "Proxima Nova Bold", size: 41)
                submittedClaimView.labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 52)
                submittedClaimView.labelStatus.font = UIFont(name: "Proxima Nova Bold", size: 47)
                submittedClaimView.labelReceived.font = UIFont(name: "Proxima Nova", size: 38)
                submittedClaimView.labelProcessing.font = UIFont(name: "Proxima Nova", size: 38)
                submittedClaimView.labelClaimed.font = UIFont(name: "Proxima Nova", size: 38)
                submittedClaimView.stackViewMain.spacing = 80
                submittedClaimView.stackViewButtons.spacing = 40
                submittedClaimView.firstStackWidth.constant = 250
                submittedClaimView.secondStackWidth.constant = 250
                submittedClaimView.ImageToTitleHeight.constant = 72
                submittedClaimView.titleToStatusHeight.constant = 32
                submittedClaimView.statusToStatusImagesHeight.constant = 32
                submittedClaimView.statusToButtonsHeight.constant = 40
                
                drawDottedLine(start: CGPoint(x: submittedClaimView.secondView.bounds.minX, y: submittedClaimView.secondView.bounds.minY), end: CGPoint(x: 250, y: submittedClaimView.secondView.bounds.minY), view: submittedClaimView.secondView)
            } else {
                buttonNextHeight.constant = 80
                buttonNext.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 40)
                buttonNext.layer.cornerRadius = 40
                submitClaimView.labelPet.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelAdditionalInfo.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelVetClinic.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelReasonForVisit.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimView.labelName.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimView.labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimView.labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimView.labelReasonForVisitDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimView.labelVetClinicDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimView.labelAdditionalInfoDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimView.buttonSubmitClaim.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 40)
                submitClaimView.labelAgree.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimView.labelFraudWarning.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimView.labelFraudWarningSecond.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimView.labelFraudWarningLink.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimView.labelPolicyHolderDeclarationLink.font = UIFont(name: "Proxima Nova", size: 20)
                
                submitClaimView.labelClaimHeight.constant = 70
                submitClaimView.labelPetHeight.constant = 50
                submitClaimView.labelClaimTypeHeight.constant = 50
                submitClaimView.labelServiceDateHeight.constant = 50
                submitClaimView.labelReasonHeight.constant = 70
                submitClaimView.labelVetClinicHeight.constant = 70
                submitClaimView.labelAdditionalInfoHeight.constant = 50
                submitClaimView.imageCheckboxPolicyHeight.constant = 60
                submitClaimView.imageCheckboxPolicyWidth.constant = 60
                submitClaimView.imageCheckboxFraudHeight.constant = 60
                submitClaimView.imageCheckboxFraudWidth.constant = 60
                submitClaimView.buttonSubmitClaimHeight.constant = 70
                submitClaimView.buttonSubmitClaim.layer.cornerRadius = 35
                submitClaimView.labelAgreeWidth.constant = 120
                submitClaimView.labelFraudWidth.constant = 130
                
                submitClaimViewWellness.labelPet.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimViewWellness.labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimViewWellness.labelAdditionalInfo.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimViewWellness.labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimViewWellness.labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimViewWellness.labelVetClinic.font = UIFont(name: "Proxima Nova Bold", size: 30)
                submitClaimViewWellness.labelName.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimViewWellness.labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimViewWellness.labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimViewWellness.labelVetClinicDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimViewWellness.labelAdditionalInfoDetails.font = UIFont(name: "Proxima Nova", size: 30)
                submitClaimViewWellness.buttonSubmitClaim.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 40)
                submitClaimViewWellness.labelAgree.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimViewWellness.labelFraudWarning.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimViewWellness.labelFraudWarningSecond.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimViewWellness.labelFraudWarningLink.font = UIFont(name: "Proxima Nova", size: 20)
                submitClaimViewWellness.labelPolicyHolderDeclarationLink.font = UIFont(name: "Proxima Nova", size: 20)
                
                submitClaimViewWellness.labelClaimHeight.constant = 70
                submitClaimViewWellness.labelPetHeight.constant = 50
                submitClaimViewWellness.labelClaimTypeHeight.constant = 50
                submitClaimViewWellness.labelServiceDateHeight.constant = 50
                submitClaimViewWellness.labelVetClinicHeight.constant = 70
                submitClaimViewWellness.labelAdditionalInfoHeight.constant = 50
                submitClaimViewWellness.imageCheckboxPolicyHeight.constant = 60
                submitClaimViewWellness.imageCheckboxPolicyWidth.constant = 60
                submitClaimViewWellness.imageCheckboxFraudHeight.constant = 60
                submitClaimViewWellness.imageCheckboxFraudWidth.constant = 60
                submitClaimViewWellness.buttonSubmitClaimHeight.constant = 70
                submitClaimViewWellness.buttonSubmitClaim.layer.cornerRadius = 35
                submitClaimViewWellness.labelAgreeWidth.constant = 120
                submitClaimViewWellness.labelFraudWidth.constant = 130
                
                submittedClaimView.imageClaimWidth.constant = 270
                submittedClaimView.imageClaimHeight.constant = 270
                submittedClaimView.imageReceivedHeight.constant = 50
                submittedClaimView.imageReceivedWidth.constant = 50
                submittedClaimView.imageProcessingWidth.constant = 50
                submittedClaimView.imageProcessingHeight.constant = 50
                submittedClaimView.imageCompleteHeight.constant = 50
                submittedClaimView.imageCompleteWidth.constant = 50
                submittedClaimView.buttonSubmitHeight.constant = 60
                submittedClaimView.buttonSubmitClaim.layer.cornerRadius = 30
                submittedClaimView.labelStatusDetailsHeight.constant = 60
                submittedClaimView.labelDoneHeght.constant = 50
                submittedClaimView.buttonSubmitClaim.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 35)
                submittedClaimView.labelDone.font = UIFont(name: "Proxima Nova Bold", size: 33)
                submittedClaimView.labelDetails.font = UIFont(name: "Proxima Nova Bold", size: 36)
                submittedClaimView.labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 47)
                submittedClaimView.labelStatus.font = UIFont(name: "Proxima Nova Bold", size: 42)
                submittedClaimView.labelReceived.font = UIFont(name: "Proxima Nova", size: 33)
                submittedClaimView.labelProcessing.font = UIFont(name: "Proxima Nova", size: 33)
                submittedClaimView.labelClaimed.font = UIFont(name: "Proxima Nova", size: 33)
                submittedClaimView.stackViewMain.spacing = 40
                submittedClaimView.stackViewButtons.spacing = 30
                submittedClaimView.firstStackWidth.constant = 230
                submittedClaimView.secondStackWidth.constant = 230
                submittedClaimView.ImageToTitleHeight.constant = 64
                submittedClaimView.titleToStatusHeight.constant = 24
                submittedClaimView.statusToStatusImagesHeight.constant = 24
                submittedClaimView.statusToButtonsHeight.constant = 32
                
                drawDottedLine(start: CGPoint(x: submittedClaimView.secondView.bounds.minX, y: submittedClaimView.secondView.bounds.minY), end: CGPoint(x: 230, y: submittedClaimView.secondView.bounds.minY), view: submittedClaimView.secondView)
            }
            
            
        } else {
            drawDottedLine(start: CGPoint(x: submittedClaimView.secondView.bounds.minX, y: submittedClaimView.secondView.bounds.minY), end: CGPoint(x: submittedClaimView.secondView.bounds.maxX, y: submittedClaimView.secondView.bounds.minY), view: submittedClaimView.secondView)
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyb))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func appMovedToBackground() {
        let notificationCenter = NotificationCenter.default
          notificationCenter.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    
    @objc private func dismissKeyb() {
        view.endEditing(true)
    }
    
    // MARK: - Set tableView
    
    private func configTableViewProperties() {
        tableView.registerCell(PetTableViewCell.self)
        tableView.registerCell(TitleTableViewCell.self)
        tableView.registerCell(OneTitleTableViewCell.self)
        tableView.registerCell(ShowCanceledPolciesTableViewCell.self)
        tableView.registerCell(SelectTableViewCell.self)
        tableView.registerCell(DatePickerTableViewCell.self)
        tableView.registerCell(VetClinicTableViewCell.self)
        tableView.registerCell(UploadClaimTableViewCell.self)
        tableView.registerCell(UploadedDocumentTableViewCell.self)
        tableView.registerCell(VetClinicSavetTableViewCell.self)
        tableView.registerCell(ReasonTableViewCell.self)
        tableView.registerCell(IncludedDocumentsTableViewCell.self)
        tableView.registerCell(UploadClaimIPadTableViewCell.self)
        tableView.registerCell(VetClinicPickerTableViewCell.self)

        tableView.separatorStyle = .none
        
    }
    
    private func startLoadingScreen() {
        DispatchQueue.main.async {
            self.activityIndicatorScreen.startAnimating()
        }
    }
    
    private func stopLoadingScreen() {
        DispatchQueue.main.async {
            self.activityIndicatorScreen.stopAnimating()
        }
    }
    
    private func startLoadingWholeScreen() {
        DispatchQueue.main.async {
            self.loadingScreenView.activityIndicatorScreen?.startAnimating()
        }
    }
    
    private func stopLoadingWholeScreen() {
        DispatchQueue.main.async {
            self.loadingScreenView.activityIndicatorScreen?.stopAnimating()
        }
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [7, 3] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    @objc func openFraudWarning() {
        let vc = FraudWarningViewController()
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @objc func openPolicyDeclaration() {
        let vc = PolicyHolderViewController()
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @objc func checkAgree() {
        if submitClaimView.imageViewCheckboxAgree.image == UIImage(named: "empty") {
            submitClaimView.imageViewCheckboxAgree.image = UIImage(named: "ticked")
            isPolicyChecked = true
            if isFraudChecked == true {
                submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                submitClaimView.buttonSubmitClaim.isEnabled = true
                selectedOptionsForEndpoint["DeclarationsChecked"] = true
            } else {
                selectedOptionsForEndpoint["DeclarationsChecked"] = false
            }
        } else {
            submitClaimView.imageViewCheckboxAgree.image = UIImage(named: "empty")
            isPolicyChecked = false
            submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.gray
            submitClaimView.buttonSubmitClaim.isEnabled = false
            selectedOptionsForEndpoint["DeclarationsChecked"] = false
        }
    }
    
    @objc func checkAgree1() {
        if submitClaimViewWellness.imageViewCheckboxAgree.image == UIImage(named: "empty") {
            submitClaimViewWellness.imageViewCheckboxAgree.image = UIImage(named: "ticked")
            isPolicyChecked = true
            if isFraudChecked == true {
                submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                selectedOptionsForEndpoint["DeclarationsChecked"] = true
            } else {
                selectedOptionsForEndpoint["DeclarationsChecked"] = false
            }
        } else {
            submitClaimViewWellness.imageViewCheckboxAgree.image = UIImage(named: "empty")
            isPolicyChecked = false
            submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.gray
            submitClaimViewWellness.buttonSubmitClaim.isEnabled = false
            selectedOptionsForEndpoint["DeclarationsChecked"] = false
        }
    }
    
    @objc func checkFraud() {
        if submitClaimView.imageViewCheckboxFraud.image == UIImage(named: "empty") {
            submitClaimView.imageViewCheckboxFraud.image = UIImage(named: "ticked")
            isFraudChecked = true
            if isPolicyChecked == true {
                submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                submitClaimView.buttonSubmitClaim.isEnabled = true
                selectedOptionsForEndpoint["FraudWarningChecked"] = true
                selectedOptionsForEndpoint["DeclarationsChecked"] = true
            }
        } else {
            submitClaimView.imageViewCheckboxFraud.image = UIImage(named: "empty")
            isFraudChecked = false
            submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.gray
            submitClaimView.buttonSubmitClaim.isEnabled = false
            selectedOptionsForEndpoint["FraudWarningChecked"] = false
        }
    }
    
    @objc func checkFraud1() {
        if submitClaimViewWellness.imageViewCheckboxFraud.image == UIImage(named: "empty") {
            submitClaimViewWellness.imageViewCheckboxFraud.image = UIImage(named: "ticked")
            isFraudChecked = true
            if isPolicyChecked == true {
                submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                selectedOptionsForEndpoint["FraudWarningChecked"] = true
                selectedOptionsForEndpoint["DeclarationsChecked"] = true
            }
        } else {
            submitClaimViewWellness.imageViewCheckboxFraud.image = UIImage(named: "empty")
            isFraudChecked = false
            submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.gray
            submitClaimViewWellness.buttonSubmitClaim.isEnabled = false
            selectedOptionsForEndpoint["FraudWarningChecked"] = false
        }
    }
    
    func reloadTableView() {
        
        let alert:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)

        let cameraAction = UIAlertAction(title: "Take a photo", style: UIAlertAction.Style.default) {
            UIAlertAction in
            let scannerViewController = ImageScannerController()
            scannerViewController.toolbar.tintColor = UIColor.black
            scannerViewController.toolbar.backgroundColor = UIColor.black
            scannerViewController.imageScannerDelegate = self
            self.present(scannerViewController, animated: true)
        }
        let gallaryAction = UIAlertAction(title: "Photo Library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallary()
        }
        let browseAction = UIAlertAction(title: "Browse", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openICloud()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(browseAction)
        alert.addAction(cancelAction)
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func openGallary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            picker.delegate = self
            let barApperance = UINavigationBar.appearance()
            barApperance.tintColor = .systemBlue
            picker.sourceType = .savedPhotosAlbum
            picker.allowsEditing = false
            
            present(picker, animated: true, completion: nil)
            DispatchQueue.main.async {
                self.startLoadingScreen()
            }
        }
    }
    
    private func openICloud() {
        if #available(iOS 11.0, *) {
            UINavigationBar.appearance(whenContainedInInstancesOf: [UIDocumentBrowserViewController.self]).tintColor = nil
        }
        let documentsPicker = UIDocumentPickerViewController(documentTypes: ["public.image", "public.jpeg", "public.png", String(kUTTypePDF)], in: .open)
        documentsPicker.delegate = self
        documentsPicker.allowsMultipleSelection = false
        documentsPicker.modalPresentationStyle = .fullScreen
        self.present(documentsPicker, animated: true, completion: nil)
    }
    
    @objc private func networkingPolicyDetails() {
        self.allPets = []
        self.canceledPets = []
        
        if ReachabilityClass.isConnectedToNetwork(){
            worker.getPolicyDetails(completionHandler: { [weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    
                    var canceled = [PetCoversList]()
                    for pet in resultUser {
                        if pet.IsCanceled == false {
                            self?.allPets.append(pet)
                        } else {
                            canceled.append(pet)
                        }
                    }
                    self?.canceledPets.append(contentsOf: self?.allPets ?? [])
                    self?.canceledPets.append(contentsOf: canceled)
                    
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                        self?.stopLoadingScreen()
                    }
                    
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        self?.stopLoadingScreen()
                    }
                }
            }) { [weak self] error  in
                DispatchQueue.main.async {
                    self?.stopLoadingScreen()
                }
            }
        } else {
            DispatchQueue.main.async {
                self.stopLoadingScreen()
                self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
            }
        }
    }

    private func networkingConditionReason() {
        primaryReasonsArray = [String]()
        if ReachabilityClass.isConnectedToNetwork(){
            workerGetConditions.getConditionsReasons(completionHandler: { [weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    
                    for reason in resultUser {
                        var subConditions = [String]()
                        var subConditionsCodes = [Int]()
                        
                        for subCondition in reason.value.SubConditions {
                            subConditions.append(subCondition.SubConditionDescription)
                            subConditionsCodes.append(subCondition.SubConditionCode)
                        }
                        
                        if subConditions.count > 0 {
                            self?.primaryReasonsArray.append(reason.value.ConditionDescription)
                            self?.primaryReasonsCodeArray.append(reason.value.ConditionCode)
                            self?.conditionsArray[reason.value.ConditionDescription] = subConditions
                            self?.conditionsCodesArray[reason.value.ConditionCode] = subConditionsCodes
                        }
                    }
                    
                    for reason in resultUser {
                        print(reason.value.ConditionDescription)
                        if reason.value.ConditionDescription == "Other -Not listed" || reason.value.ConditionDescription == "Other" {
                            self?.primaryReasonsArray.append(reason.value.ConditionDescription)
                            self?.primaryReasonsCodeArray.append(reason.value.ConditionCode)
                            self?.conditionsArray[reason.value.ConditionDescription] = []
                            self?.conditionsCodesArray[reason.value.ConditionCode] = []
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self?.stopLoadingScreen()
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        self?.stopLoadingScreen()
                    }
                }
            }) { [weak self] error  in
                DispatchQueue.main.async {
                    self?.stopLoadingScreen()
                }
            }
        } else {
            DispatchQueue.main.async {
                self.stopLoadingScreen()
                self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
            }
        }
    }
    
    @objc func submitClaim() {
        //print(selectedOptionsForEndpoint)
        
        submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.gray
        submitClaimView.buttonSubmitClaim.isEnabled = false
        
        submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.gray
        submitClaimViewWellness.buttonSubmitClaim.isEnabled = false
        
        var vetTypehead = ""
        var diagnosis = ""
        var savedHospitalId = 0
        var hospitalId = 0
        
        let policyNumber = selectedOptionsForEndpoint["PolicyLineNumber"]
        let claimType = selectedOptionsForEndpoint["ClaimType"]
        let dateString = selectedOptionsForEndpoint["IncidentDate"]
        let fraudChecked = selectedOptionsForEndpoint["FraudWarningChecked"]
        let declarationsChecked = selectedOptionsForEndpoint["DeclarationsChecked"]
        let conditionCode = selectedOptionsForEndpoint["PrimaryReason"]
        let subconditionCode = selectedOptionsForEndpoint["Condition"]
        var subconditionCodeText = ""
        
        if selectedOptionsForEndpoint["ConditionText"] != nil {
            subconditionCodeText = selectedOptionsForEndpoint["ConditionText"] as! String
        }
        
        if selectedOptionsForEndpoint["VetTypehead"] == nil {
            selectedOptionsForEndpoint["VetTypehead"] = nil
        } else {
            vetTypehead = selectedOptionsForEndpoint["VetTypehead"] as! String
        }
        if selectedOptionsForEndpoint["VetClinic"] == nil {
            selectedOptionsForEndpoint["VetClinic"] = nil
        } else {
            savedHospitalId = selectedOptionsForEndpoint["VetClinic"] as! Int
        }
        if selectedOptionsForEndpoint["VetClinicSecond"] == nil {
            selectedOptionsForEndpoint["VetClinicSecond"] = nil
        } else {
            hospitalId = selectedOptionsForEndpoint["VetClinicSecond"] as! Int
        }
        
        if selectedOptionsForEndpoint["Diagnosis"] == nil {
            selectedOptionsForEndpoint["Diagnosis"] = ""
        } else {
            diagnosis = selectedOptionsForEndpoint["Diagnosis"] as! String
        }
        
        guard let fraud = fraudChecked as? Bool,
              let declaration = declarationsChecked as? Bool,
              let type = claimType as? Int,
              let number = policyNumber as? Int,
              let date = dateString as? String
        else { return }
        
        //print(selectedOptionsForEndpoint)
        DispatchQueue.main.async {
            self.startLoadingWholeScreen()
        }
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let systemVersion = UIDevice.current.systemVersion
        let usernameLocal = self.keychain.get("username")
        
        let localDateFormatter = DateFormatter()
        localDateFormatter.dateStyle = .medium
        localDateFormatter.timeStyle = .medium

        // Printing a Date
        let dateTime = Date()
        //print(localDateFormatter.string(from: dateTime))

        let currentDateTime = localDateFormatter.string(from: dateTime)

        if ReachabilityClass.isConnectedToNetwork() {
            testSubmitClaim.getTestDetails { [weak self] resultUser in
                //1 document
                if self?.imageDocument != nil && self?.imageSecondDocument == nil && self?.imageThirdDocument == nil && self?.imageFourthDocument == nil {
                    let nameImage = "Invoice1"
                    let imageData:Data = self!.imageDocument!.jpegData(compressionQuality: 0.6)!
                    var strBase64: String?
                    
                    if self?.isPDF[0] == false {
                        strBase64 = imageData.base64EncodedString(options: [])
                    } else {
                        strBase64 = self?.fileFirstString
                    }
                    
                    let fileDictionary = [["Filename": nameImage, "ContentType": "image/jpeg",
                                           "Base64FileData": strBase64 ?? ""]] as [[String : Any]]
                    
                    DispatchQueue.main.async {
                        self?.loadingScreenView.isHidden = false
                    }

                    self?.workerSubmitClaim.submitClaim(diagnosis: diagnosis, fraudChecked: fraud, declarationsChecked: declaration, policyNumber: number, claimType: type, withIncidentDate: date, conditionCode: conditionCode as? Int ?? 0, subConditionCode: subconditionCode as? Int ?? 0, subConditionCodeText: subconditionCodeText, savedHospitalId: savedHospitalId , hospitalId: hospitalId , vetTypehead: vetTypehead , withFiles: fileDictionary) { [weak self] resultUser in
                        switch resultUser {
                        case .success(let value):
                            Analytics.logEvent(Constants.FirebaseEvents.submitClaim, parameters: nil)
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.CLAIMS_UPDATE_NOTIFICAITON), object: nil)
                            
                            DispatchQueue.main.async {
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                self?.submittedClaimView.isHidden = false
                                self?.defaults.setValue(self!.selectedPet!.PetName, forKey: "submittedClaimPet")
                                self?.defaults.setValue(value.ClaimId, forKey: "submittedClaimId")

                                self?.keychain.set("true", forKey: "isSubmitted")
                                
                                self?.submitClaimViewWellness.imageViewCheckboxFraud.image = UIImage(named: "empty")
                                self?.submitClaimView.imageViewCheckboxFraud.image = UIImage(named: "empty")
                                self?.submitClaimViewWellness.imageViewCheckboxAgree.image = UIImage(named: "empty")
                                self?.submitClaimView.imageViewCheckboxAgree.image = UIImage(named: "empty")
                                
                                self?.submittedClaimView.labelDetails.text = "\(String(describing: self!.selectedPet!.PetName))'s claim #\(value.ClaimId) has been successfully submitted and is now being reviewed. You will be notified once your claim has been processed."
                                
                                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.CLAIMS_UPDATE_NOTIFICAITON), object: nil)
                                
                                Auth.auth().signInAnonymously { authResult, error in
                                    if self?.isPDF[0] == true {
                                        self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                            "app_version": appVersion ?? "",
                                            "claim#": "\(value.ClaimId)",
                                            "date_time": currentDateTime,
                                            "device_info": UIDevice.modelName,
                                            "os_version": systemVersion,
                                            "policy#": self!.policyNumberWhole,
                                            "user": usernameLocal ?? "",
                                            "filenames": [
                                                "0": "\(nameImage).pdf"
                                            ]
                                        ]) { err in
                                            if let err = err {
                                                print("Error writing document: \(err)")
                                            } else {
                                                print("Document successfully written!")
                                            }
                                        }
                                    } else {
                                        self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                            "app_version": appVersion ?? "",
                                            "claim#": "\(value.ClaimId)",
                                            "date_time": currentDateTime,
                                            "device_info": UIDevice.modelName,
                                            "os_version": systemVersion,
                                            "policy#": self!.policyNumberWhole,
                                            "user": usernameLocal ?? "",
                                            "filenames": [
                                                "0": "\(nameImage).jpg"
                                            ]
                                        ]) { err in
                                            if let err = err {
                                                print("Error writing document: \(err)")
                                            } else {
                                                print("Document successfully written!")
                                            }
                                        }
                                    }
                                }
                                
                                //Ask for AppStore review
                                StoreReviewHelper.checkAndAskForReview()
                                
                                self?.navigationController?.navigationBar.isHidden = true
                                let tapSubmitNewClaim = UITapGestureRecognizer(target: self, action:  #selector(self?.submitNewClaim))
                                self?.submittedClaimView.buttonSubmitClaim.addGestureRecognizer(tapSubmitNewClaim)
                            }
                        case .failure(let error):
                            DispatchQueue.main.async {
                                self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                if self?.isWellness == true {
                                    self?.submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                                }
                                if self?.isWellness == false {
                                    self?.submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimView.buttonSubmitClaim.isEnabled = true
                                }
                            }
                        }
                    } onErrorHandler: { [weak self] error  in
                        switch error {
                        case .success(_):
                            DispatchQueue.main.async {
                                Analytics.logEvent(Constants.FirebaseEvents.submitClaimError, parameters: nil)
                                self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                if self?.isWellness == true {
                                    self?.submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                                }
                                if self?.isWellness == false {
                                    self?.submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimView.buttonSubmitClaim.isEnabled = true
                                }
                                
                            }
                        case .failure(let error):
                            DispatchQueue.main.async {
                                self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                if self?.isWellness == true {
                                    self?.submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                                }
                                if self?.isWellness == false {
                                    self?.submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimView.buttonSubmitClaim.isEnabled = true
                                }
                            }
                        }
                    }
                }
                
                //2 documents
                if self?.imageDocument != nil && self?.imageSecondDocument != nil && self?.imageThirdDocument == nil && self?.imageFourthDocument == nil {
                    
                    let nameImage = "Invoice1"
                    let imageData:Data = self!.imageDocument!.jpegData(compressionQuality: 0.6)!
                    var strBase64: String?
                    
                    if self?.isPDF[0] == false {
                        strBase64 = imageData.base64EncodedString(options: [])
                    } else {
                        strBase64 = self?.fileFirstString
                    }
                    
                    let nameImageSecond = "Invoice2"
                    let imageDataSecond:Data = self!.imageSecondDocument!.jpegData(compressionQuality: 0.6)!
                    var strBase64Second: String?
                    
                    if self?.isPDF[1] == false {
                        strBase64Second = imageDataSecond.base64EncodedString(options: [])
                    } else {
                        strBase64Second = self?.fileSecondString
                    }
                    
                    let fileDictionary = [["Filename": nameImage, "ContentType": "image/jpeg",
                                           "Base64FileData": strBase64 ?? ""],["Filename": nameImageSecond, "ContentType": "image/jpeg","Base64FileData": strBase64Second ?? ""] ] as [[String : Any]]
                    
                    DispatchQueue.main.async {
                        self?.loadingScreenView.isHidden = false
                    }
                    
                    self?.workerSubmitClaim.submitClaim(diagnosis: diagnosis, fraudChecked: fraud, declarationsChecked: declaration, policyNumber: number, claimType: type, withIncidentDate: date, conditionCode: conditionCode as? Int ?? 0, subConditionCode: subconditionCode as? Int ?? 0, subConditionCodeText: subconditionCodeText, savedHospitalId: savedHospitalId , hospitalId: hospitalId , vetTypehead: vetTypehead , withFiles: fileDictionary) { [weak self] resultUser in
                        switch resultUser {
                        case .success(let value):
                            
                            DispatchQueue.main.async {
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                self?.submittedClaimView.isHidden = false
                                
                                self?.defaults.setValue(self!.selectedPet!.PetName, forKey: "submittedClaimPet")
                                self?.defaults.setValue(value.ClaimId, forKey: "submittedClaimId")

                                self?.keychain.set("true", forKey: "isSubmitted")
                                
                                self?.submitClaimViewWellness.imageViewCheckboxFraud.image = UIImage(named: "empty")
                                self?.submitClaimView.imageViewCheckboxFraud.image = UIImage(named: "empty")
                                self?.submitClaimViewWellness.imageViewCheckboxAgree.image = UIImage(named: "empty")
                                self?.submitClaimView.imageViewCheckboxAgree.image = UIImage(named: "empty")
                                
                                self?.submittedClaimView.labelDetails.text = "\(String(describing: self!.selectedPet!.PetName))'s claim #\(value.ClaimId) has been successfully submitted and is now being reviewed. You will be notified once your claim has been processed."

                                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.CLAIMS_UPDATE_NOTIFICAITON), object: nil)
                                
                                Auth.auth().signInAnonymously { authResult, error in
                                    if self?.isPDF[0] == true {
                                        if self?.isPDF[1] == true {
                                            self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                "app_version": appVersion ?? "",
                                                "claim#": "\(value.ClaimId)",
                                                "date_time": currentDateTime,
                                                "device_info": UIDevice.modelName,
                                                "os_version": systemVersion,
                                                "policy#": self!.policyNumberWhole,
                                                "user": usernameLocal ?? "",
                                                "filenames": [
                                                    "0": "\(nameImage).pdf",
                                                    "1": "\(nameImageSecond).pdf"
                                                ]
                                            ]) { err in
                                                if let err = err {
                                                    print("Error writing document: \(err)")
                                                } else {
                                                    print("Document successfully written!")
                                                }
                                            }
                                        } else {
                                            self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                "app_version": appVersion ?? "",
                                                "claim#": "\(value.ClaimId)",
                                                "date_time": currentDateTime,
                                                "device_info": UIDevice.modelName,
                                                "os_version": systemVersion,
                                                "policy#": self!.policyNumberWhole,
                                                "user": usernameLocal ?? "",
                                                "filenames": [
                                                    "0": "\(nameImage).pdf",
                                                    "1": "\(nameImageSecond).img"
                                                ]
                                            ]) { err in
                                                if let err = err {
                                                    print("Error writing document: \(err)")
                                                } else {
                                                    print("Document successfully written!")
                                                }
                                            }
                                        }
                                    } else {
                                        if self?.isPDF[1] == true {
                                            self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                "app_version": appVersion ?? "",
                                                "claim#": "\(value.ClaimId)",
                                                "date_time": currentDateTime,
                                                "device_info": UIDevice.modelName,
                                                "os_version": systemVersion,
                                                "policy#": self!.policyNumberWhole,
                                                "user": usernameLocal ?? "",
                                                "filenames": [
                                                    "0": "\(nameImage).img",
                                                    "1": "\(nameImageSecond).pdf"
                                                ]
                                            ]) { err in
                                                if let err = err {
                                                    print("Error writing document: \(err)")
                                                } else {
                                                    print("Document successfully written!")
                                                }
                                            }
                                        } else {
                                            self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                "app_version": appVersion ?? "",
                                                "claim#": "\(value.ClaimId)",
                                                "date_time": currentDateTime,
                                                "device_info": UIDevice.modelName,
                                                "os_version": systemVersion,
                                                "policy#": self!.policyNumberWhole,
                                                "user": usernameLocal ?? "",
                                                "filenames": [
                                                    "0": "\(nameImage).img",
                                                    "1": "\(nameImageSecond).img"
                                                ]
                                            ]) { err in
                                                if let err = err {
                                                    print("Error writing document: \(err)")
                                                } else {
                                                    print("Document successfully written!")
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                //Ask for AppStore review
                                StoreReviewHelper.checkAndAskForReview()
                                
                                self?.keychain.set("true", forKey: "isSubmitted")
                                self?.navigationController?.navigationBar.isHidden = true
                                let tapSubmitNewClaim = UITapGestureRecognizer(target: self, action:  #selector(self?.submitNewClaim))
                                self?.submittedClaimView.buttonSubmitClaim.addGestureRecognizer(tapSubmitNewClaim)
                            }
                        case .failure(let error):
                            DispatchQueue.main.async {
                                self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                if self?.isWellness == true {
                                    self?.submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                                }
                                if self?.isWellness == false {
                                    self?.submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimView.buttonSubmitClaim.isEnabled = true
                                }
                            }
                        }
                    } onErrorHandler: { [weak self] error  in
                        switch error {
                        case .success(_):
                            DispatchQueue.main.async {
                                Analytics.logEvent(Constants.FirebaseEvents.submitClaimError, parameters: nil)
                                self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                if self?.isWellness == true {
                                    self?.submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                                }
                                if self?.isWellness == false {
                                    self?.submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimView.buttonSubmitClaim.isEnabled = true
                                }
                            }
                        case .failure(let error):
                            DispatchQueue.main.async {
                                self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                if self?.isWellness == true {
                                    self?.submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                                }
                                if self?.isWellness == false {
                                    self?.submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimView.buttonSubmitClaim.isEnabled = true
                                }
                            }
                        }
                    }
                }
                
                //3 documents
                if self?.imageDocument != nil && self?.imageSecondDocument != nil && self?.imageThirdDocument != nil && self?.imageFourthDocument == nil {
                    
                    let nameImage = "Invoice1"
                    let imageData:Data = self!.imageDocument!.jpegData(compressionQuality: 0.6)!
                    var strBase64: String?
                    
                    if self?.isPDF[0] == false {
                        strBase64 = imageData.base64EncodedString(options: [])
                    } else {
                        strBase64 = self?.fileFirstString
                    }
                    
                    let nameImageSecond = "Invoice2"
                    let imageDataSecond:Data = self!.imageSecondDocument!.jpegData(compressionQuality: 0.6)!
                    var strBase64Second: String?
                    
                    if self?.isPDF[1] == false {
                        strBase64Second = imageDataSecond.base64EncodedString(options: [])
                    } else {
                        strBase64Second = self?.fileSecondString
                    }
                    
                    let nameImageThird = "Invoice3"
                    let imageDataThird:Data = self!.imageThirdDocument!.jpegData(compressionQuality: 0.6)!

                    var strBase64Third: String?
                    
                    if self?.isPDF[2] == false {
                        strBase64Third = imageDataThird.base64EncodedString(options: [])
                    } else {
                        strBase64Third = self?.fileThirdString
                    }

                    let fileDictionary = [["Filename": nameImage, "ContentType": "image/jpeg",
                                           "Base64FileData": strBase64 ?? ""],["Filename": nameImageSecond, "ContentType": "image/jpeg","Base64FileData": strBase64Second ?? ""],["Filename": nameImageThird, "ContentType": "image/jpeg","Base64FileData": strBase64Third ?? ""]] as [[String : Any]]
                    
                    DispatchQueue.main.async {
                        self?.loadingScreenView.isHidden = false
                    }
                    self?.workerSubmitClaim.submitClaim(diagnosis: diagnosis, fraudChecked: fraud, declarationsChecked: declaration, policyNumber: number, claimType: type, withIncidentDate: date, conditionCode: conditionCode as? Int ?? 0, subConditionCode: subconditionCode as? Int ?? 0, subConditionCodeText: subconditionCodeText, savedHospitalId: savedHospitalId , hospitalId: hospitalId , vetTypehead: vetTypehead , withFiles: fileDictionary) { [weak self] resultUser in
                        switch resultUser {
                        case .success(let value):
                            
                            DispatchQueue.main.async {
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                self?.submittedClaimView.isHidden = false
                                
                                self?.defaults.setValue(self!.selectedPet!.PetName, forKey: "submittedClaimPet")
                                self?.defaults.setValue(value.ClaimId, forKey: "submittedClaimId")

                                self?.keychain.set("true", forKey: "isSubmitted")
                                
                                self?.submitClaimViewWellness.imageViewCheckboxFraud.image = UIImage(named: "empty")
                                self?.submitClaimView.imageViewCheckboxFraud.image = UIImage(named: "empty")
                                self?.submitClaimViewWellness.imageViewCheckboxAgree.image = UIImage(named: "empty")
                                self?.submitClaimView.imageViewCheckboxAgree.image = UIImage(named: "empty")
                                
                                self?.submittedClaimView.labelDetails.text = "\(String(describing: self!.selectedPet!.PetName))'s claim #\(value.ClaimId) has been successfully submitted and is now being reviewed. You will be notified once your claim has been processed."
                                
                                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.CLAIMS_UPDATE_NOTIFICAITON), object: nil)
                                
                                Auth.auth().signInAnonymously { authResult, error in
                                    if self?.isPDF[0] == true {
                                        if self?.isPDF[1] == true {
                                            if self?.isPDF[2] == true {
                                                //1-pdf
                                                //2-pdf
                                                //3-pdf
                                                self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                    "app_version": appVersion ?? "",
                                                    "claim#": "\(value.ClaimId)",
                                                    "date_time": currentDateTime,
                                                    "device_info": UIDevice.modelName,
                                                    "os_version": systemVersion,
                                                    "policy#": self!.policyNumberWhole,
                                                    "user": usernameLocal ?? "",
                                                    "filenames": [
                                                        "0": "\(nameImage).pdf",
                                                        "1": "\(nameImageSecond).pdf",
                                                        "2": "\(nameImageThird).pdf"
                                                    ]
                                                ]) { err in
                                                    if let err = err {
                                                        print("Error writing document: \(err)")
                                                    } else {
                                                        print("Document successfully written!")
                                                    }
                                                }
                                            } else {
                                                //1-pdf
                                                //2-pdf
                                                //3-img
                                                self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                    "app_version": appVersion ?? "",
                                                    "claim#": "\(value.ClaimId)",
                                                    "date_time": currentDateTime,
                                                    "device_info": UIDevice.modelName,
                                                    "os_version": systemVersion,
                                                    "policy#": self!.policyNumberWhole,
                                                    "user": usernameLocal ?? "",
                                                    "filenames": [
                                                        "0": "\(nameImage).pdf",
                                                        "1": "\(nameImageSecond).pdf",
                                                        "2": "\(nameImageThird).img"
                                                    ]
                                                ]) { err in
                                                    if let err = err {
                                                        print("Error writing document: \(err)")
                                                    } else {
                                                        print("Document successfully written!")
                                                    }
                                                }
                                            }
                                        } else {
                                            //1-pdf
                                            //2-img
                                            //3-pdf
                                            if self?.isPDF[2] == true {
                                                self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                    "app_version": appVersion ?? "",
                                                    "claim#": "\(value.ClaimId)",
                                                    "date_time": currentDateTime,
                                                    "device_info": UIDevice.modelName,
                                                    "os_version": systemVersion,
                                                    "policy#": self!.policyNumberWhole,
                                                    "user": usernameLocal ?? "",
                                                    "filenames": [
                                                        "0": "\(nameImage).pdf",
                                                        "1": "\(nameImageSecond).img",
                                                        "2": "\(nameImageThird).pdf"
                                                    ]
                                                ]) { err in
                                                    if let err = err {
                                                        print("Error writing document: \(err)")
                                                    } else {
                                                        print("Document successfully written!")
                                                    }
                                                }
                                            } else {
                                                //1-pdf
                                                //2-img
                                                //3-img
                                                self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                    "app_version": appVersion ?? "",
                                                    "claim#": "\(value.ClaimId)",
                                                    "date_time": currentDateTime,
                                                    "device_info": UIDevice.modelName,
                                                    "os_version": systemVersion,
                                                    "policy#": self!.policyNumberWhole,
                                                    "user": usernameLocal ?? "",
                                                    "filenames": [
                                                        "0": "\(nameImage).pdf",
                                                        "1": "\(nameImageSecond).img",
                                                        "2": "\(nameImageThird).img"
                                                    ]
                                                ]) { err in
                                                    if let err = err {
                                                        print("Error writing document: \(err)")
                                                    } else {
                                                        print("Document successfully written!")
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if self?.isPDF[1] == true {
                                            if self?.isPDF[2] == true {
                                                //1-img
                                                //2-pdf
                                                //3-pdf
                                                self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                    "app_version": appVersion ?? "",
                                                    "claim#": "\(value.ClaimId)",
                                                    "date_time": currentDateTime,
                                                    "device_info": UIDevice.modelName,
                                                    "os_version": systemVersion,
                                                    "policy#": self!.policyNumberWhole,
                                                    "user": usernameLocal ?? "",
                                                    "filenames": [
                                                        "0": "\(nameImage).img",
                                                        "1": "\(nameImageSecond).pdf",
                                                        "2": "\(nameImageThird).pdf"
                                                    ]
                                                ]) { err in
                                                    if let err = err {
                                                        print("Error writing document: \(err)")
                                                    } else {
                                                        print("Document successfully written!")
                                                    }
                                                }
                                            } else {
                                                //1-img
                                                //2-pdf
                                                //3-img
                                                self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                    "app_version": appVersion ?? "",
                                                    "claim#": "\(value.ClaimId)",
                                                    "date_time": currentDateTime,
                                                    "device_info": UIDevice.modelName,
                                                    "os_version": systemVersion,
                                                    "policy#": self!.policyNumberWhole,
                                                    "user": usernameLocal ?? "",
                                                    "filenames": [
                                                        "0": "\(nameImage).img",
                                                        "1": "\(nameImageSecond).pdf",
                                                        "2": "\(nameImageThird).img"
                                                    ]
                                                ]) { err in
                                                    if let err = err {
                                                        print("Error writing document: \(err)")
                                                    } else {
                                                        print("Document successfully written!")
                                                    }
                                                }
                                            }
                                        } else {
                                            //1-img
                                            //2-img
                                            //3-pdf
                                            if self?.isPDF[2] == true {
                                                self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                    "app_version": appVersion ?? "",
                                                    "claim#": "\(value.ClaimId)",
                                                    "date_time": currentDateTime,
                                                    "device_info": UIDevice.modelName,
                                                    "os_version": systemVersion,
                                                    "policy#": self!.policyNumberWhole,
                                                    "user": usernameLocal ?? "",
                                                    "filenames": [
                                                        "0": "\(nameImage).img",
                                                        "1": "\(nameImageSecond).img",
                                                        "2": "\(nameImageThird).pdf"
                                                    ]
                                                ]) { err in
                                                    if let err = err {
                                                        print("Error writing document: \(err)")
                                                    } else {
                                                        print("Document successfully written!")
                                                    }
                                                }
                                            } else {
                                                //1-img
                                                //2-img
                                                //3-img
                                                self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                    "app_version": appVersion ?? "",
                                                    "claim#": "\(value.ClaimId)",
                                                    "date_time": currentDateTime,
                                                    "device_info": UIDevice.modelName,
                                                    "os_version": systemVersion,
                                                    "policy#": self!.policyNumberWhole,
                                                    "user": usernameLocal ?? "",
                                                    "filenames": [
                                                        "0": "\(nameImage).img",
                                                        "1": "\(nameImageSecond).img",
                                                        "2": "\(nameImageThird).img"
                                                    ]
                                                ]) { err in
                                                    if let err = err {
                                                        print("Error writing document: \(err)")
                                                    } else {
                                                        print("Document successfully written!")
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                //Ask for AppStore review
                                StoreReviewHelper.checkAndAskForReview()

                                self?.keychain.set("true", forKey: "isSubmitted")
                                self?.navigationController?.navigationBar.isHidden = true
                                let tapSubmitNewClaim = UITapGestureRecognizer(target: self, action:  #selector(self?.submitNewClaim))
                                self?.submittedClaimView.buttonSubmitClaim.addGestureRecognizer(tapSubmitNewClaim)
                            }
                        case .failure(let error):
                            DispatchQueue.main.async {
                                self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                if self?.isWellness == true {
                                    self?.submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                                }
                                if self?.isWellness == false {
                                    self?.submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimView.buttonSubmitClaim.isEnabled = true
                                }
                            }
                        }
                    } onErrorHandler: { [weak self] error  in
                        switch error {
                        case .success(_):
                            Analytics.logEvent(Constants.FirebaseEvents.submitClaimError, parameters: nil)
                            DispatchQueue.main.async {
                                self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                if self?.isWellness == true {
                                    self?.submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                                }
                                if self?.isWellness == false {
                                    self?.submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimView.buttonSubmitClaim.isEnabled = true
                                }
                            }
                        case .failure(let error):
                            DispatchQueue.main.async {
                                self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                if self?.isWellness == true {
                                    self?.submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                                }
                                if self?.isWellness == false {
                                    self?.submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimView.buttonSubmitClaim.isEnabled = true
                                }
                            }
                        }
                    }
                }
                
                //4 documents
                if self?.imageDocument != nil && self?.imageSecondDocument != nil && self?.imageThirdDocument != nil && self?.imageFourthDocument != nil {

                    let nameImage = "Invoice1"
                    let imageData:Data = self!.imageDocument!.jpegData(compressionQuality: 0.6)!
                    var strBase64: String?
                    
                    if self?.isPDF[0] == false {
                        strBase64 = imageData.base64EncodedString(options: [])
                    } else {
                        strBase64 = self?.fileFirstString
                    }
                    
                    let nameImageSecond = "Invoice2"
                    let imageDataSecond:Data = self!.imageSecondDocument!.jpegData(compressionQuality: 0.6)!
                    var strBase64Second: String?
                    
                    if self?.isPDF[1] == false {
                        strBase64Second = imageDataSecond.base64EncodedString(options: [])
                    } else {
                        strBase64Second = self?.fileSecondString
                    }
                    
                    let nameImageThird = "Invoice3"
                    let imageDataThird:Data = self!.imageThirdDocument!.jpegData(compressionQuality: 0.6)!

                    var strBase64Third: String?
                    
                    if self?.isPDF[2] == false {
                        strBase64Third = imageDataThird.base64EncodedString(options: [])
                    } else {
                        strBase64Third = self?.fileThirdString
                    }
                    
                    let nameImageFourth = "Invoice4"
                    let imageDataFourth:Data = self!.imageFourthDocument!.jpegData(compressionQuality: 0.6)!
                    var strBase64Fourth: String?
                    
                    if self?.isPDF[3] == false {
                        strBase64Fourth = imageDataFourth.base64EncodedString(options: [])
                    } else {
                        strBase64Fourth = self?.fileFourthString
                    }
                    
                    let fileDictionary = [["Filename": nameImage, "ContentType": "image/jpeg",
                                           "Base64FileData": strBase64 ?? ""],["Filename": nameImageSecond, "ContentType": "image/jpeg","Base64FileData": strBase64Second ?? ""],["Filename": nameImageThird, "ContentType": "image/jpeg","Base64FileData": strBase64Third ?? ""],["Filename": nameImageFourth, "ContentType": "image/jpeg","Base64FileData": strBase64Fourth ?? ""]] as [[String : Any]]
                    
                    DispatchQueue.main.async {
                        self?.loadingScreenView.isHidden = false
                    }
                    self?.workerSubmitClaim.submitClaim(diagnosis: diagnosis, fraudChecked: fraud, declarationsChecked: declaration, policyNumber: number, claimType: type, withIncidentDate: date, conditionCode: conditionCode as? Int ?? 0, subConditionCode: subconditionCode as? Int ?? 0, subConditionCodeText: subconditionCodeText, savedHospitalId: savedHospitalId , hospitalId: hospitalId , vetTypehead: vetTypehead , withFiles: fileDictionary) { [weak self] resultUser in
                        switch resultUser {
                        case .success(let value):
                            
                            DispatchQueue.main.async {
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                self?.submittedClaimView.isHidden = false
                                
                                self?.defaults.setValue(self!.selectedPet!.PetName, forKey: "submittedClaimPet")
                                self?.defaults.setValue(value.ClaimId, forKey: "submittedClaimId")

                                self?.keychain.set("true", forKey: "isSubmitted")
                                
                                self?.submitClaimViewWellness.imageViewCheckboxFraud.image = UIImage(named: "empty")
                                self?.submitClaimView.imageViewCheckboxFraud.image = UIImage(named: "empty")
                                self?.submitClaimViewWellness.imageViewCheckboxAgree.image = UIImage(named: "empty")
                                self?.submitClaimView.imageViewCheckboxAgree.image = UIImage(named: "empty")
                                
                                self?.submittedClaimView.labelDetails.text = "\(String(describing: self!.selectedPet!.PetName))'s claim #\(value.ClaimId) has been successfully submitted and is now being reviewed. You will be notified once your claim has been processed."
                                
                                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.CLAIMS_UPDATE_NOTIFICAITON), object: nil)
                                
                                Auth.auth().signInAnonymously { authResult, error in
                                    if self?.isPDF[0] == true {
                                        if self?.isPDF[1] == true {
                                            if self?.isPDF[2] == true {
                                                if self?.isPDF[3] == true {
                                                    //1-pdf
                                                    //2-pdf
                                                    //3-pdf
                                                    //4-pdf
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).pdf",
                                                            "1": "\(nameImageSecond).pdf",
                                                            "2": "\(nameImageThird).pdf",
                                                            "3": "\(nameImageFourth).pdf"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                } else {
                                                    //1-pdf
                                                    //2-pdf
                                                    //3-pdf
                                                    //4-img
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).pdf",
                                                            "1": "\(nameImageSecond).pdf",
                                                            "2": "\(nameImageThird).pdf",
                                                            "3": "\(nameImageThird).pdf",
                                                            "3": "\(nameImageFourth).img"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                }
                                            } else {
                                                //1-pdf
                                                //2-pdf
                                                //3-img
                                                //4-pdf
                                                if self?.isPDF[3] == true {
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).pdf",
                                                            "1": "\(nameImageSecond).pdf",
                                                            "2": "\(nameImageThird).img",
                                                            "3": "\(nameImageFourth).pdf"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                } else {
                                                    //1-pdf
                                                    //2-pdf
                                                    //3-img
                                                    //4-img
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).pdf",
                                                            "1": "\(nameImageSecond).pdf",
                                                            "2": "\(nameImageThird).pdf",
                                                            "3": "\(nameImageThird).img",
                                                            "3": "\(nameImageFourth).img"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if self?.isPDF[2] == true {
                                                if self?.isPDF[3] == true {
                                                    //1-pdf
                                                    //2-img
                                                    //3-pdf
                                                    //4-pdf
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).pdf",
                                                            "1": "\(nameImageSecond).img",
                                                            "2": "\(nameImageThird).pdf",
                                                            "3": "\(nameImageFourth).pdf"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                } else {
                                                    //1-pdf
                                                    //2-img
                                                    //3-pdf
                                                    //4-img
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).pdf",
                                                            "1": "\(nameImageSecond).img",
                                                            "2": "\(nameImageThird).pdf",
                                                            "3": "\(nameImageThird).img"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                }
                                            } else {
                                                //1-pdf
                                                //2-img
                                                //3-img
                                                //4-pdf
                                                if self?.isPDF[3] == true {
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).pdf",
                                                            "1": "\(nameImageSecond).img",
                                                            "2": "\(nameImageThird).img",
                                                            "3": "\(nameImageFourth).pdf"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                } else {
                                                    //1-pdf
                                                    //2-img
                                                    //3-img
                                                    //4-img
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).pdf",
                                                            "1": "\(nameImageSecond).img",
                                                            "2": "\(nameImageThird).img",
                                                            "3": "\(nameImageThird).img"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if self?.isPDF[1] == true {
                                            if self?.isPDF[2] == true {
                                                if self?.isPDF[3] == true {
                                                    //1-img
                                                    //2-pdf
                                                    //3-pdf
                                                    //4-pdf
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).img",
                                                            "1": "\(nameImageSecond).pdf",
                                                            "2": "\(nameImageThird).pdf",
                                                            "3": "\(nameImageFourth).pdf"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                } else {
                                                    //1-img
                                                    //2-pdf
                                                    //3-pdf
                                                    //4-img
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).img",
                                                            "1": "\(nameImageSecond).pdf",
                                                            "2": "\(nameImageThird).pdf",
                                                            "3": "\(nameImageThird).img"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                }
                                            } else {
                                                //1-img
                                                //2-pdf
                                                //3-img
                                                //4-pdf
                                                if self?.isPDF[3] == true {
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).img",
                                                            "1": "\(nameImageSecond).pdf",
                                                            "2": "\(nameImageThird).img",
                                                            "3": "\(nameImageFourth).pdf"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                } else {
                                                    //1-img
                                                    //2-pdf
                                                    //3-img
                                                    //4-img
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).img",
                                                            "1": "\(nameImageSecond).pdf",
                                                            "2": "\(nameImageThird).img",
                                                            "3": "\(nameImageThird).img"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if self?.isPDF[2] == true {
                                                if self?.isPDF[3] == true {
                                                    //1-img
                                                    //2-img
                                                    //3-pdf
                                                    //4-pdf
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).img",
                                                            "1": "\(nameImageSecond).img",
                                                            "2": "\(nameImageThird).pdf",
                                                            "3": "\(nameImageFourth).pdf"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                } else {
                                                    //1-img
                                                    //2-img
                                                    //3-pdf
                                                    //4-img
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).img",
                                                            "1": "\(nameImageSecond).img",
                                                            "2": "\(nameImageThird).pdf",
                                                            "3": "\(nameImageThird).img"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                }
                                            } else {
                                                //1-img
                                                //2-img
                                                //3-img
                                                //4-pdf
                                                if self?.isPDF[3] == true {
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).img",
                                                            "1": "\(nameImageSecond).img",
                                                            "2": "\(nameImageThird).img",
                                                            "3": "\(nameImageFourth).pdf"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                } else {
                                                    //1-img
                                                    //2-img
                                                    //3-img
                                                    //4-img
                                                    self?.db.collection("user_claims_info").document("\(value.ClaimId)").setData([
                                                        "app_version": appVersion ?? "",
                                                        "claim#": "\(value.ClaimId)",
                                                        "date_time": currentDateTime,
                                                        "device_info": UIDevice.modelName,
                                                        "os_version": systemVersion,
                                                        "policy#": self!.policyNumberWhole,
                                                        "user": usernameLocal ?? "",
                                                        "filenames": [
                                                            "0": "\(nameImage).img",
                                                            "1": "\(nameImageSecond).img",
                                                            "2": "\(nameImageThird).img",
                                                            "3": "\(nameImageThird).img"
                                                        ]
                                                    ]) { err in
                                                        if let err = err {
                                                            print("Error writing document: \(err)")
                                                        } else {
                                                            print("Document successfully written!")
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                //Ask for AppStore review
                                StoreReviewHelper.checkAndAskForReview()

                                self?.keychain.set("true", forKey: "isSubmitted")
                                self?.navigationController?.navigationBar.isHidden = true
                                let tapSubmitNewClaim = UITapGestureRecognizer(target: self, action:  #selector(self?.submitNewClaim))
                                self?.submittedClaimView.buttonSubmitClaim.addGestureRecognizer(tapSubmitNewClaim)
                            }
                        case .failure(let error):
                            DispatchQueue.main.async {
                                self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                if self?.isWellness == true {
                                    self?.submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                                }
                                if self?.isWellness == false {
                                    self?.submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimView.buttonSubmitClaim.isEnabled = true
                                }
                            }
                        }
                    } onErrorHandler: { [weak self] error  in
                        switch error {
                        case .success(_):
                            Analytics.logEvent(Constants.FirebaseEvents.submitClaimError, parameters: nil)
                            DispatchQueue.main.async {
                                self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                if self?.isWellness == true {
                                    self?.submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                                }
                                if self?.isWellness == false {
                                    self?.submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimView.buttonSubmitClaim.isEnabled = true
                                }
                            }
                        case .failure(let error):
                            DispatchQueue.main.async {
                                self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                self?.stopLoadingWholeScreen()
                                self?.loadingScreenView.isHidden = true
                                if self?.isWellness == true {
                                    self?.submitClaimViewWellness.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimViewWellness.buttonSubmitClaim.isEnabled = true
                                }
                                if self?.isWellness == false {
                                    self?.submitClaimView.buttonSubmitClaim.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)
                                    self?.submitClaimView.buttonSubmitClaim.isEnabled = true
                                }
                            }
                        }
                    }
                }
            } onErrorHandler: { [weak self] error in
                switch error {
                case .success(let errorString):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: errorString, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        self?.stopLoadingWholeScreen()
                        self?.loadingScreenView.isHidden = true
                    }
                case .failure(_):
                    Analytics.logEvent(Constants.FirebaseEvents.submitClaimError, parameters: nil)
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        self?.stopLoadingWholeScreen()
                        self?.loadingScreenView.isHidden = true
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.noInternetError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                self.stopLoadingWholeScreen()
                self.loadingScreenView.isHidden = true
            }
        }
    }
    
    @objc func submitNewClaim() {
        
        self.keychain.set("true", forKey: "isSubmitted")
        
        self.submittedClaimView.isHidden = true
        self.submitClaimViewWellness.isHidden = true
        self.submitClaimView.isHidden = true
        self.defaults.setValue(nil, forKey: "submittedClaimPet")
        self.defaults.setValue(nil, forKey: "submittedClaimId")
        
        isService = false
        isPickDate = false
        isUploadFile = false
        isWellness = false
        isReasonWritten = false
        isCondition = false
        isVetClinic = false
        isDiagnosis = false
        isUploadedDocument = false
        twoUploadedDocuments = false
        canceledPolicies = false
        isPrimaryReasonSelected = false
        isConditionCodeText = false
        
        isPolicyChecked = false
        isFraudChecked = false
        
        isSearchedSelected = false
        isVetSavedSelected = false
        isOneLabelTitle = false
        
        imageDocument = nil
        imageSecondDocument = nil
        imageThirdDocument = nil
        imageFourthDocument = nil
        
        selectedCondition = [String]()
        selectedOptionsForEndpoint = [String:Any]()
        
        selectedOptionsVet = [String:Any]()
        
        selectedReason = ""
        selectedConditionShow = ""
        
        selectedOptions = [0,0,0,0,0,0,0,0]
        selectedIndexes = [Int]()
        selectedIndexesReasons = [Int]()
        selectedIndexesConditions = [Int]()
        
        allVetsSearch = [String]()
        allVetsSearchCities = [String]()
        allVetsSearchIds = [String]()
        heightVetSearch = 0.0
        
        allVetsSaved = [String]()
        allVetsSavedCities = [String]()
        allVetsSavedIds = [String]()
        heightVetSaved = 0.0
        self.navigationController?.navigationBar.isHidden = false
        
        self.navigationItem.title = Constants.titlesScreens.newClaim
        self.navigationItem.leftBarButtonItem = nil
        buttonNext.isHidden = true
        self.tableView.reloadData()
        self.tableView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
        
    }
    
    @objc func doneClaim() {
        self.submittedClaimView.isHidden = true
        self.submitClaimViewWellness.isHidden = true
        self.submitClaimView.isHidden = true
        self.defaults.setValue(nil, forKey: "submittedClaimPet")
        self.defaults.setValue(nil, forKey: "submittedClaimId")
        
        isService = false
        isPickDate = false
        isUploadFile = false
        isWellness = false
        isReasonWritten = false
        isCondition = false
        isVetClinic = false
        isDiagnosis = false
        isUploadedDocument = false
        twoUploadedDocuments = false
        canceledPolicies = false
        isPrimaryReasonSelected = false
        isConditionCodeText = false
        
        isPolicyChecked = false
        isFraudChecked = false
        
        isSearchedSelected = false
        isVetSavedSelected = false
        isOneLabelTitle = false
        
        imageDocument = nil
        imageSecondDocument = nil
        imageThirdDocument = nil
        imageFourthDocument = nil
        
        selectedCondition = [String]()
        selectedOptionsForEndpoint = [String:Any]()
        
        selectedOptionsVet = [String:Any]()
        
        selectedReason = ""
        selectedConditionShow = ""
        
        selectedOptions = [0,0,0,0,0,0,0,0]
        selectedIndexes = [Int]()
        selectedIndexesReasons = [Int]()
        selectedIndexesConditions = [Int]()
        
        allVetsSearch = [String]()
        allVetsSearchCities = [String]()
        allVetsSearchIds = [String]()
        heightVetSearch = 0.0
        
        allVetsSaved = [String]()
        allVetsSavedCities = [String]()
        allVetsSavedIds = [String]()
        heightVetSaved = 0.0
        self.navigationController?.navigationBar.isHidden = false
        
        self.navigationItem.title = Constants.titlesScreens.newClaim
        self.navigationItem.leftBarButtonItem = nil
        buttonNext.isHidden = true
        self.tabBarController?.selectedIndex = 0 //you can select another tab if needed
        let vc = HomeViewController(withCoreDataStack: CoreDataStack())
        if let navController = tabBarController?.viewControllers?[0] as? UINavigationController {
           navController.pushViewController(vc, animated: true)
        }
    }
    
    @objc private func back(){
        if selectedOptions[0] == 1 && isService == true {
            if selectedOptions[1] == 1 && isPickDate == true {
                if selectedOptions[2] == 1 && isUploadFile == true {
                    if selectedOptions[3] == 1 && isReasonWritten == true {
                        if selectedOptions[4] == 1 && isCondition == true {
                            if selectedOptions[5] == 1 && isVetClinic == true {
                                if selectedOptions[6] == 1 && isDiagnosis == true {
                                    if isWellness == true {
                                        if submitClaimViewWellness.isHidden {
                                            selectedOptions[6] = 0
                                            isDiagnosis = false
                                        } else {
                                            submitClaimViewWellness.isHidden = true
                                        }
                                    } else {
                                        if submitClaimView.isHidden {
                                            selectedOptions[6] = 0
                                            isDiagnosis = false
                                        } else {
                                            submitClaimView.isHidden = true
                                        }
                                    }
                                } else {
                                    if isWellness == true {
                                        selectedOptions[4] = 0
                                        if selectedOptionsForEndpoint["Condition"] != nil || selectedOptionsForEndpoint["ConditionText"] != nil {
                                            self.buttonNext.isEnabled = true
                                            self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                        }
                                        isCondition = false
                                        selectedOptions[3] = 0
                                        if selectedOptionsForEndpoint["PrimaryReason"] != nil {
                                            self.buttonNext.isEnabled = true
                                            self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                        }
                                        isReasonWritten = false
                                    }
                                    selectedOptions[5] = 0
                                    if selectedOptionsForEndpoint["VetClinicSecond"] != nil && selectedOptionsForEndpoint["VetClinic"] != nil && selectedOptionsForEndpoint["VetTypehead"] != nil {
                                        self.buttonNext.isEnabled = true
                                        self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                    }
                                    if selectedOptionsForEndpoint["Condition"] != nil || selectedOptionsForEndpoint["ConditionText"] != nil {
                                        self.buttonNext.isEnabled = true
                                        self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                    }
                                    isVetClinic = false
                                }
                            } else {
                                selectedOptions[4] = 0
                                if (selectedOptionsForEndpoint["Condition"] != nil || selectedOptionsForEndpoint["ConditionText"] != nil) || selectedOptionsForEndpoint["PrimaryReason"] != nil {
                                    self.buttonNext.isEnabled = true
                                    self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                }
                                isCondition = false
                            }
                        } else {
                            selectedOptions[3] = 0
                            if selectedOptionsForEndpoint["PrimaryReason"] != nil {
                                self.buttonNext.isEnabled = true
                                self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                            } else {
                                if isUploadFile == true {
                                    self.buttonNext.isEnabled = true
                                    self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                }
                            }
                            isReasonWritten = false
                        }
                    } else {
                        selectedOptions[2] = 0
                        self.isOneLabelTitle = false
                        isUploadFile = false
                        self.buttonNext.isEnabled = true
                        self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                    }
                } else {
                    selectedOptions[1] = 0
                    isPickDate = false
                    buttonNext.isHidden = false
                }
            } else {
                selectedOptions[0] = 0
                selectedIndexes = []
                selectedIndexesReasons = []
                selectedIndexesConditions = []
                selectedOptionsForEndpoint = [String:Any]()
                isService = false
                buttonNext.isHidden = true
                navigationItem.leftBarButtonItem = nil
            }
        }
        tableView.reloadData()
    }
    
    @IBAction private func actionNext(_ sender: UIButton) {
        if dateFromPicker == nil {
            dateFromPicker = Date()
        }
        
        guard selectedOptions[0] == 1 && isService == true && !selectedIndexes.isEmpty else {
            
            if selectedIndexes.isEmpty {
                self.buttonNext.isEnabled = false
                self.buttonNext.backgroundColor = UIColor.gray
            } else {
                selectedOptions[0] = 1
                isService = true
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.buttonNext.isEnabled = false
                    self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                }
            }
            return
        }
        
        guard selectedOptions[1] == 1 && isPickDate == true else {
            selectedOptions[1] = 1
            isPickDate = true
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            return
        }
        
        guard selectedOptions[2] == 1 && isUploadFile == true else {
            
            DispatchQueue.main.async {
                self.startLoadingScreen()
            }
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            selectedOptionsForEndpoint["IncidentDate"] = formatter.string(from: dateFromPicker!)
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "MM/dd/YYYY"
            
            guard let policyNumber = selectedOptionsForEndpoint["PolicyLineNumber"], let claimType = selectedOptionsForEndpoint["ClaimType"],
                  let dateString = selectedOptionsForEndpoint["IncidentDate"] else {
                return
            }
            
            if ReachabilityClass.isConnectedToNetwork(){
                workerEligibility.getValidPolicy(withPolicyNumber: policyNumber as! Int, withClaimType: claimType as! Int, withIncidentDate: dateString as! String) { [weak self] resultUser in
                    switch resultUser {
                    case .success(let resultUser):
                        if resultUser == true {
                            self?.selectedOptions[2] = 1
                            self?.isUploadFile = true
                            self?.isOneLabelTitle = true
                            if self?.selectedOptionsForEndpoint["ClaimType"] as! Int == 2 {
                                self?.selectedOptions[4] = 1
                                self?.isCondition = true
                                self?.selectedOptions[5] = 1
                                self?.isVetClinic = true
                                self?.isPickDate = true
                                self?.isWellness = true
                                self?.selectedOptionsForEndpoint["PrimaryReason"] = nil
                                self?.selectedOptionsForEndpoint["Condition"] = nil
                                self?.selectedOptionsForEndpoint["ConditionText"] = nil
                                self?.selectedReason = ""
                                self?.selectedConditionShow = ""
                            } else {
                                self?.isWellness = false
                                self?.selectedOptions[4] = 0
                                self?.isCondition = false
                                self?.selectedOptions[5] = 0
                                self?.isVetClinic = false
                                self?.isPickDate = true
                            }
                            DispatchQueue.main.async {
                                self?.tableView.reloadData()
                                self?.stopLoadingScreen()
                                if self?.imageDocument != nil || self?.imageSecondDocument != nil || self?.imageThirdDocument != nil || self?.imageFourthDocument != nil {
                                    self?.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                    self?.buttonNext.isEnabled = true
                                } else {
                                    self?.buttonNext.isEnabled = false
                                    self?.buttonNext.backgroundColor = UIColor.gray
                                }
                            }
                        } else {
                            if self?.selectedOptionsForEndpoint["ClaimType"] as! Int == 0 {
                                DispatchQueue.main.async {
                                    self?.showAlert(withTitle: "", message: "Our records show that \(String(describing: self!.selectedPet!.PetName)) did not have Accident coverage on the date of treatment you specified (\(formatter2.string(from: (self?.dateFromPicker!)!)))", buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                    self?.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                    self?.buttonNext.isEnabled = true
                                    self?.stopLoadingScreen()
                                }
                            }
                            if self?.selectedOptionsForEndpoint["ClaimType"] as! Int == 1 {
                                DispatchQueue.main.async {
                                    self?.showAlert(withTitle: "", message: "Our records show that \(String(describing: self!.selectedPet!.PetName)) did not have Illness coverage on the date of treatment you specified (\(formatter2.string(from: (self?.dateFromPicker!)!)))", buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                    self?.stopLoadingScreen()
                                    self?.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                    self?.buttonNext.isEnabled = true
                                }
                            }
                            if self?.selectedOptionsForEndpoint["ClaimType"] as! Int == 2 {
                                Analytics.logEvent(Constants.FirebaseEvents.wellnessCoverageUnavailable, parameters: nil)
                                DispatchQueue.main.async {
                                    self?.showAlert(withTitle: "", message: "Our records show that \(String(describing: self!.selectedPet!.PetName)) did not have Wellness coverage on the date of treatment you specified (\(formatter2.string(from: (self?.dateFromPicker!)!)))", buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                    self?.stopLoadingScreen()
                                    self?.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                    self?.buttonNext.isEnabled = true
                                }
                            }
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self?.stopLoadingScreen()
                            self?.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                            self?.buttonNext.isEnabled = true
                        }
                    }
                } onErrorHandler: { error in
                    switch error {
                    case .success(let resultError):
                        DispatchQueue.main.async {
                            self.showAlert(withTitle: "", message: resultError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self.stopLoadingScreen()
                            
                            //Testing for 500 error
//                            self.selectedOptions[2] = 1
//                            self.isUploadFile = true
//                            self.tableView.reloadData()
                                                        
                            self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                            self.buttonNext.isEnabled = true
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            self.stopLoadingScreen()
                            self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                            self.buttonNext.isEnabled = true
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(withTitle: "", message: Constants.errorMessages.generalError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    self.stopLoadingScreen()
                    self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                    self.buttonNext.isEnabled = true
                }
            }
            if imageDocument == nil {
                self.buttonNext.isEnabled = false
                self.buttonNext.backgroundColor = UIColor.gray
            }
            
            return
        }
        
        if isWellness == true {
            self.selectedOptions[4] = 1
            self.isCondition = true
            self.selectedOptions[5] = 1
            self.isVetClinic = true
            self.isPickDate = true
            isWellness = true
        }
        if imageDocument != nil {
            guard selectedOptions[3] == 1 && isReasonWritten == true else {
                selectedOptions[3] = 1
                self.isOneLabelTitle = false
//                selectedOptionsForEndpoint["PrimaryReason"] = primaryReasonsArray[0]
                isReasonWritten = true
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                }

                if selectedOptionsForEndpoint["PrimaryReason"] != nil {
                    self.buttonNext.isEnabled = true
                    self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                } else {
                    if selectedOptionsForEndpoint["VetClinicSecond"] != nil || selectedOptionsForEndpoint["VetClinic"] != nil || selectedOptionsForEndpoint["VetTypehead"] != nil {
                        self.buttonNext.isEnabled = true
                        self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                    } else {
                        self.buttonNext.isEnabled = false
                        self.buttonNext.backgroundColor = UIColor.gray
                    }
                }
                return
            }
        }
        
        guard selectedOptions[4] == 1 && isCondition == true else {
            selectedOptions[4] = 1
            isCondition = true
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
            if selectedOptionsForEndpoint["ConditionText"] == nil {
                if selectedOptionsForEndpoint["Condition"] == nil {
                    self.buttonNext.isEnabled = false
                    self.buttonNext.backgroundColor = UIColor.gray
                } else {
                    if selectedReason == primaryReasonsArray.last {
                        self.buttonNext.isEnabled = false
                        self.buttonNext.backgroundColor = UIColor.gray
                    } else {
                        self.buttonNext.isEnabled = true
                        self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                    }
                }
            } else {
                if selectedOptionsForEndpoint["Condition"] == nil {
                    self.buttonNext.isEnabled = false
                    self.buttonNext.backgroundColor = UIColor.gray
                } else {
                    if selectedReason == primaryReasonsArray.last {
                        self.buttonNext.isEnabled = false
                        self.buttonNext.backgroundColor = UIColor.gray
                    } else {
                        self.buttonNext.isEnabled = true
                        self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                    }
                }
            }
            return
        }
        
        guard (selectedOptions[5] == 1 && isVetClinic == true) || (selectedOptions[5] == 1 && isVetClinic == true && isConditionCodeText == true) else {
            if isConditionCodeText == false {
                selectedOptionsForEndpoint["ConditionText"] = nil
                selectedOptions[5] = 1
                isVetClinic = true
                if selectedOptionsForEndpoint["VetClinicSecond"] != nil || selectedOptionsForEndpoint["VetClinic"] != nil || selectedOptionsForEndpoint["VetTypehead"] != nil {
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                } else {
                    DispatchQueue.main.async {
                        self.buttonNext.isEnabled = false
                        self.buttonNext.backgroundColor = UIColor.gray
                        self.tableView.reloadData()
                    }
                }
            } else {
                if isConditionCodeText == true {
                    selectedConditionShow = ""
                    selectedOptionsForEndpoint["Condition"] = 0
                    selectedOptions[5] = 1
                    isVetClinic = true
                    if selectedOptionsForEndpoint["VetClinicSecond"] != nil || selectedOptionsForEndpoint["VetClinic"] != nil || selectedOptionsForEndpoint["VetTypehead"] != nil {
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.buttonNext.isEnabled = false
                            self.buttonNext.backgroundColor = UIColor.gray
                            self.tableView.reloadData()
                        }
                    }
                } else {
                    selectedOptionsForEndpoint["ConditionText"] = nil
                    selectedOptions[5] = 1
                    isVetClinic = true
                    DispatchQueue.main.async {
                        self.buttonNext.isEnabled = false
                        self.buttonNext.backgroundColor = UIColor.gray
                        self.tableView.reloadData()
                    }
                }
            }
            return
        }
        
        guard selectedOptions[6] == 1 && isDiagnosis == true else {
            
            if selectedOptionsForEndpoint["VetClinicSecond"] == nil && selectedOptionsForEndpoint["VetClinic"] == nil && selectedOptionsForEndpoint["VetTypehead"] == nil {
                DispatchQueue.main.async {
                    self.buttonNext.isEnabled = false
                    self.buttonNext.backgroundColor = UIColor.gray
                }
            } else {
                Analytics.logEvent(Constants.FirebaseEvents.vetNotInList, parameters: nil)
                selectedOptions[6] = 1
                isDiagnosis = true
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.buttonNext.isEnabled = true
                    self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                }
            }
            return
        }
        
        guard selectedOptions[7] == 1 else {
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "MM/dd/YYYY"
            
            if isWellness == true {
                self.submitClaimViewWellness.isHidden = false
                submitClaimViewWellness.labelName.text = selectedOptionsForEndpoint["name"] as? String
                if selectedOptionsForEndpoint["ClaimType"] as! Int == 0 {
                    submitClaimViewWellness.labelClaimTypeDetails.text = "Accident"
                }
                if selectedOptionsForEndpoint["ClaimType"] as! Int == 1 {
                    submitClaimViewWellness.labelClaimTypeDetails.text = "Illness"
                }
                if selectedOptionsForEndpoint["ClaimType"] as! Int == 2 {
                    submitClaimViewWellness.labelClaimTypeDetails.text = "Wellness"
                }
                submitClaimViewWellness.labelServiceDateDetails.text = formatter2.string(from: dateFromPicker!)
                if selectedOptionsVet["VetClinic"] != nil {
                    submitClaimViewWellness.labelVetClinicDetails.text = selectedOptionsVet["VetClinic"] as? String
                }
                if selectedOptionsVet["VetClinicSecond"] != nil {
                    submitClaimViewWellness.labelVetClinicDetails.text = selectedOptionsVet["VetClinicSecond"] as? String
                }
                if selectedOptionsVet["VetTypehead"] != nil {
                    submitClaimViewWellness.labelVetClinicDetails.text = selectedOptionsVet["VetTypehead"] as? String
                }

                submitClaimViewWellness.labelAdditionalInfoDetails.text = selectedOptionsForEndpoint["Diagnosis"] as? String
            }
            if isWellness == false {
                self.submitClaimView.isHidden = false
                submitClaimView.labelName.text = selectedOptionsForEndpoint["name"] as? String
                if selectedOptionsForEndpoint["ClaimType"] as! Int == 0 {
                    submitClaimView.labelClaimTypeDetails.text = "Accident"
                }
                if selectedOptionsForEndpoint["ClaimType"] as! Int == 1 {
                    submitClaimView.labelClaimTypeDetails.text = "Illness"
                }
                if selectedOptionsForEndpoint["ClaimType"] as! Int == 2 {
                    submitClaimView.labelClaimTypeDetails.text = "Wellness"
                }
                submitClaimView.labelServiceDateDetails.text = formatter2.string(from: dateFromPicker!)
                if selectedOptionsForEndpoint["ConditionText"] != nil {
                    submitClaimView.labelReasonForVisitDetails.text = "\(selectedReason)\n\(String(describing: selectedOptionsForEndpoint["ConditionText"]!))"
                } else {
                    submitClaimView.labelReasonForVisitDetails.text = "\(selectedReason)\n\(selectedConditionShow)"
                }
                if selectedOptionsVet["VetClinic"] != nil {
                    submitClaimView.labelVetClinicDetails.text = selectedOptionsVet["VetClinic"] as? String
                }
                if selectedOptionsVet["VetClinicSecond"] != nil {
                    submitClaimView.labelVetClinicDetails.text = selectedOptionsVet["VetClinicSecond"] as? String
                }
                if selectedOptionsVet["VetTypehead"] != nil {
                    submitClaimView.labelVetClinicDetails.text = selectedOptionsVet["VetTypehead"] as? String
                }

                submitClaimView.labelAdditionalInfoDetails.text = selectedOptionsForEndpoint["Diagnosis"] as? String
            }
            
            self.navigationItem.title = Constants.titlesScreens.reviewClaim
//            submittedClaimView.isHidden = false
//            self.navigationController?.navigationBar.isHidden = true
            return
        }
    }
}

extension SubmitClaimViewController: UploadClaimDelegate, ImageScannerControllerDelegate, UIDocumentPickerDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imageScannerController(_ scanner: ImageScannerController, didFinishScanningWithResults results: ImageScannerResults) {
        //print(results.croppedScan.image)
        
        DispatchQueue.main.async {
            self.startLoadingScreen()
        }

        
        if imageDocument != nil && imageSecondDocument == nil && imageThirdDocument == nil && imageFourthDocument == nil {
            imageSecondDocument = results.croppedScan.image
            imageSecondDocument = imageSecondDocument!.resizeImage(image: imageSecondDocument!, newWidth: 1300)
            isPDF[1] = false
            
            DispatchQueue.main.async {
                self.selectedOptions[3] = 1
                self.tableView.reloadData()
                self.stopLoadingScreen()
            }
        } else {
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil {
                imageThirdDocument = results.croppedScan.image
                imageThirdDocument = imageThirdDocument!.resizeImage(image: imageThirdDocument!, newWidth: 1300)
                isPDF[2] = false
                
                DispatchQueue.main.async {
                    self.selectedOptions[3] = 1
                    self.tableView.reloadData()
                    self.stopLoadingScreen()
                }
            } else {
                if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                    imageFourthDocument = results.croppedScan.image
                    imageFourthDocument = imageFourthDocument!.resizeImage(image: imageFourthDocument!, newWidth: 1300)
                    isPDF[3] = false
                    
                    DispatchQueue.main.async {
                        self.selectedOptions[3] = 1
                        let indexPath = IndexPath(item: 3, section: 0)
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                        self.stopLoadingScreen()
                    }
                    
                } else {
                    imageDocument = results.croppedScan.image
                    imageDocument = imageDocument!.resizeImage(image: imageDocument!, newWidth: 1300)
                    isPDF[0] = false
                    
                    DispatchQueue.main.async {
                        self.selectedOptions[3] = 1
                        self.isUploadedDocument = true
                        self.tableView.reloadData()
                        self.stopLoadingScreen()
                    }
                }
            }
        }
        
        scanner.dismiss(animated: true)
        
    }
    
    func imageScannerControllerDidCancel(_ scanner: ImageScannerController) {
        scanner.dismiss(animated: true)
    }
    
    func imageScannerController(_ scanner: ImageScannerController, didFailWithError error: Error) {
        print(error)
    }
    
    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }

        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)

            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)

            ctx.cgContext.drawPDFPage(page)
        }

        return img
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard controller.documentPickerMode == .open, let url = urls.first, url.startAccessingSecurityScopedResource() else { return }
        defer {
            DispatchQueue.main.async {
                url.stopAccessingSecurityScopedResource()
            }
        }

        //print(url.pathExtension)
        startLoadingScreen()
        if url.pathExtension == "pdf" {
            let url = URL(fileURLWithPath: url.path)
            guard let image = drawPDFfromURL(url: url) else { return }

            if imageDocument != nil && imageSecondDocument == nil && imageThirdDocument == nil && imageFourthDocument == nil {
                imageSecondDocument = image
                imageSecondDocument = imageSecondDocument!.resizeImage(image: imageSecondDocument!, newWidth: 1300)
                isPDF[1] = true

                let fileData = try? Data.init(contentsOf: url)
                fileSecondString = (fileData?.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0)))!
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.stopLoadingScreen()
                }
            } else {
                if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil {
                    imageThirdDocument = image
                    imageThirdDocument = imageThirdDocument!.resizeImage(image: imageThirdDocument!, newWidth: 1300)
                    isPDF[2] = true
                    
                    let fileData = try? Data.init(contentsOf: url)
                    fileThirdString = (fileData?.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0)))!
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.stopLoadingScreen()
                    }
                } else {
                    if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                        imageFourthDocument = image
                        imageFourthDocument = imageFourthDocument!.resizeImage(image: imageFourthDocument!, newWidth: 1300)
                        isPDF[3] = true

                        let fileData = try? Data.init(contentsOf: url)
                        fileFourthString = (fileData?.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0)))!
                        DispatchQueue.main.async { [self] in
                            let indexPath = IndexPath(item: 3, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .fade)
                            self.stopLoadingScreen()
                        }
                    } else {
                        imageDocument = image
                        imageDocument = imageDocument!.resizeImage(image: imageDocument!, newWidth: 1300)
                        isPDF[0] = true
                        
                        let fileData = try? Data.init(contentsOf: url)
                        fileFirstString = (fileData?.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0)))!
                        DispatchQueue.main.async {
                            self.isUploadedDocument = true
                            self.tableView.reloadData()
                            self.stopLoadingScreen()
                        }
                    }
                }
            }
            
        } else {
            guard let image = UIImage(contentsOfFile: url.path) else { return }
            if imageDocument != nil && imageSecondDocument == nil && imageThirdDocument == nil && imageFourthDocument == nil {
                imageSecondDocument = image
                imageSecondDocument = imageSecondDocument!.resizeImage(image: imageSecondDocument!, newWidth: 1300)
                isPDF[1] = false
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.stopLoadingScreen()
                }
            } else {
                if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil {
                    imageThirdDocument = image
                    imageThirdDocument = imageThirdDocument!.resizeImage(image: imageThirdDocument!, newWidth: 1300)
                    isPDF[2] = false
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.stopLoadingScreen()
                    }
                } else {
                    if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                        imageFourthDocument = image
                        imageFourthDocument = imageFourthDocument!.resizeImage(image: imageFourthDocument!, newWidth: 1300)
                        isPDF[3] = false
                        
                        DispatchQueue.main.async { [self] in
                            let indexPath = IndexPath(item: 3, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .fade)
                            self.stopLoadingScreen()
                        }
                    } else {
                        imageDocument = image
                        imageDocument = imageDocument!.resizeImage(image: imageDocument!, newWidth: 1300)
                        isPDF[0] = false
                        
                        DispatchQueue.main.async {
                            self.isUploadedDocument = true
                            self.tableView.reloadData()
                            self.stopLoadingScreen()
                        }
                    }
                }
            }
        }
        controller.dismiss(animated: true)
    }

    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    // For Swift 4.2+
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else {
            return
            //fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        if imageDocument != nil && imageSecondDocument == nil && imageThirdDocument == nil && imageFourthDocument == nil {
            imageSecondDocument = image
            imageSecondDocument = imageSecondDocument!.resizeImage(image: imageSecondDocument ?? UIImage(), newWidth: 1300)
            isPDF[1] = false
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.stopLoadingScreen()
            }
        } else {
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil {
                imageThirdDocument = image
                imageThirdDocument = imageThirdDocument!.resizeImage(image: imageThirdDocument ?? UIImage(), newWidth: 1300)
                isPDF[2] = false
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.stopLoadingScreen()
                }
            } else {
                if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                    imageFourthDocument = image
                    imageFourthDocument = imageFourthDocument!.resizeImage(image: imageFourthDocument ?? UIImage(), newWidth: 1300)
                    isPDF[3] = false
                    
                    DispatchQueue.main.async {
                        let indexPath = IndexPath(item: 3, section: 0)
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                        self.stopLoadingScreen()
                    }
                } else {
                    imageDocument = image
                    imageDocument = imageDocument!.resizeImage(image: imageDocument ?? UIImage(), newWidth: 1300)
                    isPDF[0] = false
                    
                    DispatchQueue.main.async {
                        self.isUploadedDocument = true
                        self.tableView.reloadData()
                        self.stopLoadingScreen()
                    }
                }
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        DispatchQueue.main.async {
            self.stopLoadingScreen()
        }
        dismiss(animated: true, completion:nil)
    }
    
    func deleteImage(atIndex: Int) {
        print(atIndex)
    }
}

extension SubmitClaimViewController: DatePickerDelegate {
    
    func isDatePicked(date: Date) {
        dateFromPicker = date
    }
}

extension SubmitClaimViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isService == true  {
            if isPickDate == true {
                if isUploadFile == true{
                    if isReasonWritten == true {
                        if isCondition == true {
                            if isVetClinic == true {
                                if isDiagnosis == true {
                                    return 2
                                } else {
                                    return 5
                                }
                            } else {
                                if selectedReason == primaryReasonsArray.last {
                                    return 2
                                } else {
                                    return selectedCondition.count + 2
                                }
                            }
                        } else {
                            return primaryReasonsArray.count + 2
                        }
                    } else {
                        if imageSecondDocument != nil {
                            return 5
                        } else {
                            return 3
                        }
                    }
                } else {
                    return 2
                }
            } else {
                return typeServicesArray.count + 1

            }
        } else {
            if allPetsCoreDate.count != 0 {
                if canceledPolicies == false {
                    return allPets.count + 2
                } else {
                    return canceledPets.count + 2
                }
            } else {
                return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            if UIDevice().userInterfaceIdiom == .pad {
                if UIScreen.main.nativeBounds.height == 2048 {
                    return 150.0
                } else {
                    return 200.0
                }
            } else {
                if isOneLabelTitle == true {
                    return 80.0
                } else {
                    return 110.0
                }
            }
        } else {
            if isService == true {
                if isPickDate == true {
                    if isUploadFile == true {
                        if isReasonWritten == true {
                            if isVetClinic == true {
                                if isDiagnosis == true {
                                    if UIDevice().userInterfaceIdiom == .pad {
                                        if UIScreen.main.nativeBounds.height == 2048 {
                                            return 100.0
                                        } else {
                                            return 120.0
                                        }
                                    } else {
                                        return 60.0
                                    }
                                } else {
                                    if UIDevice().userInterfaceIdiom == .pad {
                                        if indexPath.row == 1 {
                                            return 100.0
                                        } else {
                                            if isVetSavedSelected == true && indexPath.row == 2 {
                                                return CGFloat(heightVetSaved)
                                            }
                                            if isSearchedSelected == true && indexPath.row == 4 {
                                                return CGFloat(heightVetSearch)
                                            } else {
                                                if indexPath.row == 2 {
                                                    return 0.0
                                                }
                                                if indexPath.row == 4 {
                                                    return 0.0
                                                } else {
                                                    return 200.0
                                                }
                                            }
                                        }
                                    } else {
                                        if indexPath.row == 1 {
                                            return 50.0
                                        } else {
                                            if isVetSavedSelected == true && indexPath.row == 2 {
                                                return CGFloat(heightVetSaved)
                                            }
                                            if isSearchedSelected == true && indexPath.row == 4 {
                                                return CGFloat(heightVetSearch)
                                            } else {
                                                if indexPath.row == 2 {
                                                    return 0.0
                                                }
                                                if indexPath.row == 4 {
                                                    return 0.0
                                                } else {
                                                    return 100.0
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                if UIDevice().userInterfaceIdiom == .pad {
                                    if UIScreen.main.nativeBounds.height == 2048 {
                                        return 100.0
                                    } else {
                                        return 120.0
                                    }
                                } else {
                                    if indexPath.row == primaryReasonsArray.count {
                                        return 90.0
                                    } else {
                                        return 70.0
                                    }
                                }
                            }
                        } else {
                            if isUploadedDocument == true {
                                if UIDevice().userInterfaceIdiom == .pad {
                                    if indexPath.row == 1 {
                                        return 220.0
                                    } else {
                                        return 420.0
                                    }
                                } else {
                                    if indexPath.row == 1 {
                                        return 140.0
                                    } else {
                                        if indexPath.row == 4 {
                                            return 70.0
                                        } else {
                                            return 180.0
                                        }
                                    }
                                }
                            } else {
                                if UIDevice().userInterfaceIdiom == .pad {
                                    if indexPath.row == 1 {
                                        return 220.0
                                    } else {
                                        return 420.0
                                    }
                                } else {
                                    if indexPath.row == 3 {
                                        return 140.0
                                    } else {
                                        return 170.0
                                    }
                                }
                            }
                        }
                    } else {
                        if UIDevice().userInterfaceIdiom == .pad {
                            return 180.0
                        } else {
                            return 180.0
                        }
                    }
                } else {
                    if UIDevice().userInterfaceIdiom == .pad {
                        if UIScreen.main.nativeBounds.height == 2048 {
                            return 100.0
                        } else {
                            return 120.0
                        }
                    } else {
                        return 80.0
                    }
                }
            } else {
                if UIDevice().userInterfaceIdiom == .pad {
                    if UIScreen.main.nativeBounds.height == 2048 {
                        return 100.0
                    } else {
                        return 120.0
                    }
                } else {
                    return 80.0
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TitleTableViewCell = tableView.dequeueCell(TitleTableViewCell.self)
        let cell14: OneTitleTableViewCell = tableView.dequeueCell(OneTitleTableViewCell.self)
        let cell1: PetTableViewCell = tableView.dequeueCell(PetTableViewCell.self)
        let cell2: ShowCanceledPolciesTableViewCell = tableView.dequeueCell(ShowCanceledPolciesTableViewCell.self)
        let cell3: SelectTableViewCell = tableView.dequeueCell(SelectTableViewCell.self)
        let cell4: DatePickerTableViewCell = tableView.dequeueCell(DatePickerTableViewCell.self)
        let cell6: UploadClaimTableViewCell = tableView.dequeueCell(UploadClaimTableViewCell.self)
        let cell7: UploadedDocumentTableViewCell = tableView.dequeueCell(UploadedDocumentTableViewCell.self)
        let cell8: VetClinicTableViewCell = tableView.dequeueCell(VetClinicTableViewCell.self)
        let cell9: VetClinicSavetTableViewCell = tableView.dequeueCell(VetClinicSavetTableViewCell.self)
        let cell10: ReasonTableViewCell = tableView.dequeueCell(ReasonTableViewCell.self)
        let cell11: IncludedDocumentsTableViewCell = tableView.dequeueCell(IncludedDocumentsTableViewCell.self)
        
        let cell12: UploadClaimIPadTableViewCell = tableView.dequeueCell(UploadClaimIPadTableViewCell.self)
        
        let cell13: VetClinicPickerTableViewCell = tableView.dequeueCell(VetClinicPickerTableViewCell.self)

        cell.selectionStyle = .none
        cell1.selectionStyle = .none
        cell2.selectionStyle = .none
        cell3.selectionStyle = .none
        cell4.selectionStyle = .none
        cell6.selectionStyle = .none
        cell7.selectionStyle = .none
        cell8.selectionStyle = .none
        cell9.selectionStyle = .none
        cell10.selectionStyle = .none
        cell11.selectionStyle = .none
        cell12.selectionStyle = .none
        cell13.selectionStyle = .none
        cell14.selectionStyle = .none
        cell4.delegate = self
        cell6.delegate = self
        cell7.delegate = self
        cell7.delegate1 = self
        cell8.delegate = self
        cell9.delegate = self
        cell12.delegate = self
        cell13.delegate = self
        
        cell8.searchVetClinic.delegate = self
        //cell10.textViewReason.delegate = self
        cell10.delegate = self
        
        if isUploadFile == true {
            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
        }

        if indexPath.row == 0 {
            if selectedOptions[safe: 0] == 1 && isService == true {
                if selectedOptions[safe: 1] == 1 && isPickDate == true {
                    if selectedOptions[safe: 2] == 1 && isUploadFile == true {
                        if selectedOptions[safe: 3] == 1 && isReasonWritten == true {
                            if selectedOptions[safe: 4] == 1 && isCondition == true {
                                if selectedOptions[safe: 5] == 1 && isVetClinic == true {
                                    if selectedOptions[safe: 6] == 1 && isDiagnosis == true {
                                        DispatchQueue.main.async {
                                            self.stopLoadingScreen()
                                        }
                                        cell.configureCell(withTitle: Constants.titlesLabels.labelDiagnosis,withTitle2: Constants.titlesLabels.labelDiagnosis1, withTitle3: Constants.titlesLabels.labelDiagnosis2)
                                        return cell
                                    } else {
                                        cell.configureCell(withTitle: Constants.titlesLabels.labelVeterinaryClinic,withTitle2: Constants.titlesLabels.labelVeterinaryClinic1, withTitle3: "")
                                        return cell
                                    }
                                } else {
                                    cell.configureCell(withTitle: Constants.titlesLabels.labelWhatCondition,withTitle2: "", withTitle3: "")
                                    return cell
                                }
                            } else {
                                cell.configureCell(withTitle: Constants.titlesLabels.labelPrimaryReason,withTitle2: Constants.titlesLabels.labelPrimaryReason1, withTitle3: "")
                                return cell
                            }
                        } else {
                            cell14.configureCell(withTitle: Constants.titlesLabels.labelUploadInvoice)
                            return cell14
                        }
                    } else {
                        cell.configureCell(withTitle: Constants.titlesLabels.labelFirstDay,withTitle2: Constants.titlesLabels.labelFirstDay1, withTitle3: "")
                        return cell
                    }
                } else {
                    if selectedPet != nil {
                        cell.configureCell(withTitle: "What type of service was", withTitle2: " \(selectedPet!.PetName) at the vet for?", withTitle3: "")
                    }
                    return cell
                }
            } else {
                cell.configureCell(withTitle: Constants.titlesLabels.labelFirstWhoIs,withTitle2: "", withTitle3: "")
                return cell
            }
        }
        
        if selectedOptions[0] == 1 && isService == true {
            if selectedOptions[1] == 1 && isPickDate == true {
                if selectedOptions[2] == 1 && isUploadFile == true {
                    if selectedOptions[3] == 1 && isReasonWritten == true {
                        if selectedOptions[4] == 1 && isCondition == true {
                            if selectedOptions[5] == 1 && isVetClinic == true {
                                if selectedOptions[6] == 1 && isDiagnosis == true {
                                    if selectedOptionsForEndpoint["Diagnosis"] != nil {
                                        if selectedOptionsForEndpoint["Diagnosis"] as! String != "" {
                                            let diagnosis = selectedOptionsForEndpoint["Diagnosis"] as! String
                                            cell10.configureCell(withDiagnosis: diagnosis, isCondition: false)
                                        }
                                    } else {
                                        cell10.configureCell(withDiagnosis: "", isCondition: false)
                                    }
                                    return cell10
                                } else {
                                    if indexPath.row == 1 {
                                        if selectedOptionsVet["VetClinic"] != nil {
                                            let vetClinic = selectedOptionsVet["VetClinic"]
                                            cell9.configureCell(vetClinic: vetClinic as? String)
                                        }
                                        return cell9
                                    }
                                    if indexPath.row == 3 {
                                        if selectedOptionsVet["VetClinicSecond"] != nil {
                                            let vetClinic = selectedOptionsVet["VetClinicSecond"]
                                            cell8.configureCell(vetClinic: vetClinic as? String)
                                            return cell8
                                        }
                                        if selectedOptionsVet["VetTypehead"] != nil {
                                            let vetClinic = selectedOptionsVet["VetTypehead"]
                                            cell8.configureCell(vetClinic: vetClinic as? String)
                                            return cell8
                                        } else {
                                            return cell8
                                        }
                                    }
                                    if indexPath.row == 2 {
                                        if !allVetsSaved.isEmpty {
                                            cell13.configureCell(withClinics: allVetsSaved, withClinicsCities: allVetsSavedCities, withClinicsIDs: allVetsSavedIds, isSaved: true, index: indexPath.row)
                                            return cell13
                                        } else {
                                            return cell13
                                        }
                                    }
                                    if indexPath.row == 4 {
                                        if isSearchedSelected == true && selectedOptionsVet["VetTypehead"] != nil {
                                            cell13.configureCell(withClinics: allVetsSearch, withClinicsCities: allVetsSearchCities, withClinicsIDs: allVetsSearchIds, isSaved: false, index: indexPath.row)
                                        } else {
                                            cell13.configureCell(withClinics: [], withClinicsCities: [], withClinicsIDs: [], isSaved: false, index: indexPath.row)
                                        }
                                        return cell13
                                    } else {
                                        return cell8
                                    }
                                }
                            } else {
                                if selectedReason == primaryReasonsArray.last {
                                    DispatchQueue.main.async {
                                        if UIDevice().userInterfaceIdiom == .pad {
                                            cell3.constantWidth.constant = 320
                                        } else {
                                            cell3.constantWidth.constant = 170
                                        }
                                    }
                                    if selectedOptionsForEndpoint["ConditionText"] != nil {
                                        let condition = selectedOptionsForEndpoint["ConditionText"] as! String
                                        DispatchQueue.main.async {
                                            self.buttonNext.isEnabled = true
                                            self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                        }
                                        cell10.configureCell(withDiagnosis: condition, isCondition: true)
                                    } else {
                                        DispatchQueue.main.async {
                                            self.buttonNext.isEnabled = false
                                            self.buttonNext.backgroundColor = UIColor.gray
                                        }
                                        cell10.configureCell(withDiagnosis: "", isCondition: true)
                                    }
                                    return cell10
                                } else {
                                    if !selectedIndexesConditions.isEmpty {
                                        if selectedIndexesConditions[0] == indexPath.row {
                                            DispatchQueue.main.async {
                                                if UIDevice().userInterfaceIdiom == .pad {
                                                    cell3.constantWidth.constant = 320
                                                } else {
                                                    cell3.constantWidth.constant = 170
                                                }
                                            }
                                            cell3.configureCell(withTitle: selectedCondition[indexPath.row-1], isSelected: true, isLast: false)
                                            return cell3
                                        } else {
                                            DispatchQueue.main.async {
                                                if UIDevice().userInterfaceIdiom == .pad {
                                                    cell3.constantWidth.constant = 320
                                                } else {
                                                    cell3.constantWidth.constant = 170
                                                }
                                            }
                                            if selectedCondition.count > indexPath.row - 1{
                                                cell3.configureCell(withTitle: selectedCondition[indexPath.row-1], isSelected: false, isLast: false)
                                                return cell3
                                            } else {
                                                cell3.configureCell(withTitle: "", isSelected: false, isLast: true)
                                                return cell3
                                            }
                                        }
                                    } else {
                                        DispatchQueue.main.async {
                                            if UIDevice().userInterfaceIdiom == .pad {
                                                cell3.constantWidth.constant = 320
                                            } else {
                                                cell3.constantWidth.constant = 170
                                            }
                                        }
                                        if selectedCondition.count > indexPath.row - 1{
                                            cell3.configureCell(withTitle: selectedCondition[indexPath.row-1], isSelected: false, isLast: false)
                                        } else {
                                            cell3.configureCell(withTitle: "", isSelected: false, isLast: true)
                                        }
                                        return cell3
                                    }
                                }
                            }
                        } else {
                            if (!selectedIndexesReasons.isEmpty) {
                                DispatchQueue.main.async {
                                    if UIDevice().userInterfaceIdiom == .pad {
                                        cell3.constantWidth.constant = 320
                                    } else {
                                        cell3.constantWidth.constant = 170
                                    }
                                }
                                if(selectedIndexesReasons[0] == indexPath.row) {
                                    cell3.configureCell(withTitle: primaryReasonsArray[indexPath.row-1], isSelected: true, isLast: false)
                                    return cell3
                                } else {
                                    if primaryReasonsArray.count > indexPath.row - 1{
                                        cell3.configureCell(withTitle: primaryReasonsArray[indexPath.row-1], isSelected: false, isLast: false)
                                    } else {
                                        cell3.configureCell(withTitle: "", isSelected: false, isLast: true)
                                    }
                                    return cell3
                                }
                            } else {
                                DispatchQueue.main.async {
                                    if UIDevice().userInterfaceIdiom == .pad {
                                        cell3.constantWidth.constant = 320
                                    } else {
                                        cell3.constantWidth.constant = 170
                                    }
                                }
                                if primaryReasonsArray.count > indexPath.row - 1{
                                    cell3.configureCell(withTitle: primaryReasonsArray[indexPath.row-1], isSelected: false, isLast: false)
                                } else {
                                    cell3.configureCell(withTitle: "", isSelected: false, isLast: true)
                                }
                                return cell3
                            }
                        }
                    } else {
                        if indexPath.row == 1 {
                            cell11.configureCell(withFirstTitleInvoice: Constants.titlesLabels.firstDocuments, withSecondTitleInvoice: Constants.titlesLabels.secondDocuments, withFirstTitleReceipt: Constants.titlesLabels.thirdDocuments, withSecondTitleReceipt: Constants.titlesLabels.fourthsDocuments, withThirdTitleReceipt: Constants.titlesLabels.fifthsDocuments, withTitle: Constants.titlesLabels.titleDocuments)
                            return cell11
                        } else {
                            if isUploadedDocument == false {
                                if UIDevice().userInterfaceIdiom == .pad {
                                    return cell12
                                } else {
                                    return cell6
                                }
                            } else {
                                if imageSecondDocument != nil {
                                    //2 Documents
                                    if imageThirdDocument != nil {
                                        if imageFourthDocument != nil {
                                            //4 Documents
                                            if indexPath.row == 2 {
                                                cell7.configureCell(withImage: imageDocument!, withSecondImage: imageSecondDocument!, withDetails: "", atIndex: indexPath.row)
                                                return cell7
                                            }
                                            if indexPath.row == 4 {
                                                return cell
                                            } else {
                                                cell7.configureCell(withImage: imageThirdDocument!, withSecondImage: imageFourthDocument!, withDetails: "", atIndex: indexPath.row)
                                                return cell7
                                            }
                                        } else {
                                            //3 Documents
                                            if indexPath.row == 2 {
                                                cell7.configureCell(withImage: imageDocument!, withSecondImage: imageSecondDocument!, withDetails: "", atIndex: indexPath.row)
                                                return cell7
                                            }
                                            if indexPath.row == 4 {
                                                return cell
                                            }else {
                                                cell7.configureCell(withImage: imageThirdDocument!, withSecondImage: nil, withDetails: "", atIndex: indexPath.row)
                                                return cell7
                                            }
                                        }
                                    } else {
                                        //2 Documents
                                        if indexPath.row == 2 {
                                            cell7.configureCell(withImage: imageDocument!, withSecondImage: imageSecondDocument!, withDetails: "", atIndex: indexPath.row)
                                            return cell7
                                        }
                                        if indexPath.row == 4 {
                                            return cell
                                        } else {
                                            if UIDevice().userInterfaceIdiom == .pad {
                                                return cell12
                                            } else {
                                                return cell6
                                            }
                                        }
                                    }
                                } else {
                                    //1 Document
                                    DispatchQueue.main.async {
                                        self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                        self.buttonNext.isEnabled = true
                                    }
                                    cell7.configureCell(withImage: imageDocument!, withSecondImage: nil, withDetails: "", atIndex: indexPath.row)
                                    return cell7
                                }
                            }
                        }
                    }
                } else {
                    if dateFromPicker != nil {
                        cell4.configureCell(withDate: dateFromPicker!)
                    }
                    return cell4
                }
            } else {
                if (!selectedIndexes.isEmpty) {
                    if(selectedIndexes[0] == indexPath.row) {
                        cell3.configureCell(withTitle: typeServicesArray[indexPath.row-1], isSelected: true, isLast: false)
                        return cell3
                    } else {
                        cell3.configureCell(withTitle: typeServicesArray[indexPath.row-1], isSelected: false, isLast: false)
                        return cell3
                    }
                } else {
                    cell3.configureCell(withTitle: typeServicesArray[indexPath.row-1], isSelected: false, isLast: false)
                    return cell3
                }
            }
        } else {
            if !allPets.isEmpty {
                if canceledPolicies == false {
                    if indexPath.row == allPets.count+1 {
                        cell2.configureCell(withTitle: Constants.titlesLabels.labelCanceledPolicies)
                        return cell2
                    } else {
                        if !allPetsCoreDate.isEmpty && allPetsCoreDate.count >= indexPath.row {
                            cell1.configureCell(withName: allPets[indexPath.row-1].PetName, withPolicyNumber: "# \(allPets[indexPath.row-1].PolicyNumber)", withImage: UIImage(data:allPetsCoreDate[indexPath.row-1].image ?? Data()), canceled: false)
                        } else {
                            cell1.configureCell(withName: allPets[indexPath.row-1].PetName, withPolicyNumber: "# \(allPets[indexPath.row-1].PolicyNumber)", withImage: nil, canceled: false)
                        }
                        return cell1
                    }
                } else {
                    if indexPath.row == canceledPets.count+1 {
                        cell2.configureCell(withTitle: Constants.titlesLabels.labelValidPolicies)
                        return cell2
                    } else {
                        if canceledPets[indexPath.row-1].IsCanceled == true {
                            cell1.configureCell(withName: canceledPets[indexPath.row-1].PetName, withPolicyNumber: "# \(canceledPets[indexPath.row-1].PolicyNumber)", withImage: nil, canceled: true)
                        } else {
                            if !canceledPets.isEmpty && canceledPets.count > indexPath.row-1{
                                if !allPetsCoreDate.isEmpty && allPetsCoreDate.count >= indexPath.row {
                                    cell1.configureCell(withName: canceledPets[indexPath.row-1].PetName, withPolicyNumber: "# \(canceledPets[indexPath.row-1].PolicyNumber)", withImage: UIImage(data:allPetsCoreDate[indexPath.row-1].image ?? Data()), canceled: false)
                                } else {
                                    cell1.configureCell(withName: canceledPets[indexPath.row-1].PetName, withPolicyNumber: "# \(canceledPets[indexPath.row-1].PolicyNumber)", withImage: nil, canceled: false)
                                }
                            }
                        }
                        return cell1
                    }
                }
            } else {
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = self.tableView.cellForRow(at: indexPath) else { return }
        if allPets.count != 0 && !tableView.visibleCells.isEmpty {
            if canceledPolicies == false {
                if cell.tag == 0 && indexPath.row != 0{
                    selectedOptions[0] = 1
                    isService = true
                    
                    policyNumberWhole = "\(self.allPets[indexPath.row-1].PolicyNumber)"

                    self.keychain.delete("isSubmitted")
                    if !allPets.isEmpty && allPets.count > indexPath.row - 1 {
                        selectedPet = allPets[indexPath.row-1]
                        submitClaimView.labelName.text = selectedPet?.PetName
                        selectedOptionsForEndpoint["PolicyLineNumber"] = selectedPet?.PolicyLineNum
                        selectedOptionsForEndpoint["name"] = selectedPet?.PetName
                    } else {
                        self.allPets = []
                        self.canceledPets = []
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LoadPetList"), object: self)
                    }
                    let date = Date()
                    let format = date.getFormattedDate(format: "MM/dd/yyyy")
                    
                    DispatchQueue.main.async {
                        self.submitClaimView.labelServiceDateDetails.text = format
                        self.buttonNext.isHidden = false
                        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self.back))
                        self.tableView.reloadData()
                        self.buttonNext.isEnabled = false
                        self.buttonNext.backgroundColor = UIColor.gray
                    }
                }
            } else {
                if cell.tag == 0 && indexPath.row != 0{
                    selectedOptions[0] = 1
                    isService = true
                    
                    self.keychain.delete("isSubmitted")
                    if !canceledPets.isEmpty && canceledPets.count > indexPath.row - 1 {
                        selectedPet = canceledPets[indexPath.row-1]
                        submitClaimView.labelName.text = selectedPet?.PetName
                        selectedOptionsForEndpoint["PolicyLineNumber"] = selectedPet?.PolicyLineNum
                        selectedOptionsForEndpoint["name"] = selectedPet?.PetName
                    } else {
                        self.allPets = []
                        self.canceledPets = []
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LoadPetList"), object: self)
                    }

                    let date = Date()
                    let format = date.getFormattedDate(format: "MM/dd/yyyy")
                    submitClaimView.labelServiceDateDetails.text = format
                    buttonNext.isHidden = false
                    let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
                    navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.buttonNext.isEnabled = false
                        self.buttonNext.backgroundColor = UIColor.gray
                    }
                }
            }
            
            if cell.tag == 1 || cell.tag == 1 && selectedOptions[2] != 1 {
                if selectedOptions[0] == 1 && isService == true {
                    if selectedOptions[3] == 1 && isReasonWritten == true {
                        if selectedOptions[4] == 1 && isCondition == true {
                            isCondition = true
                            selectedOptions[6] = 1
                            selectedIndexesConditions = []
                            selectedIndexesConditions.append(indexPath.row)
                            if isConditionCodeText == true {
                                selectedConditionShow = ""
                                selectedOptionsForEndpoint["Condition"] = 0
                            } else {
                                let selectedCondition = selectedOptionsForEndpoint["PrimaryReason"] as? Int
                                if selectedCondition != nil {
                                    selectedOptionsForEndpoint["Condition"] = conditionsCodesArray[selectedOptionsForEndpoint["PrimaryReason"] as! Int]?[indexPath.row-1]
                                    selectedConditionShow = conditionsArray[selectedReason]?[indexPath.row-1] ?? ""
                                }
                            }
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                                self.buttonNext.isEnabled = true
                                self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                            }
                        } else {
                            isPrimaryReasonSelected = true
                            if indexPath.row == primaryReasonsArray.count {
                                selectedOptionsForEndpoint["Condition"] = 0
                                selectedOptionsForEndpoint["ConditionText"] = nil
                                isConditionCodeText = true
                            } else {
                                if !primaryReasonsArray.isEmpty {
                                    if conditionsArray[primaryReasonsArray[indexPath.row-1]] != nil {
                                        selectedCondition = conditionsArray[primaryReasonsArray[indexPath.row-1]] ?? [""]
                                        selectedOptionsForEndpoint["Condition"] = nil
                                        isConditionCodeText = false
                                    }
                                }
                            }
                            selectedIndexesReasons = []
                            selectedIndexesConditions = []
                            selectedIndexesReasons.append(indexPath.row)
                            selectedOptionsForEndpoint["PrimaryReason"] = primaryReasonsCodeArray[indexPath.row-1]
                            selectedReason = primaryReasonsArray[indexPath.row-1]
                            DispatchQueue.main.async {
                                self.buttonNext.isEnabled = true
                                self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                                self.tableView.reloadData()
                            }
                        }
                    } else {
                        selectedIndexes = []
                        selectedIndexes.append(indexPath.row)
                        submitClaimView.labelClaimTypeDetails.text = typeServicesArray[indexPath.row-1]
                        if typeServicesArray[indexPath.row-1] == "Accident" {
                            selectedOptionsForEndpoint["ClaimType"] = 0
                            //Firebase Analytics
                            Analytics.logEvent(Constants.FirebaseEvents.claimTypeAccident, parameters: nil)
                        }
                        if typeServicesArray[indexPath.row-1] == "Illness" {
                            selectedOptionsForEndpoint["ClaimType"] = 1
                            //Firebase Analytics
                            Analytics.logEvent(Constants.FirebaseEvents.claimTypeIllness, parameters: nil)
                        }
                        if typeServicesArray[indexPath.row-1] == "Wellness" {
                            selectedOptionsForEndpoint["ClaimType"] = 2
                            //Firebase Analytics
                            Analytics.logEvent(Constants.FirebaseEvents.claimTypeWellness, parameters: nil)
                        }
                        DispatchQueue.main.async {
                            self.buttonNext.isEnabled = true
                            self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                            self.tableView.reloadData()
                        }
                    }
                }
            }
            if indexPath.row == allPets.count + 1 || indexPath.row == canceledPets.count + 1 {
                canceledPolicies.toggle()
                self.tableView.reloadData()
            }
        }
    }
}

extension SubmitClaimViewController: UploadedDocumentDelegate {
    func reloadTableView(atIndex: Int, atRow: Int) {
        if atRow == 2 && atIndex == 0 {
            if imageDocument != nil && imageSecondDocument == nil && imageThirdDocument == nil && imageFourthDocument == nil {
                imageDocument = nil
                isUploadedDocument = false
                self.buttonNext.isEnabled = false
                self.buttonNext.backgroundColor = UIColor.gray
            }
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil{
                imageDocument = imageSecondDocument
                imageSecondDocument = nil
            }
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                imageDocument = imageSecondDocument
                imageSecondDocument = imageThirdDocument
                imageThirdDocument = nil
            }
            if imageDocument != nil && imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument != nil{
                imageDocument = imageSecondDocument
                imageSecondDocument = imageThirdDocument
                imageThirdDocument = imageFourthDocument
                imageFourthDocument = nil
            }
            tableView.reloadData()
        }
        if atRow == 2 && atIndex == 1 {
            if imageSecondDocument != nil && imageThirdDocument == nil && imageFourthDocument == nil {
                imageSecondDocument = nil
            }
            if imageSecondDocument != nil && imageThirdDocument != nil && imageFourthDocument == nil {
                imageSecondDocument = imageThirdDocument
                imageThirdDocument = nil
            }
            if imageSecondDocument != nil && imageFourthDocument != nil && imageFourthDocument != nil  {
                imageSecondDocument = imageThirdDocument
                imageThirdDocument = imageFourthDocument
                imageFourthDocument = nil
            }
            tableView.reloadData()
        }
        if atRow == 3 && atIndex == 0 {
            if imageFourthDocument != nil {
                imageThirdDocument = imageFourthDocument
                imageFourthDocument = nil
            } else {
                imageThirdDocument = nil
            }
            let indexPath = IndexPath(item: atRow, section: 0)
            tableView.reloadRows(at: [indexPath], with: .fade)
        }
        if atRow == 3 && atIndex == 1 {
            imageFourthDocument = nil
            let indexPath = IndexPath(item: atRow, section: 0)
            tableView.reloadRows(at: [indexPath], with: .fade)
        }
    }
}

extension SubmitClaimViewController: VetClinicDelegate {
    func opendDropDown(index: IndexPath) {
        self.startLoadingScreen()
        workerGetSavedVets.getVets{ [weak self] resultVets in
            switch resultVets {
            case .success(let resultVets):
                self?.allVetsSaved = []
                self?.allVetsSavedIds = []
                self?.allVetsSavedCities = []
                for vet in resultVets {
                    self?.allVetsSaved.append(vet.Name)
                    self?.allVetsSavedCities.append(vet.City)
                    self?.allVetsSavedIds.append("\(vet.HospitalId)")
                }
                
                self?.isVetSavedSelected = true
                self?.isSearchedSelected = false
                guard let count = self?.allVetsSavedCities.count else { return }
                if UIDevice().userInterfaceIdiom == .pad {
                    if self?.heightVetSaved == Double(count) * 140.0 {
                        self?.heightVetSaved = 0.0
                    } else {
                        self?.heightVetSaved = Double(count) * 140.0
                    }
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                        self?.stopLoadingScreen()
                    }
                } else {
                    if self?.heightVetSaved == Double(count) * 70.0 {
                        self?.heightVetSaved = 0.0
                    } else {
                        self?.heightVetSaved = Double(count) * 70.0
                    }
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                        self?.stopLoadingScreen()
                    }
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    self?.stopLoadingScreen()
                }
            }
        } onErrorHandler: { error in
            switch error {
            case .success(let resultError):
                DispatchQueue.main.async {
                    self.showAlert(withTitle: "", message: resultError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    self.stopLoadingScreen()
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    self.stopLoadingScreen()
                }
            }
        
        }
    }
}

extension SubmitClaimViewController: VetClinicSearchDelegate, UITextFieldDelegate {
    func openLocationServicesScreen() {
        
        let vc = DrawerLocationViewController()
        present(vc, animated: true, completion: nil)
    }

    func getTextFieldValue(withText: String) {
        self.startLoadingScreen()
        if withText.count == 0 {
            selectedOptionsForEndpoint["VetTypehead"] = nil
            selectedOptionsVet["VetTypehead"] = nil
            selectedOptionsVet["VetClinicSecond"] = nil
            selectedOptionsForEndpoint["VetClinicSecond"] = nil
            heightVetSearch = 0.0
            isEmptySearchVet = true
            DispatchQueue.main.async {
                self.stopLoadingScreen()
                self.tableView.reloadData()
                self.buttonNext.isEnabled = false
                self.buttonNext.backgroundColor = UIColor.gray
            }
        } else {
            selectedOptionsForEndpoint["VetTypehead"] = withText
            selectedOptionsVet["VetTypehead"] = withText
            isEmptySearchVet = false
        }
    }
    
    func addErroMessage(withError: String) {
        DispatchQueue.main.async {
            self.showAlert(withTitle: "", message: withError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
        }
    }
    
    func addGeneralError(withError: String) {
        if defaults.bool(forKey: "LocationServices") == false {
//            DispatchQueue.main.async {
//                self.showAlert(withTitle: "Location Services disabled.", message: withError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
//            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: withError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
            }
        }
    }
    
    func opendDropDownSearch(withURL: String, withLatitude: Double, withLongitude: Double, withEmptyField: Bool) {
        let worker = VetClinicWorker()
        allVetsSearchCities = [String]()
        allVetsSearchIds = [String]()
        allVetsSearch = [String]()
        
        if isEmptySearch == true && withEmptyField == true{
        } else {
            worker.getNearestVetClinic(searchValue: withURL, longitude: withLongitude, latitude: withLatitude) {[weak self] resultVets in
                switch resultVets {
                case .success(let resultVets):
                    for vet in resultVets {
                        self?.allVetsSearch.append(vet.NAMEONLY)
                        self?.allVetsSearchCities.append(vet.CITYONLY)
                        self?.allVetsSearchIds.append(vet.ID)
                    }
                    
                    self?.isSearchedSelected = true
                    self?.isVetSavedSelected = false
                    
                    DispatchQueue.main.async {

                        if !self!.allVetsSearch.isEmpty && self?.isEmptySearchVet == false {
                            self?.isEmptySearchVet = false
                            if UIDevice().userInterfaceIdiom == .pad {
                                self?.heightVetSearch = Double((self!.allVetsSearch.count) + 2) * 140.0
                            } else {
                                self?.heightVetSearch = Double(self!.allVetsSearch.count + 2) * 70.0
                            }
                        } else {
                            self?.heightVetSearch = 0.0
                            self?.isEmptySearchVet = true
                            self?.stopLoadingScreen()
                        }
 
                        if let cell = self?.tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as? VetClinicPickerTableViewCell {
                            cell.configureCell(withClinics: self!.allVetsSearch, withClinicsCities: self!.allVetsSearchCities, withClinicsIDs: self!.allVetsSearchIds, isSaved: false, index: 4)
                            let indexPath = IndexPath(item: 4, section: 0)
                            self?.tableView.beginUpdates()
                            self?.tableView.reloadRows(at: [indexPath], with: .fade)
                            self?.tableView.endUpdates()
                            self?.stopLoadingScreen()
                       }
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    }
                }
            } onErrorHandler: { [self] _ in
                DispatchQueue.main.async {
                    self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if isDiagnosis == true && isCondition == false {
            //print(textField.text!)
            selectedOptionsForEndpoint["Diagnosis"] = textField.text!
            selectedOptions[7] = 1
            DispatchQueue.main.async {
                self.buttonNext.isEnabled = true
                self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
            }
        }
        
        if isCondition == true && selectedOptionsForEndpoint["Condition"] != nil && isDiagnosis == false {
            
            if selectedOptionsVet["VetTypehead"] != nil {
                selectedOptionsVet["VetClinic"] = nil
                selectedOptionsVet["VetClinicSecond"] = nil
                selectedOptionsForEndpoint["VetTypehead"] = textField.text!
                if selectedOptionsForEndpoint["VetTypehead"] as! String == "" {
                    DispatchQueue.main.async {
                        self.buttonNext.isEnabled = false
                        self.buttonNext.backgroundColor = UIColor.gray
                        self.heightVetSearch = 0.0
                        self.tableView.reloadData()
                    }
                } else {
                    DispatchQueue.main.async {
                        self.buttonNext.isEnabled = true
                        self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                        self.tableView.reloadData()
                    }
                }
            } else {
                selectedOptionsForEndpoint["ConditionText"] = textField.text!
                selectedConditionShow = selectedOptionsForEndpoint["ConditionText"] as! String
                selectedOptionsForEndpoint["Condition"] = 0
                selectedOptionsForEndpoint["Diagnosis"] = ""
                isCondition = true
                selectedOptions[6] = 1
                DispatchQueue.main.async {
                    self.buttonNext.isEnabled = true
                    self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
                }
            }
        }

        self.view.endEditing(true)
        return false
    }
}

extension SubmitClaimViewController: VetClinicPickeDelegate {
    func getVetNameAndID(name: String, id: String, index: Int) {
        if index == 2 {
            self.selectedOptionsForEndpoint["VetClinic"] = Int(id)
            self.selectedOptionsForEndpoint["VetClinicSecond"] = nil
            self.selectedOptionsForEndpoint["VetTypehead"] = nil
            
            self.selectedOptionsVet["VetClinic"] = name
            self.selectedOptionsVet["VetClinicSecond"] = nil
            self.selectedOptionsVet["VetTypehead"] = nil
            self.selectedOptions[6] = 1
            isSearchedSelected = false
            isVetSavedSelected = false
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.buttonNext.isEnabled = true
                self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
            }
        }
        if index == 4 {
            self.selectedOptionsForEndpoint["VetClinicSecond"] = Int(id)
            self.selectedOptionsForEndpoint["VetClinic"] = nil
            self.selectedOptionsForEndpoint["VetTypehead"] = nil
            
            self.selectedOptionsVet["VetClinicSecond"] = name
            self.selectedOptionsVet["VetClinic"] = nil
            self.selectedOptionsVet["VetTypehead"] = nil
            self.selectedOptions[6] = 1
            isSearchedSelected = false
            isVetSavedSelected = false
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.buttonNext.isEnabled = true
                self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
            }
        }
    }
}

extension SubmitClaimViewController: ReasonTableDelegate {
    func getCondition(text: String) {
        isConditionCodeText = true
        if text.isEmpty {
            selectedOptionsForEndpoint["Condition"] = 0
            selectedOptionsForEndpoint["ConditionText"] = nil
            DispatchQueue.main.async {
                self.buttonNext.isEnabled = false
                self.buttonNext.backgroundColor = UIColor.gray
            }
        } else {
            selectedOptionsForEndpoint["Condition"] = text
            selectedOptionsForEndpoint["ConditionText"] = text
            DispatchQueue.main.async {
                self.buttonNext.isEnabled = true
                self.buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
            }
        }
    }
    
    func getDiagnosis(text: String) {
        selectedOptionsForEndpoint["Diagnosis"] = text
        //print(selectedOptionsForEndpoint)
    }
}

extension SubmitClaimViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    
        let tabBarIndex = tabBarController.selectedIndex
                    
        let isSubmitted = keychain.get( "isSubmitted")
        
        if isSubmitted == "true" && tabBarIndex == 1 {
            self.submittedClaimView.isHidden = true
            self.submitClaimViewWellness.isHidden = true
            self.submitClaimView.isHidden = true
            self.defaults.setValue(nil, forKey: "submittedClaimPet")
            self.defaults.setValue(nil, forKey: "submittedClaimId")
            
            isService = false
            isPickDate = false
            isUploadFile = false
            isWellness = false
            isReasonWritten = false
            isCondition = false
            isVetClinic = false
            isDiagnosis = false
            isUploadedDocument = false
            twoUploadedDocuments = false
            canceledPolicies = false
            isPrimaryReasonSelected = false
            isConditionCodeText = false
            
            isPolicyChecked = false
            isFraudChecked = false
            
            isSearchedSelected = false
            isVetSavedSelected = false
            isOneLabelTitle = false
            
            imageDocument = nil
            imageSecondDocument = nil
            imageThirdDocument = nil
            imageFourthDocument = nil
            
            selectedCondition = [String]()
            selectedOptionsForEndpoint = [String:Any]()
            
            selectedOptionsVet = [String:Any]()
            
            primaryReasonsArray = [String]()
            selectedReason = ""
            selectedConditionShow = ""
            
            selectedOptions = [0,0,0,0,0,0,0,0]
            selectedIndexes = [Int]()
            selectedIndexesReasons = [Int]()
            selectedIndexesConditions = [Int]()
            
            allVetsSearch = [String]()
            allVetsSearchCities = [String]()
            allVetsSearchIds = [String]()
            heightVetSearch = 0.0
            
            allVetsSaved = [String]()
            allVetsSavedCities = [String]()
            allVetsSavedIds = [String]()
            heightVetSaved = 0.0
            self.tableView.reloadData()
            self.navigationController?.navigationBar.isHidden = false
        }
    }
}
