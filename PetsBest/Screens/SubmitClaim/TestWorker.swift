//
//  TestWorker.swift
//  PetsBest
//
//  Created by Plamena on 11.11.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation
import KeychainSwift

typealias OnSuccessCompletionTest = (Result<String>) -> ()
typealias OnErrorHandlerTest = (Result<String>) -> ()

class TestWorker {

    private let keychain = KeychainSwift()
    
    func getTestDetails(completionHandler: @escaping OnSuccessCompletionTest,
                       onErrorHandler: @escaping OnErrorHandlerTest) {
        
        if var url = try? APIRouter.test.asURLRequest() {
            
            let appToken = keychain.get("appToken")
            
            guard let token = appToken?.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil) else { return }
            print(token)
            
            url.httpMethod = HTTPMethod.get.rawValue
            url.setValue(token, forHTTPHeaderField: "AuthToken")
            
            url.timeoutInterval = 30
            
            URLSession.shared.dataTask(with: url) { data, response, error in
                if let error = error {
                    return onErrorHandler(.failure(error))
                }
                
                guard let content = data else {
                    print("No data")
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    let statusCode = httpResponse.statusCode
                    
                    if (statusCode != 200) {
                        if statusCode == 500 || statusCode == 502 || statusCode == 503{
                            return onErrorHandler(.success(Constants.errorMessages.generalHomeError))
                        } else {
                            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                                print("Not containing JSON")
                                return onErrorHandler(.success(Constants.errorMessages.generalHomeError))
                            }
                            guard let error = json["Message"] else { return }
                            let errorString = "\(error)"
                            return onErrorHandler(.success(errorString))
                        }
                    } else {
                        guard
                            let tokenSuccess = String(data: content, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                            else { return }
                        DispatchQueue.main.async {
                            completionHandler(.success(tokenSuccess))
                        }
                    }
                }
            }.resume()
        }
        
    }
}

