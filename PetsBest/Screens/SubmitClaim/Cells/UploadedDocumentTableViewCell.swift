//
//  UploadedDocumentTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 31.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol UploadedDocumentDelegate {
    func reloadTableView(atIndex: Int, atRow: Int)
}

class UploadedDocumentTableViewCell: UITableViewCell, Identifiable, ReusableView, UICollectionViewDelegate, UICollectionViewDataSource, UploadClaimDelegate, DocumentDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionViewDocument: UICollectionView!
    @IBOutlet private weak var collectionViewConstraintLeft: NSLayoutConstraint!
    @IBOutlet private weak var collectionViewConstraintRight: NSLayoutConstraint!
    
    var delegate: UploadClaimDelegate?
    var delegate1: UploadedDocumentDelegate?
    var index : Int?
    
    private var images: [UIImage?] = []
    
    private var imageDocument: UIImage?
    private var imageSecondDocument: UIImage?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configCollectionViewProperties()
    }
    
    func configureCell(withImage: UIImage, withSecondImage: UIImage?, withDetails: String, atIndex: Int) {
        self.imageDocument = withImage
        self.imageSecondDocument = withSecondImage
        self.index = atIndex
        DispatchQueue.main.async {
            self.collectionViewDocument.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DocumentCollectionViewCell = collectionView.dequeueCell(at: indexPath)
        let cell1: AddDocumentCollectionViewCell = collectionView.dequeueCell(at: indexPath)
        let cell2: DocumentIPadCollectionViewCell = collectionView.dequeueCell(at: indexPath)
        let cell3: AddDocumentIPadCollectionViewCell = collectionView.dequeueCell(at: indexPath)
        let cell4: AddDocumentIPadProCollectionViewCell = collectionView.dequeueCell(at: indexPath)
        
        cell.delegate = self
        cell2.delegate = self
        
        let tapAddImage = UITapGestureRecognizer(target: self, action:  #selector(reloadTableView))
        cell1.imageViewClaim.isUserInteractionEnabled = true
        cell1.imageViewClaim.addGestureRecognizer(tapAddImage)
        
        let tapAddImageLabel = UITapGestureRecognizer(target: self, action:  #selector(reloadTableView))
        cell1.labelTakePhoto.isUserInteractionEnabled = true
        cell1.labelTakePhoto.addGestureRecognizer(tapAddImageLabel)
        
        let tapAddImageIPad = UITapGestureRecognizer(target: self, action:  #selector(reloadTableView))
        cell3.imageViewClaim.isUserInteractionEnabled = true
        cell3.imageViewClaim.addGestureRecognizer(tapAddImageIPad)
        
        let tapAddImageIPadLabel = UITapGestureRecognizer(target: self, action:  #selector(reloadTableView))
        cell3.labelTakePhoto.isUserInteractionEnabled = true
        cell3.labelTakePhoto.addGestureRecognizer(tapAddImageIPadLabel)
        
        let tapAddImageIPadPro = UITapGestureRecognizer(target: self, action:  #selector(reloadTableView))
        cell4.imageViewClaim.isUserInteractionEnabled = true
        cell4.imageViewClaim.addGestureRecognizer(tapAddImageIPadPro)
        
        let tapAddImageIPadProLabel = UITapGestureRecognizer(target: self, action:  #selector(reloadTableView))
        cell4.labelTakePhoto.isUserInteractionEnabled = true
        cell4.labelTakePhoto.addGestureRecognizer(tapAddImageIPadProLabel)
        
        if indexPath.row == 0 {
            if UIDevice().userInterfaceIdiom == .pad {
                cell2.configureCell(withIndex: indexPath.row, withImage: imageDocument!)
                return cell2
            } else {
                cell.configureCell(withIndex: indexPath.row, withImage: imageDocument!)
                return cell
            }
        } else {
            if UIDevice().userInterfaceIdiom == .pad {
                if imageSecondDocument != nil  {
                    cell2.configureCell(withIndex: indexPath.row, withImage: imageSecondDocument!)
                    return cell2
                } else {
                    if UIScreen.main.nativeBounds.height == 2732 {
                        return cell4
                    } else {
                        return cell3
                    }
                }
            } else {
                if imageSecondDocument != nil  {
                    cell.configureCell(withIndex: indexPath.row, withImage: imageSecondDocument!)
                    return cell
                } else {
                    return cell1
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice().userInterfaceIdiom == .phone {
            if UIScreen.main.nativeScale > UIScreen.main.scale {
                return CGSize(width: 130.0, height: 130.0)
            } else {
                return CGSize(width: 150.0, height: 160.0)
            }
        } else {
            if UIScreen.main.nativeBounds.height == 2732 {
                return CGSize(width: 400.0, height: 430.0)
            } else {
                return CGSize(width: 300.0, height: 330.0)
            }
        }
    }
    
    func deleteImage(atIndex: Int) {
        print(atIndex)
        delegate1?.reloadTableView(atIndex: atIndex, atRow: index!)
    }
    
    @objc internal func reloadTableView() {
        delegate?.reloadTableView()
    }
    // MARK: - Set collectionView
    
    private func configCollectionViewProperties() {
        collectionViewDocument.delegate = self
        collectionViewDocument.dataSource = self
        collectionViewDocument.registerCellNib(DocumentCollectionViewCell.self)
        collectionViewDocument.registerCellNib(AddDocumentCollectionViewCell.self)
        
        collectionViewDocument.registerCellNib(DocumentIPadCollectionViewCell.self)
        collectionViewDocument.registerCellNib(AddDocumentIPadCollectionViewCell.self)
        collectionViewDocument.registerCellNib(AddDocumentIPadProCollectionViewCell.self)
        collectionViewDocument.translatesAutoresizingMaskIntoConstraints = false
        
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1334 {
            collectionViewConstraintLeft.constant = 16
            collectionViewConstraintRight.constant = 16
        }
    }
}

