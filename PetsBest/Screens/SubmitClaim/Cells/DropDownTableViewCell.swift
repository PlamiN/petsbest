//
//  DropDownTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 3.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class DropDownTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var labelVetName: UILabel!
    @IBOutlet private weak var labelVetCity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice().userInterfaceIdiom == .pad {
            labelVetName.font = UIFont(name: "Proxima Nova Bold", size: 34)
            labelVetCity.font = UIFont(name: "Proxima Nova", size: 32)
        } else {
            labelVetName.font = UIFont(name: "Proxima Nova Bold", size: 17)
            labelVetCity.font = UIFont(name: "Proxima Nova", size: 15)
        }
    }
    
    func configureCell(withClinic: String, withCity: String) {
        self.labelVetName.text = withClinic
        self.labelVetCity.text = withCity
    }
}
