//
//  OneTitleTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 4.02.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit

class OneTitleTableViewCell: UITableViewCell, Identifiable, ReusableView  {

    @IBOutlet private weak var labelTitle1: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }

    private func setViews() {
        if UIDevice().userInterfaceIdiom == .pad {
            labelTitle1.font = UIFont(name: "Proxima Nova Bold", size: 50)
        } else {
            if UIScreen.main.nativeBounds.height == 1334 {
                labelTitle1.font = UIFont(name: "Proxima Nova Bold", size: 21)
            } else {
                labelTitle1.font = UIFont(name: "Proxima Nova Bold", size: 23)
            }
        }
        labelTitle1.textColor = UIColor.init(hex: Constants.Colors.labels)
    }
    
    func configureCell(withTitle: String) {
        labelTitle1.text = withTitle
    }
    
}
