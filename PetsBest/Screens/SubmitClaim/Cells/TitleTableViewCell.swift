//
//  TitleTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 23.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class TitleTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var labelTitle1: UILabel!
    @IBOutlet private weak var labelTitle2: UILabel!
    @IBOutlet private weak var labelTitle3: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        if UIDevice().userInterfaceIdiom == .pad {
            labelTitle1.font = UIFont(name: "Proxima Nova Bold", size: 50)
            labelTitle2.font = UIFont(name: "Proxima Nova Bold", size: 50)
            labelTitle3.font = UIFont(name: "Proxima Nova Bold", size: 50)
        } else {
            if UIScreen.main.nativeBounds.height == 1334 {
                labelTitle1.font = UIFont(name: "Proxima Nova Bold", size: 21)
                labelTitle2.font = UIFont(name: "Proxima Nova Bold", size: 21)
                labelTitle3.font = UIFont(name: "Proxima Nova Bold", size: 21)
            } else {
                labelTitle1.font = UIFont(name: "Proxima Nova Bold", size: 23)
                labelTitle2.font = UIFont(name: "Proxima Nova Bold", size: 23)
                labelTitle3.font = UIFont(name: "Proxima Nova Bold", size: 23)
            }
        }
        labelTitle1.textColor = UIColor.init(hex: Constants.Colors.labels)
        labelTitle2.textColor = UIColor.init(hex: Constants.Colors.labels)
        labelTitle3.textColor = UIColor.init(hex: Constants.Colors.labels)
    }
    
    func configureCell(withTitle: String, withTitle2: String, withTitle3: String) {
        labelTitle1.text = withTitle
        labelTitle2.text = withTitle2
        labelTitle3.text = withTitle3
    }
}
