//
//  ReasonTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 19.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol ReasonTableDelegate {
    func getDiagnosis(text: String)
    func getCondition(text: String)
}

class ReasonTableViewCell: UITableViewCell, Identifiable, ReusableView, UITextViewDelegate{

    @IBOutlet weak var textViewReason: UITextView!

    private var diagnosisText: String?
    private var condition: Bool?
    
    var placeholder = "Please type here..."
    
    let maxChars = 350
    
    var delegate: ReasonTableDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textViewReason.delegate = self
        textViewReason.textColor = .lightGray
        
        textViewReason.returnKeyType = UIReturnKeyType.done
        if UIDevice().userInterfaceIdiom == .pad {
            textViewReason.font = UIFont(name: "Proxima Nova", size: 30)
        }
    }
    
    func configureCell(withDiagnosis: String, isCondition: Bool) {
        
        if !withDiagnosis.isEmpty {
            textViewReason.text = withDiagnosis
            placeholder = withDiagnosis
            textViewReason.textColor = UIColor.black
            let bottom = NSMakeRange(textViewReason.text.count - 1, 1)
            textViewReason.scrollRangeToVisible(bottom)
        }
        condition = isCondition
    }
    
    @objc func textViewDidChange(_ textView: UITextView) {
        placeholder = textView.text
        if condition == true {
            delegate?.getCondition(text: textView.text)
        } else {
            delegate?.getDiagnosis(text: textView.text)
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = .black
        } else {
            textView.text = placeholder
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
              textView.text = "Please type here..."
              textView.textColor = UIColor.lightGray
              placeholder = ""
          } else {
              textView.textColor = UIColor.black
              placeholder = textView.text
          }
      }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        if maxChars - textViewReason.text.count == 0 {
            if range.length != 1 {
               return false
          }
        }
        return true
    }
}
