//
//  UploadClaimTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 30.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

protocol UploadClaimDelegate {
    func reloadTableView()
}

import UIKit

class UploadClaimTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var viewAllElements: UIView!
    @IBOutlet private weak var imageUploadImage: UIImageView!
    @IBOutlet private weak var labelAddDocuments: UnderlinedLabel!
    @IBOutlet private weak var viewAllElementsHeight: NSLayoutConstraint!
    @IBOutlet private weak var viewAllElementsWidth: NSLayoutConstraint!
    @IBOutlet private weak var imageUploadHeight: NSLayoutConstraint!
    @IBOutlet private weak var imageUploadWidth: NSLayoutConstraint!
    @IBOutlet private weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet private weak var trailingConstraint: NSLayoutConstraint!
    
    var delegate: UploadClaimDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        viewAllElements.backgroundColor = UIColor.init(hex: Constants.Colors.backgroundColor)
        
        let tapReload = UITapGestureRecognizer(target: self, action:  #selector(reloadData))
        viewAllElements.isUserInteractionEnabled = true
        viewAllElements.addGestureRecognizer(tapReload)
        labelAddDocuments.text = "ADD DOCUMENTS"
        labelAddDocuments.textColor = UIColor.init(hex: Constants.Colors.loginTextfield)
        
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1334 {
            leadingConstraint.constant = 16
            trailingConstraint.constant = 16
        }
//        let yourViewBorder = CAShapeLayer()
//        yourViewBorder.strokeColor = UIColor.black.cgColor
//        yourViewBorder.lineDashPattern = [2, 2]
//        yourViewBorder.frame = viewAllElements.bounds
//        yourViewBorder.fillColor = nil
//        yourViewBorder.path = UIBezierPath(rect: viewAllElements.bounds).cgPath
//        viewAllElements.layer.addSublayer(yourViewBorder)
    }
    
    func configureCell(withImage: UIImage) {
        imageUploadImage.image = withImage
        
    }
    
    @objc func reloadData() {
        delegate?.reloadTableView()
    }
}
