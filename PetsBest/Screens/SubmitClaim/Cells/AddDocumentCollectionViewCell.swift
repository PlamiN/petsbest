//
//  AddDocumentCollectionViewCell.swift
//  PetsBest
//
//  Created by Plamena on 31.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class AddDocumentCollectionViewCell: UICollectionViewCell, Identifiable, ReusableView {

    @IBOutlet weak var viewClaim: UIView!
    @IBOutlet weak var imageViewClaim: UIImageView!
    @IBOutlet weak var labelTakePhoto: UnderlinedLabel!
    @IBOutlet weak var imageHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var imageWidthConstant: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
        if UIScreen.main.nativeScale > UIScreen.main.scale {
            imageHeightConstant.constant = 75
            imageWidthConstant.constant = 75
        }
    }
    
    private func setViews() {
        viewClaim.backgroundColor = UIColor.init(hex: Constants.Colors.backgroundColor)
        
//        let yourViewBorder = CAShapeLayer()
//        yourViewBorder.strokeColor = UIColor.black.cgColor
//        yourViewBorder.lineDashPattern = [2, 2]
//        yourViewBorder.frame = viewClaim.bounds
//        yourViewBorder.fillColor = nil
//        yourViewBorder.path = UIBezierPath(rect: viewClaim.bounds).cgPath
//        viewClaim.layer.addSublayer(yourViewBorder)
        
        labelTakePhoto.text = "ADD DOCUMENTS"
        labelTakePhoto.textColor = UIColor.init(hex: Constants.Colors.loginTextfield)
    }
}
