//
//  AddDocumentIPadProCollectionViewCell.swift
//  PetsBest
//
//  Created by Plamena on 2.12.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class AddDocumentIPadProCollectionViewCell: UICollectionViewCell, Identifiable, ReusableView {

    @IBOutlet weak var viewClaim: UIView!
    @IBOutlet weak var imageViewClaim: UIImageView!
    @IBOutlet weak var labelTakePhoto: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        viewClaim.backgroundColor = UIColor.init(hex: Constants.Colors.backgroundColor)
        
//        let yourViewBorder = CAShapeLayer()
//        yourViewBorder.strokeColor = UIColor.black.cgColor
//        yourViewBorder.lineDashPattern = [2, 2]
//        yourViewBorder.frame = viewClaim.bounds
//        yourViewBorder.fillColor = nil
//        yourViewBorder.path = UIBezierPath(rect: viewClaim.bounds).cgPath
//        viewClaim.layer.addSublayer(yourViewBorder)
    }
}
