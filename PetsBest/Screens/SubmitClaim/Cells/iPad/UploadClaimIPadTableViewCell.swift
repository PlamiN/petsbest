//
//  UploadClaimIPadTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 1.12.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class UploadClaimIPadTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var viewAllElements: UIView!
    @IBOutlet private weak var imageUploadImage: UIImageView!
    @IBOutlet private weak var labelAddDocuments: UnderlinedLabel!
    
    var delegate: UploadClaimDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        viewAllElements.backgroundColor = UIColor.init(hex: Constants.Colors.backgroundColor)
        
        let tapReload = UITapGestureRecognizer(target: self, action:  #selector(reloadData))
        viewAllElements.isUserInteractionEnabled = true
        viewAllElements.addGestureRecognizer(tapReload)
        labelAddDocuments.text = "ADD DOCUMENTS"
        labelAddDocuments.textColor = UIColor.init(hex: Constants.Colors.main)
        
        labelAddDocuments.font = UIFont(name: "Proxima Nova", size: 25)
        print(viewAllElements.bounds)
//        let yourViewBorder = CAShapeLayer()
//        yourViewBorder.strokeColor = UIColor.black.cgColor
//        yourViewBorder.lineDashPattern = [2, 2]
//        yourViewBorder.frame = viewAllElements.bounds
//        yourViewBorder.fillColor = nil
//        yourViewBorder.path = UIBezierPath(rect: viewAllElements.bounds).cgPath
//        viewAllElements.layer.addSublayer(yourViewBorder)
    }
    
    func configureCell(withImage: UIImage) {
        imageUploadImage.image = withImage
        
    }
    
    @objc func reloadData() {
        delegate?.reloadTableView()
    }
}
