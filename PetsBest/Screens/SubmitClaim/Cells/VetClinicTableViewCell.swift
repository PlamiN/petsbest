//
//  VetClinicTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 28.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import CoreLocation

class YourCustomTextField: UITextField {

    private let defaults = UserDefaults.standard
    
    override func deleteBackward() {
        if text == "" {
            print("Empty Textfield")
             // do something when backspace is tapped/entered in an empty text field
        }
        // do something for every backspace
        super.deleteBackward()
    }

}

protocol VetClinicSearchDelegate {
    func opendDropDownSearch(withURL: String, withLatitude: Double, withLongitude: Double, withEmptyField: Bool)
    func addErroMessage(withError: String)
    func addGeneralError(withError: String)
    func getTextFieldValue(withText: String)
    func openLocationServicesScreen()
}

class VetClinicTableViewCell: UITableViewCell, Identifiable, ReusableView, CLLocationManagerDelegate, UITextFieldDelegate {

    @IBOutlet weak var searchVetClinic: YourCustomTextField!
    @IBOutlet private weak var labelOrFind: UILabel!
    private let defaults = UserDefaults.standard
    var fromViewController : UIViewController?
    
    var delegate: VetClinicSearchDelegate?
    private var worker = VetClinicWorker()
    private var usersLatitude: Double?
    private var usersLongitude: Double?
    
    lazy var locationManager = CLLocationManager()
    
    var allVetsSearch = [VetClinicModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        locationManager.delegate = self
        
        let button1 = UIButton(type: .custom)
        button1.setImage(UIImage(named: "search"), for: .normal)
        button1.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button1.frame = CGRect(x: CGFloat(searchVetClinic.frame.size.width - 30), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        searchVetClinic.rightView = button1
        searchVetClinic.rightViewMode = .always
        
        labelOrFind.text = "or find your vet"
        labelOrFind.textColor = UIColor.init(hex: Constants.Colors.labels)
        
        searchVetClinic.addTarget(searchVetClinic, action: #selector(resignFirstResponder), for: .editingDidEndOnExit)
        
        searchVetClinic.returnKeyType = UIReturnKeyType.done
        searchVetClinic.clearButtonMode = .whileEditing
        
        searchVetClinic.delegate = self
        searchVetClinic.addTarget(self, action: #selector(openLocationServices), for: .editingDidBegin)
        
        searchVetClinic.addTarget(self,
            action : #selector(textFieldDidChange),
            for : .editingChanged)
        
        if UIDevice().userInterfaceIdiom == .pad {
            searchVetClinic.font = UIFont(name: "Proxima Nova", size: 30)
            labelOrFind.font = UIFont(name: "Proxima Nova", size: 30)
        }
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        
        return hasPermission
    }
    
    @objc func openLocationServices(textField: UITextField) {
        if !hasLocationPermission() {
            if (self.defaults.string(forKey: "notNowActivatedLocationServices") != nil){
            } else {
                self.searchVetClinic.resignFirstResponder()
                delegate?.openLocationServicesScreen()
            }
        }
    }
    
    func configureCell(vetClinic: String?) {
        searchVetClinic.text = vetClinic
    }
    
    @objc func textFieldDidChange() {
        guard let writtenText = searchVetClinic.text else { return }
        delegate?.getTextFieldValue(withText: writtenText)
        
        let longitude = self.defaults.double(forKey: "usersLongitude")
        let latitude = self.defaults.double(forKey: "usersLatitude")
        
        
        let urlWritten = writtenText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        if writtenText.count != 0 {
            if self.defaults.string(forKey: "LocationServicesLocally") == "activate" {
                
                let urlWritten = writtenText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                self.delegate?.opendDropDownSearch(withURL: urlWritten, withLatitude: latitude, withLongitude: longitude, withEmptyField: false)
            }
            
            if self.defaults.string(forKey: "LocationServicesLocally") == "deactivate" {
                let urlWritten = writtenText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                self.delegate?.opendDropDownSearch(withURL: urlWritten, withLatitude: latitude, withLongitude: longitude, withEmptyField: false)
            }
            
            if self.defaults.string(forKey: "LocationServices") == nil {
                let urlWritten = writtenText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                self.delegate?.opendDropDownSearch(withURL: urlWritten, withLatitude: latitude, withLongitude: longitude, withEmptyField: false)
            }
        }
    }
}
