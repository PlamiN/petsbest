//
//  ShowCanceledPolciesTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 23.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class ShowCanceledPolciesTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var labelTitle: UnderlinedLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        if UIDevice().userInterfaceIdiom == .pad {
            labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 30)
        }
        labelTitle.textColor = UIColor.init(hex: Constants.Colors.main)
    }
    
    func configureCell(withTitle: String) {
        labelTitle.text = withTitle
    }
}
