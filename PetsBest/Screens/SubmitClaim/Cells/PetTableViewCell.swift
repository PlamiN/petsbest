//
//  PetTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 23.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class PetTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var imageViewPet: UIImageView!
    @IBOutlet private weak var labelName: UILabel!
    @IBOutlet private weak var labelPolicyNumber: UILabel!
    @IBOutlet private weak var imageViewPetWidth: NSLayoutConstraint!
    @IBOutlet private weak var imageViewPetHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        labelName.textColor = UIColor.init(hex: Constants.Colors.labels)
        labelPolicyNumber.textColor = UIColor.init(hex: Constants.Colors.labels)
        if UIDevice().userInterfaceIdiom == .pad {
            labelName.font = UIFont(name: "Proxima Nova", size: 30)
            labelPolicyNumber.font = UIFont(name: "Proxima Nova", size: 26)
            imageViewPetWidth.constant = 100
            imageViewPetWidth.constant = 100
        }
    }
    
    func configureCell(withName: String, withPolicyNumber: String, withImage: UIImage?, canceled: Bool) {
        if canceled == true {
            labelPolicyNumber.text = withPolicyNumber + " - Cancelled"
            labelPolicyNumber.textColor = UIColor.init(hex: Constants.Colors.orangeButton)
        } else {
            labelPolicyNumber.text = withPolicyNumber
            labelPolicyNumber.textColor = UIColor.init(hex: Constants.Colors.labels)
        }
        labelName.text = withName
        
        if withImage != nil {
            imageViewPet.image = withImage
            imageViewPet.maskCircleSubmission(anyImage: withImage!)
            imageViewPet.contentMode = .scaleAspectFill
        } else {
            imageViewPet.image = UIImage(named: "placeHolder_submit")
        }
    }
}
