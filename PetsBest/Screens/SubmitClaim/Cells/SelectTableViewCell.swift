//
//  SelectTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 23.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class SelectTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var imageViewButton: UIImageView!
    @IBOutlet private weak var imageViewButtonWidth: NSLayoutConstraint!
    @IBOutlet private weak var imageViewButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var constantWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        if UIDevice().userInterfaceIdiom == .pad {
            labelTitle.font = UIFont(name: "Proxima Nova", size: 30)
            imageViewButtonWidth.constant = 70
            imageViewButtonHeight.constant = 70
        }
        labelTitle.textColor = UIColor.init(hex: Constants.Colors.labels)
    }
    
    func configureCell(withTitle: String, isSelected: Bool, isLast: Bool) {
        labelTitle.text = withTitle
        if isLast == true {
            imageViewButton.image = UIImage(named: "")
        } else {
            if isSelected == true {
                imageViewButton.image = UIImage(named: "radio1")
            } else {
                imageViewButton.image = UIImage(named: "radio2")
            }
        }
    }
}
