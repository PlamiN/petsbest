//
//  VetClinicPickerTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 20.01.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit
import FirebaseAnalytics

protocol VetClinicPickeDelegate {
    func getVetNameAndID(name: String, id: String, index: Int)
}

class VetClinicPickerTableViewCell: UITableViewCell, Identifiable, ReusableView, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet weak var tableViewPickVet: UITableView!
    
    var delegate: VetClinicPickeDelegate?
    
    var allVetsSearch = [String]()
    var allVetsSearchCities = [String?]()
    var allVetsSearchIds = [String]()
    
    var allVetsSaved = [String]()
    var allVetsSavedIds = [String]()
    var allVetsSavedCities = [String?]()
    
    var isSavedChecked: Bool = false
    var indexCell: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableViewPickVet.dataSource = self
        tableViewPickVet.delegate = self
        tableViewPickVet.registerCell(DropDownTableViewCell.self)
        allVetsSearch = []
        allVetsSaved = []
        allVetsSearchCities = []
        allVetsSearchIds = []
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .pad {
            return 140.0
        } else {
            return 70.0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSavedChecked == true {
            return allVetsSaved.count
        } else {
            return allVetsSearch.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DropDownTableViewCell = tableView.dequeueCell(DropDownTableViewCell.self)
        if isSavedChecked == true {
            cell.configureCell(withClinic: allVetsSaved[indexPath.row], withCity: allVetsSavedCities[indexPath.row] ?? "")
        } else {
            cell.configureCell(withClinic: allVetsSearch[indexPath.row], withCity: allVetsSearchCities[indexPath.row] ?? "")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSavedChecked == true {
            delegate?.getVetNameAndID(name: allVetsSaved[indexPath.row], id: allVetsSavedIds[indexPath.row], index: indexCell)
        }
        if isSavedChecked == false {
            delegate?.getVetNameAndID(name: allVetsSearch[indexPath.row], id: allVetsSearchIds[indexPath.row], index: indexCell)
        }
    }
    
    func configureCell(withClinics: [String], withClinicsCities: [String], withClinicsIDs: [String], isSaved: Bool, index: Int) {
        isSavedChecked = isSaved
        indexCell = index
        if isSaved == false {
            allVetsSearch = withClinics
            allVetsSearchIds = withClinicsIDs
            allVetsSearchCities = withClinicsCities
            tableViewPickVet.reloadData()
        } else {
            allVetsSaved = []
            allVetsSavedIds = []
            allVetsSaved = withClinics
            allVetsSavedIds = withClinicsIDs
            allVetsSavedCities = withClinicsCities
            tableViewPickVet.reloadData()
        }
    }
}
