//
//  DatePickerTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 27.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol DatePickerDelegate {
    func isDatePicked(date: Date)
}

class DatePickerTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var datePicker: UIDatePicker!
    
    var currentDate: String?
    var delegate: DatePickerDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    func configureCell(withDate: Date) {
        datePicker.date = withDate
    }

    private func setViews() {
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        delegate?.isDatePicked(date: datePicker.date)
        datePicker.addTarget(self, action: #selector(datePickerChanged), for: .valueChanged)
    }
    
    // We implement that function
    @objc private func datePickerChanged(sender:UIDatePicker) {
      let date = sender.date
      delegate?.isDatePicked(date: date)
    }
}
