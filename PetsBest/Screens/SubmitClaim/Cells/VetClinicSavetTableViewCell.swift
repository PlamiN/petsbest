//
//  VetClinicSavetTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 16.10.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

protocol VetClinicDelegate {
    func opendDropDown(index: IndexPath)
}

import UIKit

class VetClinicSavetTableViewCell: UITableViewCell, Identifiable, ReusableView, UITextFieldDelegate {

    @IBOutlet weak var selectedVetClinics: UITextField!
    
    var delegate: VetClinicDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    func configureCell(vetClinic: String?) {
        selectedVetClinics.text = vetClinic
    }

    private func setViews() {
        
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "downArrow"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(selectedVetClinics.frame.size.width - 30), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        selectedVetClinics.rightView = button
        selectedVetClinics.rightViewMode = .always
        let tapOpen = UITapGestureRecognizer(target: self, action:  #selector(openSavedClinics))
        selectedVetClinics.rightView!.isUserInteractionEnabled = true
        selectedVetClinics.addGestureRecognizer(tapOpen)
        
        let tapOpenDropdown = UITapGestureRecognizer(target: self, action:  #selector(openSavedClinicsDropdown))
        selectedVetClinics.rightView!.addGestureRecognizer(tapOpenDropdown)
        
        self.selectedVetClinics.inputView = UIView()
        
        if UIDevice().userInterfaceIdiom == .pad {
            selectedVetClinics.font = UIFont(name: "Proxima Nova", size: 30)
        }
    }
    
    @objc func openSavedClinicsDropdown() {
        delegate?.opendDropDown(index: IndexPath(item: 1, section: 0))
    }
    
    @objc func openSavedClinics() {
        delegate?.opendDropDown(index: IndexPath(item: 1, section: 0))
    }
}
