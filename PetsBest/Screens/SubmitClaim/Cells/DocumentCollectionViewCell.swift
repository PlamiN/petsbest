//
//  DocumentCollectionViewCell.swift
//  PetsBest
//
//  Created by Plamena on 31.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol DocumentDelegate {
    func deleteImage(atIndex: Int)
}

class DocumentCollectionViewCell: UICollectionViewCell, Identifiable, ReusableView {

    @IBOutlet weak var viewDocument: UIView!
    @IBOutlet weak var imageViewClaim: UIImageView!
    @IBOutlet weak var imageClose: UIImageView!
    
    var delegate: DocumentDelegate?
    var index : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let tapClose = UITapGestureRecognizer(target: self, action:  #selector(handleTap))
        imageClose.isUserInteractionEnabled = true
        imageClose.addGestureRecognizer(tapClose)
        viewDocument.backgroundColor = UIColor.init(hex: Constants.Colors.backgroundColor)
    }

    func configureCell(withIndex: Int, withImage: UIImage) {
        index = withIndex
        self.imageViewClaim.image = withImage
    }
    
    @objc func handleTap(){
        delegate?.deleteImage(atIndex: index!)
    }
}
