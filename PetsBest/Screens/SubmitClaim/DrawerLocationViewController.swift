//
//  DrawerLocationViewController.swift
//  PetsBest
//
//  Created by Plamena on 4.02.22.
//  Copyright © 2022 Plamena. All rights reserved.
//

import UIKit
import CoreLocation

class DrawerLocationViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var imageViewLocation: UIImageView!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonNotNow: UIButton!
    @IBOutlet weak var labelEnhanceExperience: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var dimmerView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var stackViewTop: NSLayoutConstraint!
    @IBOutlet weak var buttonNotNowBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonAllowHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonNotNowHeight: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var labelDetailsHeight: NSLayoutConstraint!
    @IBOutlet weak var stackViewTopConstraint: NSLayoutConstraint!
    
    lazy var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    private let defaults = UserDefaults.standard
    
    var location: AcousticLocationModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.setViews()
        }
    }
    
    private func setViews() {
        
        // round the top left and top right corner of card view
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 20.0
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        if UIScreen.main.nativeBounds.height != 1136 {
            stackView.spacing = 20
            stackViewTop.constant = 20
            buttonNotNowBottomConstraint.constant = 64
        }
        
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1334 {
            stackViewTopConstraint.constant = 16
            stackViewTop.constant = 32
            labelDetailsHeight.constant = 170
            imageViewHeight.constant = 200
            stackView.spacing = 0
            buttonNotNowBottomConstraint.constant = 8
        }
        
        view.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        // dimmerViewTapped() will be called when user tap on the dimmer view
        let dimmerTap = UITapGestureRecognizer(target: self, action: #selector(dimmerViewTapped))
        dimmerView.addGestureRecognizer(dimmerTap)
        dimmerView.isUserInteractionEnabled = true
        
        labelEnhanceExperience.text = Constants.titlesButtons.enhanceExperience
        labelDetails.text = Constants.titlesButtons.detailsLocationServices
        labelEnhanceExperience.font = UIFont(name: "Proxima Nova Bold", size: 25)
        buttonNext.setTitle(Constants.titlesButtons.allowLocationServices, for: .normal)
        buttonNext.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        buttonNext.setTitleColor(UIColor.white, for: .normal)
        buttonNext.layer.cornerRadius = 25
        let tapActivateLocation = UITapGestureRecognizer(target: self, action:  #selector(allowLocationServices))
        buttonNext.addGestureRecognizer(tapActivateLocation)
        
        let yourAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "Proxima Nova Bold", size: 18)!,
            .foregroundColor: UIColor.init(hex: Constants.Colors.welcomeLabels)!,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ] // .double.rawValue, .thick.rawValue
        
        let attributeString = NSMutableAttributedString(
           string: Constants.titlesButtons.notNow,
           attributes: yourAttributes
        )
        buttonNotNow.setAttributedTitle(attributeString, for: .normal)
        labelsAndViewIPads()
    }
    
    private func labelsAndViewIPads() {
        if UIDevice().userInterfaceIdiom == .pad {
            buttonAllowHeight.constant = 80
            buttonNotNowHeight.constant = 80
            stackViewTopConstraint.constant = 64
            labelDetailsHeight.constant = 300
            imageViewHeight.constant = 300
            buttonNext.layer.cornerRadius = 40
            
            buttonNext.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 40)
            let yourAttributes: [NSAttributedString.Key: Any] = [
                .font: UIFont(name: "Proxima Nova Bold", size: 40)!,
                .foregroundColor: UIColor.init(hex: Constants.Colors.welcomeLabels)!,
                .underlineStyle: NSUnderlineStyle.single.rawValue
            ] // .double.rawValue, .thick.rawValue
            
            let attributeString = NSMutableAttributedString(
               string: Constants.titlesButtons.notNow,
               attributes: yourAttributes
            )
            buttonNotNow.setAttributedTitle(attributeString, for: .normal)
            
            labelEnhanceExperience.font = UIFont(name: "Proxima Nova Bold", size: 40)
            labelDetails.font = UIFont(name: "Proxima Nova", size: 30)
        
        }
    }
    
    @IBAction func actionNotNow(_ sender: UIButton) {
        self.defaults.set(-116.215019, forKey: "usersLatitude")
        self.defaults.set(43.618881, forKey: "usersLongitude")
        self.defaults.set("true", forKey: "notNowActivatedLocationServices")
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Animations
    private func showLocationServicesView() {
        
        // ensure there's no pending layout changes before animation runs
        self.view.layoutIfNeeded()
        
        // set the new top constraint value for card view
        // card view won't move up just yet, we need to call layoutIfNeeded()
        // to tell the app to refresh the frame/position of card view
        if let safeAreaHeight = UIApplication.shared.keyWindow?.safeAreaLayoutGuide.layoutFrame.size.height,
           let bottomPadding = UIApplication.shared.keyWindow?.safeAreaInsets.bottom {
            
            // when card state is normal, its top distance to safe area is
            // (safe area height + bottom inset) / 2.0
            topViewConstraint.constant = (safeAreaHeight + bottomPadding) / 2.0
        }
        
        // move card up from bottom by telling the app to refresh the frame/position of view
        // create a new property animator
        let allowLocationServices = UIViewPropertyAnimator(duration: 0.25, curve: .easeIn, animations: {
            self.view.layoutIfNeeded()
        })
        
        // show dimmer view
        // this will animate the dimmerView alpha together with the card move up animation
        allowLocationServices.addAnimations({
            self.dimmerView.alpha = 0.7
        })
        
        // run the animation
        allowLocationServices.startAnimation()
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
                self.defaults.set(-116.215019, forKey: "usersLatitude")
                self.defaults.set(43.618881, forKey: "usersLongitude")
                self.defaults.set("deactivate", forKey: "LocationServicesLocally")
                self.defaults.set("deactivate", forKey: "LocationServices")
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
                self.defaults.set("activate", forKey: "LocationServicesLocally")
                self.defaults.set("activate", forKey: "LocationServices")
                let locManager = CLLocationManager()
                var currentLocation: CLLocation!
                
                currentLocation = locManager.location
                
                if currentLocation != nil {
                    DispatchQueue.main.async{ [weak self] in
                        self?.defaults.set(currentLocation.coordinate.latitude, forKey: "usersLatitude")
                        self?.defaults.set(currentLocation.coordinate.longitude, forKey: "usersLongitude")
                    }
                }
            }
        } else {
            hasPermission = false
        }
        
        return hasPermission
    }
    
    func readAcousticConfigurationFile(filename fileName: String) -> AcousticLocationModel? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(AcousticLocationModel.self, from: data)
//                print(jsonData.location.autoInitialize)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
    
    @objc private func allowLocationServices() {

        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
    }
    
    @objc private func dimmerViewTapped() {
        
        // ensure there's no pending layout changes before animation runs
        self.view.layoutIfNeeded()
        
        // set the new top constraint value for card view
        // card view won't move down just yet, we need to call layoutIfNeeded()
        // to tell the app to refresh the frame/position of card view
        if let safeAreaHeight = UIApplication.shared.keyWindow?.safeAreaLayoutGuide.layoutFrame.size.height,
           let bottomPadding = UIApplication.shared.keyWindow?.safeAreaInsets.bottom {
            
            // move the card view to bottom of screen
            topViewConstraint.constant = safeAreaHeight + bottomPadding
        }
        
        // move card down to bottom
        // create a new property animator
        let hideLocationServices = UIViewPropertyAnimator(duration: 0.25, curve: .easeIn, animations: {
            self.view.layoutIfNeeded()
        })
        
        // hide dimmer view
        // this will animate the dimmerView alpha together with the card move down animation
        hideLocationServices.addAnimations {
            self.dimmerView.alpha = 0.0
        }
        
        // when the animation completes, (position == .end means the animation has ended)
        // dismiss this view controller (if there is a presenting view controller)
        hideLocationServices.addCompletion({ position in
            if position == .end {
                if(self.presentingViewController != nil) {
                    self.dismiss(animated: false, completion: nil)
                }
            }
        })
        
        // run the animation
        hideLocationServices.startAnimation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied) {
            self.defaults.set("deactivate", forKey: "LocationServicesLocally")
            
            self.dismiss(animated: true, completion: nil)
        }
        if (status == CLAuthorizationStatus.authorizedWhenInUse) {
            // The user accepted authorization
            self.defaults.set("activate", forKey: "LocationServicesLocally")
            
            currentLocation = locationManager.location
            
            if currentLocation != nil {
                DispatchQueue.main.async{ [weak self] in
                    self?.defaults.set(self?.currentLocation?.coordinate.latitude, forKey: "usersLatitude")
                    self?.defaults.set(self?.currentLocation?.coordinate.longitude, forKey: "usersLongitude")
                }
            }
            
            location = readAcousticConfigurationFile(filename: "MceConfig")
            if location != nil {
                location!.location.autoInitialize = true
            }
            saveToFile()
            
            self.dismiss(animated: true, completion: nil)
    
        } else if (status == CLAuthorizationStatus.authorizedAlways) {
            // The user accepted authorization
            self.defaults.set("activate", forKey: "LocationServicesLocally")
            
            currentLocation = locationManager.location
            
            if currentLocation != nil {
                DispatchQueue.main.async{ [weak self] in
                    self?.defaults.set(self?.currentLocation?.coordinate.latitude, forKey: "usersLatitude")
                    self?.defaults.set(self?.currentLocation?.coordinate.longitude, forKey: "usersLongitude")
                }
            }
            
            location = readAcousticConfigurationFile(filename: "MceConfig")
            if location != nil {
                location!.location.autoInitialize = true
            }
            saveToFile()
            readAcousticConfigurationFile(filename: "MceConfig")
            
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    private func saveToFile(){

        let from = Bundle.main.url(forResource: "MceConfig", withExtension: "json")!

        let to = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("MceConfig.json")

        do {

            try FileManager.default.copyItem(at: from, to: to)

            let jsonData = try JSONEncoder().encode(location)
//            print(location?.location.autoInitialize)
            try jsonData.write(to: to)

        }
        catch {

            print(error)
        }
    }
}

