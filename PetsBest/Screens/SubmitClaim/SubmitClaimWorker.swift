//
//  SubmitClaimWorker.swift
//  PetsBest
//
//  Created by Plamena on 17.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation
import KeychainSwift

typealias OnSuccessCompletionSubmitClaim = (Result<SubmittedClaimModel>) -> ()
typealias OnErrorHandlerSubmitClaim = (Result<String>) -> ()

class SubmitClaimWorker {
    private let defaults = UserDefaults.standard
    private let keychain = KeychainSwift()
    
    func submitClaim(diagnosis: String, fraudChecked: Bool, declarationsChecked: Bool, policyNumber: Int, claimType: Int, withIncidentDate: String, conditionCode: Int, subConditionCode: Int, subConditionCodeText: String, savedHospitalId: Int, hospitalId: Int, vetTypehead: String, withFiles: [[String : Any]], completionHandler: @escaping OnSuccessCompletionSubmitClaim,
                   onErrorHandler: @escaping OnErrorHandlerSubmitClaim) {

        var favouriteHospital: String?
        
        if var url = try? APIRouter.start_claim.asURLRequest() {
            if savedHospitalId == 0 {
                favouriteHospital = nil
            }

            let parameterDictionary = ["Diagnosis": diagnosis,
                                       "FraudWarningChecked": fraudChecked,
                                       "DeclarationsChecked": declarationsChecked,
                                       "ConditionCode": conditionCode,
                                       "SubConditionCode": subConditionCode,
                                       "ConditionFreeText": subConditionCodeText,
                                       "SavedPrimaryHospitalId": favouriteHospital as Any,
                                       "VetHospitalId1": hospitalId,
                                       "VetTypeahead1": vetTypehead,
                                       "PolicyLineNumber": policyNumber,
                                       "ClaimType": claimType,
                                       "IncidentDate": withIncidentDate,
                                       "Files": withFiles] as [String : Any]
            url.httpMethod = HTTPMethod.post.rawValue
            
            url.timeoutInterval = 180
            
            let appToken = keychain.get("appToken")
            
            guard let token = appToken?.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil) else { return }
            url.setValue(token, forHTTPHeaderField: "AuthToken")
            url.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
                return
            }
            url.httpBody = httpBody
            
            URLSession.shared.dataTask(with: url) { data, response, error in
                
                if let error = error {
                    return onErrorHandler(.failure(error))
                }
                
                guard let content = data else {
                    print("No data")
                    return
                }
                
                
                if let httpResponse = response as? HTTPURLResponse {
                    let statusCode = httpResponse.statusCode
                    
                    if (statusCode != 200) {
                        if statusCode == 500 || statusCode == 502 || statusCode == 503{
                            return onErrorHandler(.success(Constants.errorMessages.generalHomeError))
                        } else {
                            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                                print("Not containing JSON")
                                return onErrorHandler(.success(Constants.errorMessages.generalHomeError))
                            }
                            guard let error = json["Message"] else { return }
                            let errorString = "\(error)"
                            return onErrorHandler(.success(errorString))
                        }
                    } else {
                        let response = try? JSONDecoder().decode(SubmittedClaimModel.self, from: content)
                        print(response)
                        if let response = response {
                            DispatchQueue.main.async {
                                completionHandler(.success(response))
                            }
                        }
                    }
                }
            }.resume()
        }
    }
}

