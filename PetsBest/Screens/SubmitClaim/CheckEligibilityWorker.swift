//
//  CheckEligibilityWorker.swift
//  PetsBest
//
//  Created by Plamena on 21.10.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation
import KeychainSwift

typealias OnSuccessCompletionCheckEligibility = (Result<Bool>) -> ()
typealias OnErrorHandlerCheckEligibility = (Result<String>) -> ()

class CheckEligibilityWorker {
    
    private let keychain = KeychainSwift()
    
    func getValidPolicy(withPolicyNumber: Int, withClaimType: Int, withIncidentDate: String,completionHandler: @escaping OnSuccessCompletionCheckEligibility,
                       onErrorHandler: @escaping OnErrorHandlerCheckEligibility) {
        
        if var url = try? APIRouter.check_eligibility.asURLRequest() {
            
            
            let parameterDictionary = ["PolicyLineNumber": withPolicyNumber, "ClaimType": withClaimType,
                                  "IncidentDate": withIncidentDate] as [String : Any]
            url.httpMethod = HTTPMethod.post.rawValue
            let appToken = keychain.get("appToken")
            guard let token = appToken?.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil) else { return }
            url.setValue(token, forHTTPHeaderField: "AuthToken")
            url.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
                return
            }
            url.httpBody = httpBody
            
            URLSession.shared.dataTask(with: url) { data, response, error in
                if let error = error {
                    return onErrorHandler(.failure(error))
                }
                
                guard let content = data else {
                    print("No data")
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    let statusCode = httpResponse.statusCode
                    
                    if (statusCode != 200) {
                        if statusCode == 500 || statusCode == 502 || statusCode == 503{
                            return onErrorHandler(.success(Constants.errorMessages.generalHomeError))
                        } else {
                            let response = try? JSONDecoder().decode(CheckEligibilityModel.self, from: content)
     
                            return onErrorHandler(.success(response?.Message ?? ""))
                        }
                    } else {
                        guard
                            let successString = String(data: content, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                            else { return }
                        //print(successString)
                        completionHandler(.success(Bool(successString)!))
                    }
                }
            }.resume()
        }
        
    }
}
