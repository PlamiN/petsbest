//
//  AcousticLocationModel.swift
//  PetsBest
//
//  Created by Plamena on 17.02.22.
//  Copyright © 2022 Plamena. All rights reserved.
//

import Foundation

struct AcousticLocationModel: Codable {
    var location: Location
}

struct Location: Codable {
    var autoInitialize: Bool
}
