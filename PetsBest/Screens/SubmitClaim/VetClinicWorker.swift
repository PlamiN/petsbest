//
//  VetClinicWorker.swift
//  PetsBest
//
//  Created by Plamena on 23.10.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation
import KeychainSwift

typealias OnSuccessCompletionVetClinic = (Result<[VetClinicModel]>) -> ()
typealias OnErrorHandlerVetClinic = (Result<String>) -> ()

class VetClinicWorker {
    
    private let keychain = KeychainSwift()
    
    func getNearestVetClinic(searchValue:String, longitude: Double, latitude: Double, completionHandler: @escaping OnSuccessCompletionVetClinic,
                       onErrorHandler: @escaping OnErrorHandlerVetClinic) {
        let devURL = "https://qa.petsbest.com/mobileApi/Vet/VetSearch?searchVal=\(searchValue)&lng=\(longitude)&lat=\(latitude)"
        let prodURL =  "https://petsbest.com/api/Vet/VetSearch?searchVal=\(searchValue)&lng=\(longitude)&lat=\(latitude)"

        guard let encodedURL = URL(string: prodURL) else { return }
        var request = URLRequest(url: encodedURL)
        request.httpMethod = HTTPMethod.get.rawValue
        
        let appToken = keychain.get("appToken")
        
        guard let token = appToken?.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil) else { return }
        print(token)
        
        request.setValue(token, forHTTPHeaderField: "AuthToken")
        
        request.timeoutInterval = 30
        
        print(encodedURL)
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                return onErrorHandler(.failure(error))
            }
            
            guard let content = data else {
                print("No data")
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                let statusCode = httpResponse.statusCode
                
                if (statusCode != 200) {
                    if statusCode == 500 || statusCode == 502 || statusCode == 503{
                        return onErrorHandler(.success(Constants.errorMessages.generalHomeError))
                    } else {
                        guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                            print("Not containing JSON")
                            return
                        }
                        guard let error = json["Message"] else { return }
                        let errorString = "\(error)"
                        return onErrorHandler(.success(errorString))
                    }
                } else {
                    let response = try? JSONDecoder().decode([VetClinicModel].self, from: content)
                    if let response = response {
                        DispatchQueue.main.async {
                            completionHandler(.success(response))
                        }
                    }
                }
            }
        }.resume()
        
    }
}
