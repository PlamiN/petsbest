//
//  SavedVetsWorker.swift
//  PetsBest
//
//  Created by Plamena on 4.12.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation
import KeychainSwift

typealias OnSuccessCompletionGetSavedVets = (Result<[SavedVets]>) -> ()
typealias OnErrorHandlerGetSavedVets = (Result<String>) -> ()

class SavetVetsWorker {
    
    private let defaults = UserDefaults.standard
    private let keychain = KeychainSwift()
    
    func getVets(completionHandler: @escaping OnSuccessCompletionGetSavedVets,
                       onErrorHandler: @escaping OnErrorHandlerGetSavedVets) {
        
        if var url = try? APIRouter.saved_vets.asURLRequest() {
            
            let appToken = keychain.get("appToken")
            
            guard let token = appToken?.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil) else { return }
            print(token)
            url.httpMethod = HTTPMethod.get.rawValue
            url.setValue(token, forHTTPHeaderField: "AuthToken")
            
            URLSession.shared.dataTask(with: url) { data, response, error in
                if let error = error {
                    return onErrorHandler(.failure(error))
                }
                
                guard let content = data else {
                    print("No data")
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    let statusCode = httpResponse.statusCode
                    
                    if (statusCode != 200) {
                        if statusCode == 500 || statusCode == 502 || statusCode == 503{
                            return onErrorHandler(.success(Constants.errorMessages.generalHomeError))
                        } else {
                            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                                print("Not containing JSON")
                                return
                            }
                            guard let error = json["Message"] else { return }
                            let errorString = "\(error)"
                            return onErrorHandler(.success(errorString))
                        }
                    } else {
                        let response = try? JSONDecoder().decode([SavedVets].self, from: content)
                        if let response = response {
                            DispatchQueue.main.async {
                                completionHandler(.success(response))
                            }
                        }
                    }
                }
            }.resume()
        }
        
    }
}

