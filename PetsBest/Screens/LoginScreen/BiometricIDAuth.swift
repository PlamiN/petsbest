//
//  BiometricIDAuth.swift
//  PetsBest
//
//  Created by Plamena on 5.03.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit
import Foundation
import LocalAuthentication


class BiometricIDAuth {
    
    var loginReason = "Logging in with Touch ID"
    private let defaults = UserDefaults.standard
    
    let context = LAContext()
    
    enum BiometricType {
        case none
        case touchID
        case faceID
    }

    func biometricType() -> BiometricType {

        if #available(iOS 11, *) {
            let _ = context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
            switch(context.biometryType) {
            case .faceID:
                return .faceID
            case .touchID:
                return .touchID
            default:
                return .none
            }
        }
        
        return context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touchID : .none
    }
    
    func authenticateUser(completion: @escaping (String?) -> Void) { // 1
        // 3
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
                               localizedReason: loginReason) { (success, evaluateError) in
            // 4
            if success {
                DispatchQueue.main.async {
                    // User authenticated successfully, take appropriate action
                    self.defaults.set("activate", forKey: "FaceID")
                    completion(success.description)
                }
            } else {
                // TODO: deal with LAError cases
                self.defaults.set("deactivate", forKey: "FaceID")
                completion("Biometry unavailable.Your device is not configured for biometric authentication.")
            }
        }
    }
    
    // function to detect an error type
    func showLAError(laError: Error) -> Void {
        
        var message = ""
        
        switch laError {
            
        case LAError.appCancel:
            message = "Authentication was cancelled by application"
            
        case LAError.authenticationFailed:
            message = "The user failed to provide valid credentials"
            
        case LAError.invalidContext:
            message = "The context is invalid"
            
        case LAError.passcodeNotSet:
            message = "Passcode is not set on the device"
            
        case LAError.systemCancel:
            message = "Authentication was cancelled by the system"
            
        case LAError.userCancel:
            message = "The user did cancel"
            
        case LAError.userFallback:
            message = "The user chose to use the fallback"
            
        default:
            
            if #available(iOS 11.0, *) {
                
                switch laError {
                    
                case LAError.biometryNotAvailable:
                    message = "Biometry is not available"
                    
                case LAError.biometryNotEnrolled:
                    message = "Authentication could not start, because biometry has no enrolled identities"
                    
                case LAError.biometryLockout:
                    message = "Biometry is locked. Use passcode."

                default:
                    message = "Did not find error code on LAError object"
                }
                
            }
            message = "Did not find error code on LAError object"
            
            
        }
        
        //return message
        print("LAError message - \(message)")
        
    }
    
}
