//
//  LoginViewController.swift
//  PetsBest
//
//  Created by Plamena on 9.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import LocalAuthentication
import KeychainSwift
import CoreData
import Reachability
import FirebaseAnalytics

class LoginViewController: UIViewController, PasswordToggleVisibilityDelegate {
    
    @IBOutlet private weak var textFieldUsername: SkyFloatingLabelTextField!
    @IBOutlet private weak var textFieldPassword: SkyFloatingLabelTextField!
    @IBOutlet private weak var buttonLogIn: UIButton!
    @IBOutlet private weak var labelNotRegistered: UILabel!
    @IBOutlet private weak var labelForgotPassword: UnderlinedLabel!
    @IBOutlet private weak var labelCreateAccount: UnderlinedLabel!
    @IBOutlet private weak var labelCopyright: UILabel!
    @IBOutlet private weak var labelCopyrightSecond: UILabel!
    @IBOutlet private weak var buttonLoginHeight: NSLayoutConstraint!
    @IBOutlet private weak var buttonLoginWidth: NSLayoutConstraint!
    @IBOutlet private weak var imageLogoHeight: NSLayoutConstraint!
    
    fileprivate var passwordToggleVisibilityView: PasswordToggleVisibilityView!
    
    private let worker = LoginWorker()
    private var usernameTextField: String?
    private var passwordTextField: String?
    private var isLogout: Bool?
    private var isLoginTapped: Bool = false
    private var isLoginIconTapped: Bool = false
    private var cancelWhenActiveBiometricLogin = false
    private var dontAllowBiometris = false
    
    private let defaults = UserDefaults.standard
    private let keychain = KeychainSwift()

    var coreDataStack = CoreDataStack()
    private var petCoreDataService: PetsCoreDataService?
    
    var reachability: Reachability!
    
    private let biometricLogin = BiometricIDAuth()
    
    var userID: String = ""
    var channelID: String = ""
   
    var moc: NSManagedObjectContext? {
        didSet {
            if let moc = moc {
                petCoreDataService = PetsCoreDataService(moc: moc)
            }
        }
    }
    
    init(withCoreDataStack: CoreDataStack, withAutoLogout: Bool) {
        super.init(nibName: nil, bundle: nil)
        isLogout = withAutoLogout
        moc = withCoreDataStack.objectContext
        if let moc = moc {
            petCoreDataService = PetsCoreDataService(moc: moc)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.setViews()
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.navigationBar.isHidden = true
        }
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    //Firebase Analytics
    override func viewDidAppear(_ animated: Bool) {
        Analytics.logEvent(AnalyticsEventScreenView,
                           parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.login])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        if isLogout == true {
            appMovedToForeground()
        }
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    @objc func appMovedToForeground() {
        print("App moved to background!")
        reachability = try! Reachability()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged(_:)), name: Notification.Name.reachabilityChanged, object: reachability)

        do {
            try reachability?.startNotifier()
        } catch {
            print("This is not working.")
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.noInternetError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                Spinner.stop()
            }
            return
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func reachabilityChanged(_ note: NSNotification) {

        let reachability = note.object as! Reachability

        isLoginTapped = false
        if reachability.connection != .unavailable {
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
                if defaults.string(forKey: "FaceID") == "activate" {
                    biometricLogin.authenticateUser { [weak self] message in
                        if message == "true" {
                            self?.loginActivity()
                        } else {
                            self?.cancelWhenActiveBiometricLogin = true
                            //When disabling FaceID in phone settings of the application -> direct login
                        }
                    }
                }
                if defaults.string(forKey: "FaceIDLocally") == "activate" {
                    switch biometricLogin.biometricType() {
                    case .faceID:
                        openApplicationSettings()
                    case .touchID:
                        openApplicationSettings()
                    default:
                        return
                    }
                } else {
                    DispatchQueue.main.async {
                        Spinner.stop()
                    }
                }
            } else {
                if defaults.string(forKey: "FaceID") == "activate" {
                    biometricLogin.authenticateUser { [weak self] message in
                        if message == "true" {
                            self?.loginActivity()
                        } else {
                            //When disabling FaceID in phone settings of the application -> direct login
                            self?.cancelWhenActiveBiometricLogin = true
                        }
                    }
                }
                if defaults.string(forKey: "FaceIDLocally") == "activate" {
                    switch biometricLogin.biometricType() {
                    case .faceID:
                        openApplicationSettings()
                    case .touchID:
                        openApplicationSettings()
                    default:
                        return
                    }
                } else {
                    DispatchQueue.main.async {
                        Spinner.stop()
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.noInternetError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                Spinner.stop()
            }
            print("Not reachable")
        }
    }
    
    private func setViews() {
        
        switch biometricLogin.biometricType() {
        case .faceID:
            
            textFieldPassword.rightViewMode = UITextField.ViewMode.always
            textFieldPassword.rightView = UIImageView(image: UIImage(systemName: "faceid"))
            textFieldPassword.rightView?.tintColor = UIColor.gray
            let tapOpenBiometricLogin = UITapGestureRecognizer(target: self, action:  #selector(getBiometricLogin))
            textFieldPassword.rightView?.isUserInteractionEnabled = true
            textFieldPassword.rightView?.addGestureRecognizer(tapOpenBiometricLogin)
            
        case .touchID:
            textFieldPassword.rightViewMode = UITextField.ViewMode.always
            textFieldPassword.rightView = UIImageView(image: UIImage(systemName: "touchid"))
            textFieldPassword.rightView?.tintColor = UIColor.gray
            let tapOpenBiometricLogin = UITapGestureRecognizer(target: self, action:  #selector(getBiometricLogin))
            textFieldPassword.rightView?.isUserInteractionEnabled = true
            textFieldPassword.rightView?.addGestureRecognizer(tapOpenBiometricLogin)
        default: break
        }
        
        textFieldUsername.titleFormatter = { $0 }
        textFieldPassword.titleFormatter = { $0 }
        textFieldUsername.selectedTitleColor = UIColor.init(hex: Constants.Colors.loginTextfield)!
        textFieldPassword.selectedTitleColor = UIColor.init(hex: Constants.Colors.loginTextfield)!
        
        addNavBarImage()
        setBackgroundImage("backgroundImageLogin", contentMode: UIView.ContentMode.scaleAspectFill)
        
        buttonLogIn.setTitle(Constants.titlesButtons.logIn, for: .normal)
        buttonLogIn.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        buttonLogIn.setTitleColor(UIColor.white, for: .normal)
        buttonLogIn.layer.cornerRadius = 25
        
        labelCreateAccount.textColor = UIColor.white
        labelCreateAccount.text = Constants.titlesLabels.createAccount
        let year = Calendar.current.component(.year, from: Date())
        labelCopyright.text = Constants.titlesLabels.copyrightDetails + "\(year)"
        labelCopyrightSecond.text = Constants.titlesLabels.copyrightDetailsSecond
        labelCopyright.textColor = UIColor.white
        labelCopyrightSecond.textColor = UIColor.white
        textFieldPassword.placeholder = Constants.textFieldsPlaceholder.password
        textFieldUsername.placeholder = Constants.textFieldsPlaceholder.email
        textFieldPassword.isSecureTextEntry = true
        
        labelForgotPassword.text = Constants.titlesLabels.forgotPassword
        labelForgotPassword.textColor = UIColor.init(hex: Constants.Colors.main)
        labelNotRegistered.text = Constants.titlesLabels.notRegistered
        labelNotRegistered.textColor = UIColor.init(hex: Constants.Colors.labels)
        
        let tapCreateAccount = UITapGestureRecognizer(target: self, action:  #selector(register))
        labelCreateAccount.isUserInteractionEnabled = true
        labelCreateAccount.addGestureRecognizer(tapCreateAccount)
        
        textFieldUsername.delegate = self
        textFieldPassword.delegate = self
        
        let tapForgotPassword = UITapGestureRecognizer(target: self, action:  #selector(goToForgotPassword))
        labelForgotPassword.isUserInteractionEnabled = true
        labelForgotPassword.addGestureRecognizer(tapForgotPassword)
    
        hideKeyboardWhenTappedAround()
        DispatchQueue.main.async {
            self.labelsAndViewIPads()
        }
    }
    
    @objc private func getBiometricLogin() {
        self.defaults.set("false", forKey: "notNowActivatedBiometrics")
        
        if self.defaults.string(forKey: "FaceID") == "activate" {
            switch biometricLogin.biometricType() {
            case .faceID:
                print("Face ID")
                biometricLogin.authenticateUser { [weak self] message in
                    if message == "true" {
                        self?.loginActivity()
                    } else {
                        self?.cancelWhenActiveBiometricLogin = true
                        //When disabling FaceID in phone settings of the application -> direct login
                    }
                }
            case .touchID:
                print("Touch ID")
                biometricLogin.authenticateUser { [weak self] message in
                    if message == "true" {
                        self?.loginActivity()
                    } else {
                        self?.cancelWhenActiveBiometricLogin = true
                        //When disabling FaceID in phone settings of the application -> direct login
                    }
                }
            case .none:
                print("No ID")
                self.defaults.set("deactivate", forKey: "FaceID")
            default: break
            }
        }
    }
    
    private func labelsAndViewIPads() {
        if UIDevice().userInterfaceIdiom == .pad {
            if UIScreen.main.nativeBounds.height == 2048 || UIScreen.main.nativeBounds.height == 2160{
                labelCopyright.font = UIFont(name: "Proxima Nova", size: 20)
                labelCopyrightSecond.font = UIFont(name: "Proxima Nova", size: 20)
                labelForgotPassword.font = UIFont(name: "Proxima Nova", size: 27)
                labelNotRegistered.font = UIFont(name: "Proxima Nova Bold", size: 27)
                labelCreateAccount.font = UIFont(name: "Proxima Nova Bold", size: 27)
                textFieldUsername.font = UIFont(name: "Proxima Nova", size: 27)
                textFieldPassword.font = UIFont(name: "Proxima Nova", size: 27)
                textFieldUsername.titleFont = UIFont(name: "Proxima Nova", size: 27)!
                textFieldPassword.titleFont = UIFont(name: "Proxima Nova", size: 27)!
                buttonLogIn.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 27)
                buttonLoginWidth.constant = 250
                buttonLoginHeight.constant = 80
                buttonLogIn.layer.cornerRadius = 40
                imageLogoHeight.constant = 100
            } else {
                labelCopyright.font = UIFont(name: "Proxima Nova", size: 28)
                labelCopyrightSecond.font = UIFont(name: "Proxima Nova", size: 28)
                labelForgotPassword.font = UIFont(name: "Proxima Nova", size: 35)
                labelNotRegistered.font = UIFont(name: "Proxima Nova Bold", size: 35)
                labelCreateAccount.font = UIFont(name: "Proxima Nova Bold", size: 35)
                textFieldUsername.font = UIFont(name: "Proxima Nova", size: 35)
                textFieldPassword.font = UIFont(name: "Proxima Nova", size: 35)
                textFieldUsername.titleFont = UIFont(name: "Proxima Nova", size: 35)!
                textFieldPassword.titleFont = UIFont(name: "Proxima Nova", size: 35)!
                buttonLogIn.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 35)
                buttonLoginWidth.constant = 250
                buttonLoginHeight.constant = 100
                buttonLogIn.layer.cornerRadius = 50
                imageLogoHeight.constant = 150
            }
        }
    }
    
    private func addNavBarImage() {
        let image = UIImage(named: "petsBestLogo") //Your logo url here
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
    }
    
    func viewWasToggled(_ passwordToggleVisibilityView: PasswordToggleVisibilityView, isSelected selected: Bool) {
        textFieldPassword.isSecureTextEntry.toggle()
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        // move back the root view origin to zero
        self.view.frame.origin.y = 0
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyb))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyb() {
        view.endEditing(true)
    }
    
    @objc private func goToForgotPassword(){
        if let url = try? APIRouter.forgot_password.asURLRequestCustomerPortal() {
            let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.forgotPassword)
            vc.openedFromScreen = "LoginScreen"
            let navigationController = NavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .overFullScreen
            present(navigationController, animated: true, completion: nil)
        }
    }
    
    @objc private func hideUnhidePassword(){
        passwordToggleVisibilityView.eyeState = PasswordToggleVisibilityView.EyeState.closed
    }
    
    @objc private func register(){
        if let url = try? APIRouter.register.asURLRequestCustomerPortal() {
            let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.register)
            vc.openedFromScreen = "CreateAccountScreen"
            let navigationController = NavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .overFullScreen
            present(navigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction private func actionLogin(_ sender: UIButton) {
        
        self.usernameTextField = self.textFieldUsername.text
        self.passwordTextField = self.textFieldPassword.text
        guard let user = self.usernameTextField else { return }
        isLoginTapped = true
        
        DispatchQueue.main.async {
            Spinner.start()
        }
        
        
        if cancelWhenActiveBiometricLogin == true && defaults.string(forKey: "FaceID") != nil && isValidEmail(user) && isLoginIconTapped == true {
            //BiometricAuthentication Canceled Locally and Enable in the phone settings -> login without FaceID/TouchID
            if defaults.string(forKey: "FaceID") == "deactivate" {
                //Activate BiometricAuthentication Locally
                self.loginActivity()
            } else {
                self.textFieldPassword.rightView = nil
                self.loginActivity()
                //Don't allow BiometricAuthentication when it is accepted locally and enable in the phone settings
            }
        }
        if defaults.string(forKey: "FaceID") == "deactivate" && defaults.string(forKey: "FaceIDLocally") == "activate"{
            textFieldPassword.rightView = nil
            openApplicationSettings()
        } else {
            loginActivity()
        }
    }
    
    private func openApplicationSettings() {
        let alertController = UIAlertController (title: "Face ID/Touch ID disabled", message: "Please go to Settings.", preferredStyle: .alert)
           let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in

               guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                   return
               }

               if UIApplication.shared.canOpenURL(settingsUrl) {
                   UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                       print("Settings opened: \(success)") // Prints true
                       self.defaults.removeObject(forKey: "FaceIDLocally")
                       self.defaults.set("activate", forKey: "FaceID")
                   })
               }
           }
           alertController.addAction(settingsAction)
           let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
           alertController.addAction(cancelAction)

           self.present(alertController, animated: true, completion: nil)
    }
    
    private func loginActivity() {
        
//        print(userID)
//        print(channelID)
        if userID != "" && channelID != "" {
            if self.keychain.get("mobileUserId") != nil && self.keychain.get("channelId") != nil{
                userID = self.keychain.get("mobileUserId")!
                channelID = self.keychain.get("channelId")!
            }
        }
        
        //Should save it locally on a second login
        
        self.usernameTextField = self.textFieldUsername.text
        self.passwordTextField = self.textFieldPassword.text
        guard let newUser = self.usernameTextField else { return }
        guard let newPass = self.passwordTextField else { return }
        
        if self.keychain.get("username") == nil && self.keychain.get("password") == nil {
            DispatchQueue.main.async {
                Spinner.start()
            }
            
            if self.usernameTextField == "" && self.passwordTextField == "" && isLoginTapped == true{
                DispatchQueue.main.async {
                    Spinner.stop()
                    self.showAlert(withTitle: "", message: Constants.errorMessages.usernamePasswordRequiredError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                }
            } else {
                if (self.isValidEmail(newUser)) {
                    self.worker.loginUser(withUsername: newUser, withPassword: newPass, mobileUserId: userID, channelId: channelID, appKey: Constants.productionAcousticSDKKey, completionHandler: { [weak self] resultUser in
                        switch resultUser {
                        case .success(_):
                            
                            guard let user = self?.usernameTextField else { return }
                            guard let pass = self?.passwordTextField else { return }
                            let usernameLocal = self?.keychain.get("username")
                            let usernamePassword = self?.keychain.get("password")
                            
                            if (!user.isEmpty && !pass.isEmpty){
                                //case when user logs in with different username & password & valid email
                                if user != usernameLocal && ((self?.isValidEmail(user)) != nil) && (pass != usernamePassword) {
                                    self?.petCoreDataService?.deleteAllRecords()
                                    self?.keychain.set("true", forKey: "deleteCookies")
                                    self?.keychain.delete("username")
                                    self?.keychain.delete("password")
                                    self?.keychain.delete("appToken")
                                }
                                
                                //case when user logs in with different username & password
                                if user != usernameLocal && (pass != usernamePassword) {
                                    self?.petCoreDataService?.deleteAllRecords()
                                    self?.keychain.set("true", forKey: "deleteCookies")
                                    self?.keychain.delete("username")
                                    self?.keychain.delete("password")
                                    self?.keychain.delete("appToken")
                                }
                                
                                //case when user logs in with the same username, but different password
                                if user == usernameLocal && ((self?.isValidEmail(user)) != nil)  && (pass != usernamePassword ) {
                                    self?.petCoreDataService?.deleteAllRecords()
                                    self?.keychain.set("true", forKey: "deleteCookies")
                                    self?.keychain.delete("username")
                                    self?.keychain.delete("password")
                                    self?.keychain.delete("appToken")
                                }
                                
                                //case when user logs in with the same password, but different username
                                if user != usernameLocal && ((self?.isValidEmail(user)) != nil)  && (pass == usernamePassword ) {
                                    self?.petCoreDataService?.deleteAllRecords()
                                    self?.keychain.set("true", forKey: "deleteCookies")
                                    self?.keychain.delete("username")
                                    self?.keychain.delete("password")
                                    self?.keychain.delete("appToken")
                                }
                            }
                            if (!user.isEmpty || !pass.isEmpty){
                                //case when user logs in with different username & password & valid email
                                if user != usernameLocal && ((self?.isValidEmail(user)) != nil)  && (pass != usernamePassword) {
                                    self?.petCoreDataService?.deleteAllRecords()
                                    self?.keychain.set("true", forKey: "deleteCookies")
                                    self?.keychain.delete("username")
                                    self?.keychain.delete("password")
                                    self?.keychain.delete("appToken")
                                }
                                
                                //case when user logs in with different username & password
                                if user != usernameLocal && (pass != usernamePassword) {
                                    self?.petCoreDataService?.deleteAllRecords()
                                    self?.keychain.set("true", forKey: "deleteCookies")
                                    self?.keychain.delete("username")
                                    self?.keychain.delete("password")
                                    self?.keychain.delete("appToken")
                                }
                                
                                //case when user logs in with the same username, but different password
                                if user == usernameLocal && ((self?.isValidEmail(user)) != nil)  && (pass != usernamePassword ) {
                                    self?.petCoreDataService?.deleteAllRecords()
                                    self?.keychain.set("true", forKey: "deleteCookies")
                                    self?.keychain.delete("username")
                                    self?.keychain.delete("password")
                                    self?.keychain.delete("appToken")
                                }
                                
                                //case when user logs in with the same password, but different username
                                if user != usernameLocal && ((self?.isValidEmail(user)) != nil)  && (pass == usernamePassword ) {
                                    self?.petCoreDataService?.deleteAllRecords()
                                    self?.keychain.set("true", forKey: "deleteCookies")
                                    self?.keychain.delete("username")
                                    self?.keychain.delete("password")
                                    self?.keychain.delete("appToken")
                                }
                            }
                            
                            self?.keychain.set(user, forKey: "username")
                            self?.keychain.set(pass, forKey: "password")
                            
                            UserDefaults.standard.set(nil, forKey: "isAppKilled")
                            UserDefaults.standard.set(nil, forKey: "isAppLogout")
                            
                            DispatchQueue.main.async {
                                Spinner.stop()
                            }
                            if self?.keychain.get("isFirstLaunch") == "disabled" {
                                DispatchQueue.main.async {
                                    self?.tabBarController?.selectedIndex = 0 //you can select another tab if needed
                                    let vc = HomeViewController(withCoreDataStack: CoreDataStack())
                                    if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                                       navController.pushViewController(vc, animated: true)
                                    }
                                }
                            } else {
                                DispatchQueue.main.async {
                                    self?.tabBarController?.selectedIndex = 0 //you can select another tab if needed
                                    let vc = InformationViewController()
                                    if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                                       navController.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                        case .failure(let error):
                            DispatchQueue.main.async {
                                Spinner.stop()
                                self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)

                            }
                        }
                    }) { [weak self] error in
                        switch error {
                        case .success(let value):
                            DispatchQueue.main.async {
                                Spinner.stop()
                                self?.showAlert(withTitle: "", message: value, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            }
                        case .failure(_):
                            DispatchQueue.main.async {
                                Spinner.stop()
                                self?.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            }
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        Spinner.stop()
                        self.showAlert(withTitle: "", message: Constants.errorMessages.emptyField, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    }
                }
            }
        } else {
            let usernameLocal = self.keychain.get("username")
            let usernamePassword = self.keychain.get("password")
            if newUser != "" && newUser != usernameLocal {
                self.keychain.set(newUser, forKey: "username")
            }
            if newPass != "" && newPass != usernamePassword {
                self.keychain.set(newPass, forKey: "password")
            }
            guard let user = self.keychain.get("username"), let pass = self.keychain.get("password") else { return }
            DispatchQueue.main.async {
                Spinner.start()
            }
            
            if (self.usernameTextField == "" && self.passwordTextField == "") && isLoginTapped == true {
                DispatchQueue.main.async {
                    Spinner.stop()
                    self.showAlert(withTitle: "", message: Constants.errorMessages.enterFields, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                }
            }
            if (user != textFieldUsername.text && pass != textFieldPassword.text && textFieldUsername.text != "" && textFieldPassword.text != "") {
                DispatchQueue.main.async {
                    Spinner.stop()
                    self.showAlert(withTitle: "", message: Constants.errorMessages.emptyField, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                }
            }
//            if (user != textFieldUsername.text || pass != textFieldPassword.text) && (textFieldUsername.text != "" && textFieldPassword.text != "") {
//                DispatchQueue.main.async {
//                    Spinner.stop()
//                    self.showAlert(withTitle: "", message: Constants.errorMessages.emptyField, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
//                }
//            }
            else {
                if (user != textFieldUsername.text || pass != textFieldPassword.text) && isLoginTapped == true {
                    if (self.isValidEmail(self.usernameTextField!)) {
                        self.worker.loginUser(withUsername: self.usernameTextField!, withPassword: self.passwordTextField!, mobileUserId: userID, channelId: channelID, appKey: Constants.productionAcousticSDKKey, completionHandler: { [weak self] resultUser in
                            switch resultUser {
                            case .success(_):

                                if self?.usernameTextField != "" && self?.passwordTextField != "" && user != usernameLocal {
                                    self?.keychain.set(user, forKey: "username")
                                    self?.keychain.set(pass, forKey: "password")
                                    self?.petCoreDataService?.deleteAllRecords()
                                    self?.keychain.set("true", forKey: "deleteCookies")
                                    self?.keychain.delete("appToken")
                                }
                                
                                UserDefaults.standard.set(nil, forKey: "isAppKilled")
                                UserDefaults.standard.set(nil, forKey: "isAppLogout")
                                
                                DispatchQueue.main.async {
                                    Spinner.stop()
                                }
                                if self?.keychain.get("isFirstLaunch") == "disabled" {
                                    DispatchQueue.main.async {
                                        self?.tabBarController?.selectedIndex = 0 //you can select another tab if needed
                                        let vc = HomeViewController(withCoreDataStack: CoreDataStack())
                                        if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                                           navController.pushViewController(vc, animated: true)
                                        }
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        self?.tabBarController?.selectedIndex = 0 //you can select another tab if needed
                                        let vc = InformationViewController()
                                        if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                                           navController.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                            case .failure(let error):
                                DispatchQueue.main.async {
                                    Spinner.stop()
                                    self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)

                                }
                            }
                        }) { [weak self] error in
                            switch error {
                            case .success(let value):
                                DispatchQueue.main.async {
                                    Spinner.stop()
                                    self?.showAlert(withTitle: "", message: value, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                }
                            case .failure(_):
                                DispatchQueue.main.async {
                                    Spinner.stop()
                                    self?.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                }
                            }
                        }
                    }
                } else {
                    if (self.isValidEmail(user)) {
                        self.worker.loginUser(withUsername: user, withPassword: pass, mobileUserId: userID, channelId: channelID, appKey: Constants.productionAcousticSDKKey, completionHandler: { [weak self] resultUser in
                            switch resultUser {
                            case .success(_):
                                
                                if self?.usernameTextField != "" && self?.passwordTextField != "" && user != usernameLocal {
                                    self?.keychain.set(user, forKey: "username")
                                    self?.keychain.set(pass, forKey: "password")
                                    self?.petCoreDataService?.deleteAllRecords()
                                    self?.keychain.set("true", forKey: "deleteCookies")
                                    self?.keychain.delete("appToken")
                                }
                                
                                UserDefaults.standard.set(nil, forKey: "isAppKilled")
                                UserDefaults.standard.set(nil, forKey: "isAppLogout")
                                
                                DispatchQueue.main.async {
                                    Spinner.stop()
                                }
                                if self?.keychain.get("isFirstLaunch") == "disabled" {
                                    DispatchQueue.main.async {
                                        self?.tabBarController?.selectedIndex = 0 //you can select another tab if needed
                                        let vc = HomeViewController(withCoreDataStack: CoreDataStack())
                                        if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                                           navController.pushViewController(vc, animated: true)
                                        }
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        self?.tabBarController?.selectedIndex = 0 //you can select another tab if needed
                                        let vc = InformationViewController()
                                        if let navController = self?.tabBarController?.viewControllers?[0] as? UINavigationController {
                                           navController.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                            case .failure(let error):
                                DispatchQueue.main.async {
                                    Spinner.stop()
                                    self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)

                                }
                            }
                        }) { [weak self] error in
                            switch error {
                            case .success(let value):
                                DispatchQueue.main.async {
                                    Spinner.stop()
                                    self?.showAlert(withTitle: "", message: value, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                }
                            case .failure(_):
                                DispatchQueue.main.async {
                                    Spinner.stop()
                                    self?.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textFieldPassword {
            self.passwordTextField = textFieldPassword.text
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldUsername {
            textField.resignFirstResponder()
            textFieldUsername.becomeFirstResponder()
        } else{
            self.passwordTextField = textFieldPassword.text
            textField.resignFirstResponder()
            textFieldPassword.resignFirstResponder()
        }
        return true
    }
}
