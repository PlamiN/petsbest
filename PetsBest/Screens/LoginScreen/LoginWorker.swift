//
//  LoginWorker.swift
//  PetsBest
//
//  Created by Plamena on 9.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation
import KeychainSwift

typealias OnSuccessCompletionLogin = (Result<String>) -> ()
typealias OnErrorHandlerLogin = (Result<String>) -> ()

class LoginWorker {
    private let defaults = UserDefaults.standard
    private let keychain = KeychainSwift()
    
    func loginUser(withUsername: String, withPassword: String, mobileUserId: String, channelId: String, appKey: String, completionHandler: @escaping OnSuccessCompletionLogin,
                   onErrorHandler: @escaping OnErrorHandlerLogin) {
        if var url = try? APIRouter.loginUser.asURLRequest() {
            
            let parameterDictionary1 = ["Username": withUsername, "Password": withPassword,"grant_type": "password", "MobileUserId": mobileUserId, "ChannelId": channelId, "AppKey": appKey]
            
//            let parameterDictionary = ["Username": withUsername, "Password": withPassword,"grant_type": "password"]
            url.httpMethod = HTTPMethod.post.rawValue
            url.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary1, options: []) else {
                return
            }
            url.httpBody = httpBody
            
            URLSession.shared.dataTask(with: url) { data, response, error in
                
                if let error = error {
                    return onErrorHandler(.failure(error))
                }
                
                guard let content = data else {
                    print("No data")
                    return
                }
                
                
                if let httpResponse = response as? HTTPURLResponse {
                    let statusCode = httpResponse.statusCode
                    
                    if (statusCode != 200) {
                        //401 - Unauthorized
                        //403 - Forbidden
                        //403 - Not found
                        
                        if statusCode == 500 || statusCode == 502 || statusCode == 503 || statusCode == 404 {
                            return onErrorHandler(.success(Constants.errorMessages.serverError))
                        }
                        if statusCode == 400 {
                            return onErrorHandler(.success(Constants.errorMessages.usernamePasswordRequiredError))
                        }
                        if statusCode == 401 {
                            return onErrorHandler(.success(Constants.errorMessages.incorrectUsernamePasswordError))
                        } else {
                            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                                print("Not containing JSON")
                                return onErrorHandler(.success(Constants.errorMessages.emptyField))
                            }
                            
//                            guard let error = json["Message"] else { return }
//                            let errorString = "\(error)"
                            return onErrorHandler(.success(Constants.errorMessages.resetPassword))
                        }
                    } else {
                        
                        guard
                            let tokenString = String(data: content, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                            else { return }
                        print(tokenString)
                        UserDefaults.standard.set(tokenString, forKey: "appToken")
                        self.keychain.set(tokenString, forKey: "appToken")
                        completionHandler(.success(tokenString))
                    }
                }
            }.resume()
        }
    }
}
