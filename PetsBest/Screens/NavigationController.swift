//
//  NavigationController.swift
//  PetsBest
//
//  Created by Plamena on 9.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    // MARK: - Object lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
    }
    
    // MARK: - Navigation Settings
    // Note: Add more...
    private func setupNavigation() {
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = UIColor.init(hex: Constants.Colors.main)
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            UINavigationBar.appearance().tintColor = .white
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().compactAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
            if UIDevice().userInterfaceIdiom == .pad {
                if let font = UIFont (name: "Proxima Nova Bold", size: 35) {
                    let attributes = [.foregroundColor: UIColor.white, NSAttributedString.Key.font: font]
                    appearance.titleTextAttributes = attributes
                }
            }
        } else {
            UINavigationBar.appearance().tintColor = .white
            UINavigationBar.appearance().barTintColor = UIColor.init(hex: Constants.Colors.main)
            UINavigationBar.appearance().isTranslucent = false
            self.navigationBar.prefersLargeTitles = true
            self.navigationItem.largeTitleDisplayMode = .automatic
        }
    }
}
