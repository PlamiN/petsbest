//
//  FilterView.swift
//  PetsBest
//
//  Created by Plamena on 21.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import FSCalendar

class FilterView: UIView {
    
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var textFieldStatus: UITextField!
    @IBOutlet weak var textFieldStartDate: UITextField!
    @IBOutlet weak var textFieldEndDate: UITextField!
    @IBOutlet weak var labelFilterClaims: UILabel!
    @IBOutlet weak var imageClose: UIImageView!
    @IBOutlet weak var buttonFilter: UIButton!
    @IBOutlet weak var buttonClearResults: UIButton!
    @IBOutlet weak var labelFilterHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonFilterHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonClearResultsHeight: NSLayoutConstraint!
    @IBOutlet weak var imageCloseHeight: NSLayoutConstraint!
    @IBOutlet weak var imageCloseWidth: NSLayoutConstraint!
    @IBOutlet weak var textFieldStatusHeight: NSLayoutConstraint!
    @IBOutlet weak var textFieldStartDateHeight: NSLayoutConstraint!
    @IBOutlet weak var textFieldEndDateHeight: NSLayoutConstraint!
}

