//
//  CoreBenefitsViewController.swift
//  PetsBest
//
//  Created by Plamena on 12.11.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit
import KeychainSwift
import WebKit

class CoreBenefitsViewController: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var firstBenefit: UILabel!
    @IBOutlet weak var secondBenefit: UILabel!
    @IBOutlet weak var thirdBenefit: UILabel!
    @IBOutlet weak var fourthBenefit: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var buttonDownloadPolicy: UIButton!
    @IBOutlet weak var imageCheckboxFirstBenefit: UIImageView!
    @IBOutlet weak var imageCheckboxSecondBenefit: UIImageView!
    @IBOutlet weak var imageCheckboxThirdBenefit: UIImageView!
    @IBOutlet weak var imageCheckboxFourthBenefit: UIImageView!
    
    var selectedPet: PetCovers?
    var policyDocuments: [PolicyDocumentsModel]?
    
    private let keychain = KeychainSwift()
    
    private var webView = WKWebView()
    private let screenSize: CGRect = UIScreen.main.bounds
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setViews()
        labelsAndViewsIPadIphone()
    }
    
    // MARK: - Set UI
    private func setViews() {

        self.navigationItem.title = Constants.titlesScreens.coreBenefits
        self.navigationController?.navigationBar.isHidden = false
        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
        
        labelTitle.text = Constants.coreBenefitsScreen.title
        firstBenefit.text = Constants.coreBenefitsScreen.acciddents
        secondBenefit.text = Constants.coreBenefitsScreen.takeHome
        thirdBenefit.text = Constants.coreBenefitsScreen.fees
        fourthBenefit.text = Constants.coreBenefitsScreen.rehabilitation
        labelDetails.text = Constants.coreBenefitsScreen.details
        buttonDownloadPolicy.setTitle(Constants.coreBenefitsScreen.button, for: .normal)
        buttonDownloadPolicy.backgroundColor = UIColor.init(hex: Constants.Colors.gray)
        buttonDownloadPolicy.setTitleColor( UIColor.init(hex: Constants.Colors.PBHelpline), for: .normal)
        buttonDownloadPolicy.clipsToBounds = true
        buttonDownloadPolicy.layer.cornerRadius = 20
        
        labelTitle.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        firstBenefit.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        secondBenefit.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        thirdBenefit.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        fourthBenefit.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        labelDetails.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        
        if selectedPet?.IsBestBenefitPlan == true {
            imageCheckboxFirstBenefit.image = UIImage(named: "checkbox")
        } else {
            imageCheckboxFirstBenefit.image = UIImage(named: "closeRed")
        }
        
        if selectedPet?.HasTakeHomePrescriptions == true {
            imageCheckboxSecondBenefit.image = UIImage(named: "checkbox")
        } else {
            imageCheckboxSecondBenefit.image = UIImage(named: "closeRed")
        }
        
        if selectedPet?.HasExamFees == true {
            imageCheckboxThirdBenefit.image = UIImage(named: "checkbox")
        } else {
            imageCheckboxThirdBenefit.image = UIImage(named: "closeRed")
        }
        
        if selectedPet?.HasRehab == true {
            imageCheckboxFourthBenefit.image = UIImage(named: "checkbox")
        } else {
            imageCheckboxFourthBenefit.image = UIImage(named: "closeRed")
        }
    }
    
    private func labelsAndViewsIPadIphone() {
        if UIDevice().userInterfaceIdiom == .pad {
            labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 35)
            firstBenefit.font = UIFont(name: "Proxima Nova", size: 25)
            secondBenefit.font = UIFont(name: "Proxima Nova", size: 25)
            thirdBenefit.font = UIFont(name: "Proxima Nova", size: 25)
            fourthBenefit.font = UIFont(name: "Proxima Nova", size: 25)
            buttonDownloadPolicy.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 25)
        } else {
            labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 30)
            firstBenefit.font = UIFont(name: "Proxima Nova", size: 20)
            secondBenefit.font = UIFont(name: "Proxima Nova", size: 20)
            thirdBenefit.font = UIFont(name: "Proxima Nova", size: 20)
            fourthBenefit.font = UIFont(name: "Proxima Nova", size: 20)
            buttonDownloadPolicy.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 20)
        }
    }
    
    @objc private func back(){
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionDownloadPolicy(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            Spinner.start()
        }
        guard let document = policyDocuments?[0].DocumentUrl else {
            return
        }
        webView = WKWebView(frame: CGRect(x: 0, y: 80, width: screenSize.width, height: screenSize.height))
        let newDocument = document.dropLast(2)
        let fileName = newDocument.components(separatedBy: "/")[2].dropLast(4)
        
        print(fileName)
        savePdf(urlString: "https://qa.petsbest.com/customerportal\(document)", fileName: "\(fileName)")
    }
    
    func savePdf(urlString:String, fileName:String) {
        DispatchQueue.main.async { [self] in
            let url = URL(string: urlString)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "\(fileName).pdf"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            do {
                try pdfData?.write(to: actualPath, options: .atomic)
                print("pdf successfully saved!")
                
                if #available(iOS 10.0, *) {
                    do {
                        guard
                            var documentsURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last else {
                                //handle error when getting documents URL
                                return
                            }
                        documentsURL.appendPathComponent("\(fileName).pdf")
                        
                        DispatchQueue.main.async {
                            self.webView = WKWebView(frame: CGRect(x: 0, y: 80, width: self.screenSize.width, height: self.screenSize.height))
                            let urlRequest = URLRequest(url: documentsURL)
                            self.webView.load(urlRequest)
                            self.view.addSubview(self.webView)
                            Spinner.stop()
                            let objectsToShare = [documentsURL]
                            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                            self.present(activityVC, animated: true)
                            activityVC.popoverPresentationController?.sourceView = view
                        }
                        
                    } catch {
                        print("could not locate pdf file !!!!!!!")
                    }
                }
            } catch {
                print("Pdf could not be saved")
            }
        }
    }
}
