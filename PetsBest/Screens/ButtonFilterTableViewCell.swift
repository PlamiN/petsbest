//
//  ButtonFilterTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 3.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class ButtonFilterTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet weak var buttonFilter: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
