//
//  PetsCollectionViewCell.swift
//  PetsBest
//
//  Created by Plamena on 20.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol PetsCollectionViewCellDelegate {
    func openMyPetsScreen(index: Int)
}

class PetsCollectionViewCell: UICollectionViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var imageViewPet: UIImageView!
    @IBOutlet private weak var labelName: UILabel!
    @IBOutlet private weak var outerView: UIView!
    @IBOutlet private weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet private weak var imageViewWidth: NSLayoutConstraint!
    @IBOutlet private weak var topConstraint: NSLayoutConstraint!
    
    var delegate: PetsCollectionViewCellDelegate?
    var index: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        if UIDevice().userInterfaceIdiom == .pad {
            imageViewHeight.constant = 430
            imageViewWidth.constant = 430
            labelName.textColor = UIColor.white
            outerView.layer.cornerRadius = (430 / 2)
            outerView.layer.shadowColor = UIColor.lightGray.cgColor
            outerView.layer.shadowOffset = CGSize(width: 0, height: 2)
            outerView.layer.shadowOpacity = 1
            outerView.layer.shadowRadius = 7
            outerView.backgroundColor = UIColor.white
            labelName.font = UIFont(name: "Proxima Nova Bold", size: 55)
        }
        if UIDevice().userInterfaceIdiom == .phone {
            if UIScreen.main.nativeBounds.height == 1334 {
                imageViewHeight.constant = 200
                imageViewWidth.constant = 200
                outerView.layer.cornerRadius = (200 / 2)
            } else {
                outerView.layer.cornerRadius = (imageViewPet.frame.height / 2)
            }
            if UIScreen.main.nativeBounds.height == 1334 {
                topConstraint.constant = 56
            } else {
                topConstraint.constant = 24
            }
            labelName.textColor = UIColor.white
            outerView.layer.shadowColor = UIColor.lightGray.cgColor
            outerView.layer.shadowOffset = CGSize(width: 0, height: 2)
            outerView.layer.shadowOpacity = 1
            outerView.layer.shadowRadius = 7
            outerView.backgroundColor = UIColor.white
            labelName.font = UIFont(name: "Proxima Nova Bold", size: 35)
        }
        
        let tapOpenMyPets = UITapGestureRecognizer(target: self, action:  #selector(openMyPets))
        imageViewPet.isUserInteractionEnabled = true
        imageViewPet.addGestureRecognizer(tapOpenMyPets)
    }
    
    func configureCell(withName: String, withImage: UIImage?, withIndex: Int) {
        labelName.text = withName
        index = withIndex
        if withImage != nil {
            imageViewPet.image = withImage
            imageViewPet.maskCircle(anyImage: withImage!)
            imageViewPet.contentMode = .scaleAspectFill
        } else {
            imageViewPet.image = UIImage(named: "dog_placeholder")
        }
    }
    
    @objc func openMyPets(){
        delegate?.openMyPetsScreen(index: index!)
    }
}
