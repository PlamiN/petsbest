//
//  ProfileImageTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 23.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol ProfileImageDelegate {
    func closeView()
    func openDeviceImages()
}

class ProfileImageTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var viewProfilePhoto: UIView!
    @IBOutlet private weak var imageProfilePlaceHolder: UIImageView!
    @IBOutlet private weak var imageViewClose: UIImageView!
    @IBOutlet private weak var imageViewPhoto: UIImageView!
    @IBOutlet private weak var imageUploadPhoto: UIImageView!
    @IBOutlet private weak var viewOverlappingPhoto: UIView!
    @IBOutlet private weak var labelName: UILabel!
    @IBOutlet private weak var imageUploadPhotoHeight: NSLayoutConstraint!
    @IBOutlet private weak var imageProfileHeight: NSLayoutConstraint!
    @IBOutlet private weak var imageProfileWidth: NSLayoutConstraint!
    @IBOutlet private weak var imageCloseWidth: NSLayoutConstraint!
    @IBOutlet private weak var imageCloseHeight: NSLayoutConstraint!
    
    var delegate: ProfileImageDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        viewProfilePhoto.backgroundColor = UIColor.init(hex: Constants.Colors.main)!
        let opacity:CGFloat = 0.1
        let backgroundColor = UIColor.init(hex: Constants.Colors.main)
        viewProfilePhoto.backgroundColor = backgroundColor!.withAlphaComponent(opacity)
        
        let tapClose = UITapGestureRecognizer(target: self, action:  #selector(closeProfileView))
        imageViewClose.isUserInteractionEnabled = true
        imageViewClose.addGestureRecognizer(tapClose)
        
        let tapOpenImages1 = UITapGestureRecognizer(target: self, action:  #selector(openDeviceImages))
        imageUploadPhoto.isUserInteractionEnabled = true
        imageUploadPhoto.addGestureRecognizer(tapOpenImages1)
        
        let tapOpenImages = UITapGestureRecognizer(target: self, action:  #selector(openDeviceImages))
        imageProfilePlaceHolder.isUserInteractionEnabled = true
        imageProfilePlaceHolder.addGestureRecognizer(tapOpenImages)
        
        viewOverlappingPhoto.clipsToBounds = true
        viewOverlappingPhoto.layer.cornerRadius = 35
        viewOverlappingPhoto.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        labelName.textColor = UIColor.init(hex: Constants.Colors.labels)!
        
        if UIDevice().userInterfaceIdiom == .pad {
            imageProfileHeight.constant = 280
            imageProfileWidth.constant = 280
            imageCloseWidth.constant = 90
            imageCloseWidth.constant = 90
            imageUploadPhotoHeight.constant = 70
            labelName.font = UIFont(name: "Proxima Nova Bold", size: 45)
        }
    }
    
    func configureCell(withName: String, withImage: UIImage?) {
        labelName.text = withName
        if withImage != nil {
            DispatchQueue.main.async {
                self.imageViewPhoto.image = withImage
            }
            imageProfilePlaceHolder.isHidden = true
            imageUploadPhoto.isHidden = false
        } else {
            DispatchQueue.main.async {
                self.imageViewPhoto.image = UIImage(named: "backgroundGradient")
            }
            imageProfilePlaceHolder.isHidden = false
            imageUploadPhoto.isHidden = true
        }
    }
    
    @objc func closeProfileView() {
        delegate?.closeView()
    }
    
    @objc func openDeviceImages() {
        delegate?.openDeviceImages()
    }
}
