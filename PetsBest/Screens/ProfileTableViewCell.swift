//
//  ProfileTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 14.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol ProfileDelegate {
    func downloadPolicy()
}

class ProfileTableViewCell: UITableViewCell, Identifiable, ReusableView {
    
    @IBOutlet private weak var labelName: UILabel!
    @IBOutlet private weak var labelPolicyNumber: UILabel!
    @IBOutlet private weak var labelDetails: UILabel!
    @IBOutlet private weak var labelBreed: UILabel!
    @IBOutlet private weak var labelMemberSince: UILabel!
    @IBOutlet private weak var viewAllElements: UIView!
    @IBOutlet private weak var viewSeparator: UIView!
    @IBOutlet private weak var imageLogoHeight: NSLayoutConstraint!
    @IBOutlet private weak var stackViewHeight: NSLayoutConstraint!
    @IBOutlet private weak var stackViewWidth: NSLayoutConstraint!
    
    var delegate: ProfileDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        viewAllElements.layer.cornerRadius = 10.0
        
        if self.traitCollection.userInterfaceStyle == .dark{
            labelName.textColor = UIColor.white
            labelPolicyNumber.textColor = UIColor.white
            labelDetails.textColor = UIColor.white
            labelBreed.textColor = UIColor.white
            labelMemberSince.textColor = UIColor.white
            labelPolicyNumber.textColor = UIColor.white
        } else{
            labelName.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
            labelPolicyNumber.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
            labelDetails.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
            labelBreed.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
            labelMemberSince.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
            labelPolicyNumber.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        }
        //labelBenefits.textColor = UIColor.init(hex: Constants.Colors.main)
        viewSeparator.backgroundColor = UIColor.init(hex: Constants.Colors.main)

        contentView.layer.borderWidth = 1
        contentView.layer.cornerRadius = 5
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true
        contentView.layer.shadowOpacity = 0.18
        contentView.layer.shadowOffset = CGSize(width: 0, height: 2)
        contentView.layer.shadowRadius = 2
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.masksToBounds = false
        
        if UIDevice().userInterfaceIdiom == .pad {
            labelName.font = UIFont(name: "Proxima Nova Bold", size: 35)
            labelPolicyNumber.font = UIFont(name: "Proxima Nova", size: 25)
            labelDetails.font = UIFont(name: "Proxima Nova", size: 25)
            labelBreed.font = UIFont(name: "Proxima Nova", size: 25)
            labelMemberSince.font = UIFont(name: "Proxima Nova", size: 25)
            labelPolicyNumber.font = UIFont(name: "Proxima Nova", size: 25)
            imageLogoHeight.constant = 80
            stackViewWidth.constant = 300
            stackViewHeight.constant = 80
        } else {
            labelName.font = UIFont(name: "Proxima Nova Bold", size: 20)
            labelPolicyNumber.font = UIFont(name: "Proxima Nova", size: 12)
            labelDetails.font = UIFont(name: "Proxima Nova", size: 14)
            labelBreed.font = UIFont(name: "Proxima Nova", size: 14)
            labelMemberSince.font = UIFont(name: "Proxima Nova", size: 14)
            labelPolicyNumber.font = UIFont(name: "Proxima Nova", size: 14)
        }
        
        selectionStyle = .none
//        let tapDownloadPolicy = UITapGestureRecognizer(target: self, action:  #selector(downloadPolicy))
//        labelBenefits.isUserInteractionEnabled = true
//        labelBenefits.addGestureRecognizer(tapDownloadPolicy)
    }
    
    func configureCell(withName: String, withPolicyNumber: String, withDetails: String, withBreed: String, withMembership: String) {
        labelName.text = withName
        labelBreed.text = withBreed
        labelDetails.text = withDetails
        labelMemberSince.text = withMembership
        labelPolicyNumber.text = ""
    }
    
    @objc func downloadPolicy() {
        //delegate?.downloadPolicy()
        //https://stagingnew.petsbest.com/mobileApi/Policy/download.ashx?file=Policies/POL_APIC_1_PETS_PolicyBookletAnnualIllness_ID_v1.pdf&a
    }
}
