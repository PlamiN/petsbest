//
//  FirstProcessedTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 5.02.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit

class FirstProcessedTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var viewAllElements: UIView!
    @IBOutlet private weak var labelClaim: UILabel!
    @IBOutlet private weak var labelPolicy: UILabel!
    @IBOutlet private weak var labelServiceDate: UILabel!
    @IBOutlet private weak var labelServiceDateDetails: UILabel!
    @IBOutlet private weak var labelClaimType: UILabel!
    @IBOutlet private weak var labelClaimTypeDetails: UILabel!
    @IBOutlet private weak var labelCondition: UILabel!
    @IBOutlet private weak var labelConditionDetails: UILabel!
    @IBOutlet private weak var labelClaimStatus: UILabel!
    @IBOutlet private weak var labelClaimStatusDetails: UILabel!
    @IBOutlet private weak var firstImage: UIImageView!
    @IBOutlet private weak var viewFirst: UIView!
    @IBOutlet private weak var secondImage: UIImageView!
    @IBOutlet private weak var secondView: UIView!
    @IBOutlet private weak var thirdImage: UIImageView!
    @IBOutlet private weak var topStack: UIStackView!
    @IBOutlet private weak var viewGreyTop: UIView!
    @IBOutlet private weak var buttonViewDetails: UIButton!
    @IBOutlet private weak var labelClaimHeight: NSLayoutConstraint!
    @IBOutlet private weak var stackViewClaimHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelServiceDateHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelClaimTypeHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelConditionHeight: NSLayoutConstraint!
    @IBOutlet private weak var lableClaimStatusHeight: NSLayoutConstraint!
    @IBOutlet private weak var firstImageHeight: NSLayoutConstraint!
    @IBOutlet private weak var firstImageWidth: NSLayoutConstraint!
    @IBOutlet private weak var secondImageWidth: NSLayoutConstraint!
    @IBOutlet private weak var secondImageHeight: NSLayoutConstraint!
    @IBOutlet private weak var thirdImageWidth: NSLayoutConstraint!
    @IBOutlet private weak var firstStackWidth: NSLayoutConstraint!
    @IBOutlet private weak var secondStackWidth: NSLayoutConstraint!
    @IBOutlet private weak var buttonHeight: NSLayoutConstraint!
    
    var delegate: ProcessedClaimDelegate?
    var delegate1: NotFinishedClaimDelegate?
    
    private var index: Int?
    private let worker = ClaimDetailsWorker()
    private var statusName = ""
    private var availableEOB = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }

    private func setViews() {
        viewAllElements.layer.cornerRadius = 10.0
        
        viewGreyTop.clipsToBounds = true
        viewGreyTop.layer.cornerRadius = 10
        viewGreyTop.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        buttonViewDetails.clipsToBounds = true
        buttonViewDetails.layer.cornerRadius = 10
        buttonViewDetails.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        
        if self.traitCollection.userInterfaceStyle == .dark{
            labelClaim.textColor = UIColor.white
            //labelPolicy.textColor = UIColor.white
            labelServiceDate.textColor = UIColor.white
            labelServiceDateDetails.textColor = UIColor.white
            labelClaimType.textColor = UIColor.white
            labelClaimTypeDetails.textColor = UIColor.white
            labelCondition.textColor = UIColor.white
            labelConditionDetails.textColor = UIColor.white
            labelClaimStatus.textColor = UIColor.white
            labelClaimStatusDetails.textColor = UIColor.white
        } else{
            labelClaim.textColor = UIColor.init(hex: Constants.Colors.labels)!
            //labelPolicy.textColor = UIColor.init(hex: Constants.Colors.labels)!
            labelServiceDate.textColor = UIColor.init(hex: Constants.Colors.labels)!
            labelServiceDateDetails.textColor = UIColor.darkGray
            labelClaimType.textColor = UIColor.init(hex: Constants.Colors.labels)!
            labelClaimTypeDetails.textColor = UIColor.darkGray
            labelCondition.textColor = UIColor.init(hex: Constants.Colors.labels)!
            labelConditionDetails.textColor = UIColor.darkGray
            labelClaimStatus.textColor = UIColor.init(hex: Constants.Colors.labels)!
            labelClaimStatusDetails.textColor = UIColor.darkGray
        }
        
        viewFirst.backgroundColor = UIColor.init(hex: Constants.Colors.main)!
        secondView.backgroundColor = UIColor.init(hex: Constants.Colors.main)!
        firstImage.tintColor = UIColor.init(hex: Constants.Colors.main)!
        secondImage.tintColor = UIColor.init(hex: Constants.Colors.main)!
        thirdImage.tintColor = UIColor.init(hex: Constants.Colors.main)!
        
        contentView.layer.borderWidth = 1
        contentView.layer.cornerRadius = 5
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true
        contentView.layer.shadowOpacity = 0.18
        contentView.layer.shadowOffset = CGSize(width: 0, height: 2)
        contentView.layer.shadowRadius = 2
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.masksToBounds = false
        
        selectionStyle = .none
        
        if UIDevice().userInterfaceIdiom == .pad {
            labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 25)
            //labelPolicy.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelServiceDateDetails.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelClaimTypeDetails.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelCondition.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelConditionDetails.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelClaimStatus.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelClaimStatusDetails.font = UIFont(name: "Proxima Nova Bold", size: 25)
            buttonViewDetails.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelClaimHeight.constant = 80
            stackViewClaimHeight.constant = 80
            labelServiceDateHeight.constant = 60
            labelClaimTypeHeight.constant = 50
            labelConditionHeight.constant = 64
            lableClaimStatusHeight.constant = 60
            firstImageHeight.constant = 50
            firstImageWidth.constant = 50
            secondImageWidth.constant = 50
            secondImageHeight.constant = 50
            thirdImageWidth.constant = 50
            firstStackWidth.constant = 250
            secondStackWidth.constant = 250
            buttonHeight.constant = 70
        }
    }
    
    @IBAction func actionNavigateToDetails(_ sender: UIButton) {
        delegate?.navigateToDetails(index: self.index ?? 0, withEOB: availableEOB)
    }
    
    func configureCell(withClaimNumber: Int,withClaimType: String, withCondition: String, withDate: String, withClaimStatus: String, withPolicyNumber: String, withIndex: Int, withAvailableEOB: Bool, withButtonColor: String, withButtonName: String) {
        statusName = withClaimStatus
        availableEOB = withAvailableEOB
        buttonViewDetails.setTitle(Constants.titlesButtons.viewDetails, for: .normal)
        labelClaim.text = Constants.titlesLabels.labelClaim + "\(withClaimNumber)"
        labelClaimStatus.text = Constants.titlesLabels.labelStatusClaim
        labelClaimStatusDetails.text = withClaimStatus
        labelConditionDetails.text = withCondition
        labelServiceDateDetails.text = withDate
        //labelPolicy.text =  Constants.titlesLabels.labelPolicyNumber + withPolicyNumber
        index = withIndex
        
        buttonViewDetails.setTitle(withButtonName, for: .normal)
        buttonViewDetails.backgroundColor = UIColor.init(hex: withButtonColor)
        buttonViewDetails.setTitleColor(UIColor.white, for: .normal)
        
        worker.getClaimDetails(withClaimId: withClaimNumber, completionHandler: { [weak self] resultUser in
            switch resultUser {
            case .success(let resultUser):
                if resultUser.ClaimType == 0 {
                    self?.labelClaimTypeDetails.text = Constants.ClaimTypes.accident
                }
                if resultUser.ClaimType == 1 {
                    self?.labelClaimTypeDetails.text = Constants.ClaimTypes.ilness
                }
                if resultUser.ClaimType == 2 {
                    self?.labelClaimTypeDetails.text = Constants.ClaimTypes.wellness
                }
                if resultUser.ClaimType == 3 {
                    self?.labelClaimTypeDetails.text = Constants.ClaimTypes.cancer
                }
                if resultUser.ClaimType == 4 {
                    self?.labelClaimTypeDetails.text = Constants.ClaimTypes.feline
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.delegate1?.addErroMessage(withError: error.localizedDescription)
                }
            }
        }) { [weak self] error  in
            switch error {
            case .success(let errorString):
                DispatchQueue.main.async {
                    self?.delegate1?.addGeneralError(withError: errorString)
                }
            case .failure(let errorString):
                DispatchQueue.main.async {
                    self?.delegate1?.addGeneralError(withError: errorString.localizedDescription)
                }
            }
        }
    }
}

