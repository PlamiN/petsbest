//
//  ClaimPaymentsModelList.swift
//  PetsBest
//
//  Created by Plamena on 20.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

struct ClaimPaymentsModelList: Codable {
     let CheckDate: String
     let CheckNum: Int
     let ServiceDateRange: String
     let PaymentItems: [PaymentItems]
     let EOBDocumentIds: [String]
     let ClaimId: Int
     let StatusCode: String
     let DateReceived: String
     let StatusName: String
     let Description: String
     let PetInfo: PetInfo
     let IncidentDate: String
     let PolicyNumber: Int
     let ClaimType: Int
     let Diagnosis: String?
}

struct PaymentItems: Codable {
    let Id: Int
    let ChargedAmount: Double
    let IneligibleAmount: Double
    let DeductibleAmount: Double
    let CoPayAmount: Double
    let ReimbursementAmount: Double
    let Description: String
    let FullReasonDescription: String
}

