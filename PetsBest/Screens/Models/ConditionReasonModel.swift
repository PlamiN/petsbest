//
//  ConditionReasonModel.swift
//  PetsBest
//
//  Created by Plamena on 3.12.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

struct ConditionReasonModel: Codable {
    let ConditionCode: Int
    let ConditionDescription: String
    let SubConditions: [SubConditions]
}

struct SubConditions: Codable {
    let SubConditionCode: Int
    let SubConditionDescription: String
}

