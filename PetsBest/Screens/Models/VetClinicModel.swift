//
//  VetClinicModel.swift
//  PetsBest
//
//  Created by Plamena on 23.10.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

struct VetClinicModel: Codable {
    let NAME: String
    let URL: String
    let NAMEONLY: String
    let CITYONLY: String
    let ID: String
}
