//
//  UpdateClaimModel.swift
//  PetsBest
//
//  Created by Plamena on 29.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

struct UpdateClaimModel: Codable {
    let Success: Bool
    let Message: String?
    let ClaimId: Int
}
