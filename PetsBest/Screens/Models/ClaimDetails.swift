//
//  ClaimDetails.swift
//  PetsBest
//
//  Created by Plamena on 17.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

struct ClaimDetails: Codable {
     let ClaimId: Int
     let StatusCode: String
     let DateReceived: String
     let StatusName: String
     let Description: String
     let PetInfo: PetInfo
     let IncidentDate: String
     let PolicyNumber: Int
     let ClaimType: Int
     let Diagnosis: String?
}

struct PetInfo: Codable {
    let PolicyNumber: Int
    let PolicyLineNum: Int
    let PetName: String
    let BreedName: String
    let Sex: String
    let Species: String
    let Age: String
    let IsCanceled: Bool
    let PolicyRenewalDate: String
    let IsHidden: Bool
    let FirstStartDate: String
}
