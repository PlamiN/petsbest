//
//  TabBarModel.swift
//  PetsBest
//
//  Created by Plamena on 20.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

enum TabBarModel {

    struct Response {

        var children: [TabBarItemType]
        var isError: Bool
        var errorMessage: String?
    }

    struct ViewModel {
        var type: TabBarItemType
        var tabBarItem: UITabBarItem {
            return TabBarItemFactory.create(type: type)
        }
    }
}

enum TabBarItemType: CaseIterable {
    case myPets
    case submitClaim
    case petHelp
    case more
}
