//
//  ClaimModel.swift
//  PetsBest
//
//  Created by Plamena on 11.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

struct ClaimModel: Codable {
    let ClaimId: Int
    let StatusCode: String
    let DateReceived: String
    let StatusName: String
    let Description: String
    let PetName: String
    let PaymentNum: Int
    let CanUpdate: Bool
}
