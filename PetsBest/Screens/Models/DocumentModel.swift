//
//  DocumentModel.swift
//  PetsBest
//
//  Created by Plamena on 20.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

struct DocumentModel: Codable {
    let Filename: String
    let MimeType: String
    let DataBase64Encoded: String
}
