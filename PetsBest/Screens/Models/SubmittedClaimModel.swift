//
//  SubmittedClaimModel.swift
//  PetsBest
//
//  Created by Plamena on 17.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

struct SubmittedClaimModel: Codable {
     let ClaimId: Int
     let Message: String?
     let Success: Bool
}
