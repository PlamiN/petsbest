//
//  SavedVetModel.swift
//  PetsBest
//
//  Created by Plamena on 4.12.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

struct SavedVets: Codable {
    let RowNum: Int
    let PolicyHolderId: Int
    let FullName: String
    let Name: String
    let City: String
    let HospitalId: Int
    let Url: String
}
