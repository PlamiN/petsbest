//
//  PolicyHolderModel.swift
//  PetsBest
//
//  Created by Plamena on 11.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

struct PolicyHolderModel: Codable {
    let FirstName: String
    let LastName: String
    let Address: String
    let City: String
    let State: String
    let Zip: String
    let PhoneNumber: String
    let Email: String
}

