//
//  PolicyModel.swift
//  PetsBest
//
//  Created by Plamena on 13.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

struct PetCoversList: Codable {
     let PetCovers: [PetCovers]
     let InsuranceCardId: String
     let PolicyDocuments: [PolicyDocumentsModel]
     let PolicyNumber: Int
     let PolicyLineNum: Int
     let PetName: String
     let BreedName: String
     let Sex: String
     let Species: String
     let Age: String
     let IsCanceled: Bool
     let PolicyRenewalDate: String
     let IsHidden: Bool
     let FirstStartDate: String
}

struct PetCovers: Codable {
    let PlanName: String
    let PlanId: Int
    let IsBestBenefitPlan: Bool
    let IsAccidentPlan: Bool
    let IsCancerPlan: Bool
    let IsFelinePlan: Bool
    let IsRoutineCarePlan: Bool
    let Limit: String
    let PlanDescription: String
    let PlanTypeDescription: String
    let Deductible: Int
    let DeductibleDescription: String
    let Reimbursement: Int
    let UnderWriter: String
    let HasTakeHomePrescriptions: Bool
    let HasExamFees: Bool
    let HasRehab: Bool
    let HasHolistic: Bool
}

struct PolicyDocumentsModel: Codable {
    let Title: String
    let Description: String?
    let DocumentId: String?
    let DocumentUrl: String?
    let Category: String
}
