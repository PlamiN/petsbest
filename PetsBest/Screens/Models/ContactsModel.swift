//
//  ContactsModel.swift
//  PetsBest
//
//  Created by Plamena on 13.11.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

struct ContactsModel: Codable {
    let Monday: Monday
    let Tuesday: Tuesday
    let Wednesday: Wednesday
    let Thursday: Thursday
    let Friday: Friday
    let Saturday: Saturday
    let Sunday: String?
}

struct Monday: Codable {
    let OpenTime12: String
    let CloseTime12: String
    let OpenTime24: String
    let CloseTime24: String
}

struct Tuesday: Codable {
    let OpenTime12: String
    let CloseTime12: String
    let OpenTime24: String
    let CloseTime24: String
}

struct Wednesday: Codable {
    let OpenTime12: String
    let CloseTime12: String
    let OpenTime24: String
    let CloseTime24: String
}

struct Thursday: Codable {
    let OpenTime12: String
    let CloseTime12: String
    let OpenTime24: String
    let CloseTime24: String
}

struct Friday: Codable {
    let OpenTime12: String
    let CloseTime12: String
    let OpenTime24: String
    let CloseTime24: String
}

struct Saturday: Codable {
    let OpenTime12: String
    let CloseTime12: String
    let OpenTime24: String
    let CloseTime24: String
}

