//
//  AccountSettingsViewController.swift
//  PetsBest
//
//  Created by Plamena on 16.11.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class AccountSettingsViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    private let titleArray = [Constants.titlesScreens.personlaInfo, Constants.titlesScreens.paymentInformation]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        configTableViewProperties()
    }
    
    private func setViews() {
        navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = Constants.titlesScreens.accountSettings
        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
        
        self.tabBarController?.delegate = self
    }
    
    @objc private func back(){
        self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
        let vc = MoreViewController()
        if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
           navController.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - Set tableView
    
    private func configTableViewProperties() {
        tableView.registerCell(SettingsTableViewCell.self)
        tableView.separatorStyle = .none
    }
}

extension AccountSettingsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .pad {
            if UIScreen.main.nativeBounds.height == 2048 {
                return 110.0
            } else {
                return 140.0
            }
        } else {
            return 80.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingsTableViewCell = tableView.dequeueCell(SettingsTableViewCell.self)
        
        cell.selectionStyle = .none
        
        cell.configureCell(withTitle: titleArray[indexPath.row], withImage: UIImage(named: "arrowRight"), withIndexPath: IndexPath(row: indexPath.row, section: indexPath.section))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
//            let vc = EditPersonalInformationViewController()
//            navigationController?.pushViewController(vc, animated: true)
            
            if let url = try? APIRouter.personal_info.asURLRequestCustomerPortal() {
                
                self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
                let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                vc.openedFromScreen = "AccountSettings"
                if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
                   navController.pushViewController(vc, animated: true)
                }
            }
        }
        if indexPath.row == 1 {
            if let url = try? APIRouter.payment_information.asURLRequestCustomerPortal() {
                self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
                let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                vc.openedFromScreen = "AccountSettings"
                if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
                   navController.pushViewController(vc, animated: true)
                }
            }
        }
    }
}

extension AccountSettingsViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
//        if tabBarIndex == 0 {
//            guard let tabBarViewController = TabBarViewController.storyboardInstance() else { return }
//            self.present(tabBarViewController, animated: false, completion: nil)
//        }
    }
}
