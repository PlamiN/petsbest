var timeoutSeconds = 30;

var username = "%@";
var password = "%@";

var checkLogin = setInterval(checkLoginFields, 1000);
setTimeout(function() {
       clearInterval(checkLogin);
       }, timeoutSeconds * 1000);

function checkLoginFields() {
  var usernameInput = document.getElementById('Username');
  var passwordInput = document.getElementById('Password');
  var signInButton = document.getElementById("login-form");
  if (signInButton == null) {
    return;
  }
  usernameInput.value = username;
  passwordInput.value = password;
  document.getElementById("RememberMe").checked = true;
  signInButton.click();
  clearInterval(checkLogin);
}
