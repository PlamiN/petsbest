//
//  MoreViewController.swift
//  PetsBest
//
//  Created by Plamena on 5.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import KeychainSwift

class MoreViewController: UIViewController {

    @IBOutlet private weak var labelCopyright: UILabel!
    @IBOutlet private weak var labelCopyrightSecond: UILabel!
    @IBOutlet private weak var labelCopyrightThird: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var buttonLogOut: UIButton!
    @IBOutlet private weak var imageLogoWidth: NSLayoutConstraint!
    @IBOutlet private weak var imageLogoHeight: NSLayoutConstraint!
    @IBOutlet private weak var buttonLogOutHeight: NSLayoutConstraint!
    @IBOutlet private weak var buttonLogOutWidth: NSLayoutConstraint!
    @IBOutlet private weak var labelVersionApp: UILabel!
    
    private let keychain = KeychainSwift()
    
    private let defaults = UserDefaults.standard
    
    private let titleArray = [Constants.titlesScreens.inbox, Constants.titlesScreens.myPets, Constants.titlesScreens.claimCenter, Constants.titlesScreens.accountSettings, Constants.titlesScreens.appSettings, Constants.titlesScreens.petsBestSupport, Constants.titlesScreens.underwriting]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        configTableViewProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        navigationController?.navigationBar.isHidden = true
    }
    
    private func setViews() {
        labelCopyright.textColor = UIColor.black
        
        if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            labelVersionApp.text = "Version: \(text)"
        }
        
        let year = Calendar.current.component(.year, from: Date())
        labelCopyrightSecond.text = Constants.titlesLabels.copyrightDetails + "\(year)"
        labelCopyright.text = Constants.titlesLabels.copyrightDetailsSecond
        labelCopyrightThird.text = Constants.titlesLabels.copyrightDetailsThird

        buttonLogOut.setTitle("Log Out", for: .normal)
        buttonLogOut.backgroundColor = UIColor.init(hex: Constants.Colors.orangeButton)

        buttonLogOut.setTitleColor(UIColor.white, for: .normal)
        buttonLogOut.layer.cornerRadius = 20
        labelsAndViewIPads()
        
        self.tabBarController?.delegate = self
    }
    
    private func labelsAndViewIPads() {
        if UIDevice().userInterfaceIdiom == .pad {
            if UIScreen.main.nativeBounds.height == 2048 || UIScreen.main.nativeBounds.height == 2388 || UIScreen.main.nativeBounds.height == 2160{
                labelCopyright.font = UIFont(name: "Proxima Nova", size: 20)
                labelCopyrightSecond.font = UIFont(name: "Proxima Nova", size: 20)
                labelCopyrightThird.font = UIFont(name: "Proxima Nova", size: 20)
                labelVersionApp.font = UIFont(name: "Proxima Nova", size: 20)
                buttonLogOut.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 40)
                buttonLogOutWidth.constant = 250
                buttonLogOutHeight.constant = 80
                buttonLogOut.layer.cornerRadius = 40
                imageLogoHeight.constant = 80
                imageLogoWidth.constant = 250
            }
            if UIScreen.main.nativeBounds.height == 2360 {
                labelCopyright.font = UIFont(name: "Proxima Nova", size: 25)
                labelCopyrightSecond.font = UIFont(name: "Proxima Nova", size: 25)
                labelCopyrightThird.font = UIFont(name: "Proxima Nova", size: 25)
                labelVersionApp.font = UIFont(name: "Proxima Nova", size: 25)
                buttonLogOut.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 40)
                buttonLogOutWidth.constant = 250
                buttonLogOutHeight.constant = 80
                buttonLogOut.layer.cornerRadius = 40
                imageLogoHeight.constant = 150
                imageLogoWidth.constant = 350
            } else {
                labelCopyright.font = UIFont(name: "Proxima Nova", size: 28)
                labelCopyrightSecond.font = UIFont(name: "Proxima Nova", size: 28)
                labelCopyrightThird.font = UIFont(name: "Proxima Nova", size: 28)
                labelVersionApp.font = UIFont(name: "Proxima Nova", size: 28)
                buttonLogOut.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 35)
                buttonLogOutWidth.constant = 250
                buttonLogOutHeight.constant = 90
                buttonLogOut.layer.cornerRadius = 45
                imageLogoHeight.constant = 120
                imageLogoWidth.constant = 400
            }
        }
    }
    
    @IBAction private func actionLogOut(_ sender: UIButton) {
        DispatchQueue.main.async {
            UserDefaults.standard.set(nil, forKey: "chosenOption")
            UserDefaults.standard.set("true", forKey: "isAppLogout")
            self.keychain.delete("isOpenedFromMore")
            self.tabBarController?.selectedIndex = 0
            let loginViewController = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: false)
            if let navController = self.tabBarController?.viewControllers?[0] as? UINavigationController {
                navController.pushViewController(loginViewController, animated: true)
            }
        }
    }
    
    // MARK: - Set tableView
    
    private func configTableViewProperties() {
        tableView.registerCell(SettingsTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
}

extension MoreViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .pad {
            if UIScreen.main.nativeBounds.height == 2048 {
                return 110.0
            } else {
                return 140.0
            }
        } else {
            if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2532 {
                return 70.0
            }
            if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1334 {
                return 55.0
            } else {
                return 80.0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingsTableViewCell = tableView.dequeueCell(SettingsTableViewCell.self)
        
        cell.selectionStyle = .none
        
        cell.configureCell(withTitle: titleArray[indexPath.row], withImage: UIImage(named: "arrowRight"), withIndexPath: IndexPath(row: indexPath.row, section: indexPath.section))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
            let vc = InboxTableViewController()
            if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        }
        
        if indexPath.row == 1 {
            self.keychain.set("true", forKey: "isOpenedFromMore")
            self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
            let vc = MyPetsViewController()
            if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        }
        
        if indexPath.row == 2 {
            self.keychain.set("true", forKey: "isOpenedFromMore")
            self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
            let vc = ClaimCenterViewController()
            if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        }
        
        if indexPath.row == 3 {
            self.keychain.set("true", forKey: "isOpenedFromMore")
            self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
            let vc = AccountSettingsViewController()
            if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        }
        
        if indexPath.row == 4 {
            self.keychain.set("true", forKey: "isOpenedFromMore")
            self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
            let vc = AppSettingsViewController()
            if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        }
        
        if indexPath.row == 5 {
            self.keychain.set("true", forKey: "isOpenedFromMore")
            if UIDevice().userInterfaceIdiom == .pad {
                self.tabBarController?.selectedIndex = 2 //you can select another tab if needed
                let vc = PetSupportPadViewController()
                vc.isOpenedFromMoreScreen = true
                if let navController = tabBarController?.viewControllers?[2] as? UINavigationController {
                   navController.pushViewController(vc, animated: true)
                }
            } else {
                self.tabBarController?.selectedIndex = 2 //you can select another tab if needed
                let vc = PetSupportViewController()
                vc.isOpenedFromMoreScreen = true
                if let navController = tabBarController?.viewControllers?[2] as? UINavigationController {
                   navController.pushViewController(vc, animated: true)
                }
            }
        }
        if indexPath.row == 6 {
            self.keychain.set("true", forKey: "isOpenedFromMore")
            if let url = try? APIRouter.get_underwriting.asURLRequestCustomerPortal() {
                self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
                let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.underwriting)
                vc.openedFromScreen = "Underwriting"
                if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
                   navController.pushViewController(vc, animated: true)
                }
            }
        }
    }
}

extension MoreViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 2 {
            self.defaults.removeObject(forKey: "notNowActivatedLocationServices")
        }
        if tabBarIndex == 0 {
            self.defaults.removeObject(forKey: "notNowActivatedLocationServices")
        }
        if tabBarIndex == 3 {
            self.defaults.removeObject(forKey: "notNowActivatedLocationServices")
        }
    }
}

