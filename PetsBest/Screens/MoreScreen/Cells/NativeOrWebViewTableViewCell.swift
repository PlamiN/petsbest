//
//  NativeOrWebViewTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 12.01.22.
//  Copyright © 2022 Plamena. All rights reserved.
//

import UIKit


class NativeOrWebViewTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet weak var labelNativeOrWebView: UILabel!
    @IBOutlet private weak var imageArrow: UIImageView!
    @IBOutlet private weak var imageArrowWidth: NSLayoutConstraint!
    @IBOutlet private weak var imageArrowHeight: NSLayoutConstraint!
    @IBOutlet private weak var trailingConstraintImageDownload: NSLayoutConstraint!
    
    private var indexPath: IndexPath?
    
    var delegate: SettingsTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        let tapOpenDocument = UITapGestureRecognizer(target: self, action:  #selector(openDocument))
        imageArrow.isUserInteractionEnabled = true
        imageArrow.addGestureRecognizer(tapOpenDocument)
        
        labelNativeOrWebView.textColor = UIColor.init(hex: Constants.Colors.orangeButton)
    }
    
    @objc private func openDocument(){
        delegate?.openDocument(byIndex: indexPath!)
    }
    
    func configureCell(withTitle: String, withSubtitle: String, withImage: UIImage?, withIndexPath: IndexPath) {
        
        indexPath = withIndexPath
        
        if UIDevice().userInterfaceIdiom == .pad {
            labelTitle.font = UIFont(name: "Proxima Nova", size: 35)
            labelNativeOrWebView.font = UIFont(name: "Proxima Nova Bold", size: 35)
            imageArrowHeight.constant = 35
            imageArrowWidth.constant = 75
        }
        
        imageArrowHeight.constant = 25
        imageArrowWidth.constant = 25
        trailingConstraintImageDownload.constant = 10
        imageArrow.image = withImage
        labelTitle.text = withTitle
        labelNativeOrWebView.text = withSubtitle
    }
}
