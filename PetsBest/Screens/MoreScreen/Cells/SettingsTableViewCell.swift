//
//  SettingsTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 5.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

protocol SettingsTableViewCellDelegate {
    func openDocument(byIndex: IndexPath)
}


class SettingsTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var imageArrow: UIImageView!
    @IBOutlet private weak var imageArrowWidth: NSLayoutConstraint!
    @IBOutlet private weak var imageArrowHeight: NSLayoutConstraint!
    @IBOutlet private weak var greyView: UIView!
    @IBOutlet private weak var trailingConstraintImageDownload: NSLayoutConstraint!
    
    private var indexPath: IndexPath?
    
    var delegate: SettingsTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.greyView.layer.cornerRadius = 20
        
        let tapOpenDocument = UITapGestureRecognizer(target: self, action:  #selector(openDocument))
        imageArrow.isUserInteractionEnabled = true
        imageArrow.addGestureRecognizer(tapOpenDocument)
    }
    
    @objc private func openDocument(){
        delegate?.openDocument(byIndex: indexPath!)
    }
    
    func configureCell(withTitle: String, withImage: UIImage?, withIndexPath: IndexPath) {
        
        indexPath = withIndexPath
        
        if UIDevice().userInterfaceIdiom == .pad {
            labelTitle.font = UIFont(name: "Proxima Nova", size: 35)
            imageArrowHeight.constant = 35
            imageArrowWidth.constant = 75
        }
        
        if withImage == UIImage(named: "download") {
            imageArrowHeight.constant = 25
            imageArrowWidth.constant = 25
            self.greyView.isHidden = false
            trailingConstraintImageDownload.constant = 10
        } else {
            self.greyView.isHidden = true
        }
        imageArrow.image = withImage
        labelTitle.text = withTitle
    }
}
