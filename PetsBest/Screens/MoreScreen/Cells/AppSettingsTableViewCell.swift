//
//  AppSettingsTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 18.11.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import CoreLocation

protocol AppSettingsDelegate {
    func enableFaceId(withMessages: String)
    func disableFaceId(withMessages: String)
    func enablePushNotifications(withMessages: String)
    func disablePushNotifications(withMessages: String)
    func enableLocationServices(withMessages: String)
    func disableLocationServices(withMessages: String)
}

class AppSettingsTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var switchONOFF: UISwitch!
    @IBOutlet private weak var labelTitle: UILabel!
    
    private var faceID: Bool = false
    private var pushNotifications: Bool = false
    private var cellIndex: Int?
    private let defaults = UserDefaults.standard
    var delegate: AppSettingsDelegate?
    var  locationManager = CLLocationManager()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        switchONOFF.tintColor = UIColor.init(hex: Constants.Colors.main)
        switchONOFF.onTintColor = UIColor.init(hex: Constants.Colors.main)
    }
    
    @IBAction func actionSwitch(_ sender: UISwitch) {
        if faceID == true {
            if sender.isOn {
                DispatchQueue.main.async {
                    self.delegate?.enableFaceId(withMessages: "Do you want to enable your Face ID?")
                }
            } else {
                DispatchQueue.main.async {
                    self.delegate?.disableFaceId(withMessages: "Do you want to disable your Face ID?")
                }
            }
        }
        if pushNotifications == true {
            if sender.isOn {
                DispatchQueue.main.async {
                    self.delegate?.enablePushNotifications(withMessages: "Do you want to enable your Push Notifications?")
                }
            } else {
                DispatchQueue.main.async {
                    self.delegate?.disablePushNotifications(withMessages: "Do you want to disable your Push Notifications?")
                }
            }
        }
        if faceID == false && pushNotifications == false {
            if sender.isOn {
                DispatchQueue.main.async {
                    self.delegate?.enableLocationServices(withMessages: "Do you want to enable your Location Services?")
                }
            } else {
                DispatchQueue.main.async {
                    self.delegate?.disableLocationServices(withMessages: "Do you want to disable your Location Services?")
                }
            }
        }
    }
    
    func configureCell(withTitle: String, isFaceID: Bool, isPushNotificationsDisabled: Bool, index: Int) {
        labelTitle.text = withTitle
        faceID = isFaceID
        cellIndex = index
        pushNotifications = isPushNotificationsDisabled
        
        if UIDevice().userInterfaceIdiom == .pad {
            labelTitle.font = UIFont(name: "Proxima Nova", size: 35)
        }
        
        let activatedPushNotifications = defaults.string(forKey: "PushNotifications")
        let firstActivatedPushNotifications = defaults.string(forKey: "firstPushNotifications")
        
        let activatedFaceId = defaults.string(forKey: "FaceID")
        let activatedLocationServices = defaults.string(forKey: "LocationServicesLocally")
        
        if cellIndex == 0 {
            if activatedFaceId == "activate" {
                DispatchQueue.main.async {
                    self.switchONOFF.setOn(true, animated: false)
                }
            }
            if defaults.string(forKey: "FaceID") == nil {
                DispatchQueue.main.async {
                    self.switchONOFF.setOn(false, animated: false)
                }
            }
            if activatedFaceId == "deactivate" {
                DispatchQueue.main.async {
                    self.switchONOFF.setOn(false, animated: false)
                }
            }
        }
        if cellIndex == 1 {
            if firstActivatedPushNotifications == nil {
                if activatedPushNotifications == "activate" {
                    DispatchQueue.main.async {
                        self.switchONOFF.setOn(true, animated: false)
                    }
                }
                if activatedPushNotifications == "deactivate" {
                    DispatchQueue.main.async {
                        self.switchONOFF.setOn(false, animated: false)
                    }
                }
            } else {
                if activatedPushNotifications == "activate" {
                    DispatchQueue.main.async {
                        self.switchONOFF.setOn(true, animated: false)
                    }
                }
                if activatedPushNotifications == "deactivate" {
                    DispatchQueue.main.async {
                        self.switchONOFF.setOn(false, animated: false)
                    }
                }
            }
        }
        if cellIndex == 2 {
            if activatedLocationServices != nil {
                if activatedLocationServices == "activate" {
                    DispatchQueue.main.async {
                        self.switchONOFF.setOn(true, animated: false)
                    }
                }
                if activatedLocationServices == "deactivate" {
                    DispatchQueue.main.async {
                        self.switchONOFF.setOn(false, animated: false)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.switchONOFF.setOn(false, animated: false)
                }
            }
        }
    }
}
