//
//  AccountWebViewViewController.swift
//  PetsBest
//
//  Created by Plamena on 16.11.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import WebKit
import KeychainSwift
import NVActivityIndicatorView
import FirebaseAnalytics

class AccountWebViewViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet private weak var webViewAccount: WKWebView!
    @IBOutlet private weak var activityIndicatorScreen: NVActivityIndicatorView!
    private var urlString: NSMutableURLRequest?
    private var titleString: String?
    private let keychain = KeychainSwift()
    private let defaults = UserDefaults.standard
    
    var openedFromScreen = ""
    
    init(withUrl: NSMutableURLRequest, withTitle: String) {
        super.init(nibName: nil, bundle: nil)
        urlString = withUrl
        titleString = withTitle
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        loadingWebView()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if keychain.get("deleteCookies") == "true" {
//            webViewAccount.cleanAllCookies()
//            webViewAccount.refreshCookies()
            self.keychain.delete("deleteCookies")
        }
    }
    
    //Firebase Analytics
    override func viewDidAppear(_ animated: Bool) {
        if titleString == Constants.titlesScreens.documentCenter {
            Analytics.logEvent(AnalyticsEventScreenView,
                               parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.documents])
        }
        if titleString == Constants.titlesScreens.forgotPassword {
            Analytics.logEvent(AnalyticsEventScreenView,
                               parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.forgotPassword])
        }
        
        if titleString == Constants.titlesScreens.register {
            Analytics.logEvent(AnalyticsEventScreenView,
                               parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.register])
        }
        
        if titleString == Constants.titlesScreens.accountSettings {
            Analytics.logEvent(AnalyticsEventScreenView,
                               parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.policyHolder])
        }
        
        if titleString == Constants.titlesScreens.petsBestSupport {
            Analytics.logEvent(AnalyticsEventScreenView,
                               parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.petHelpline])
        }
        

        Analytics.logEvent(Constants.FirebaseEvents.launchWebview, parameters: nil)
    }
    
    private func setViews() {
        webViewAccount.navigationDelegate = self
        webViewAccount.scrollView.bounces = false
        navigationController?.navigationBar.isHidden = false
        
        self.tabBarController?.delegate = self
        
        DispatchQueue.main.async {
            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self.back))
            self.startLoadingWholeScreen()
        }
    }
    
    private func startLoadingWholeScreen() {
        activityIndicatorScreen?.startAnimating()
    }
    
    private func stopLoadingWholeScreen() {
        activityIndicatorScreen?.stopAnimating()
    }
    
    private func loadingWebView() {
        
        DispatchQueue.main.async {
            if let urlStringUnwrapped = self.urlString {
                self.webViewAccount.load(urlStringUnwrapped as URLRequest)
            }
        }
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
       //load cookie of current domain
        webViewAccount.loadDiskCookies(for: (urlString?.url?.host)!){
            decisionHandler(.allow)
        }
    }

    public func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
       //write cookie for current domain
        webViewAccount.writeDiskCookies(for: (urlString?.url?.host)!){
            decisionHandler(.allow)
        }
    }
    
    internal func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        guard let jsFile = Bundle.main.url(forResource: "index", withExtension: "js") else {
            return
        }
        
        do {
            guard let username = self.keychain.get("username"),
                  let password = self.keychain.get("password")
            else {
                DispatchQueue.main.async {
                    self.stopLoadingWholeScreen()
                }
                return
            }
            
            let injectJS = try String(contentsOf: jsFile)
            let formatted = String(format: injectJS, username, password)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                webView.evaluateJavaScript(formatted) { [self] (value, error) in
                    self.stopLoadingWholeScreen()
                }
            }
        } catch {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                self.stopLoadingWholeScreen()
            }
        }
    }
    
    @objc private func back(){
        if openedFromScreen == "ClaimCenter" {
            self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
            let vc = ClaimCenterViewController()
            if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        }
        if openedFromScreen == "AccountSettings" {
            self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
            let vc = AccountSettingsViewController()
            if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        }
        if openedFromScreen == "MyPets" {
            self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
            let vc = MyPetsViewController()
            if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        }
        if openedFromScreen == "Underwriting" {
            let vc = MoreViewController()
            navigationController?.pushViewController(vc, animated: true)
        }
        
        if openedFromScreen == "LoginScreen" {
            self.dismiss(animated: false, completion: nil)
        }
        
        if openedFromScreen == "CreateAccountScreen" {
            self.dismiss(animated: false, completion: nil)
        }
        
        self.tabBarController?.tabBar.isHidden = false
    }
}

extension AccountWebViewViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
//        if tabBarIndex == 0 {
//            guard let tabBarViewController = TabBarViewController.storyboardInstance() else { return }
//            self.present(tabBarViewController, animated: false, completion: nil)
//        }
    }
}
