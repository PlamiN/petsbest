//
//  EditPersonalInformationViewController.swift
//  PetsBest
//
//  Created by Plamena on 6.10.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import KeychainSwift

public class UpdatedPolicy
{
    public var Address: String?
    public var City: String?
    public var Zip: String?
    public var PhoneNumber: String?
    public var Email: String?
}

extension UpdatedPolicy {
    convenience init(address: String, city: String, zipCode: String, phoneNumber: String, email: String) {
        self.init()
        self.Address = address
        self.City = city
        self.Zip = zipCode
        self.PhoneNumber = phoneNumber
        self.Email = email
    }
}

class EditPersonalInformationViewController: UIViewController {
    
    @IBOutlet private weak var textFieldFirstName: SkyFloatingLabelTextField!
    @IBOutlet private weak var textFieldLastName: SkyFloatingLabelTextField!
    @IBOutlet private weak var textfieldAddress: SkyFloatingLabelTextField!
    @IBOutlet private weak var textFieldCity: SkyFloatingLabelTextField!
    @IBOutlet private weak var textFieldState: SkyFloatingLabelTextField!
    @IBOutlet private weak var textFieldZipCode: SkyFloatingLabelTextField!
    @IBOutlet private weak var textFieldPhone: SkyFloatingLabelTextField!
    @IBOutlet private weak var textFieldEmail: SkyFloatingLabelTextField!
    @IBOutlet private weak var buttonSaveChanges: UIButton!
    @IBOutlet private weak var labelErrorMessageZipCode: UILabel!
    @IBOutlet private weak var labelCancel: UnderlinedLabel!
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var buttonSaveHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelErrorMessagePhoneNumber: UILabel!
    @IBOutlet private weak var labelErrorMessageEmail: UILabel!
    
    var policy: PolicyHolderModel?
    
    private let worker = PolicyHolderUpdateWorker()
    private let workerGet = PolicyHolderWorker()
    
    private let keychain = KeychainSwift()
    
    private var addressTextField: String?
    private var cityTextField: String?
    private var zipTextField: String?
    private var phoneTextField: String?
    private var emailTextField: String?
    
    private var editedTextField: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        labelsAndViewIPads()
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        //getNetworkingPolicyHolder()
        textFieldZipCode.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textFieldPhone.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textFieldEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textfieldAddress.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textFieldCity.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getNetworkingPolicyHolder()
    }
    
    private func labelsAndViewIPads() {
        textfieldAddress.returnKeyType = UIReturnKeyType.done
        textFieldCity.returnKeyType = UIReturnKeyType.done
        textFieldZipCode.returnKeyType = UIReturnKeyType.done
        textFieldEmail.returnKeyType = UIReturnKeyType.done
        textFieldPhone.returnKeyType = UIReturnKeyType.done
        
        textFieldZipCode.keyboardType = .numbersAndPunctuation
        textFieldPhone.keyboardType = .numbersAndPunctuation
        textFieldEmail.keyboardType = .emailAddress
        
        textfieldAddress.delegate = self
        textFieldCity.delegate = self
        textFieldZipCode.delegate = self
        textFieldEmail.delegate = self
        textFieldPhone.delegate = self
        
        if UIDevice().userInterfaceIdiom == .pad {
            if UIScreen.main.nativeBounds.height == 2048 || UIScreen.main.nativeBounds.height == 2160{
                textFieldFirstName.font = UIFont(name: "Proxima Nova", size: 27)
                textFieldLastName.font = UIFont(name: "Proxima Nova", size: 27)
                textFieldFirstName.titleFont = UIFont(name: "Proxima Nova", size: 25)!
                textFieldLastName.titleFont = UIFont(name: "Proxima Nova", size: 25)!
                buttonSaveChanges.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 27)
                textfieldAddress.font = UIFont(name: "Proxima Nova", size: 27)
                textFieldCity.font = UIFont(name: "Proxima Nova", size: 27)
                textfieldAddress.titleFont = UIFont(name: "Proxima Nova", size: 25)!
                textFieldCity.titleFont = UIFont(name: "Proxima Nova", size: 25)!
                textFieldState.font = UIFont(name: "Proxima Nova", size: 27)
                textFieldZipCode.font = UIFont(name: "Proxima Nova", size: 27)
                textFieldState.titleFont = UIFont(name: "Proxima Nova", size: 25)!
                textFieldZipCode.titleFont = UIFont(name: "Proxima Nova", size: 25)!
                textFieldPhone.font = UIFont(name: "Proxima Nova", size: 27)
                textFieldEmail.font = UIFont(name: "Proxima Nova", size: 27)
                textFieldPhone.titleFont = UIFont(name: "Proxima Nova", size: 25)!
                textFieldEmail.titleFont = UIFont(name: "Proxima Nova", size: 25)!
                buttonSaveChanges.layer.cornerRadius = 40
                labelCancel.font = UIFont(name: "Proxima Nova Bold", size: 27)
                labelErrorMessageZipCode.font = UIFont(name: "Proxima Nova Bold", size: 20)
                labelErrorMessageEmail.font = UIFont(name: "Proxima Nova Bold", size: 20)
                labelErrorMessagePhoneNumber.font = UIFont(name: "Proxima Nova Bold", size: 20)
                buttonSaveHeight.constant = 80
            } else {
                textFieldFirstName.font = UIFont(name: "Proxima Nova", size: 35)
                textFieldLastName.font = UIFont(name: "Proxima Nova", size: 35)
                textFieldFirstName.titleFont = UIFont(name: "Proxima Nova", size: 33)!
                textFieldLastName.titleFont = UIFont(name: "Proxima Nova", size: 33)!
                buttonSaveChanges.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 35)
                textfieldAddress.font = UIFont(name: "Proxima Nova", size: 35)
                textFieldCity.font = UIFont(name: "Proxima Nova", size: 35)
                textfieldAddress.titleFont = UIFont(name: "Proxima Nova", size: 33)!
                textFieldCity.titleFont = UIFont(name: "Proxima Nova", size: 33)!
                textFieldState.font = UIFont(name: "Proxima Nova", size: 35)
                textFieldZipCode.font = UIFont(name: "Proxima Nova", size: 35)
                textFieldState.titleFont = UIFont(name: "Proxima Nova", size: 33)!
                textFieldZipCode.titleFont = UIFont(name: "Proxima Nova", size: 33)!
                textFieldPhone.font = UIFont(name: "Proxima Nova", size: 35)
                textFieldEmail.font = UIFont(name: "Proxima Nova", size: 35)
                textFieldPhone.titleFont = UIFont(name: "Proxima Nova", size: 33)!
                textFieldEmail.titleFont = UIFont(name: "Proxima Nova", size: 33)!
                labelCancel.font = UIFont(name: "Proxima Nova Bold", size: 35)
                labelErrorMessageZipCode.font = UIFont(name: "Proxima Nova Bold", size: 20)
                labelErrorMessageEmail.font = UIFont(name: "Proxima Nova Bold", size: 20)
                labelErrorMessagePhoneNumber.font = UIFont(name: "Proxima Nova Bold", size: 20)
                buttonSaveChanges.layer.cornerRadius = 50
                buttonSaveHeight.constant = 100
                
                if UIScreen.main.nativeBounds.height == 2732 {
                    stackView.spacing = 60
                }
            }
        }
    }
    
    deinit {
      NotificationCenter.default.removeObserver(self)
    }
  
    @objc func keyboardWillShow(sender: NSNotification) {
         self.view.frame.origin.y = -150 // Move view 150 points upward
    }

    @objc func keyboardWillHide(sender: NSNotification) {
         self.view.frame.origin.y = 0 // Move view to original position
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func isValidPhoneNumber(_ phone: String) -> Bool {
        let emailRegEx = "(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: phone)
    }
    
    private func setViews() {
        navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = Constants.titlesScreens.personlaInfo
        buttonSaveChanges.setTitle(Constants.titlesButtons.saveChanges, for: .normal)
        buttonSaveChanges.setTitleColor(UIColor.white, for: .normal)
        buttonSaveChanges.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        buttonSaveChanges.layer.cornerRadius = 25
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1334:
                stackView.spacing = 17
            case 1920, 2208:
                stackView.spacing = 20
            default:
                print("Unknown")
            }
        }
        
        textFieldFirstName.title = Constants.textFieldsPlaceholder.firstName
        textFieldFirstName.titleFormatter = { $0 }
        textFieldFirstName.selectedTitleColor = UIColor.init(hex: Constants.Colors.PBHelpline)!
        textFieldFirstName.titleColor = UIColor.init(hex: Constants.Colors.PBHelpline)!
        textFieldFirstName.isEnabled = false
        textFieldFirstName.disabledColor = UIColor.darkGray
        
        textFieldLastName.title = Constants.textFieldsPlaceholder.lastName
        textFieldLastName.titleFormatter = { $0 }
        textFieldLastName.selectedTitleColor = UIColor.init(hex: Constants.Colors.PBHelpline)!
        textFieldLastName.titleColor = UIColor.init(hex: Constants.Colors.PBHelpline)!
        textFieldLastName.isEnabled = false
        textFieldLastName.disabledColor = UIColor.darkGray
        
        textfieldAddress.title = Constants.textFieldsPlaceholder.address
        textfieldAddress.titleFormatter = { $0 }
        textfieldAddress.selectedTitleColor = UIColor.init(hex: Constants.Colors.main)!
        textfieldAddress.titleColor = UIColor.init(hex: Constants.Colors.main)!
        textfieldAddress.titleFont = UIFont(name: "Proxima Nova Bold", size: 15)!
        textfieldAddress.delegate = self
        
        textFieldCity.title = Constants.textFieldsPlaceholder.city
        textFieldCity.titleFormatter = { $0 }
        textFieldCity.selectedTitleColor = UIColor.init(hex: Constants.Colors.main)!
        textFieldCity.titleColor = UIColor.init(hex: Constants.Colors.main)!
        textFieldCity.titleFont = UIFont(name: "Proxima Nova Bold", size: 15)!
        textFieldCity.delegate = self
        
        textFieldState.title = Constants.textFieldsPlaceholder.state
        textFieldState.titleFormatter = { $0 }
        textFieldState.selectedTitleColor = UIColor.init(hex: Constants.Colors.PBHelpline)!
        textFieldState.titleColor = UIColor.init(hex: Constants.Colors.PBHelpline)!
        textFieldState.isEnabled = false
        textFieldState.disabledColor = UIColor.darkGray
        
        textFieldZipCode.title = Constants.textFieldsPlaceholder.zipCode
        textFieldZipCode.titleFormatter = { $0 }
        textFieldZipCode.selectedTitleColor = UIColor.init(hex: Constants.Colors.main)!
        textFieldZipCode.titleColor = UIColor.init(hex: Constants.Colors.main)!
        textFieldZipCode.titleFont = UIFont(name: "Proxima Nova Bold", size: 15)!
        textFieldZipCode.delegate = self
        
        textFieldPhone.title = Constants.textFieldsPlaceholder.phone
        textFieldPhone.titleFormatter = { $0 }
        textFieldPhone.selectedTitleColor = UIColor.init(hex: Constants.Colors.main)!
        textFieldPhone.titleColor = UIColor.init(hex: Constants.Colors.main)!
        textFieldPhone.titleFont = UIFont(name: "Proxima Nova Bold", size: 15)!
        textFieldPhone.delegate = self
        
        textFieldEmail.title = Constants.textFieldsPlaceholder.email
        textFieldEmail.titleFormatter = { $0 }
        textFieldEmail.selectedTitleColor = UIColor.init(hex: Constants.Colors.main)!
        textFieldEmail.titleColor = UIColor.init(hex: Constants.Colors.main)!
        textFieldEmail.titleFont = UIFont(name: "Proxima Nova Bold", size: 15)!
        textFieldEmail.delegate = self
        
        labelCancel.text = Constants.titlesLabels.cancel
        let tapCancel = UITapGestureRecognizer(target: self, action:  #selector(cancelUpdate))
        labelCancel.isUserInteractionEnabled = true
        labelCancel.addGestureRecognizer(tapCancel)
        
        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
    }
    
    @objc private func cancelUpdate(){
        let vc = AccountSettingsViewController()
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc private func back(){
        let vc = AccountSettingsViewController()
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction private func actionSaveChanges(_ sender: UIButton) {
        if editedTextField == nil {
            if zipTextField!.isNumeric == false && zipTextField!.count > 5 {
                DispatchQueue.main.async {
                    self.showAlert(withTitle: "", message: Constants.errorMessages.validZipCode, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                }
            } else {
                networkingPolicyHolder()
            }
        } else {
            guard let editedRow = editedTextField?.row else { return }
            
            if editedRow == 4 {
                if zipTextField!.isNumeric == false && zipTextField!.count > 5 {
                    DispatchQueue.main.async {
                        self.showAlert(withTitle: "", message: Constants.errorMessages.validZipCode, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    }
                } else {
                    networkingPolicyHolder()
                }
            }
            if editedRow == 5 {
                if !isValidPhoneNumber(phoneTextField ?? "") {
                    DispatchQueue.main.async {
                        self.showAlert(withTitle: "", message: Constants.errorMessages.wrongPhoneNumber, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    }
                } else {
                    networkingPolicyHolder()
                }
            }
            if editedRow == 6 {
                if !isValidEmail(emailTextField ?? "") {
                    DispatchQueue.main.async {
                        self.showAlert(withTitle: "", message: Constants.errorMessages.enterEmail, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    }
                } else {
                    networkingPolicyHolder()
                }
            }
            if editedRow != 4 && editedRow != 5 && editedRow != 6 {
                networkingPolicyHolder()
            }
        }
    }
    
    private func getNetworkingPolicyHolder() {
        DispatchQueue.main.async {
            Spinner.start()
        }
        
        if ReachabilityClass.isConnectedToNetwork(){
            workerGet.getPolicyDetails(completionHandler: { [weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    DispatchQueue.main.async {
                        Spinner.stop()
                        self?.textFieldFirstName.text = resultUser.FirstName
                        self?.textFieldLastName.text = resultUser.LastName
                        self?.textfieldAddress.text = resultUser.Address
                        self?.textFieldCity.text = resultUser.City
                        self?.textFieldState.text = resultUser.State
                        self?.textFieldZipCode.text = resultUser.Zip
                        self?.textFieldPhone.text = resultUser.PhoneNumber
                        self?.textFieldEmail.text = resultUser.Email
                        
                        self?.addressTextField = self?.textfieldAddress.text
                        self?.cityTextField = self?.textFieldCity.text
                        self?.zipTextField = self?.textFieldZipCode.text
                        self?.phoneTextField = self?.textFieldPhone.text
                        self?.emailTextField = self?.textFieldEmail.text
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        Spinner.stop()
                    }
                }
            }) { error in
                DispatchQueue.main.async {
                    self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                Spinner.stop()
            }
        }
    }
    
    private func networkingPolicyHolder() {
        guard let password = self.keychain.get("password")
        else { return }
        
        let newPolicy = UpdatedPolicy(address: self.addressTextField ?? "", city: self.cityTextField ?? "", zipCode: self.zipTextField ?? "", phoneNumber: self.phoneTextField ?? "", email: self.emailTextField ?? "")
        
        DispatchQueue.main.async {
            Spinner.start()
        }
        
        if ReachabilityClass.isConnectedToNetwork(){
            if isValidPhoneNumber(phoneTextField ?? "") && isValidEmail(emailTextField ?? ""){
                worker.updatePolicyHolder(withPassword: password, withAddress: newPolicy.Address ?? "", withCity: newPolicy.City ?? "", withZip: newPolicy.Zip ?? "", withPhoneNumber: newPolicy.PhoneNumber ?? "", withEmail: newPolicy.Email ?? "", completionHandler: { [weak self] resultUser in
                    switch resultUser {
                    case .success(let resultUser):
                        DispatchQueue.main.async {
                            Spinner.stop()
                            let vc = AccountSettingsViewController()
                            self?.navigationController?.pushViewController(vc, animated: false)
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            Spinner.stop()
                        }
                    }
                }) { error in
                    if newPolicy.Zip!.count < 5 {
                        DispatchQueue.main.async {
                            self.showAlert(withTitle: "", message: Constants.errorMessages.validZipCode, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            Spinner.stop()
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            Spinner.stop()
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(withTitle: "", message: "Please enter a valid phone number or a email", buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                Spinner.stop()
            }
        }
    }
}

extension EditPersonalInformationViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textfieldAddress {
            addressTextField = textfieldAddress.text
        }
        if textField == textFieldCity {
            cityTextField = textFieldCity.text
        }
        if textField == textFieldZipCode {
            zipTextField = textFieldZipCode.text
            labelErrorMessageZipCode.text = Constants.errorMessages.wrongZipCode
        }
        if textField == textFieldPhone {
            phoneTextField = textFieldPhone.text
            if !isValidPhoneNumber(phoneTextField!) {
                labelErrorMessagePhoneNumber.text = Constants.errorMessages.wrongPhoneNumber
            } else {
                labelErrorMessagePhoneNumber.text = ""
            }
        }
        if textField == textFieldEmail {
            emailTextField = textFieldEmail.text
            if !isValidEmail(emailTextField!) {
                labelErrorMessageEmail.text = Constants.errorMessages.enterEmail
            } else {
                labelErrorMessageEmail.text = ""
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textfieldAddress {
            addressTextField = textfieldAddress.text
        }
        if textField == textFieldCity {
            cityTextField = textFieldCity.text
        }
        if textField == textFieldZipCode {
            zipTextField = textFieldZipCode.text
            labelErrorMessageZipCode.text = ""
        }
        if textField == textFieldPhone {
            phoneTextField = textFieldPhone.text
            if !isValidPhoneNumber(phoneTextField!) {
                labelErrorMessagePhoneNumber.text = Constants.errorMessages.wrongPhoneNumber
            } else {
                labelErrorMessagePhoneNumber.text = ""
            }
        }
        if textField == textFieldEmail {
            emailTextField = textFieldEmail.text
            if !isValidEmail(emailTextField!) {
                labelErrorMessageEmail.text = Constants.errorMessages.enterEmail
            } else {
                labelErrorMessageEmail.text = ""
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        editedTextField = nil
        if textField == textfieldAddress {
            textField.resignFirstResponder()
            textfieldAddress.becomeFirstResponder()
        }
        if textField == textFieldCity {
            textField.resignFirstResponder()
            textFieldCity.becomeFirstResponder()
        }
        if textField == textFieldZipCode {
            textField.resignFirstResponder()
            textFieldZipCode.becomeFirstResponder()
        }
        if textField == textFieldPhone {
            textField.resignFirstResponder()
            textFieldPhone.resignFirstResponder()
        } else {
            textField.resignFirstResponder()
            textFieldEmail.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 50
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == textFieldZipCode {
            zipTextField = textFieldZipCode.text
            editedTextField = IndexPath(row: 4, section: 0)
        }
        if textField == textFieldPhone {
            phoneTextField = textFieldPhone.text
            editedTextField = IndexPath(row: 5, section: 0)
        }
        if textField == textFieldEmail {
            emailTextField = textFieldEmail.text
            editedTextField = IndexPath(row: 6, section: 0)
        }
        if textField == textfieldAddress {
            addressTextField = textfieldAddress.text
        }
        if textField == textFieldCity {
            cityTextField = textFieldCity.text
        }
    }
}
