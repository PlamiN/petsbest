//
//  PersonalInformationViewController.swift
//  PetsBest
//
//  Created by Plamena on 6.10.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit

class PersonalInformationViewController: UIViewController {
    
    @IBOutlet private weak var imageViewProfile: UIImageView!
    @IBOutlet private weak var labelName: UILabel!
    @IBOutlet private weak var labelAddressTitle: UILabel!
    @IBOutlet private weak var labelAddressStreet: UILabel!
    @IBOutlet private weak var labelAddressCity: UILabel!
    @IBOutlet private weak var labelContactInfoTitle: UILabel!
    @IBOutlet private weak var labelTelephone: UILabel!
    @IBOutlet private weak var labelEmail: UILabel!
    @IBOutlet private weak var buttonEditPersonalInformation: UIButton!
    
    private let worker = PolicyHolderWorker()
    
    var policyDetails: PolicyHolderModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setViews()
        networkingPolicyHolder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        networkingPolicyHolder()
    }
    
    private func networkingPolicyHolder() {
        if ReachabilityClass.isConnectedToNetwork(){
            worker.getPolicyDetails(completionHandler: { [weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    self?.policyDetails = resultUser
                    DispatchQueue.main.async {
                        Spinner.stop()
                        self?.labelName.text = "\(resultUser.FirstName) \(resultUser.LastName)"
                        self?.labelAddressStreet.text = "\(resultUser.Address)"
                        self?.labelAddressCity.text = "\(resultUser.City), \(resultUser.State) \(resultUser.Zip)"
                        self?.labelTelephone.text = "\(resultUser.PhoneNumber)"
                        self?.labelEmail.text = "\(resultUser.Email)"
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        Spinner.stop()
                    }
                }
            }) { error in
                DispatchQueue.main.async {
                    self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                Spinner.stop()
            }
        }
    }
    
    private func setViews() {
        self.navigationItem.title = Constants.titlesScreens.personlaInfo
        imageViewProfile.image = UIImage(named: "profile")
        labelAddressTitle.textColor = UIColor.init(hex:Constants.Colors.PBHelpline)
        labelContactInfoTitle.textColor = UIColor.init(hex:Constants.Colors.PBHelpline)
        buttonEditPersonalInformation.setTitle(Constants.titlesButtons.editPersonalInfo, for: .normal)
        buttonEditPersonalInformation.setTitleColor(UIColor.init(hex:Constants.Colors.main), for: .normal)
        buttonEditPersonalInformation.backgroundColor = UIColor.init(hex: Constants.Colors.gray)
        buttonEditPersonalInformation.layer.cornerRadius = 20
        
        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
        
        DispatchQueue.main.async {
            Spinner.start()
        }
    }
    
    @IBAction private func actionEditPersonlaInformation(_ sender: UIButton) {
        let vc = EditPersonalInformationViewController()
        vc.policy = self.policyDetails
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func back(){
        let vc = AccountSettingsViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
}

