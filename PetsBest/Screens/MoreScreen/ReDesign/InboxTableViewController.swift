//
//  InboxTableViewController.swift
//  PetsBest
//
//  Created by Plamena on 12.01.22.
//  Copyright © 2022 Plamena. All rights reserved.
//

import UIKit
import KeychainSwift

class InboxTableViewController: MCEInboxTableViewController {
        
    var subtitle = ""
    
    var inboxMessages = [String]()
    
    private let keychain = KeychainSwift()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setViews()
        
        MCEInboxDatabase.shared.clearExpiredMessages()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.inboxUpdate), name: MCENotificationName.InboxCountUpdate.rawValue, object: nil)
        if(MCERegistrationDetails.shared.mceRegistered)
        {
            MCEInboxQueueManager.shared.syncInbox()
        }
        else
        {
            NotificationCenter.default.addObserver(forName: MCENotificationName.MCERegistered.rawValue, object: nil, queue: .main, using: { (note) in
                MCEInboxQueueManager.shared.syncInbox()
            })
        }
    }
    
    private func setViews() {
        navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = Constants.titlesScreens.inbox
        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
    }
    
    @objc private func back(){
        self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
        let vc = MoreViewController()
        if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
           navController.pushViewController(vc, animated: true)
        }
    }
    
    @objc func inboxUpdate()
    {
        let unreadCount = MCEInboxDatabase.shared.unreadMessageCount()
        let messageCount = MCEInboxDatabase.shared.messageCount()
        
        var subtitle = ""
        
        if MCERegistrationDetails.shared.mceRegistered
        {
            subtitle = "\(messageCount) messages, \(unreadCount) unread"
            self.tableView.reloadData()
        }
    }
    

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 60
    }
    
}
