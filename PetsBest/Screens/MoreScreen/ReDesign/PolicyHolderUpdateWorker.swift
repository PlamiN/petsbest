//
//  PolicyHolderUpdateWorker.swift
//  PetsBest
//
//  Created by Plamena on 6.10.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import Foundation
import KeychainSwift

typealias OnSuccessCompletionUpdatePolicyHolder = (Result<PolicyHolderModel>) -> ()
typealias OnErrorHandlerUpdatePolicyHolder = (Result<String>) -> ()

class PolicyHolderUpdateWorker {
    
    private let defaults = UserDefaults.standard
    private let keychain = KeychainSwift()
    
    func updatePolicyHolder(withPassword: String, withAddress: String, withCity: String, withZip: String, withPhoneNumber: String, withEmail: String, completionHandler: @escaping OnSuccessCompletionUpdatePolicyHolder,
                   onErrorHandler: @escaping OnErrorHandlerUpdatePolicyHolder) {
        if var url = try? APIRouter.get_policy_holder.asURLRequest() {
            
            let parameterDictionary = ["Password": withPassword,"Address": withAddress,"City": withCity,"Zip": withZip, "PhoneNumber": withPhoneNumber,"Email": withEmail]
            url.httpMethod = HTTPMethod.post.rawValue
            url.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            url.timeoutInterval = 180
            let appToken = keychain.get("appToken")
            
            guard let token = appToken?.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil) else { return }
            url.setValue(token, forHTTPHeaderField: "AuthToken")
            url.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
                return
            }
            url.httpBody = httpBody
            
            URLSession.shared.dataTask(with: url) { data, response, error in
                
                if let error = error {
                    return onErrorHandler(.failure(error))
                }
                
                guard let content = data else {
                    print("No data")
                    return
                }
                
                
                if let httpResponse = response as? HTTPURLResponse {
                    let statusCode = httpResponse.statusCode
                    
                    if (statusCode != 200) {
                        //401 - Unauthorized
                        //403 - Forbidden
                        //403 - Not found
                        
                        if statusCode == 500 || statusCode == 502 || statusCode == 503 || statusCode == 404 {
                            return onErrorHandler(.success(Constants.errorMessages.serverError))
                        }
                        if statusCode == 400 {
                            return onErrorHandler(.success(Constants.errorMessages.usernamePasswordRequiredError))
                        }
                        if statusCode == 401 {
                            return onErrorHandler(.success(Constants.errorMessages.incorrectUsernamePasswordError))
                        } else {
                            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                                print("Not containing JSON")
                                return onErrorHandler(.success(Constants.errorMessages.emptyField))
                            }
                            
//                            guard let error = json["Message"] else { return }
//                            let errorString = "\(error)"
                            return onErrorHandler(.success(Constants.errorMessages.resetPassword))
                        }
                    } else {
                        
                        let response = try? JSONDecoder().decode(PolicyHolderModel.self, from: content)
                        if let response = response {
                            DispatchQueue.main.async {
                                completionHandler(.success(response))
                            }
                        }
                    }
                }
            }.resume()
        }
    }}
