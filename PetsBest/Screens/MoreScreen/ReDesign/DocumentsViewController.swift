//
//  DocumentsViewController.swift
//  PetsBest
//
//  Created by Plamena on 3.11.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import WebKit

class DocumentsViewController: UIViewController {

    @IBOutlet private weak var tableViewDocuments: UITableView!
    @IBOutlet private weak var activityIndicatorScreen: NVActivityIndicatorView!
    
    private var titleArray = [NSMutableAttributedString]()
    private var documentsArray = [[String]]()
    private var documentsArrayIDS = [[String]]()
    private let worker = PolicyWorker()
    
    private let workerEOBDocument = DownloadEOBDocumentWorker()
    
    private var webView = WKWebView()
    private var isDocumentAvailable: Bool = false
    
    private var EOBDocument: String?
    
    private let screenSize: CGRect = UIScreen.main.bounds
    
    override func viewDidLoad() {
        super.viewDidLoad()

        networkingPolicyDetails()
        configTableViewProperties()
        setViews()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    private func setViews() {
        self.navigationItem.title = Constants.titlesScreens.documentCenter
        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
    }
    
    @objc private func back(){
        self.dismiss(animated: true, completion: nil)
    }
    
    private func stopLoadingWholeScreen() {
        DispatchQueue.main.async {
            self.activityIndicatorScreen?.stopAnimating()
        }
    }
    
    private func startLoadingWholeScreen() {
        DispatchQueue.main.async {
            self.activityIndicatorScreen?.startAnimating()
        }
    }
    
    private func networkingPolicyDetails() {

        DispatchQueue.main.async {
            Spinner.start()
        }
        
        if ReachabilityClass.isConnectedToNetwork(){
            worker.getPolicyDetails(completionHandler: { [
                weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    
                    DispatchQueue.main.async {
                        self?.stopLoadingWholeScreen()
                    }
                    
                    let yourAttributes = [NSAttributedString.Key.font: UIFont(name: "Proxima Nova Bold", size: 20)]
                    let yourOtherAttributes = [NSAttributedString.Key.font: UIFont(name: "Proxima Nova", size: 20)]
                    
                    for pet in resultUser {
                        if pet.IsCanceled == false {
                            var documentTitlesValid = [String]()
                            var documentIdsValid = [String]()
                            let partOne = NSMutableAttributedString(string: "\(pet.PetName)", attributes: yourAttributes as [NSAttributedString.Key : Any])
                            let partTwo = NSMutableAttributedString(string: " - Policy #: \(pet.PolicyNumber)", attributes: yourOtherAttributes as [NSAttributedString.Key : Any])

                            partOne.append(partTwo)
                            self?.titleArray.append(partOne)
                            for document in pet.PolicyDocuments {
                                documentTitlesValid.append(document.Title)
                                if document.DocumentId == nil {
                                    documentIdsValid.append(document.DocumentUrl ?? "")
                                } else {
                                    documentIdsValid.append(document.DocumentId ?? "")
                                }
                            }
                            self?.documentsArray.append(documentTitlesValid)
                            self?.documentsArrayIDS.append(documentIdsValid)
                        }
                    }
                    
                    for pet in resultUser {
                        var titlesArrayCancelled = [NSMutableAttributedString]()
                        if pet.IsCanceled == true {
                            var documentTitlesCanceled = [String]()
                            var documentIdsCanceled = [String]()
                            let partOne = NSMutableAttributedString(string: "\(pet.PetName)", attributes: yourAttributes as [NSAttributedString.Key : Any])
                            let partTwo = NSMutableAttributedString(string: " - Policy #: \(pet.PolicyNumber) (Cancelled)", attributes: yourOtherAttributes as [NSAttributedString.Key : Any])
                            
                            partOne.append(partTwo)
                            titlesArrayCancelled.append(partOne)
                            for document in pet.PolicyDocuments {
                                documentTitlesCanceled.append(document.Title)
                                if document.DocumentId == nil {
                                    documentIdsCanceled.append(document.DocumentUrl ?? "")
                                } else {
                                    documentIdsCanceled.append(document.DocumentId ?? "")
                                }
                            }
                            
                            self?.documentsArray.append(documentTitlesCanceled)
                            self?.documentsArrayIDS.append(documentIdsCanceled)
                        }
                        self?.titleArray.append(contentsOf: titlesArrayCancelled)
                    }
                    
                    DispatchQueue.main.async {
                        self?.tableViewDocuments.reloadData()
                        Spinner.stop()
                    }
                    
                case .failure(let error):
                    print(error)
                }
            }) { [weak self] error  in
                switch error {
                case .success(let error):
                    print(error)
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.noInternetError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                self.stopLoadingWholeScreen()
            }
        }
    }
    
    private func configTableViewProperties() {
        tableViewDocuments.registerCell(SettingsTableViewCell.self)
        tableViewDocuments.delegate = self
        tableViewDocuments.dataSource = self
        tableViewDocuments.separatorStyle = .none
        
        tableViewDocuments.showsVerticalScrollIndicator = false
    }
}

extension DocumentsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = .white
            headerView.backgroundView?.backgroundColor = .black
            headerView.textLabel?.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
            headerView.textLabel?.font = UIFont(name: "Proxima Nova Bold", size: 15)


        }
    }
    

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // Section Footer height
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let result = UIView()
        
        if documentsArray[section].count == 0 {
            // recreate insets from existing ones in the table view
            let insets = tableView.separatorInset
            let width = tableView.bounds.width - insets.left - insets.right
            let sepFrame = CGRect(x: insets.left, y: 15, width: width, height: 0.5)

            // create layer with separator, setting color
            let sep = CALayer()
            sep.frame = sepFrame
            sep.backgroundColor = tableView.separatorColor?.cgColor
            result.layer.addSublayer(sep)

            return result
        } else {
            // recreate insets from existing ones in the table view
            let insets = tableView.separatorInset
            let width = tableView.bounds.width - insets.left - insets.right
            let sepFrame = CGRect(x: insets.left, y: 10, width: width, height: 0.5)

            // create layer with separator, setting color
            let sep = CALayer()
            sep.frame = sepFrame
            sep.backgroundColor = tableView.separatorColor?.cgColor
            result.layer.addSublayer(sep)

            return result
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           
        if section == 0 {
            return 70
        } else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerView = UIView()
        let newlabel = UILabel()
        //206-250
        newlabel.textAlignment = .left
        newlabel.adjustsFontSizeToFitWidth = true
        
        newlabel.attributedText = titleArray[section]
        newlabel.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)

        headerView.addSubview(newlabel)
        newlabel.translatesAutoresizingMaskIntoConstraints = false
        headerView.addConstraint(NSLayoutConstraint(item: newlabel, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: headerView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1.0, constant: 16))
        headerView.addConstraint(NSLayoutConstraint(item: newlabel, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: headerView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1.0, constant: 0))
        
        if section == 0 {
            headerView.addConstraint(NSLayoutConstraint(item: newlabel, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: headerView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 16))
        } else {
            headerView.addConstraint(NSLayoutConstraint(item: newlabel, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: headerView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0))
        }
        headerView.addConstraint(NSLayoutConstraint(item: newlabel, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: headerView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0))

        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documentsArray[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingsTableViewCell = tableView.dequeueCell(SettingsTableViewCell.self)
        cell.selectionStyle = .none
        cell.delegate = self
        cell.configureCell(withTitle: documentsArray[indexPath.section][indexPath.row], withImage: UIImage(named: "download"), withIndexPath: IndexPath(row: indexPath.row, section: indexPath.section))
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func savePdf(urlString:String, fileName:String) {
        DispatchQueue.main.async {
            Spinner.start()
        }
        guard let url = URL(string: urlString) else { return }
        let pdfData = try? Data.init(contentsOf: url)
        let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        let pdfNameFromUrl = "\(fileName).pdf"
        let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
        webView = WKWebView(frame: CGRect(x: 0, y: 80, width: screenSize.width, height: screenSize.height))
        
        do {
            try pdfData?.write(to: actualPath, options: .atomic)
            print("pdf successfully saved!")
            
            if #available(iOS 10.0, *) {
                do {
                    guard
                        var documentsURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last else {
                            //handle error when getting documents URL
                            return
                        }
                    documentsURL.appendPathComponent("\(fileName).pdf")
                    let urlRequest = URLRequest(url: documentsURL)
                    
                    DispatchQueue.main.async {
                        self.webView.load(urlRequest)
                        self.view.addSubview(self.webView)
                        Spinner.stop()
                    }
                    
                    let objectsToShare = [documentsURL]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    present(activityVC, animated: true)
                    activityVC.popoverPresentationController?.sourceView = view
                    
                } catch {
                    print("could not locate pdf file !!!!!!!")
                }
            }
        } catch {
            print("Pdf could not be saved")
        }
    }

    
    func saveBase64StringToPDF(_ base64String: String) {
        DispatchQueue.main.async {
            Spinner.start()
        }
        guard
            var documentsURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last,
            let convertedData = Data(base64Encoded: base64String)
            else {
            //handle error when getting documents URL
            return
        }

        //name your file however you prefer
        documentsURL.appendPathComponent("Document.pdf")

        do {
            try convertedData.write(to: documentsURL)
        } catch {
            //handle write error here
        }

        //if you want to get a quick output of where your
        //file was saved from the simulator on your machine
        //just print the documentsURL and go there in Finder
        print(documentsURL)
        
        isDocumentAvailable = true
        webView = WKWebView(frame: CGRect(x: 0, y: 80, width: screenSize.width, height: screenSize.height))
        let urlRequest = URLRequest(url: documentsURL)
        
        DispatchQueue.main.async {
            self.webView.load(urlRequest)
            self.view.addSubview(self.webView)
            Spinner.stop()
        }
        
        let objectsToShare = [documentsURL]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        present(activityVC, animated: true)
        activityVC.popoverPresentationController?.sourceView = view
    }
}

extension DocumentsViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
       decisionHandler(.allow)
    }
}

extension DocumentsViewController: SettingsTableViewCellDelegate {
    func openDocument(byIndex: IndexPath) {
        let document = documentsArrayIDS[byIndex.section][byIndex.row]
        DispatchQueue.main.async {
            Spinner.start()
        }
        if document.prefix(1) == "/" {
            let newDocument = document.dropLast(2)
            let fileName = newDocument.components(separatedBy: "/")[2].dropLast(4)
            
//            print(fileName)
            savePdf(urlString: "\(Constants.baseURL)\(document)", fileName: "\(fileName)")

        } else {
            workerEOBDocument.getEOBDocument(withURLDocument: document, completionHandler: { [weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    self?.saveBase64StringToPDF(resultUser.DataBase64Encoded)
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        Spinner.stop()
                    }
                }
            }) { [weak self] error  in
                switch error {
                case .success(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        Spinner.stop()
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        Spinner.stop()
                    }
                }
            }
        }
    }
}
