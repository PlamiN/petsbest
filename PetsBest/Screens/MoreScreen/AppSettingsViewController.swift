//
//  AppSettingsViewController.swift
//  PetsBest
//
//  Created by Plamena on 18.11.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import KeychainSwift
import CoreLocation

class AppSettingsViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet private weak var tableView: UITableView!
    private let titleArray = ["Face ID® / Touch ID®", "Push Notifications", "Location Services"]
    private let defaults = UserDefaults.standard
    
    lazy var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var location: AcousticLocationModel?
    
    private let biometricLogin = BiometricIDAuth()
    
    var locationIsAllowOnce = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        configTableViewProperties()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
    }

    @objc func appMovedToForeground() {
        print("App moved to background!")
        checkPushNotifications()
        getBiometricLogin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self)
    }
    
    private func setViews() {
        navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = Constants.titlesScreens.appSettings
        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
        
        self.tabBarController?.delegate = self
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedAlways:
            print("Always")
        case .authorizedWhenInUse:
            print("Always")
        case .denied:
            print("Declined")
        case .notDetermined:
            print("Once")
            locationIsAllowOnce = true
        case .restricted:
            print("Declined")
        }
    }
    
    @objc private func back(){
        self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
        let vc = MoreViewController()
        if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
           navController.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - Set tableView
    
    private func configTableViewProperties() {
        tableView.registerCell(AppSettingsTableViewCell.self)
        tableView.separatorStyle = .none
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied) {
            self.defaults.set("deactivate", forKey: "LocationServices")
            self.defaults.set("deactivate", forKey: "LocationServicesLocally")
            self.tableView.reloadData()
        }
        if (status == CLAuthorizationStatus.authorizedWhenInUse) {
            // The user accepted authorization
            self.defaults.set("activate", forKey: "LocationServices")
            self.defaults.set("activate", forKey: "LocationServicesLocally")
            
            currentLocation = locationManager.location
            
            if currentLocation != nil {
                DispatchQueue.main.async{ [weak self] in
                    self?.defaults.set(self?.currentLocation?.coordinate.latitude, forKey: "usersLatitude")
                    self?.defaults.set(self?.currentLocation?.coordinate.longitude, forKey: "usersLongitude")
                }
            }
            
            location = readAcousticConfigurationFile(filename: "MceConfig")
            location?.location.autoInitialize = true
            saveToFile()
            self.tableView.reloadData()
    
        } else if (status == CLAuthorizationStatus.authorizedAlways) {
            // The user accepted authorization
            self.defaults.set("activate", forKey: "LocationServicesLocally")
            self.defaults.set("activate", forKey: "LocationServices")
            
            currentLocation = locationManager.location
            
            if currentLocation != nil {
                DispatchQueue.main.async{ [weak self] in
                    self?.defaults.set(self?.currentLocation?.coordinate.latitude, forKey: "usersLatitude")
                    self?.defaults.set(self?.currentLocation?.coordinate.longitude, forKey: "usersLongitude")
                }
            }
            
            location = readAcousticConfigurationFile(filename: "MceConfig")
            location?.location.autoInitialize = true
            saveToFile()
            readAcousticConfigurationFile(filename: "MceConfig")
            
            self.tableView.reloadData()
        }
    }
    
    func readAcousticConfigurationFile(filename fileName: String) -> AcousticLocationModel? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(AcousticLocationModel.self, from: data)
                print(jsonData.location.autoInitialize)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
    
    private func saveToFile(){

        let from = Bundle.main.url(forResource: "MceConfig", withExtension: "json")!

        let to = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("MceConfig.json")

        do {

            try FileManager.default.copyItem(at: from, to: to)

            let jsonData = try JSONEncoder().encode(location)
            print(location?.location.autoInitialize)
            try jsonData.write(to: to)

        }
        catch {

            print(error)
        }
    }
    
    func notificationCategories() -> Set<UNNotificationCategory> {
        // iOS 10+ Example static action category:
        let acceptAction = UNNotificationAction(identifier: "Accept", title: "Accept", options: [.foreground])
        let rejectAction = UNNotificationAction(identifier: "Reject", title: "Reject", options: [.destructive])
        let category = UNNotificationCategory(identifier: "example", actions: [acceptAction, rejectAction], intentIdentifiers: [], options: [.customDismissAction])
        
        return Set(arrayLiteral: category)
    }
    
    private func getPushNotificationsActivation() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()

            // iOS 10+ Push Message Registration, some versions of iOS have different options available
            let options: UNAuthorizationOptions = {
                if #available(iOS 12.0, *) {
                    return [.alert, .sound, .carPlay, .badge, .providesAppNotificationSettings]
                }
                return [.alert, .sound, .carPlay, .badge]
            }()
            
            center.requestAuthorization(options: options) { (granted, error) in
                if error == nil{
                    
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                        center.setNotificationCategories( self.notificationCategories() )
                        self.checkPushNotifications()
                    }
                }
            }

        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func checkPushNotifications(){
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { settings in

                switch settings.authorizationStatus {

                case .notDetermined:
                    // Authorization request has not been made yet
                    if self.defaults.string(forKey: "PushNotifications") == nil && self.defaults.string(forKey: "firstPushNotifications") == nil {
                        self.defaults.set("deactivate", forKey: "PushNotifications")
                        self.defaults.set("deactivate", forKey: "firstPushNotifications")
                        DispatchQueue.main.async {
                            let indexPath = IndexPath(item: 1, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .fade)
                        }
                    }
                case .denied:
                    // User has denied authorization.
                    // You could tell them to change this in Settings
                    if self.defaults.string(forKey: "PushNotifications") == nil && self.defaults.string(forKey: "firstPushNotifications") == nil {
                        self.defaults.set("deactivate", forKey: "PushNotifications")
                        self.defaults.set("deactivate", forKey: "firstPushNotifications")
                        DispatchQueue.main.async {
                            let indexPath = IndexPath(item: 1, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .fade)
                        }
                    }
                    
                    if self.defaults.string(forKey: "PushNotifications") == "activate" && self.defaults.string(forKey: "firstPushNotifications") == nil {
                        self.defaults.set("deactivate", forKey: "PushNotifications")
                        DispatchQueue.main.async {
                            let indexPath = IndexPath(item: 1, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .fade)
                        }
                    }
                case .authorized:
                    
                    // User has given authorization.
                    if self.defaults.string(forKey: "PushNotifications") == nil && self.defaults.string(forKey: "firstPushNotifications") == nil {
                        self.defaults.set("activate", forKey: "PushNotifications")
                        self.defaults.set("activate", forKey: "firstPushNotifications")
                        DispatchQueue.main.async {
                            let indexPath = IndexPath(item: 1, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .fade)
                        }
                    }
                    
                    if self.defaults.string(forKey: "PushNotifications") == "deactivate" && self.defaults.string(forKey: "firstPushNotifications") == nil {
                        self.defaults.set("activate", forKey: "PushNotifications")
                        DispatchQueue.main.async {
                            let indexPath = IndexPath(item: 1, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .fade)
                        }
                    }
                    
                    if self.defaults.string(forKey: "PushNotifications") == "deactivate" {
                        self.defaults.set("activate", forKey: "PushNotifications")
                        DispatchQueue.main.async {
                            let indexPath = IndexPath(item: 1, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .fade)
                        }
                    }
                case .provisional:
                    return
                case .ephemeral:
                    return
                @unknown default:
                    return
                }
            })
         } else {
             // Fallback on earlier versions
             if UIApplication.shared.isRegisteredForRemoteNotifications {
                 print("APNS-YES")
             } else {
                 print("APNS-NO")
             }
         }
    }
    
    private func getBiometricLogin() {
        self.defaults.set("false", forKey: "notNowActivatedBiometrics")
        
        switch biometricLogin.biometricType() {
        case .faceID:
            print("Face ID")
            biometricLogin.authenticateUser { [weak self] message in
                if message == "true" {
                    self?.defaults.set("activate", forKey: "FaceID")
                    DispatchQueue.main.async {
                        let indexPath = IndexPath(item: 0, section: 0)
                        self?.tableView.reloadRows(at: [indexPath], with: .fade)
                    }
                } else {
                    self?.defaults.set("deactivate", forKey: "FaceID")
                    DispatchQueue.main.async {
                        let indexPath = IndexPath(item: 0, section: 0)
                        self?.tableView.reloadRows(at: [indexPath], with: .fade)
                    }
                }
            }
        case .touchID:
            print("Touch ID")
            biometricLogin.authenticateUser { [weak self] message in
                if message == "true" {
                    self?.defaults.set("activate", forKey: "FaceID")
                    DispatchQueue.main.async {
                        let indexPath = IndexPath(item: 0, section: 0)
                        self?.tableView.reloadRows(at: [indexPath], with: .fade)
                    }
                } else {
                    self?.defaults.set("deactivate", forKey: "FaceID")
                    DispatchQueue.main.async {
                        let indexPath = IndexPath(item: 0, section: 0)
                        self?.tableView.reloadRows(at: [indexPath], with: .fade)
                    }
                }
            }
        case .none:
            print("No ID")
            self.defaults.set("deactivate", forKey: "FaceID")
        default: break
        }
    }
}

extension AppSettingsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .pad {
            if UIScreen.main.nativeBounds.height == 2048 {
                return 110.0
            } else {
                return 140.0
            }
        } else {
            return 80.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AppSettingsTableViewCell = tableView.dequeueCell(AppSettingsTableViewCell.self)
        
        cell.selectionStyle = .none
        cell.delegate = self
        
        if indexPath.row == 0 {
            cell.configureCell(withTitle: titleArray[indexPath.row], isFaceID: true, isPushNotificationsDisabled: false, index: indexPath.row)
        }
        if indexPath.row == 1 {
            cell.configureCell(withTitle: titleArray[indexPath.row],isFaceID: false, isPushNotificationsDisabled: true, index: indexPath.row)
        }
        if indexPath.row == 2 { 
            cell.configureCell(withTitle: titleArray[indexPath.row],isFaceID: false, isPushNotificationsDisabled: false, index: indexPath.row)
        }
        
        return cell
    }
}

extension AppSettingsViewController: AppSettingsDelegate {
    
    func enablePushNotifications(withMessages: String) {
        let pushNotificationsAlert = UIAlertController(title: "", message: withMessages, preferredStyle: UIAlertController.Style.alert)
        
        let pushNotificationsAlertGeneralSettings = UIAlertController(title: "", message: withMessages, preferredStyle: UIAlertController.Style.alert)
        
        let activatedPushNotifications = defaults.string(forKey: "firstPushNotifications")
        let notNowActivatedPushNotifications = defaults.string(forKey: "notNowActivatedPushNotifications")
        
        if (activatedPushNotifications == "deactivate") {
            pushNotificationsAlertGeneralSettings.addAction(UIAlertAction(title: "Go to Phone Settings", style: .default, handler: { (action: UIAlertAction!) in
                self.defaults.removeObject(forKey: "notNowActivatedPushNotifications")
                self.defaults.removeObject(forKey: "firstPushNotifications")
                self.defaults.set("activate", forKey: "PushNotifications")
                UIApplication.shared.registerForRemoteNotifications()
                let indexPath = IndexPath(item: 1, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
                
                if let bundle = Bundle.main.bundleIdentifier,
                    let settings = URL(string: UIApplication.openSettingsURLString + bundle) {
                    if UIApplication.shared.canOpenURL(settings) {
                        UIApplication.shared.open(settings)
                    }
                }
            }))
            
            pushNotificationsAlertGeneralSettings.addAction(UIAlertAction(title: "Not Now", style: .cancel, handler: { (action: UIAlertAction!) in
                let indexPath = IndexPath(item: 1, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }))
            self.present(pushNotificationsAlertGeneralSettings, animated: false, completion: nil)
        }
        if notNowActivatedPushNotifications != nil {
            pushNotificationsAlert.addAction(UIAlertAction(title: Constants.errorMessages.ok, style: .default, handler: { (action: UIAlertAction!) in
                self.getPushNotificationsActivation()
            }))
            
            pushNotificationsAlert.addAction(UIAlertAction(title: Constants.errorMessages.cancel, style: .cancel, handler: { (action: UIAlertAction!) in
                let indexPath = IndexPath(item: 1, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }))
            self.present(pushNotificationsAlert, animated: false, completion: nil)
        }
    }
    
    func disablePushNotifications(withMessages: String) {
        let pushNotificationsAlert = UIAlertController(title: "", message: withMessages, preferredStyle: UIAlertController.Style.alert)
        
        let pushNotificationsAlertGeneralSettings = UIAlertController(title: "", message: withMessages, preferredStyle: UIAlertController.Style.alert)
        
        pushNotificationsAlert.addAction(UIAlertAction(title: Constants.errorMessages.ok, style: .default, handler: { (action: UIAlertAction!) in
            self.defaults.set("deactivate", forKey: "PushNotifications")
            UIApplication.shared.unregisterForRemoteNotifications()
            
            pushNotificationsAlertGeneralSettings.addAction(UIAlertAction(title: "Go to Phone Settings", style: .default, handler: { (action: UIAlertAction!) in
                UIApplication.shared.registerForRemoteNotifications()
                let indexPath = IndexPath(item: 1, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
                
                if let bundle = Bundle.main.bundleIdentifier,
                    let settings = URL(string: UIApplication.openSettingsURLString + bundle) {
                    if UIApplication.shared.canOpenURL(settings) {
                        UIApplication.shared.open(settings)
                    }
                }
            }))
            
            pushNotificationsAlertGeneralSettings.addAction(UIAlertAction(title: "Not Now", style: .cancel, handler: { (action: UIAlertAction!) in
                self.defaults.set("activate", forKey: "PushNotifications")
                let indexPath = IndexPath(item: 1, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }))
            self.present(pushNotificationsAlertGeneralSettings, animated: false, completion: nil)
        }))
        
        pushNotificationsAlert.addAction(UIAlertAction(title: Constants.errorMessages.cancel, style: .cancel, handler: { (action: UIAlertAction!) in
            let indexPath = IndexPath(item: 1, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: .fade)
        }))
        self.present(pushNotificationsAlert, animated: false, completion: nil)
    }
    
    func enableLocationServices(withMessages: String) {
        let locationServicesAlert = UIAlertController(title: "", message: withMessages, preferredStyle: UIAlertController.Style.alert)
        
        let locationServicesAlertGeneralSettings = UIAlertController(title: "", message: withMessages, preferredStyle: UIAlertController.Style.alert)
        
        locationServicesAlertGeneralSettings.addAction(UIAlertAction(title: Constants.errorMessages.ok, style: .default, handler: { (action: UIAlertAction!) in
            if self.defaults.string(forKey: "LocationServices") == nil || self.locationIsAllowOnce == true {
                self.locationManager.delegate = self
                self.locationManager.requestAlwaysAuthorization()
                let indexPath = IndexPath(item: 2, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }
            if self.defaults.string(forKey: "LocationServicesLocally") == "deactivate" && self.defaults.string(forKey: "LocationServices") == "deactivate" {
                if let bundle = Bundle.main.bundleIdentifier,
                   let settings = URL(string: UIApplication.openSettingsURLString + bundle) {
                    if UIApplication.shared.canOpenURL(settings) {
                        UIApplication.shared.open(settings)
                    }
                }
            } else {
                self.locationManager.startUpdatingLocation()
                self.defaults.set("activate", forKey: "LocationServicesLocally")
                self.defaults.removeObject(forKey: "LocationServices")
                let indexPath = IndexPath(item: 2, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }
        }))
        
        locationServicesAlertGeneralSettings.addAction(UIAlertAction(title: Constants.errorMessages.cancel, style: .cancel, handler: { (action: UIAlertAction!) in
            let indexPath = IndexPath(item: 2, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: .fade)
        }))
        self.present(locationServicesAlertGeneralSettings, animated: false, completion: nil)
    }
    
    func disableLocationServices(withMessages: String) {
        let locationServicesAlert = UIAlertController(title: "", message: withMessages, preferredStyle: UIAlertController.Style.alert)
        
        let locationServicesAlertGeneralSettings = UIAlertController(title: "", message: withMessages, preferredStyle: UIAlertController.Style.alert)
        
        locationServicesAlert.addAction(UIAlertAction(title: Constants.errorMessages.ok, style: .default, handler: { (action: UIAlertAction!) in
            self.locationManager.stopUpdatingLocation()
            
            locationServicesAlertGeneralSettings.addAction(UIAlertAction(title: "Go to Phone Settings", style: .default, handler: { (action: UIAlertAction!) in
                self.defaults.set("deactivate", forKey: "LocationServicesLocally")
                self.defaults.removeObject(forKey: "LocationServices")
                let indexPath = IndexPath(item: 2, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
                
                if let bundle = Bundle.main.bundleIdentifier,
                    let settings = URL(string: UIApplication.openSettingsURLString + bundle) {
                    if UIApplication.shared.canOpenURL(settings) {
                        UIApplication.shared.open(settings)
                    }
                }
            }))
            
            locationServicesAlertGeneralSettings.addAction(UIAlertAction(title: "Not Now", style: .cancel, handler: { (action: UIAlertAction!) in
                self.defaults.set("activate", forKey: "LocationServicesLocally")
                let indexPath = IndexPath(item: 2, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }))
            self.present(locationServicesAlertGeneralSettings, animated: false, completion: nil)
            
        }))
        
        locationServicesAlert.addAction(UIAlertAction(title: Constants.errorMessages.cancel, style: .cancel, handler: { (action: UIAlertAction!) in
            let indexPath = IndexPath(item: 2, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: .fade)
        }))
        self.present(locationServicesAlert, animated: false, completion: nil)
    }
    
    func disableFaceId(withMessages: String) {
        let faceIdAlert = UIAlertController(title: "", message: withMessages, preferredStyle: UIAlertController.Style.alert)
        let faceIdAlertGeneralSettings = UIAlertController(title: "", message: withMessages, preferredStyle: UIAlertController.Style.alert)
        
        faceIdAlert.addAction(UIAlertAction(title: Constants.errorMessages.ok, style: .default, handler: { (action: UIAlertAction!) in
            if self.defaults.string(forKey: "FaceID") == "deactivate" {
                self.defaults.set("activate", forKey: "FaceIDLocally")
            } else {
                self.defaults.set("deactivate", forKey: "FaceID")
                self.defaults.set("deactivate", forKey: "FaceIDLocally")
            }
            
            faceIdAlertGeneralSettings.addAction(UIAlertAction(title: "Go to Phone Settings", style: .default, handler: { (action: UIAlertAction!) in
                self.defaults.set("activate", forKey: "FaceID")
                UIApplication.shared.registerForRemoteNotifications()
                let indexPath = IndexPath(item: 0, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
                
                if let bundle = Bundle.main.bundleIdentifier,
                    let settings = URL(string: UIApplication.openSettingsURLString + bundle) {
                    if UIApplication.shared.canOpenURL(settings) {
                        UIApplication.shared.open(settings)
                    }
                }
            }))
            
            faceIdAlertGeneralSettings.addAction(UIAlertAction(title: "Not Now", style: .cancel, handler: { (action: UIAlertAction!) in
                self.defaults.set("activate", forKey: "FaceID")
                let indexPath = IndexPath(item: 0, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }))
            self.present(faceIdAlertGeneralSettings, animated: false, completion: nil)
        }))
        
        faceIdAlert.addAction(UIAlertAction(title: Constants.errorMessages.cancel, style: .cancel, handler: { (action: UIAlertAction!) in
            let indexPath = IndexPath(item: 0, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: .fade)
        }))
        self.present(faceIdAlert, animated: false, completion: nil)
    }
    
    func enableFaceId(withMessages: String) {
        let faceIdAlert = UIAlertController(title: "", message: withMessages, preferredStyle: UIAlertController.Style.alert)
        let faceIdAlertGeneralSettings = UIAlertController(title: "", message: withMessages, preferredStyle: UIAlertController.Style.alert)
        let notNowActivatedBiometrics = defaults.string(forKey: "notNowActivatedBiometrics")
        
        let faceIDActivation = self.defaults.string(forKey: "FaceID")
        if faceIDActivation == "deactivate" && (notNowActivatedBiometrics == nil || notNowActivatedBiometrics == "false") {
            faceIdAlertGeneralSettings.addAction(UIAlertAction(title: "Go to Phone Settings", style: .default, handler: { (action: UIAlertAction!) in
                self.defaults.set("activate", forKey: "FaceID")
                UIApplication.shared.registerForRemoteNotifications()
                let indexPath = IndexPath(item: 0, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
                
                if let bundle = Bundle.main.bundleIdentifier,
                    let settings = URL(string: UIApplication.openSettingsURLString + bundle) {
                    if UIApplication.shared.canOpenURL(settings) {
                        UIApplication.shared.open(settings)
                    }
                }
            }))
            
            faceIdAlertGeneralSettings.addAction(UIAlertAction(title: "Not Now", style: .cancel, handler: { (action: UIAlertAction!) in
                let indexPath = IndexPath(item: 0, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }))
            self.present(faceIdAlertGeneralSettings, animated: false, completion: nil)
        }
        
        if notNowActivatedBiometrics != nil && notNowActivatedBiometrics == "true" {
            faceIdAlert.addAction(UIAlertAction(title: Constants.errorMessages.ok, style: .default, handler: { (action: UIAlertAction!) in
                self.getBiometricLogin()
            }))
            
            faceIdAlert.addAction(UIAlertAction(title: Constants.errorMessages.cancel, style: .cancel, handler: { (action: UIAlertAction!) in
                let indexPath = IndexPath(item: 0, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }))
            self.present(faceIdAlert, animated: false, completion: nil)
        } else {
            self.defaults.set("activate", forKey: "FaceID")
        }
        
    }
}

extension AppSettingsViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
//        if tabBarIndex == 0 {
//            guard let tabBarViewController = TabBarViewController.storyboardInstance() else { return }
//            self.present(tabBarViewController, animated: false, completion: nil)
//        }
    }
}
