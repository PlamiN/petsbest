//
//  CalendarViewController.swift
//  PetsBest
//
//  Created by Plamena on 28.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import FSCalendar

protocol isAbleToReceiveData {
    func passStartDate(data: String)
    func passEndDate(data: String)
    func passStartNotStringDate(data: Date)
    func passEndNotStringDate(data: Date)
}

class CalendarViewController: UIViewController {

    @IBOutlet private weak var calendar: FSCalendar!
    
    var isEnd:Bool?
    private var selectedStartDate:String?
    private var selectedEndDate:String?
    
    private var startDate:Date?
    private var endDate:Date?
    
    
    var delegate: isAbleToReceiveData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(back))
        self.navigationItem.title = "Calendar"
        navigationController?.navigationBar.isHidden = false
        
        calendar.placeholderType = .none
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if selectedStartDate != nil && isEnd == false {
            delegate?.passStartDate(data: selectedStartDate!)
            delegate?.passStartNotStringDate(data: startDate!)
        }
        if selectedEndDate != nil && isEnd == true {
            delegate?.passEndDate(data: selectedEndDate!)
            delegate?.passEndNotStringDate(data: endDate!)
        }
    }
    
    @objc private func back(){
        self.dismiss(animated: true, completion: nil)
    }
}

extension CalendarViewController: FSCalendarDataSource, FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if isEnd == false {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/YYYY"
            selectedStartDate = "\(formatter.string(from: date))"
            startDate = date
            dismiss(animated: true, completion: nil)
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/YYYY"
            selectedEndDate = "\(formatter.string(from: date))"
            endDate = date
            self.dismiss(animated: true, completion: nil)
        }
    }
}
