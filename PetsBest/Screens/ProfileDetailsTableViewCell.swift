//
//  ProfileDetailsTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 21.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

//protocol ProfileDetailsDelegate {
//    func changeCell(atIndex: Int)
//}

protocol ProfileDetailsDelegateReloadCell {
    func requestReloadTableViewCell(rowNumber: Int)
}

import UIKit

class ProfileDetailsTableViewCell: UITableViewCell, Identifiable, ReusableView {
    @IBOutlet weak var interfaceSegmented: UISegmentedControl!
    
    var delegate1: ProfileImageDelegate?
    
    private let buttonTitles = [Constants.titlesLabels.labelPolicyDocs, Constants.titlesLabels.labelPlan, Constants.titlesLabels.labelClaimHistory]
    
    var delegate: ProfileDetailsDelegateReloadCell?
    
    weak var delegate2:CustomSegmentedControlDelegate?
    private var selectedIndex: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //interfaceSegmented.delegateCell = self
        
        interfaceSegmented.addUnderlineForSelectedSegment()
        selectionStyle = .none
        
        if UIDevice().userInterfaceIdiom == .pad {
            let attr = NSDictionary(object: UIFont(name: "Proxima Nova", size: 25.0)!, forKey: NSAttributedString.Key.font as NSCopying)
            interfaceSegmented.setTitleTextAttributes(attr as! [NSAttributedString.Key : AnyObject] , for: .normal)
        }
    }
    
    func configureCell(withSelectedIndex: Int) {
        if let value = UserDefaults.standard.value(forKey: "chosenOption"){
            let selectedIndex = value as! Int
            UserDefaults.standard.set("false", forKey: "isSwipe")
            interfaceSegmented.selectedSegmentIndex = selectedIndex
            //delegate?.requestReloadTableViewCell(rowNumber: selectedIndex)
            interfaceSegmented.lastUnderlinePosition()
        } else {
            UserDefaults.standard.set("true", forKey: "isSwipe")
            interfaceSegmented.selectedSegmentIndex = withSelectedIndex
            interfaceSegmented.changeUnderlinePosition()
        }
    }
    
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl){
        UserDefaults.standard.set("false", forKey: "isSwipe")
        
        DispatchQueue.main.async {
            self.interfaceSegmented.changeUnderlinePosition()
        }
        
        UserDefaults.standard.set(sender.selectedSegmentIndex, forKey: "chosenOption")
        
        if let value = UserDefaults.standard.value(forKey: "chosenOption"){
            let selectedIndex = value as! Int
            interfaceSegmented.selectedSegmentIndex = selectedIndex
            delegate?.requestReloadTableViewCell(rowNumber: selectedIndex)
        }
    }
    
    @objc func openDeviceImages() {
        delegate1?.openDeviceImages()
    }
}
