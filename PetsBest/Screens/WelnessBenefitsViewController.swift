//
//  WelnessBenefitsViewController.swift
//  PetsBest
//
//  Created by Plamena on 12.11.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit
import KeychainSwift

class WelnessBenefitsViewController: UIViewController {

    @IBOutlet weak var tableViewBenefits: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    var arrayBenefits = [String]()
    var arrayBenefitsPrices = [String]()
    
    private let keychain = KeychainSwift()
    
    var selectedPet: PetCovers?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelsAndViewsIPadIphone()
        configTableViewProperties()
    }
    
    // MARK: - Set tableView
    
    private func configTableViewProperties() {
        tableViewBenefits.delegate = self
        tableViewBenefits.dataSource = self
        tableViewBenefits.registerCell(WelnessPlanTableViewCell.self)
        tableViewBenefits.separatorStyle = .none
    }
    
    private func labelsAndViewsIPadIphone() {
        self.navigationItem.title = Constants.titlesScreens.wellnessBenefits
        self.navigationController?.navigationBar.isHidden = false
        
        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
        labelTitle.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        labelDetails.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        if UIDevice().userInterfaceIdiom == .pad {
            labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 35)
            labelDetails.font = UIFont(name: "Proxima Nova", size: 22)
        } else {
            labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 30)
            labelDetails.font = UIFont(name: "Proxima Nova", size: 17)
        }
        
        labelTitle.text = selectedPet?.PlanName
        labelDetails.text = Constants.titlesScreens.wellnessBenefitsDetails
    }
    
    
    @objc private func back(){
        self.navigationController?.popViewController(animated: false)
    }
}

extension WelnessBenefitsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayBenefits.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: WelnessPlanTableViewCell = tableView.dequeueCell(WelnessPlanTableViewCell.self)
        cell.selectionStyle = .none
        if indexPath.row == arrayBenefits.count-1 {
            cell.configureCell(withTitle: arrayBenefits[indexPath.row], withPrice: arrayBenefitsPrices[indexPath.row], isTotalPrice: true)
        } else {
            cell.configureCell(withTitle: arrayBenefits[indexPath.row], withPrice: arrayBenefitsPrices[indexPath.row], isTotalPrice: false)
        }
        return cell
    }
}
