//
//  ClaimHistoryDetailsViewController.swift
//  PetsBest
//
//  Created by Plamena on 30.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import WebKit
import FirebaseAnalytics

class ClaimHistoryDetailsViewController: UIViewController {

    @IBOutlet private weak var viewTop: UIView!
    @IBOutlet private weak var viewAllElements: UIView!
    @IBOutlet private weak var labelClaim: UILabel!
    @IBOutlet private weak var labelPolicy: UILabel!
    @IBOutlet private weak var labelServiceDate: UILabel!
    @IBOutlet private weak var labelServiceDateDetails: UILabel!
    @IBOutlet private weak var labelClaimType: UILabel!
    @IBOutlet private weak var labelClaimTypeDetails: UILabel!
    @IBOutlet private weak var labelCondition: UILabel!
    @IBOutlet private weak var labelConditionDetails: UILabel!
    @IBOutlet private weak var labelClaimStatus: UILabel!
    @IBOutlet private weak var labelClaimStatusDetails: UILabel!
    @IBOutlet private weak var firstImage: UIImageView!
    @IBOutlet private weak var viewFirst: UIView!
    @IBOutlet private weak var secondImage: UIImageView!
    @IBOutlet private weak var secondView: UIView!
    @IBOutlet private weak var thirdImage: UIImageView!
    @IBOutlet private weak var labelSummary: UILabel!
    @IBOutlet private weak var labelPaymentDate: UILabel!
    @IBOutlet private weak var labelPaymentDateDetails: UILabel!
    @IBOutlet private weak var labelTotalBilled: UILabel!
    @IBOutlet private weak var labelTotalBilledDetails: UILabel!
    @IBOutlet private weak var labelDeductible: UILabel!
    @IBOutlet private weak var labelDeductibleDetails: UILabel!
    @IBOutlet private weak var labelCopy: UILabel!
    @IBOutlet private weak var labelCopyDetails: UILabel!
    @IBOutlet private weak var labelPaidAmmount: UILabel!
    @IBOutlet private weak var labelPaidAmmountDetails: UILabel!
    @IBOutlet private weak var labelDownloadEOB: UILabel!
    @IBOutlet private weak var labelServiceDateHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelClaimTypeHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelConditionHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelClaimStatusHeight: NSLayoutConstraint!
    @IBOutlet private weak var firstCheckboxHeight: NSLayoutConstraint!
    @IBOutlet private weak var firstCheckboxWidth: NSLayoutConstraint!
    @IBOutlet private weak var secondCheckBoxHeight: NSLayoutConstraint!
    @IBOutlet private weak var secondCheckboxWidth: NSLayoutConstraint!
    @IBOutlet private weak var thirdCheckboxWidth: NSLayoutConstraint!
    @IBOutlet private weak var thirdCheckboxHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelPaymentDateHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelTotalBilledHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelDeductibleHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelCopayHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelPaidAmountHeight: NSLayoutConstraint!
    @IBOutlet private weak var heightLabelDownloadEOB: NSLayoutConstraint!
    @IBOutlet private weak var heightClaimStackView: NSLayoutConstraint!
    @IBOutlet private weak var heightLabelClaim: NSLayoutConstraint!
    
    var claimId: Int?
    var paymentId: Int?
    var withEOB: Bool?
    
    let screenSize: CGRect = UIScreen.main.bounds
    private var webView = WKWebView()
    
    private var EOBDocument: String?
    
    private let worker = ClaimDetailsWorker()
    private let workerPayments = ClaimDetailsPaymentWorker()
    private let workerEOBDocument = DownloadEOBDocumentWorker()
    
    var isDocumentAvailables: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        networkingClaimDetails()
        networkingClaimDetailsPayments()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
    }
    
    //Firebase Analytics
    override func viewDidAppear(_ animated: Bool) {
        Analytics.logEvent(AnalyticsEventScreenView,
                           parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.claimDetails])
    }
    
    private func setViews() {
        viewAllElements.layer.cornerRadius = 10.0
        
        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
        
        viewTop.clipsToBounds = true
        viewTop.layer.cornerRadius = 10
        viewTop.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        viewAllElements.layer.borderWidth = 1
        viewAllElements.layer.cornerRadius = 10
        viewAllElements.layer.borderColor = UIColor.clear.cgColor
        viewAllElements.layer.masksToBounds = true
        viewAllElements.layer.shadowOpacity = 0.5
        viewAllElements.layer.shadowOffset = CGSize(width: 0, height: 2)
        viewAllElements.layer.shadowRadius = 10
        viewAllElements.layer.shadowColor = UIColor.black.cgColor
        viewAllElements.layer.masksToBounds = false
        
        labelDownloadEOB.textColor = UIColor.init(hex: Constants.Colors.main)
        labelDownloadEOB.clipsToBounds = true
        labelDownloadEOB.layer.cornerRadius = 15
        labelDownloadEOB.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        let tapOpenEOB = UITapGestureRecognizer(target: self, action:  #selector(openEOBPDF))
        labelDownloadEOB.isUserInteractionEnabled = true
        labelDownloadEOB.addGestureRecognizer(tapOpenEOB)

        labelsAndViewIPads()
        if withEOB == true {
            labelDownloadEOB.isHidden = false
        } else {
            labelDownloadEOB.isHidden = true
        }
        
        DispatchQueue.main.async {
            Spinner.start()
        }
    }
    
    private func labelsAndViewIPads() {
        
        labelServiceDate.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)!
        labelPolicy.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)!
        labelClaimType.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)!
        labelClaim.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)!
        labelCondition.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)!
        labelClaimStatus.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)!
        labelPaymentDate.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)!
        labelTotalBilled.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)!
        labelDeductible.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)!
        labelCopy.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)!
        labelPaidAmmount.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)!
        
        if UIDevice().userInterfaceIdiom == .pad {
        
            DispatchQueue.main.async {
                if UIScreen.main.nativeBounds.height == 2732 {
                    self.labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.labelPolicy.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 32)
                    self.labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 32)
                    self.labelCondition.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.labelConditionDetails.font = UIFont(name: "Proxima Nova", size: 32)
                    self.labelClaimStatus.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.labelClaimStatusDetails.font = UIFont(name: "Proxima Nova", size: 32)
                    self.labelSummary.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.labelPaymentDate.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.labelPaymentDateDetails.font = UIFont(name: "Proxima Nova", size: 32)
                    self.labelTotalBilled.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.labelTotalBilledDetails.font = UIFont(name: "Proxima Nova", size: 32)
                    self.labelDeductible.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.labelDeductibleDetails.font = UIFont(name: "Proxima Nova", size: 32)
                    self.labelCopy.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.labelCopyDetails.font = UIFont(name: "Proxima Nova", size: 32)
                    self.labelPaidAmmount.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.labelPaidAmmountDetails.font = UIFont(name: "Proxima Nova", size: 32)
                    self.labelDownloadEOB.font = UIFont(name: "Proxima Nova Bold", size: 32)
                    self.heightClaimStackView.constant = 70
                    self.heightLabelClaim.constant = 80
                    self.labelServiceDateHeight.constant = 80
                    self.labelClaimTypeHeight.constant = 90
                    self.labelConditionHeight.constant = 130
                    self.labelClaimStatusHeight.constant = 80
                    self.firstCheckboxHeight.constant = 60
                    self.firstCheckboxWidth.constant = 60
                    self.secondCheckBoxHeight.constant = 60
                    self.secondCheckboxWidth.constant = 60
                    self.thirdCheckboxWidth.constant = 60
                    self.thirdCheckboxHeight.constant = 60
                    self.labelPaymentDateHeight.constant = 80
                    self.labelTotalBilledHeight.constant = 80
                    self.labelDeductibleHeight.constant = 80
                    self.labelCopayHeight.constant = 80
                    self.labelPaidAmountHeight.constant = 80
                }
                else {
                    if UIScreen.main.nativeBounds.height == 2388 {
                        self.labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.labelPolicy.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 30)
                        self.labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 30)
                        self.labelCondition.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.labelConditionDetails.font = UIFont(name: "Proxima Nova", size: 30)
                        self.labelClaimStatus.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.labelClaimStatusDetails.font = UIFont(name: "Proxima Nova", size: 30)
                        self.labelSummary.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.labelPaymentDate.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.labelPaymentDateDetails.font = UIFont(name: "Proxima Nova", size: 30)
                        self.labelTotalBilled.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.labelTotalBilledDetails.font = UIFont(name: "Proxima Nova", size: 30)
                        self.labelDeductible.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.labelDeductibleDetails.font = UIFont(name: "Proxima Nova", size: 30)
                        self.labelCopy.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.labelCopyDetails.font = UIFont(name: "Proxima Nova", size: 30)
                        self.labelPaidAmmount.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.labelPaidAmmountDetails.font = UIFont(name: "Proxima Nova", size: 30)
                        self.labelDownloadEOB.font = UIFont(name: "Proxima Nova Bold", size: 30)
                        self.heightClaimStackView.constant = 70
                        self.heightLabelClaim.constant = 80
                        self.labelServiceDateHeight.constant = 70
                        self.labelClaimTypeHeight.constant = 70
                        self.labelConditionHeight.constant = 110
                        self.labelClaimStatusHeight.constant = 70
                        self.firstCheckboxHeight.constant = 60
                        self.firstCheckboxWidth.constant = 60
                        self.secondCheckBoxHeight.constant = 60
                        self.secondCheckboxWidth.constant = 60
                        self.thirdCheckboxWidth.constant = 60
                        self.thirdCheckboxHeight.constant = 60
                        self.labelPaymentDateHeight.constant = 70
                        self.labelTotalBilledHeight.constant = 70
                        self.labelDeductibleHeight.constant = 70
                        self.labelCopayHeight.constant = 70
                        self.labelPaidAmountHeight.constant = 70
                    }
                    if UIScreen.main.nativeBounds.height == 2048 {
                        self.labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelPolicy.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelCondition.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelConditionDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelClaimStatus.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelClaimStatusDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelSummary.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelPaymentDate.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelPaymentDateDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelTotalBilled.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelTotalBilledDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelDeductible.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelDeductibleDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelCopy.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelCopyDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelPaidAmmount.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelPaidAmmountDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelDownloadEOB.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.heightClaimStackView.constant = 70
                        self.heightLabelClaim.constant = 80
                        self.labelServiceDateHeight.constant = 50
                        self.labelClaimTypeHeight.constant = 50
                        self.labelConditionHeight.constant = 110
                        self.labelClaimStatusHeight.constant = 50
                        self.firstCheckboxHeight.constant = 50
                        self.firstCheckboxWidth.constant = 50
                        self.secondCheckBoxHeight.constant = 50
                        self.secondCheckboxWidth.constant = 50
                        self.thirdCheckboxWidth.constant = 50
                        self.thirdCheckboxHeight.constant = 50
                        self.labelPaymentDateHeight.constant = 50
                        self.labelTotalBilledHeight.constant = 50
                        self.labelDeductibleHeight.constant = 50
                        self.labelCopayHeight.constant = 50
                        self.labelPaidAmountHeight.constant = 50
                    } else {
                        self.labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelPolicy.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelCondition.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelConditionDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelClaimStatus.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelClaimStatusDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelSummary.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelPaymentDate.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelPaymentDateDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelTotalBilled.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelTotalBilledDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelDeductible.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelDeductibleDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelCopy.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelCopyDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelPaidAmmount.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.labelPaidAmmountDetails.font = UIFont(name: "Proxima Nova", size: 25)
                        self.labelDownloadEOB.font = UIFont(name: "Proxima Nova Bold", size: 25)
                        self.heightClaimStackView.constant = 70
                        self.heightLabelClaim.constant = 80
                        self.labelServiceDateHeight.constant = 60
                        self.labelClaimTypeHeight.constant = 60
                        self.labelConditionHeight.constant = 110
                        self.labelClaimStatusHeight.constant = 60
                        self.firstCheckboxHeight.constant = 50
                        self.firstCheckboxWidth.constant = 50
                        self.secondCheckBoxHeight.constant = 50
                        self.secondCheckboxWidth.constant = 50
                        self.thirdCheckboxWidth.constant = 50
                        self.thirdCheckboxHeight.constant = 50
                        self.labelPaymentDateHeight.constant = 60
                        self.labelTotalBilledHeight.constant = 60
                        self.labelDeductibleHeight.constant = 60
                        self.labelCopayHeight.constant = 60
                        self.labelPaidAmountHeight.constant = 60
                    }
                }
                self.heightLabelDownloadEOB.constant = 60
                self.labelDownloadEOB.clipsToBounds = true
                self.labelDownloadEOB.layer.cornerRadius = 30
                self.labelDownloadEOB.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                if let font = UIFont(name: "Proxima Nova Bold", size: 25) {
                    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: font]
                }
            }
        } else {
            if UIScreen.main.nativeBounds.height == 1334 {
                labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelPolicy.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 13)
                labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 15)
                labelCondition.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelConditionDetails.font = UIFont(name: "Proxima Nova", size: 15)
                labelClaimStatus.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelClaimStatusDetails.font = UIFont(name: "Proxima Nova", size: 12)
                labelSummary.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelPaymentDate.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelPaymentDateDetails.font = UIFont(name: "Proxima Nova", size: 15)
                labelTotalBilled.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelTotalBilledDetails.font = UIFont(name: "Proxima Nova", size: 15)
                labelDeductible.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelDeductibleDetails.font = UIFont(name: "Proxima Nova", size: 15)
                labelCopy.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelCopyDetails.font = UIFont(name: "Proxima Nova", size: 15)
                labelPaidAmmount.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelPaidAmmountDetails.font = UIFont(name: "Proxima Nova", size: 15)
                labelDownloadEOB.font = UIFont(name: "Proxima Nova", size: 15)
            }
            if UIScreen.main.nativeBounds.height == 1920 || UIScreen.main.nativeBounds.height == 2208 {
                labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 16)
                labelPolicy.font = UIFont(name: "Proxima Nova Bold", size: 16)
                labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 18)
                labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 16)
                labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 18)
                labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 18)
                labelCondition.font = UIFont(name: "Proxima Nova Bold", size: 18)
                labelConditionDetails.font = UIFont(name: "Proxima Nova", size: 18)
                labelClaimStatus.font = UIFont(name: "Proxima Nova Bold", size: 18)
                labelClaimStatusDetails.font = UIFont(name: "Proxima Nova", size: 15)
                labelSummary.font = UIFont(name: "Proxima Nova Bold", size: 18)
                labelPaymentDate.font = UIFont(name: "Proxima Nova Bold", size: 18)
                labelPaymentDateDetails.font = UIFont(name: "Proxima Nova", size: 18)
                labelTotalBilled.font = UIFont(name: "Proxima Nova Bold", size: 18)
                labelTotalBilledDetails.font = UIFont(name: "Proxima Nova", size: 18)
                labelDeductible.font = UIFont(name: "Proxima Nova Bold", size: 18)
                labelDeductibleDetails.font = UIFont(name: "Proxima Nova", size: 18)
                labelCopy.font = UIFont(name: "Proxima Nova Bold", size: 18)
                labelCopyDetails.font = UIFont(name: "Proxima Nova", size: 18)
                labelPaidAmmount.font = UIFont(name: "Proxima Nova Bold", size: 18)
                labelPaidAmmountDetails.font = UIFont(name: "Proxima Nova", size: 18)
                labelDownloadEOB.font = UIFont(name: "Proxima Nova", size: 18)
                
                labelServiceDateHeight.constant = 40
                labelClaimTypeHeight.constant = 40
                labelConditionHeight.constant = 60
                labelClaimStatusHeight.constant = 40
                firstCheckboxHeight.constant = 30
                firstCheckboxWidth.constant = 30
                secondCheckBoxHeight.constant = 30
                secondCheckboxWidth.constant = 30
                thirdCheckboxWidth.constant = 30
                thirdCheckboxHeight.constant = 30
                labelPaymentDateHeight.constant = 30
                labelTotalBilledHeight.constant = 30
                labelDeductibleHeight.constant = 30
                labelCopayHeight.constant = 30
                labelPaidAmountHeight.constant = 30
            }
            if UIScreen.main.nativeBounds.height == 2436 || UIScreen.main.nativeBounds.height == 2778 || UIScreen.main.nativeBounds.height == 2532{
                labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelPolicy.font = UIFont(name: "Proxima Nova Bold", size: 15)
                labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 15)
                labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 17)
                labelCondition.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelConditionDetails.font = UIFont(name: "Proxima Nova", size: 17)
                labelClaimStatus.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelClaimStatusDetails.font = UIFont(name: "Proxima Nova", size: 14)
                labelSummary.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelPaymentDate.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelPaymentDateDetails.font = UIFont(name: "Proxima Nova", size: 17)
                labelTotalBilled.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelTotalBilledDetails.font = UIFont(name: "Proxima Nova", size: 17)
                labelDeductible.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelDeductibleDetails.font = UIFont(name: "Proxima Nova", size: 17)
                labelCopy.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelCopyDetails.font = UIFont(name: "Proxima Nova", size: 17)
                labelPaidAmmount.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelPaidAmmountDetails.font = UIFont(name: "Proxima Nova", size: 17)
                labelDownloadEOB.font = UIFont(name: "Proxima Nova", size: 17)
                
                labelServiceDateHeight.constant = 40
                labelClaimTypeHeight.constant = 40
                labelConditionHeight.constant = 70
                labelClaimStatusHeight.constant = 40
                firstCheckboxHeight.constant = 35
                firstCheckboxWidth.constant = 35
                secondCheckBoxHeight.constant = 35
                secondCheckboxWidth.constant = 35
                thirdCheckboxWidth.constant = 35
                thirdCheckboxHeight.constant = 35
                labelPaymentDateHeight.constant = 35
                labelTotalBilledHeight.constant = 35
                labelDeductibleHeight.constant = 35
                labelCopayHeight.constant = 35
                labelPaidAmountHeight.constant = 35
                heightLabelDownloadEOB.constant = 45
                labelDownloadEOB.clipsToBounds = true
                labelDownloadEOB.layer.cornerRadius = 22
                labelDownloadEOB.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            }
            if UIScreen.main.nativeBounds.height == 1792 || UIScreen.main.nativeBounds.height == 2688 {
                labelClaim.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelPolicy.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelServiceDate.font = UIFont(name: "Proxima Nova Bold", size: 19)
                labelServiceDateDetails.font = UIFont(name: "Proxima Nova", size: 17)
                labelClaimType.font = UIFont(name: "Proxima Nova Bold", size: 19)
                labelClaimTypeDetails.font = UIFont(name: "Proxima Nova", size: 19)
                labelCondition.font = UIFont(name: "Proxima Nova Bold", size: 19)
                labelConditionDetails.font = UIFont(name: "Proxima Nova", size: 19)
                labelClaimStatus.font = UIFont(name: "Proxima Nova Bold", size: 19)
                labelClaimStatusDetails.font = UIFont(name: "Proxima Nova", size: 16)
                labelSummary.font = UIFont(name: "Proxima Nova Bold", size: 19)
                labelPaymentDate.font = UIFont(name: "Proxima Nova Bold", size: 19)
                labelPaymentDateDetails.font = UIFont(name: "Proxima Nova", size: 19)
                labelTotalBilled.font = UIFont(name: "Proxima Nova Bold", size: 19)
                labelTotalBilledDetails.font = UIFont(name: "Proxima Nova", size: 19)
                labelDeductible.font = UIFont(name: "Proxima Nova Bold", size: 19)
                labelDeductibleDetails.font = UIFont(name: "Proxima Nova", size: 19)
                labelCopy.font = UIFont(name: "Proxima Nova Bold", size: 19)
                labelCopyDetails.font = UIFont(name: "Proxima Nova", size: 19)
                labelPaidAmmount.font = UIFont(name: "Proxima Nova Bold", size: 19)
                labelPaidAmmountDetails.font = UIFont(name: "Proxima Nova", size: 19)
                labelDownloadEOB.font = UIFont(name: "Proxima Nova", size: 19)
                
                labelServiceDateHeight.constant = 45
                labelClaimTypeHeight.constant = 45
                labelConditionHeight.constant = 75
                labelClaimStatusHeight.constant = 45
                firstCheckboxHeight.constant = 40
                firstCheckboxWidth.constant = 40
                secondCheckBoxHeight.constant = 40
                secondCheckboxWidth.constant = 40
                thirdCheckboxWidth.constant = 40
                thirdCheckboxHeight.constant = 40
                labelPaymentDateHeight.constant = 40
                labelTotalBilledHeight.constant = 40
                labelDeductibleHeight.constant = 40
                labelCopayHeight.constant = 40
                labelPaidAmountHeight.constant = 40
                heightLabelDownloadEOB.constant = 50
                labelDownloadEOB.clipsToBounds = true
                labelDownloadEOB.layer.cornerRadius = 25
                labelDownloadEOB.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            }
        }
    }
    
    @objc private func openEOBPDF(){
        DispatchQueue.main.async {
            Spinner.start()
        }
        guard let document = EOBDocument else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: "No EOB available!", buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                Spinner.stop()
            }
            return }
        workerEOBDocument.getEOBDocument(withURLDocument: document, completionHandler: { [weak self] resultUser in
            switch resultUser {
            case .success(let resultUser):
                self?.saveBase64StringToPDF(resultUser.DataBase64Encoded)
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            }
        }) { [weak self] error  in
            switch error {
            case .success(let error):
                DispatchQueue.main.async {
                    self?.showAlert(withTitle: "", message: error, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            }
        }
    }
    
    func saveBase64StringToPDF(_ base64String: String) {

        guard
            var documentsURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last,
            let convertedData = Data(base64Encoded: base64String)
            else {
            //handle error when getting documents URL
            return
        }

        //name your file however you prefer
        documentsURL.appendPathComponent("EOB\(claimId!).pdf")

        do {
            try convertedData.write(to: documentsURL)
        } catch {
            //handle write error here
        }

        //if you want to get a quick output of where your
        //file was saved from the simulator on your machine
        //just print the documentsURL and go there in Finder
        print(documentsURL)
        
        isDocumentAvailables = true
        webView = WKWebView(frame: CGRect(x: 0, y: 80, width: screenSize.width, height: screenSize.height))
        let urlRequest = URLRequest(url: documentsURL)
        
        DispatchQueue.main.async {
            self.webView.load(urlRequest)
            self.view.addSubview(self.webView)
            Spinner.stop()
        }
        
        let objectsToShare = [documentsURL]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        present(activityVC, animated: true)
        activityVC.popoverPresentationController?.sourceView = view
    }
    
    @objc private func back(){
        if isDocumentAvailables == true {
            webView.isHidden = true
            isDocumentAvailables = false
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func networkingClaimDetails() {
        
        guard let id = claimId else { return }
        worker.getClaimDetails(withClaimId: id, completionHandler: { [weak self] resultUser in
            switch resultUser {
            case .success(let resultUser):
                DispatchQueue.main.async {
                    self?.labelClaim.text = "Claim #: \(resultUser.ClaimId)"
                    if resultUser.ClaimType == 0 {
                        self?.labelClaimTypeDetails.text = Constants.ClaimTypes.accident
                    }
                    if resultUser.ClaimType == 1 {
                        self?.labelClaimTypeDetails.text = Constants.ClaimTypes.ilness
                    }
                    if resultUser.ClaimType == 2 {
                        self?.labelClaimTypeDetails.text = Constants.ClaimTypes.wellness
                    }
                    
                    self?.labelClaimStatusDetails.text = resultUser.StatusName
                    self?.labelConditionDetails.text = resultUser.Description
                    self?.labelPolicy.text = "Policy #: \(resultUser.PolicyNumber)"
                    
                    self?.navigationItem.title = resultUser.PetInfo.PetName
                    
                    Spinner.stop()
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            }
        }) { [weak self] error  in
            DispatchQueue.main.async {
                self?.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                Spinner.stop()
            }
        }
    }
    
    private func networkingClaimDetailsPayments() {
        
        guard let id = claimId,
              let paymentID = paymentId
            else { return }
//        print(id)
//        print(paymentID)
        workerPayments.getClaimDetailsPayments(withClaimId: id, withPaymentId:paymentID, completionHandler: { [weak self] resultPayment in
            switch resultPayment {
            case .success(let resultPayment):
                
                var totalBilled = [Double]()
                var paymentAmount = [Double]()
                var coPayAmount = [Double]()
                var deductibleAmount = [Double]()
                for payment in resultPayment.PaymentItems {
                    totalBilled.append(payment.ChargedAmount)
                    paymentAmount.append(payment.ReimbursementAmount)
                    coPayAmount.append(payment.CoPayAmount)
                    deductibleAmount.append(payment.DeductibleAmount)
                }
                
                self?.labelServiceDateDetails.text = "\(resultPayment.ServiceDateRange)"
                
                let date = resultPayment.CheckDate.toDate()
                if let dateUnwrapped = date {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM/dd/YYYY"
                    DispatchQueue.main.async {
                        self?.labelPaymentDateDetails.text = formatter.string(from: dateUnwrapped)
                    }
                }
                let billedDetails = round(1000*totalBilled.sum())/1000
                let deductibleDetails = round(1000*deductibleAmount.sum())/1000
                let copyDetails = round(1000*coPayAmount.sum())/1000
                let paidDetails = 1000*paymentAmount.sum()/1000
                
                DispatchQueue.main.async {
                    if billedDetails.decimalCount() == 0 || billedDetails.decimalCount() == 1 {
                        self?.labelTotalBilledDetails.text = "$\(billedDetails)0"
                    } else {
                        self?.labelTotalBilledDetails.text = "$\(billedDetails)"
                    }
                    
                    if deductibleDetails.decimalCount() == 0 || deductibleDetails.decimalCount() == 1 {
                        self?.labelDeductibleDetails.text = "$\(deductibleDetails)0"
                    } else {
                        self?.labelDeductibleDetails.text = "$\(deductibleDetails)"
                    }
                    
                    if copyDetails.decimalCount() == 0 || copyDetails.decimalCount() == 1 {
                        self?.labelCopyDetails.text = "$\(copyDetails)0"
                    } else {
                        self?.labelCopyDetails.text = "$\(copyDetails)"
                    }
                    
                    if paidDetails.decimalCount() == 0 || paidDetails.decimalCount() == 1 {
                        self?.labelPaidAmmountDetails.text = "$\(paidDetails)0"
                    } else {
                        self?.labelPaidAmmountDetails.text = "$\(paidDetails)"
                    }
                    Spinner.stop()
                }
                
                self?.EOBDocument = resultPayment.EOBDocumentIds[0]
                
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    Spinner.stop()
                }
            }
        }) { [weak self] error  in
            DispatchQueue.main.async {
                self?.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                Spinner.stop()
            }
        }
    }
}

extension Sequence where Element: AdditiveArithmetic {
    func sum() -> Element { reduce(.zero, +) }
}
