//
//  DownloadEOBDocumentWorker.swift
//  PetsBest
//
//  Created by Plamena on 20.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import KeychainSwift

typealias OnSuccessCompletionDownloadEOB = (Result<DocumentModel>) -> ()
typealias OnErrorHandlerClaimDownloadEOB = (Result<String>) -> ()

class DownloadEOBDocumentWorker {

    private let defaults = UserDefaults.standard
    private let keychain = KeychainSwift()

    func getEOBDocument(withURLDocument:String, completionHandler: @escaping OnSuccessCompletionDownloadEOB,
                       onErrorHandler: @escaping OnErrorHandlerClaimDownloadEOB) {

        if let url = try? APIRouter.get_documents.asURLRequest() {
            guard var urlWithQuery = url.url else { return }
            let urlDocument = "?documentId=\(withURLDocument)"
            guard let encodedURL = URL(string: urlDocument.removingPercentEncoding!) else { return }
            urlWithQuery.appendPathComponent("\(encodedURL)")
            guard let encodedURLWhole = URL(string: "\(urlWithQuery.absoluteURL)".removingPercentEncoding!) else { return }
            var request = URLRequest(url: encodedURLWhole)
            let appToken = keychain.get("appToken")

            guard let token = appToken?.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil) else { return }
            //print(token)

            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue(token, forHTTPHeaderField: "AuthToken")

            URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    return onErrorHandler(.failure(error))
                }

                guard let content = data else {
                    print("No data")
                    return
                }

                if let httpResponse = response as? HTTPURLResponse {
                    let statusCode = httpResponse.statusCode

                    if (statusCode != 200) {
                        guard
                            let errorString = String(data: content, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                            else { return }
                        print(errorString)
                        return onErrorHandler(.success(errorString))
                    } else {
                        
                        let response = try? JSONDecoder().decode(DocumentModel.self, from: content)
                        if let response = response {
                            DispatchQueue.main.async {
                                completionHandler(.success(response))
                            }
                        }
                    }
                }
            }.resume()
        }

    }
}

import Foundation

class FileDownloader {

    static func loadFileSync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)

        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else if let dataFromURL = NSData(contentsOf: url)
        {
            if dataFromURL.write(to: destinationUrl, atomically: true)
            {
                print("file saved [\(destinationUrl.path)]")
                completion(destinationUrl.path, nil)
            }
            else
            {
                print("error saving file")
                let error = NSError(domain:"Error saving file", code:1001, userInfo:nil)
                completion(destinationUrl.path, error)
            }
        }
        else
        {
            let error = NSError(domain:"Error downloading file", code:1002, userInfo:nil)
            completion(destinationUrl.path, error)
        }
    }

    static func loadFileAsync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)

        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else
        {
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request, completionHandler:
            {
                data, response, error in
                if error == nil
                {
                    if let response = response as? HTTPURLResponse
                    {
                        if response.statusCode == 200
                        {
                            if let data = data
                            {
                                if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic)
                                {
                                    completion(destinationUrl.path, error)
                                }
                                else
                                {
                                    completion(destinationUrl.path, error)
                                }
                            }
                            else
                            {
                                completion(destinationUrl.path, error)
                            }
                        }
                    }
                }
                else
                {
                    completion(destinationUrl.path, error)
                }
            })
            task.resume()
        }
    }
}
