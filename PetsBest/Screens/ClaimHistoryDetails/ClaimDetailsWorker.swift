//
//  ClaimDetailsWorker.swift
//  PetsBest
//
//  Created by Plamena on 17.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation
import KeychainSwift

typealias OnSuccessCompletionClaimDetails = (Result<ClaimDetails>) -> ()
typealias OnErrorHandlerClaimDetails = (Result<String>) -> ()

class ClaimDetailsWorker {
    
    private let defaults = UserDefaults.standard
    private let keychain = KeychainSwift()
    
    func getClaimDetails(withClaimId:Int, completionHandler: @escaping OnSuccessCompletionClaimDetails,
                         onErrorHandler: @escaping OnErrorHandlerClaimDetails) {
        
        if let url = try? APIRouter.allClaims.asURLRequest() {
            guard var urlWithQuery = url.url else { return }
            urlWithQuery.appendPathComponent("\(withClaimId)")
            var request = URLRequest(url: urlWithQuery)
            
            let appToken = keychain.get("appToken")
            
            guard let token = appToken?.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil) else { return }
            print(token)
            
            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue(token, forHTTPHeaderField: "AuthToken")
            
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    return onErrorHandler(.failure(error))
                }
                
                guard let content = data else {
                    print("No data")
                    return
                }
                
                
                if let httpResponse = response as? HTTPURLResponse {
                    let statusCode = httpResponse.statusCode
                    
                    if (statusCode != 200) {
                        guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                            print("Not containing JSON")
                            return
                        }
                        guard let error = json["Message"] else { return }
                        let errorString = "\(error)"
                        return onErrorHandler(.success(errorString))
                    } else {
                        let response = try? JSONDecoder().decode(ClaimDetails.self, from: content)
                        if let response = response {
                            DispatchQueue.main.async {
                                completionHandler(.success(response))
                            }
                        }
                    }
                }
            }.resume()
        }
    }
}
