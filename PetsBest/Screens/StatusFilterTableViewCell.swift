//
//  StatusFilterTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 3.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class StatusFilterTableViewCell: UITableViewCell, Identifiable, ReusableView {

    override func awakeFromNib() {
        super.awakeFromNib()
        textLabel?.layer.borderColor = UIColor.lightGray.cgColor
        textLabel?.layer.borderWidth = 1.0
    }
    
    func configureCell(withStatus: String) {
        textLabel?.text = withStatus
    }
}
