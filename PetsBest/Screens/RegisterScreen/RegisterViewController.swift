//
//  RegisterViewController.swift
//  PetsBest
//
//  Created by Plamena on 10.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class RegisterViewController: UIViewController {

    @IBOutlet weak var interfaceSegmented: CustomSegmentedControl!{
        didSet{
            interfaceSegmented.setButtonTitles(buttonTitles: [Constants.textFieldsPlaceholder.email, Constants.textFieldsPlaceholder.policyNumber])
            interfaceSegmented.selectorViewColor = UIColor.init(hex: Constants.Colors.main)!
            interfaceSegmented.selectorTextColor = UIColor.gray
        }
    }
    
    @IBOutlet private weak var textFieldUsername: SkyFloatingLabelTextField!
    @IBOutlet private weak var buttonRegister: UIButton!
    @IBOutlet private weak var labelRegister: UILabel!
    @IBOutlet private weak var labelCancel: UnderlinedLabel!
    @IBOutlet private weak var labelAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
    }
    
    private func setViews() {
        if self.traitCollection.userInterfaceStyle == .dark{
            self.view.backgroundColor = UIColor.black
            textFieldUsername.textColor = UIColor.white
            labelRegister.textColor = UIColor.white
            interfaceSegmented.textColor = UIColor.white
        } else{
            self.view.backgroundColor = UIColor.white
            textFieldUsername.textColor = UIColor.gray
            labelRegister.textColor = UIColor.init(hex: Constants.Colors.labels)
            interfaceSegmented.textColor = UIColor.init(hex: Constants.Colors.main)!
        }
        
        setBackgroundImage("backgroundImage", contentMode: UIView.ContentMode.scaleAspectFill)
        
        buttonRegister.setTitle(Constants.titlesButtons.register, for: .normal)
        buttonRegister.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        buttonRegister.setTitleColor(UIColor.white, for: .normal)
        buttonRegister.layer.cornerRadius = 15
        
        textFieldUsername.placeholder = Constants.textFieldsPlaceholder.email

        labelRegister.text = Constants.titlesLabels.registerAccount
        
        labelCancel.text = Constants.titlesLabels.cancel
        labelCancel.textColor = UIColor.init(hex: Constants.Colors.main)
        
        labelAddress.textColor = UIColor.white
        
        textFieldUsername.delegate = self
        
        let tapCancel = UITapGestureRecognizer(target: self, action:  #selector(cancel))
        labelCancel.isUserInteractionEnabled = true
        labelCancel.addGestureRecognizer(tapCancel)

        // call the 'keyboardWillShow' function when the view controller receive notification that keyboard is going to be shown
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        // call the 'keyboardWillHide' function when the view controlelr receive notification that keyboard is going to be hidden
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        hideKeyboardWhenTappedAround()
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            // if keyboard size is not available for some reason, dont do anything
            return
        }
        
        // move the root view up by the distance of keyboard height
        self.view.frame.origin.y = 0 - keyboardSize.height/7
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        // move back the root view origin to zero
        self.view.frame.origin.y = 0
    }
    
    private func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyb))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyb() {
        view.endEditing(true)
    }
    
    @objc private func cancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func actionButtonRegister(_ sender: UIButton) {
    }
}

extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if Validator.isEmpty(field: textField, value: "") && textField == textFieldUsername {
            textFieldUsername.errorMessage = Constants.errorMessages.enterEmail
        } else {
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldUsername {
            textField.resignFirstResponder()
            textFieldUsername.becomeFirstResponder()
        } else{
            textField.resignFirstResponder()
        }
        return true
    }
}
