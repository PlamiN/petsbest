//
//  PolicyHolderWorker.swift
//  PetsBest
//
//  Created by Plamena on 11.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation
import KeychainSwift

typealias OnSuccessCompletionPolicyHolder = (Result<PolicyHolderModel>) -> ()
typealias OnErrorHandlerPolicyHolder = (Result<String>) -> ()

class PolicyHolderWorker {
    
    private let defaults = UserDefaults.standard
    private let keychain = KeychainSwift()
    
    func getPolicyDetails(completionHandler: @escaping OnSuccessCompletionPolicyHolder,
                       onErrorHandler: @escaping OnErrorHandlerPolicyHolder) {
        
        if var url = try? APIRouter.get_policy_holder.asURLRequest() {
            
            let appToken = keychain.get("appToken")
            
            guard let token = appToken?.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil) else { return }
            
            url.httpMethod = HTTPMethod.get.rawValue
            url.setValue(token, forHTTPHeaderField: "AuthToken")
            
            URLSession.shared.dataTask(with: url) { data, response, error in
                if let error = error {
                    return onErrorHandler(.failure(error))
                }
                
                guard let content = data else {
                    print("No data")
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    let statusCode = httpResponse.statusCode
                    
                    if (statusCode != 200) {
                        if statusCode == 500 || statusCode == 502 || statusCode == 503{
                            return onErrorHandler(.success(Constants.errorMessages.generalHomeError))
                        } else {
                            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                                print("Not containing JSON")
                                return onErrorHandler(.success(Constants.errorMessages.generalHomeError))
                            }
                            guard let error = json["Message"] else { return }
                            let errorString = "\(error)"
                            return onErrorHandler(.success(errorString))
                        }
                    } else {
                        let response = try? JSONDecoder().decode(PolicyHolderModel.self, from: content)
                        if let response = response {
                            DispatchQueue.main.async {
                                completionHandler(.success(response))
                            }
                        }
                    }
                }
            }.resume()
        }
        
    }
}
