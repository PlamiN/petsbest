//
//  WellnessCoverageTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 4.02.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit

class WellnessCoverageTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet private weak var viewAllElements: UIView!
    @IBOutlet private weak var labelMainTitle: UILabel!
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var labelDetails: UILabel!
    @IBOutlet private weak var labelTitle3: UILabel!
    @IBOutlet private weak var labelDetails3: UILabel!
    @IBOutlet private weak var labelWellnessDetails: UILabel!
    @IBOutlet private weak var labelTitle3Width: NSLayoutConstraint!
    @IBOutlet private weak var labelTitleHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelTitle2Width: NSLayoutConstraint!
    @IBOutlet private weak var labelWellnessWidth: NSLayoutConstraint!
    @IBOutlet private weak var buttonSeeWelnessBenefits: UIButton!
    
    var delegate: ProgressBarTableViewCellDelegate?
    
    private var rowNumberValue = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }
    
    private func setViews() {
        
        if self.traitCollection.userInterfaceStyle == .dark{
            labelDetails.textColor = UIColor.white
            labelMainTitle.textColor = UIColor.white
            labelTitle.textColor = UIColor.white
            labelTitle3.textColor = UIColor.white
            labelDetails.textColor = UIColor.white
            labelDetails3.textColor = UIColor.white
        }
        viewAllElements.layer.cornerRadius = 10.0
        labelMainTitle.clipsToBounds = true
        labelMainTitle.layer.cornerRadius = 10
        labelMainTitle.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        labelTitle.text = Constants.titlesLabels.labelDeductible
        labelTitle3.text = Constants.titlesLabels.labelReimbursementLevel
        
        labelMainTitle.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        labelTitle.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        labelTitle3.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        
        contentView.layer.borderWidth = 1
        contentView.layer.cornerRadius = 5
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true
        contentView.layer.shadowOpacity = 0.18
        contentView.layer.shadowOffset = CGSize(width: 0, height: 2)
        contentView.layer.shadowRadius = 2
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.masksToBounds = false
        
        selectionStyle = .none
        
        buttonSeeWelnessBenefits.setTitleColor( UIColor.init(hex: Constants.Colors.PBHelpline), for: .normal)
        buttonSeeWelnessBenefits.backgroundColor = UIColor.init(hex: Constants.Colors.gray)
        buttonSeeWelnessBenefits.clipsToBounds = true
        buttonSeeWelnessBenefits.layer.cornerRadius = 20
        
        if UIDevice().userInterfaceIdiom == .pad {
            labelMainTitle.font = UIFont(name: "Proxima Nova Bold", size: 33)
            labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelTitle3.font = UIFont(name: "Proxima Nova Bold", size: 25)
            labelDetails.font = UIFont(name: "Proxima Nova", size: 27)
            labelDetails3.font = UIFont(name: "Proxima Nova", size: 27)
            labelWellnessDetails.font = UIFont(name: "Proxima Nova", size: 23)
            labelTitle3Width.constant = 300
            labelTitleHeight.constant = 120
            labelTitle2Width.constant = 300
            labelWellnessWidth.constant = 300
        } else {
            labelMainTitle.font = UIFont(name: "Proxima Nova Bold", size: 21)
            labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 16)
            labelTitle3.font = UIFont(name: "Proxima Nova Bold", size: 16)
            labelDetails.font = UIFont(name: "Proxima Nova", size: 18)
            labelDetails3.font = UIFont(name: "Proxima Nova", size: 18)
            labelWellnessDetails.font = UIFont(name: "Proxima Nova", size: 13)
        }
        
        buttonSeeWelnessBenefits.isHidden = true
    }

    func configureCell(withTitle: [String], withDetails: [String], withPercentage: [CGFloat], withPlan: String, withRowNumber: Int) {
        labelTitle.text = withTitle[0]
        labelDetails.text = withDetails[0]
        
        labelTitle3.text = withTitle[1]
        labelDetails3.text = withDetails[1]
        
        labelMainTitle.backgroundColor = UIColor.systemGray6
        labelMainTitle.textColor = UIColor.init(hex: Constants.Colors.labels)!
        
        labelMainTitle.text = withPlan + " Plan"
        labelWellnessDetails.text = "Up to annual benefit limits"
        
        rowNumberValue = withRowNumber
    }
    
    @IBAction func actionSeeWelnessBenefits(_ sender: UIButton) {
        delegate?.seeCoreBenefits(rowNumber: rowNumberValue)
    }
}
