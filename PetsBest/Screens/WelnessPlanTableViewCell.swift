//
//  WelnessPlanTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 12.11.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit

class WelnessPlanTableViewCell: UITableViewCell, Identifiable, ReusableView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelsAndViewsIPadIphone()
    }

    func configureCell(withTitle: String, withPrice: String, isTotalPrice: Bool) {
        if isTotalPrice == true {
            if UIDevice().userInterfaceIdiom == .pad {
                labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 22)
                labelPrice.font = UIFont(name: "Proxima Nova Bold", size: 22)
            } else {
                labelTitle.font = UIFont(name: "Proxima Nova Bold", size: 17)
                labelPrice.font = UIFont(name: "Proxima Nova Bold", size: 17)
            }
        } else {
            if UIDevice().userInterfaceIdiom == .pad {
                labelTitle.font = UIFont(name: "Proxima Nova", size: 22)
                labelPrice.font = UIFont(name: "Proxima Nova Bold", size: 22)
            } else {
                labelTitle.font = UIFont(name: "Proxima Nova", size: 17)
                labelPrice.font = UIFont(name: "Proxima Nova Bold", size: 17)
            }
        }
        labelTitle.text = withTitle
        labelPrice.text = withPrice
    }
    
    private func labelsAndViewsIPadIphone() {
        labelTitle.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        labelPrice.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        if UIDevice().userInterfaceIdiom == .pad {
            labelTitle.font = UIFont(name: "Proxima Nova", size: 25)
            labelPrice.font = UIFont(name: "Proxima Nova Bold", size: 25)
        } else {
            labelTitle.font = UIFont(name: "Proxima Nova", size: 20)
            labelPrice.font = UIFont(name: "Proxima Nova Bold", size: 20)
        }
    }
}
