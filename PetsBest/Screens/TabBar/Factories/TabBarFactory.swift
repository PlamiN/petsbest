//
//  TabBarFactory.swift
//  PetsBest
//
//  Created by Plamena on 20.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class TabBarFactory {

    static func childViewControllerForModel(_ model: TabBarModel.ViewModel) -> UIViewController {
        
        let viewController: UIViewController?
        
        switch model.type {
        case .myPets:
            viewController = HomeViewController(withCoreDataStack: CoreDataStack())
        case .submitClaim:
            viewController = SubmitClaimViewController(withCoreDataStack: CoreDataStack())
        case .petHelp:
            if UIDevice().userInterfaceIdiom == .pad {
                viewController = CustomerSupporViewController()
            } else {
                viewController = CustomerSupporViewController()
            }
        case .more:
            viewController = MoreViewController()
        }
        
        viewController?.tabBarItem = model.tabBarItem
        guard let vc = viewController else {
            fatalError("No View Controller!!!")
        }
        return vc
    }
}
