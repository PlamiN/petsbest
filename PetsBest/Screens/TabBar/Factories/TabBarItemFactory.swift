//
//  TabBarItemFactory.swift
//  PetsBest
//
//  Created by Plamena on 20.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class TabBarItemFactory {
    
    static func create(type: TabBarItemType) -> UITabBarItem {
        switch type {
        case .myPets:
            return UITabBarItem(title: "My Pets",
                                image: UIImage(named: "home"),
                                selectedImage: UIImage(named: "home"))
        case .submitClaim:
            if UIDevice().userInterfaceIdiom == .pad {
                return UITabBarItem(title: "Submit Claim",
                                    image: UIImage(systemName: "camera.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 25, weight: .black, scale: .large)),
                                    selectedImage: UIImage(systemName: "camera.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 25, weight: .black, scale: .large)))
            } else {
                return UITabBarItem(title: "Submit Claim",
                                    image: UIImage(systemName: "camera.fill"),
                                    selectedImage: UIImage(systemName: "camera.fill"))
            }
        case .petHelp:
            return UITabBarItem(title: "Support",
                                image: UIImage(named: "helpline"),
                                selectedImage: UIImage(named: "helpline"))
        case .more:
            if UIDevice().userInterfaceIdiom == .pad {
                return UITabBarItem(title: "More",
                                    image: UIImage(systemName: "ellipsis", withConfiguration: UIImage.SymbolConfiguration(textStyle: .largeTitle))?.withBaselineOffset(fromBottom: UIFont.systemFontSize / 1.3),
                                    selectedImage: UIImage(systemName: "ellipsis", withConfiguration: UIImage.SymbolConfiguration(textStyle: .largeTitle))?.withBaselineOffset(fromBottom: UIFont.systemFontSize / 1.3))
            } else {
                return UITabBarItem(title: "More",
                                    image: UIImage(systemName: "ellipsis")!.withBaselineOffset(fromBottom: UIFont.systemFontSize / 2),
                                    selectedImage: UIImage(systemName: "ellipsis")!.withBaselineOffset(fromBottom: UIFont.systemFontSize / 2))
            }
        }
    }
}

