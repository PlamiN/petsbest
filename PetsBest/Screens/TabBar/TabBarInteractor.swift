//
//  TabBarInteractor.swift
//  PetsBest
//
//  Created by Plamena on 20.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

class TabBarInteractor: TabBarBusinessLogic {

    let presenter: TabBarPresentationLogic!

    init(output: TabBarPresentationLogic) {
        
        presenter = output
    }
}

extension TabBarInteractor {

    func requestChildren() {
        let response = TabBarModel.Response(children: TabBarItemType.allCases,
                                            isError: false,
                                            errorMessage: nil)
        presenter.presentChildren(response)
    }
}

// MARK: - Clean Swift Protocols

protocol TabBarBusinessLogic {

    func requestChildren()
}
