//
//  TabBarConfigurator.swift
//  PetsBest
//
//  Created by Plamena on 20.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

class TabBarConfigurator {

    // MARK: - Singleton

    static var shared = TabBarConfigurator()
    
    private init() {}

    func config(viewController: TabBarViewController) {
        let presenter = TabBarPresenter(output: viewController)
        let interactor = TabBarInteractor(output: presenter)
        viewController.interactor = interactor
    }
}
