//
//  TabBarViewController.swift
//  PetsBest
//
//  Created by Plamena on 20.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import CoreData
import KeychainSwift

class TabBarViewController: UITabBarController {
    
    private let keychain = KeychainSwift()
    
    // MARK: - Storyboard instance
    // NOTE: We have only one storyboard and we are working with xibs at all
    static func storyboardInstance() -> TabBarViewController? {
        let storyboard = UIStoryboard.main
        return storyboard.controllerWithID(StoryboardID.tabBarViewController) as? TabBarViewController
    }

    // MARK: - Clean Swift properties

    var interactor: TabBarBusinessLogic!

    // MARK: - Object lifecycle

    init() {
        super.init(nibName: nil, bundle: nil)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }

    // MARK: - View Controller Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.tabBar.tintColor = UIColor.init(hex: Constants.Colors.main)
        self.tabBar.backgroundColor = UIColor.white
        
        if #available(iOS 15.0, *) {
           let appearance = UITabBarAppearance()
           appearance.configureWithOpaqueBackground()
           
           self.tabBar.standardAppearance = appearance
           self.tabBar.scrollEdgeAppearance = self.tabBar.standardAppearance
        }
        
        requestChildrenControllers()
        
        let isSubmitted = keychain.get( "isSubmitted")
        
        if isSubmitted == "true" {
            self.selectedIndex = 1
        }
        if isSubmitted == nil {
            self.selectedIndex = 0
        }
        
        let isOpenedFromMore = keychain.get("isOpenedFromMore")
        if isOpenedFromMore != nil {
            if isOpenedFromMore == "true"{
                self.selectedIndex = 3
                keychain.delete("isOpenedFromMore")
            }
        }
        let isOpenedFromHome = keychain.get("isOpenedFromHome")
        if isOpenedFromHome != nil {
            if isOpenedFromHome == "true"{
                self.selectedIndex = 0
            }
        }
        
        let dynamicLinkMyPets = keychain.get( "dynamicLinkMyPets")
        let dynamicLinkSubmitClaim = keychain.get( "dynamicLinkSubmitClaim")
        let dynamicLinkSupport = keychain.get( "dynamicLinkSupport")
        let dynamicLinkMore = keychain.get( "dynamicLinkMore")
        
        let dynamicLinkInbox = keychain.get("dynamicLinkInbox")
        
        if dynamicLinkInbox != nil {
            self.tabBarController?.selectedIndex = 3 //you can select another tab if needed
            let vc = InboxTableViewController()
            if let navController = tabBarController?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
            keychain.delete("dynamicLinkInbox")
        }
        
        if dynamicLinkMyPets != nil {
            self.selectedIndex = 0
            keychain.delete("dynamicLinkMyPets")
        }
        
        if dynamicLinkSubmitClaim != nil {
            self.selectedIndex = 1
            keychain.delete("dynamicLinkSubmitClaim")
        }
        
        if dynamicLinkSupport != nil {
            self.selectedIndex = 2
            keychain.delete("dynamicLinkSupport")
        }
        
        if dynamicLinkMore != nil {
            self.selectedIndex = 3
            keychain.delete("dynamicLinkMore")
        }
        
        if UIDevice().userInterfaceIdiom == .pad {
            if UIScreen.main.nativeBounds.height == 2732 {
                let appearance = UITabBarItem.appearance()
                let attributes = [NSAttributedString.Key.font:UIFont(name: "Proxima Nova", size: 25)]
                appearance.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: .normal)
            } else {
                let appearance = UITabBarItem.appearance()
                let attributes = [NSAttributedString.Key.font:UIFont(name: "Proxima Nova", size: 20)]
                appearance.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: .normal)
            }
            //self.tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }

    // MARK: - Setup

    func configure() {
        TabBarConfigurator.shared.config(viewController: self)
    }

}

// MARK: - Internal Logic

extension TabBarViewController {

    private func requestChildrenControllers() {
        
        interactor.requestChildren()
    }
}

// MARK: - conform to TabBarViewControllerProtocol

extension TabBarViewController: TabBarDisplayLogic {

    func loadChildrenControllers(_ model: [TabBarModel.ViewModel]) {
        viewControllers = model.map { model in
            let viewController = TabBarFactory.childViewControllerForModel(model)
            return NavigationController(rootViewController: viewController)
        }
    }

    func errorLoadChildrenControllers(_ message: String) {
        // TODO: - show error message and/or empty state for the viewController
    }
}

// MARK: - Clean Swift Protocols

protocol TabBarDisplayLogic: class {

    func loadChildrenControllers(_ model: [TabBarModel.ViewModel])
    func errorLoadChildrenControllers(_ message: String)
}
