//
//  CustomerSupportIPadTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 16.08.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit

class CustomerSupportIPadTableViewCell: UITableViewCell {

    @IBOutlet private weak var imageViewCustomerSupport: UIImageView!
    @IBOutlet private weak var containerViewDetails: UIView!
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var labelDetails: UILabel!
    @IBOutlet private weak var labelHoursTitle: UILabel!
    @IBOutlet private weak var labelHoursFirst: UILabel!
    @IBOutlet private weak var labelHoursSecond: UILabel!
    @IBOutlet private weak var buttonReachOutAgent: UIButton!
    @IBOutlet private weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet private weak var containerViewHeight: NSLayoutConstraint!
    
    private let worker = ContactDetailsWorker()
    var delegate: CustomerSupportDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        buttonReachOutAgent.setTitle(Constants.titlesButtons.reachOutAgent, for: .normal)
        buttonReachOutAgent.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        buttonReachOutAgent.setTitleColor(UIColor.white, for: .normal)
        buttonReachOutAgent.layer.cornerRadius = 35
        
        containerViewDetails.layer.borderWidth = 1
        containerViewDetails.layer.cornerRadius = 20
        containerViewDetails.layer.borderColor = UIColor.clear.cgColor
        containerViewDetails.layer.masksToBounds = true
        containerViewDetails.layer.shadowOpacity = 0.18
        containerViewDetails.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerViewDetails.layer.shadowRadius = 2
        containerViewDetails.layer.shadowColor = UIColor.black.cgColor
        containerViewDetails.layer.masksToBounds = false
        
        labelTitle.text = Constants.titlesLabels.labelCustomerSupport
        labelDetails.text = Constants.titlesLabels.labelCustomerSupportDetails
        
        labelTitle.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        
        networkingContactDetails()
        
        if UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2048 || UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2160 {
            imageViewHeight.constant = 400
            containerViewHeight.constant = 350
            labelDetails.font = UIFont(name: "Proxima Nova", size: 20)
            labelHoursTitle.font = UIFont(name: "Proxima Nova Bold", size: 21)
            labelHoursFirst.font = UIFont(name: "Proxima Nova", size: 20)
            labelHoursSecond.font = UIFont(name: "Proxima Nova", size: 20)
        }
        if UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2360 || UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2388 {
            imageViewHeight.constant = 430
            containerViewHeight.constant = 370
            labelDetails.font = UIFont(name: "Proxima Nova", size: 20)
            labelHoursTitle.font = UIFont(name: "Proxima Nova Bold", size: 21)
            labelHoursFirst.font = UIFont(name: "Proxima Nova", size: 20)
            labelHoursSecond.font = UIFont(name: "Proxima Nova", size: 20)
        }
    }
    
    private func networkingContactDetails() {
        if ReachabilityClass.isConnectedToNetwork(){
            worker.getContactDetails(completionHandler: { [weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    let openWorkDay = resultUser.Monday.OpenTime12.dropLast(6)
                    let openWorkday = String(openWorkDay.dropFirst())
                    let closedWorkDay = resultUser.Monday.CloseTime12.dropLast(6)
                    let closedWorkday = String(closedWorkDay.dropFirst())
                    let openWeekend = resultUser.Saturday.OpenTime12.dropLast(6)
                    let openedWeekend = String(openWeekend.dropFirst())
                    let closedWeekend1 = resultUser.Saturday.CloseTime12.dropLast(6)
                    let closedWeekend = String(closedWeekend1.dropFirst())
                    self?.labelHoursFirst.text = "\(openWorkday)a - \(closedWorkday)p MST (Monday - Friday)"
                    self?.labelHoursSecond.text = "\(openedWeekend)a - \(closedWeekend)p MST (Saturday)"
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.delegate?.addErroMessage(withError: error.localizedDescription)
                    }
                }
            }) { [weak self] error  in
                switch error {
                case .success(let errorString):
                    DispatchQueue.main.async {
                        self?.delegate?.addGeneralError(withError: errorString)
                    }
                case .failure(let errorString):
                    DispatchQueue.main.async {
                        self?.delegate?.addGeneralError(withError: errorString.localizedDescription)
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.delegate?.addErroMessage(withError: Constants.errorMessages.generalHomeError)
            }
        }
    }

    @IBAction private func actionReachOutAgent(_ sender: Any) {
        if UIDevice().userInterfaceIdiom == .pad {
            delegate?.viewControllerIpad()
        } else {
            delegate?.viewController()
        }
    }
    
    
}
