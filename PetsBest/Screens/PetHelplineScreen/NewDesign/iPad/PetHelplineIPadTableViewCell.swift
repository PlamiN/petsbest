//
//  PetHelplineIPadTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 16.08.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit

class PetHelplineIPadTableViewCell: UITableViewCell {

    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var labelDetails: UILabel!
    @IBOutlet private weak var buttonReachOut: UIButton!
    @IBOutlet private weak var containerViewDetails: UIView!
    
    var delegate: PetHelplineDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        buttonReachOut.setTitle(Constants.titlesButtons.reachOutVetExpert, for: .normal)
        buttonReachOut.backgroundColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        buttonReachOut.setTitleColor(UIColor.white, for: .normal)
        buttonReachOut.layer.cornerRadius = 35
        
        containerViewDetails.layer.borderWidth = 1
        containerViewDetails.layer.cornerRadius = 20
        containerViewDetails.layer.borderColor = UIColor.clear.cgColor
        containerViewDetails.layer.masksToBounds = true
        containerViewDetails.layer.shadowOpacity = 0.18
        containerViewDetails.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerViewDetails.layer.shadowRadius = 2
        containerViewDetails.layer.shadowColor = UIColor.black.cgColor
        containerViewDetails.layer.masksToBounds = false
        
        labelTitle.text = Constants.titlesLabels.labelPetHelpline
        labelDetails.text = Constants.titlesLabels.labelPetHelplineDetails
        labelTitle.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        
        if UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2048  || UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2360 || UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2160 || UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2388 {
            labelDetails.font = UIFont(name: "Proxima Nova", size: 20)
        }
    }

    @IBAction private func actionReachOut(_ sender: Any) {
        delegate?.openChatWithVet()
    }
    
}
