//
//  PetHelplineTableViewCell.swift
//  PetsBest
//
//  Created by Plamena on 16.08.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit

protocol PetHelplineDelegate {
    func openChatWithVet()
}

class PetHelplineTableViewCell: UITableViewCell {

    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var labelDetails: UILabel!
    @IBOutlet private weak var buttonReachOut: UIButton!
    @IBOutlet private weak var containerViewDetails: UIView!
    @IBOutlet private weak var constraintTopView: NSLayoutConstraint!
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var constraintButton: NSLayoutConstraint!
    @IBOutlet private weak var titleConstraint: NSLayoutConstraint!
    
    var delegate: PetHelplineDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        buttonReachOut.setTitle(Constants.titlesButtons.reachOutVetExpert, for: .normal)
        buttonReachOut.backgroundColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        buttonReachOut.setTitleColor(UIColor.white, for: .normal)
        buttonReachOut.layer.cornerRadius = 25
        
        containerViewDetails.layer.borderWidth = 1
        containerViewDetails.layer.cornerRadius = 15
        containerViewDetails.layer.borderColor = UIColor.clear.cgColor
        containerViewDetails.layer.masksToBounds = true
        containerViewDetails.layer.shadowOpacity = 0.18
        containerViewDetails.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerViewDetails.layer.shadowRadius = 2
        containerViewDetails.layer.shadowColor = UIColor.black.cgColor
        containerViewDetails.layer.masksToBounds = false
        
        labelTitle.text = Constants.titlesLabels.labelPetHelpline
        labelDetails.text = Constants.titlesLabels.labelPetHelplineDetails
        labelTitle.textColor = UIColor.init(hex: Constants.Colors.welcomeLabels)
        
        if UIScreen.main.nativeBounds.height == 1334 || UIScreen.main.nativeBounds.height == 2208 || UIScreen.main.nativeBounds.height == 1920 {
            labelDetails.font = UIFont(name: "Proxima Nova", size: 14)
            buttonReachOut.titleLabel?.font = UIFont(name: "Proxima Nova Bold", size: 14)
            constraintTopView.constant = 4
            stackView.spacing = 0.0
            constraintButton.constant = 16
            titleConstraint.constant = 5
        }
    }

    @IBAction private func actionReachOut(_ sender: Any) {
        delegate?.openChatWithVet()
    }
    
}
