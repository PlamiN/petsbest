//
//  CustomerSupporViewController.swift
//  PetsBest
//
//  Created by Plamena on 16.08.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class CustomerSupporViewController: UIViewController {

    @IBOutlet weak var tableViewSupport: UITableView!
    
    private let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        configTableViewProperties()
    }
    
    //Firebase Analytics
    override func viewDidAppear(_ animated: Bool) {
        Analytics.logEvent(AnalyticsEventScreenView,
                           parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.whiskerDocs])
        
    }
    
    // MARK: - Set tableView
    
    private func configTableViewProperties() {
        tableViewSupport.registerCell(CustomerSupportTableViewCell.self)
        tableViewSupport.registerCell(PetHelplineTableViewCell.self)
        tableViewSupport.registerCell(CustomerSupportIPadTableViewCell.self)
        tableViewSupport.registerCell(PetHelplineIPadTableViewCell.self)
        
        tableViewSupport.delegate = self
        tableViewSupport.dataSource = self
        
        self.tabBarController?.delegate = self
        
        print(UIScreen.main.nativeBounds.height)
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                self.tableViewSupport.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
            case 1334:
                print("iPhone 6/6S/7/8, SE 2nd Generation")
                self.tableViewSupport.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
                
            case 1792:
                print("iPhone XR/11")
                self.tableViewSupport.contentInset = UIEdgeInsets(top: -65, left: 0, bottom: 0, right: 0)
                
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                self.tableViewSupport.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
                
            case 2436:
                print("iPhone X/XS/11 Pro,iPhone 12 Mini")
                self.tableViewSupport.contentInset = UIEdgeInsets(top: -65, left: 0, bottom: 0, right: 0)
            case 2532:
                print("iPhone 12, iPhone 12 Pro")
                self.tableViewSupport.contentInset = UIEdgeInsets(top: -65, left: 0, bottom: 0, right: 0)
            case 2688:
                print("iPhone XS Max/11 Pro Max")
                self.tableViewSupport.contentInset = UIEdgeInsets(top: -65, left: 0, bottom: 0, right: 0)
            case 2778:
                print("iPhone 12 Pro Max")
                self.tableViewSupport.contentInset = UIEdgeInsets(top: -65, left: 0, bottom: 0, right: 0)
                
            default:
                print("Unknown")
            }
        }
        
        if UIDevice().userInterfaceIdiom == .pad {
            self.tableViewSupport.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
        }
        

        tableViewSupport.separatorStyle = .none
        
    }

}

extension CustomerSupporViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .pad {
            if indexPath.row == 0 {
                if UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2048 || UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2160 {
                    return 730.0
                }
                if UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2360 || UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2388 {
                    return 750.0
                } else {
                    return 900.0
                }
            } else {
                if UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2048 || UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2160 {
                    return 290.0
                }
                if UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2360 || UIDevice().userInterfaceIdiom == .pad && UIScreen.main.nativeBounds.height == 2388 {
                    return 300.0
                } else {
                    return 330.0
                }
            }
        } else {
            if indexPath.row == 0 {
                if UIScreen.main.nativeBounds.height == 1334 {
                    return 435.0
                }
                if UIScreen.main.nativeBounds.height == 2208 || UIScreen.main.nativeBounds.height == 1920 {
                    return 500.0
                } else {
                    return 550.0
                }
            } else {
                if UIScreen.main.nativeBounds.height == 1334{
                    return 175.0
                }
                if UIScreen.main.nativeBounds.height == 2208 || UIScreen.main.nativeBounds.height == 1920 {
                    return 180.0
                } else {
                    return 240.0
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CustomerSupportTableViewCell = tableViewSupport.dequeueCell(CustomerSupportTableViewCell.self)
   
        let cell1: PetHelplineTableViewCell = tableViewSupport.dequeueCell(PetHelplineTableViewCell.self)
        
        let cell2: CustomerSupportIPadTableViewCell = tableViewSupport.dequeueCell(CustomerSupportIPadTableViewCell.self)
        
        let cell3: PetHelplineIPadTableViewCell = tableViewSupport.dequeueCell(PetHelplineIPadTableViewCell.self)
        
        
        cell.selectionStyle = .none
        cell1.selectionStyle = .none
        cell.delegate = self
        cell1.delegate = self
        cell2.selectionStyle = .none
        cell2.delegate = self
        
        cell3.selectionStyle = .none
        cell3.delegate = self
        
        if UIDevice().userInterfaceIdiom == .pad {
            if indexPath.row == 0 {
                return cell2
            } else {
                return cell3
            }
        } else {
            if indexPath.row == 0 {
                return cell
            } else {
                return cell1
            }
        }
    }
}

extension CustomerSupporViewController: CustomerSupportDelegate {
    func viewControllerIpad() {
        let vc = PetSupportPadViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewController() {
        let vc = PetSupportViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func addErroMessage(withError: String) {
        DispatchQueue.main.async {
            self.showAlert(withTitle: "", message: withError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
        }
    }
    
    func addGeneralError(withError: String) {
        DispatchQueue.main.async {
            self.showAlert(withTitle: "", message: withError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
        }
    }
}

extension CustomerSupporViewController: PetHelplineDelegate {
    func openChatWithVet() {
        if UIDevice().userInterfaceIdiom == .pad {
            let vc = PetHelplineIPadViewController()
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = PetHelplineViewController()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension CustomerSupporViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 2 {
            self.defaults.removeObject(forKey: "notNowActivatedLocationServices")
        }
        if tabBarIndex == 0 {
            self.defaults.removeObject(forKey: "notNowActivatedLocationServices")
        }
        if tabBarIndex == 3 {
            self.defaults.removeObject(forKey: "notNowActivatedLocationServices")
        }
    }
}
