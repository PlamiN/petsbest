//
//  PetSupportViewController.swift
//  PetsBest
//
//  Created by Plamena on 13.11.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class PetSupportViewController: UIViewController {

    @IBOutlet private weak var imageLogo: UIImageView!
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var buttonCall: UIButton!
    @IBOutlet private weak var labelCall: UILabel!
    @IBOutlet private weak var labelOperatingHours: UILabel!
    @IBOutlet private weak var labelWeekdayHours: UILabel!
    @IBOutlet private weak var labelWeekendHours: UILabel!
    @IBOutlet private weak var labelQuestions: UILabel!
    @IBOutlet private weak var labelSearchDetails: UILabel!
    @IBOutlet private weak var buttonSearchFAQ: UIButton!

    @IBOutlet weak var constraintFirst: NSLayoutConstraint!
    @IBOutlet weak var constraintSecond: NSLayoutConstraint!
    @IBOutlet weak var ConstraintFourth: NSLayoutConstraint!
    @IBOutlet weak var constriantFifth: NSLayoutConstraint!

    @IBOutlet weak var constraintSix: NSLayoutConstraint!
    @IBOutlet weak var constraintSeven: NSLayoutConstraint!
    
    var isOpenedFromMoreScreen = false
    
    private let worker = ContactDetailsWorker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
    }
    
    //Firebase Analytics
    override func viewDidAppear(_ animated: Bool) {
        Analytics.logEvent(AnalyticsEventScreenView,
                           parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.petHelpline])
        
    }
    
    private func setViews() {
        
        navigationController?.navigationBar.isHidden = false
        
        self.tabBarController?.delegate = self
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                constraintFirst.constant = 5
                constraintSecond.constant = 5
                constraintSix.constant = 5
                constraintSeven.constant = 5
                ConstraintFourth.constant = 5
                constriantFifth.constant = 5

            case 1334:
                print("iPhone 6/6S/7/8, SE 2nd Generation")
                constraintFirst.constant = 15
                constraintSecond.constant = 15
                constraintSix.constant = 12
                constraintSeven.constant = 12
                ConstraintFourth.constant = 18
                constriantFifth.constant = 18
            case 1792:
                print("iPhone XR/11")
   
                constraintFirst.constant = 40
                constraintSecond.constant = 40
                constraintSix.constant = 20
                constraintSeven.constant = 20
                ConstraintFourth.constant = 30
                constriantFifth.constant = 64
                
            case 1920:
                print("iPhone 6+/6S+/7+/8+")

                constraintFirst.constant = 15
                constraintSecond.constant = 15
                constraintSix.constant = 15
                constraintSeven.constant = 12
                ConstraintFourth.constant = 15
                constriantFifth.constant = 15
            case 2208:
                print("iPhone Simulator 6+/6S+/7+/8+")

                constraintFirst.constant = 35
                constraintSecond.constant = 35
                constraintSix.constant = 20
                constraintSeven.constant = 20
                ConstraintFourth.constant = 25
                constriantFifth.constant = 24
            case 2436:
                print("iPhone X/XS/11 Pro,iPhone 12 Mini")
                constraintFirst.constant = 40
                constraintSecond.constant = 40
                constraintSix.constant = 20
                constraintSeven.constant = 20
                ConstraintFourth.constant = 30
                constriantFifth.constant = 64
            case 2532:
                print("iPhone 12, iPhone 12 Pro, iPhone 13, iPhone 13 Pro")
                constraintFirst.constant = 30
                constraintSecond.constant = 30
                constraintSix.constant = 20
                constraintSeven.constant = 20
                ConstraintFourth.constant = 20
                constriantFifth.constant = 64
            case 2688:
                print("iPhone XS Max/11 Pro Max")
                constraintFirst.constant = 40
                constraintSecond.constant = 40
                constraintSix.constant = 20
                constraintSeven.constant = 20
                ConstraintFourth.constant = 30
                constriantFifth.constant = 64
            case 2778:
                print("iPhone 12 Pro Max, iPhone 13 Pro Max")
                constraintFirst.constant = 40
                constraintSecond.constant = 40
                constraintSix.constant = 20
                constraintSeven.constant = 20
                ConstraintFourth.constant = 30
                constriantFifth.constant = 64
                
            case 2340:
                print("iPhone 13 mini")
                constraintFirst.constant = 40
                constraintSecond.constant = 40
                constraintSix.constant = 20
                constraintSeven.constant = 20
                ConstraintFourth.constant = 30
                constriantFifth.constant = 64
            default:
                print("Unknown")
            }
        }
        
        buttonCall.layer.cornerRadius = buttonCall.frame.size.height/2
//        labelTitle.textColor = UIColor.init(hex: Constants.Colors.labels)
        labelCall.textColor = UIColor.init(hex: Constants.Colors.labels)
//        labelOperatingHours.textColor = UIColor.init(hex: Constants.Colors.labels)
//        labelWeekdayHours.textColor = UIColor.init(hex: Constants.Colors.labels)
//        labelWeekendHours.textColor = UIColor.init(hex: Constants.Colors.labels)

        labelQuestions.textColor = UIColor.init(hex: Constants.Colors.labels)
        buttonCall.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        
        buttonSearchFAQ.backgroundColor = UIColor.init(hex: Constants.Colors.gray)
        buttonSearchFAQ.setTitleColor(UIColor.init(hex: Constants.Colors.main), for: .normal)
        buttonSearchFAQ.layer.cornerRadius = 25
        
        self.navigationItem.title = Constants.titlesScreens.petsBestSupport
        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
        
        networkingContactDetails()
    }
    
    private func networkingContactDetails() {
        if ReachabilityClass.isConnectedToNetwork(){
            worker.getContactDetails(completionHandler: { [weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    print(resultUser)
                    let openWorkDay = resultUser.Monday.OpenTime12.dropLast(6)
                    let openWorkday = String(openWorkDay.dropFirst())
                    let closedWorkDay = resultUser.Monday.CloseTime12.dropLast(6)
                    let closedWorkday = String(closedWorkDay.dropFirst())
                    let openWeekend = resultUser.Saturday.OpenTime12.dropLast(6)
                    let openedWeekend = String(openWeekend.dropFirst())
                    let closedWeekend1 = resultUser.Saturday.CloseTime12.dropLast(6)
                    let closedWeekend = String(closedWeekend1.dropFirst())
                    self?.labelWeekdayHours.text = "\(openWorkday)a - \(closedWorkday)p MST (Monday - Friday)"
                    self?.labelWeekendHours.text = "\(openedWeekend)a - \(closedWeekend)p MST (Saturday)"
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    }
                }
            }) { [weak self] error  in
                switch error {
                case .success(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
            }
        }
    }
    @IBAction func actionOpenFAQ(_ sender: Any) {
        let vc = LoadFAQViewController()
        let navigationController = NavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .overFullScreen
        present(navigationController, animated: true, completion: nil)
    }
    
    @objc private func back(){
        if isOpenedFromMoreScreen == true {
            self.tabBarController?.selectedIndex = 2 //you can select another tab if needed
            let vc = CustomerSupporViewController()
            if let navController = tabBarController?.viewControllers?[2] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        } else {
            let vc = CustomerSupporViewController()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func actionCall(_ sender: UIButton) {
        //Firebase Analytics
        Analytics.logEvent(Constants.FirebaseEvents.pressCall, parameters: nil)
        let helpLine = "1-877-738-7237"
        if let url = URL(string: "tel://\(helpLine)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

extension PetSupportViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
//        if tabBarIndex == 0 {
//            guard let tabBarViewController = TabBarViewController.storyboardInstance() else { return }
//            self.present(tabBarViewController, animated: false, completion: nil)
//        }
    }
}
