//
//  PetHelplineViewController.swift
//  PetsBest
//
//  Created by Plamena on 4.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class PetHelplineViewController: UIViewController {

    @IBOutlet private weak var labelDetails: UILabel!
    @IBOutlet private weak var buttonChat: UIButton!
    @IBOutlet private weak var buttonCall: UIButton!
    @IBOutlet private weak var labelChat: UILabel!
    @IBOutlet private weak var labelCall: UILabel!
    @IBOutlet private weak var buttonChatHeight: NSLayoutConstraint!
    @IBOutlet private weak var buttonCallHeight: NSLayoutConstraint!
    @IBOutlet private weak var buttonCallWidth: NSLayoutConstraint!
    @IBOutlet private weak var imageWhiskerHeight: NSLayoutConstraint!
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var stackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
    }
    
    //Firebase Analytics
    override func viewDidAppear(_ animated: Bool) {
        Analytics.logEvent(AnalyticsEventScreenView,
                           parameters: [AnalyticsParameterScreenClass: Constants.FirebaseScreens.whiskerDocs])
        
    }

    private func setViews() {
        
        navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Pet Helpline"

        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))

        buttonChat.layer.cornerRadius = buttonChat.frame.size.height/2
        buttonCall.layer.cornerRadius = buttonCall.frame.size.height/2
        buttonChat.backgroundColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        buttonCall.backgroundColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        labelTitle.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)

        labelChat.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        labelCall.textColor = UIColor.init(hex: Constants.Colors.PBHelpline)
        
        labelDetails.text = Constants.titlesLabels.labelContactDetails
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                stackView.spacing = 10
                imageWhiskerHeight.constant = 200
            case 1792:
                stackView.spacing = 60
                imageWhiskerHeight.constant = 300
            case 2208:
                stackView.spacing = 60
                imageWhiskerHeight.constant = 300
            case 1920:
                stackView.spacing = 30
                imageWhiskerHeight.constant = 250
            case 2436:
                stackView.spacing = 60
                imageWhiskerHeight.constant = 300
            case 2532:
                stackView.spacing = 60
                imageWhiskerHeight.constant = 300
            case 2688:
                stackView.spacing = 60
                imageWhiskerHeight.constant = 300
            case 2778:
                stackView.spacing = 60
                imageWhiskerHeight.constant = 300
                
            default:
                print("Unknown")
            }
        }
    }
    
    @objc private func back(){
        let vc = CustomerSupporViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionStartChat(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "You are now leaving Pets Best and being directed to a veterinary expert for your pet health related questions.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {[weak self] _ in
            Analytics.logEvent(Constants.FirebaseEvents.whiskerdocsChat, parameters: nil)
            let vc = ChatViewController()
            let navigationController = NavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .overFullScreen
            self?.present(navigationController, animated: true, completion: nil)
        }))
        let cancelAlert = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler:nil)
                        cancelAlert.setValue(UIColor.gray, forKey: "titleTextColor")
                        alert.addAction(cancelAlert)

        self.present(alert, animated: true)
    }
    
    @IBAction func actionCallHelpLine(_ sender: UIButton) {
        Analytics.logEvent(Constants.FirebaseEvents.whiskerdocsCall, parameters: nil)
        let helpLine = "877-473-4679"
        if let url = URL(string: "tel://\(helpLine)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
