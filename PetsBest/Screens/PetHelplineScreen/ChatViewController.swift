//
//  ChatViewController.swift
//  PetsBest
//
//  Created by Plamena on 13.11.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import WebKit

class ChatViewController: UIViewController {

    @IBOutlet private weak var webViewChat: WKWebView!
    private let worker = PolicyHolderWorker()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Pet Helpline Chat"

        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
        
        networkingPolicyDetails()
        //https://whiskerdocs.custhelp.com/app/chat/chat_landing/Contact.Name.First/Paul/Contact.Name.Last/Crane/Contact.Email.0.Address/paulandali@gmail.com
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @objc private func back(){
        self.dismiss(animated: true, completion: nil)
    }
    
    private func networkingPolicyDetails() {
        DispatchQueue.main.async {
            Spinner.start()
        }
        if ReachabilityClass.isConnectedToNetwork(){
            worker.getPolicyDetails(completionHandler: { [weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    print(resultUser.FirstName)
                    print(resultUser.LastName)
                    print(resultUser.Email)
                    let link = URL(string:"https://whiskerdocs.custhelp.com/app/chat/chat_landing/Contact.Name.First/\(resultUser.FirstName)/Contact.Name.Last/\(resultUser.LastName)/Contact.Email.0.Address/\(resultUser.Email)")
                    if link != nil {
                        let request = NSMutableURLRequest(url: link!)
                        self?.webViewChat.load(request as URLRequest)
                    } else {
                        DispatchQueue.main.async {
                            self?.showAlert(withTitle: "", message: "WhiskerDocs chat is currently unavailable. Please try again later.", buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                            Spinner.stop()
                        }
                    }
                    
                    DispatchQueue.main.async {
                        Spinner.stop()
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        Spinner.stop()
                    }
                }
            }) { [weak self] error  in
                switch error {
                case .success(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        Spinner.stop()
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                        Spinner.stop()
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                Spinner.stop()
            }
        }
    }
}
