//
//  ContactDetailsWorker.swift
//  PetsBest
//
//  Created by Plamena on 13.11.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation
import KeychainSwift

typealias OnSuccessCompletionContactDetails = (Result<ContactsModel>) -> ()
typealias OnErrorHandlerContactDetails = (Result<String>) -> ()

class ContactDetailsWorker {

    private let keychain = KeychainSwift()
    
    func getContactDetails(completionHandler: @escaping OnSuccessCompletionContactDetails,
                       onErrorHandler: @escaping OnErrorHandlerContactDetails) {
        
        if var url = try? APIRouter.get_contact_options.asURLRequest() {
            
            let appToken = keychain.get("appToken")
            
            guard let token = appToken?.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil) else { return }
            print(token)
            
            url.httpMethod = HTTPMethod.get.rawValue
            url.setValue(token, forHTTPHeaderField: "AuthToken")
            
            URLSession.shared.dataTask(with: url) { data, response, error in
                if let error = error {
                    return onErrorHandler(.failure(error))
                }
                
                guard let content = data else {
                    print("No data")
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    let statusCode = httpResponse.statusCode
                    
                    if (statusCode != 200) {
                        guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                            print("Not containing JSON")
                            return onErrorHandler(.success(Constants.errorMessages.generalHomeError))
                        }
                        guard let error = json["Message"] else { return }
                        let errorString = "\(error)"
                        return onErrorHandler(.success(errorString))
                    } else {
                        let response = try? JSONDecoder().decode(ContactsModel.self, from: content)
                        if let response = response {
                            DispatchQueue.main.async {
                                completionHandler(.success(response))
                            }
                        }
                    }
                }
            }.resume()
        }
        
    }
}


