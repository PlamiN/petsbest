//
//  LoadFAQViewController.swift
//  PetsBest
//
//  Created by Plamena on 16.08.21.
//  Copyright © 2021 Plamena. All rights reserved.
//

import UIKit
import WebKit
import NVActivityIndicatorView

class LoadFAQViewController: UIViewController {

    @IBOutlet private weak var webViewAddPet: WKWebView!
    @IBOutlet private weak var activityIndicatorScreen: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        loadingWebView()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
    }
    
    private func setViews() {
        DispatchQueue.main.async {
            let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self.back))
            self.webViewAddPet.scrollView.bounces = false
            self.startLoadingWholeScreen()
        }
    }
    
    private func loadingWebView() {
        DispatchQueue.main.async {
            if let url = try? APIRouter.FAQ.asURLRequestCustomerPortal() {
                self.webViewAddPet.load(url as URLRequest)
                self.stopLoadingWholeScreen()
            }
        }
    }
    
    @objc private func back(){
        self.dismiss(animated: true, completion: nil)
    }
    
    private func startLoadingWholeScreen() {
        activityIndicatorScreen?.startAnimating()
    }
    
    private func stopLoadingWholeScreen() {
        activityIndicatorScreen?.stopAnimating()
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        if let urlResponse = navigationResponse.response as? HTTPURLResponse,
           let url = urlResponse.url,
           let allHeaderFields = urlResponse.allHeaderFields as? [String : String] {
           let cookies = HTTPCookie.cookies(withResponseHeaderFields: allHeaderFields, for: url)
           HTTPCookieStorage.shared.setCookies(cookies , for: urlResponse.url!, mainDocumentURL: nil)
           decisionHandler(.allow)
        }
    }
}
