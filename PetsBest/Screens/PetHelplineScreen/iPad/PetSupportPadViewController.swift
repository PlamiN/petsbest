//
//  PetSupportPadViewController.swift
//  PetsBest
//
//  Created by Plamena on 25.11.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class PetSupportPadViewController: UIViewController {

    @IBOutlet private weak var imageLogo: UIImageView!
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var buttonCall: UIButton!
    @IBOutlet private weak var labelCall: UILabel!
    @IBOutlet private weak var labelOperatingHours: UILabel!
    @IBOutlet private weak var labelWeekdayHours: UILabel!
    @IBOutlet private weak var labelWeekendHours: UILabel!
    @IBOutlet private weak var labelQuestions: UILabel!
    @IBOutlet private weak var labelSearchDetails: UILabel!
    @IBOutlet private weak var buttonSearchFAQ: UIButton!
    
    var isOpenedFromMoreScreen = false


    private let worker = ContactDetailsWorker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
    }
    
    private func setViews() {
        navigationController?.navigationBar.isHidden = false
        buttonCall.layer.cornerRadius = buttonCall.frame.size.height/2
        labelCall.textColor = UIColor.init(hex: Constants.Colors.labels)
        buttonCall.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        
        labelQuestions.textColor = UIColor.init(hex: Constants.Colors.labels)
        buttonCall.backgroundColor = UIColor.init(hex: Constants.Colors.main)
        
        buttonSearchFAQ.backgroundColor = UIColor.init(hex: Constants.Colors.gray)
        buttonSearchFAQ.setTitleColor(UIColor.init(hex: Constants.Colors.main), for: .normal)
        buttonSearchFAQ.layer.cornerRadius = 35
        
        self.navigationItem.title = Constants.titlesScreens.petsBestSupport
        let backImage = (UIImage(named: "leftArrow"))!.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(back))
        
        networkingContactDetails()
    }
    
    private func networkingContactDetails() {
        if ReachabilityClass.isConnectedToNetwork(){
            worker.getContactDetails(completionHandler: { [weak self] resultUser in
                switch resultUser {
                case .success(let resultUser):
                    print(resultUser)
                    let openWorkDay = resultUser.Monday.OpenTime12.dropLast(6)
                    let openWorkday = String(openWorkDay.dropFirst())
                    let closedWorkDay = resultUser.Monday.CloseTime12.dropLast(6)
                    let closedWorkday = String(closedWorkDay.dropFirst())
                    let openWeekend = resultUser.Saturday.OpenTime12.dropLast(6)
                    let openedWeekend = String(openWeekend.dropFirst())
                    let closedWeekend1 = resultUser.Saturday.CloseTime12.dropLast(6)
                    let closedWeekend = String(closedWeekend1.dropFirst())
                    self?.labelWeekdayHours.text = "\(openWorkday)a - \(closedWorkday)p MST (Monday - Friday)"
                    self?.labelWeekendHours.text = "\(openedWeekend)a - \(closedWeekend)p MST (Saturday)"
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    }
                }
            }) { [weak self] error  in
                switch error {
                case .success(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showAlert(withTitle: "", message: error.localizedDescription, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showAlert(withTitle: "", message: Constants.errorMessages.generalHomeError, buttonTitle: Constants.errorMessages.ok, preferredStyle: .alert, completion: nil)
            }
        }
    }
    
    @objc private func back(){
        if isOpenedFromMoreScreen == true {
            self.tabBarController?.selectedIndex = 2 //you can select another tab if needed
            let vc = CustomerSupporViewController()
            if let navController = tabBarController?.viewControllers?[2] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        } else {
            let vc = CustomerSupporViewController()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func actionOpenFAQ(_ sender: Any) {
        let vc = LoadFAQViewController()
        let navigationController = NavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .overFullScreen
        present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func actionCall(_ sender: UIButton) {
        let helpLine = "1-877-738-7237"
        if let url = URL(string: "tel://\(helpLine)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

