//
//  main.swift
//  PetsBest
//
//  Created by Plamena on 6.01.22.
//  Copyright © 2022 Plamena. All rights reserved.
//

import Foundation
import UIKit
import AcousticMobilePush

//let _ = UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, nil, NSStringFromClass(MCEAppDelegate.self) )

UIApplicationMain(
    CommandLine.argc,
    CommandLine.unsafeArgv,
    nil,
    NSStringFromClass(MCEAppDelegate.self)
)
