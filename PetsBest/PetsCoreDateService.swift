//
//  PetsCoreDateService.swift
//  PetsBest
//
//  Created by Plamena on 21.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation
import CoreData
import UIKit

typealias PetsCoreDataHandler = (Bool, [Pet]) -> ()

class PetsCoreDataService {
    private var moc: NSManagedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    private var pets = [Pet]()
    
    init(moc: NSManagedObjectContext) {
        self.moc = moc
    }
    
    //MARK - Public
    
    //READ
    func getAppPets() -> [Pet]? {
        let request: NSFetchRequest<Pet> = Pet.fetchRequest()
        request.fetchLimit = 3
        
        do {
            if let fetchResults = try moc.fetch(request) as? [Pet] {
                if ( fetchResults.count > 0 ) {
                    pets = try moc.fetch(request)
                    return pets
                }
            }
            
        } catch let error {
            print("Error fetching pets: \(error.localizedDescription)")
        }
        
        return nil
    }
    
    //MARK - Private
    private func petExists(_ type: String) -> Pet? {
        let request: NSFetchRequest<Pet> = Pet.fetchRequest()
        request.predicate = NSPredicate(format: "policyNumber = %@", type)
        
        var report: Pet?
        do {
            let result = try moc.fetch(request)
            report = result.isEmpty ? addNewPet(report: type) : result.first
        } catch let error {
            print("Error getting error: \(error.localizedDescription)")
        }
        
        return report
    }
    
    private func addNewPet(report: String) -> Pet {
        let rep = Pet(context: moc)
        rep.policyNumber = report
        
        return rep
    }
    
    //UPDATE
    func addImageToPet(policyNumber: String, image: Data, completion: PetsCoreDataHandler){
        if let pet = petExists(policyNumber) {
            pet.image = image
            
            completion(true, pets)
        }
        
        save()
    }
    
    //CREATE
    func addPet(policyNumber: String, petName: String, breedName: String, image: Data, sex: String, age: String, species: String, planDescription: String, planType: String, deductible: String, reimbursement: String, completion: PetsCoreDataHandler){
        let pet = Pet(context: moc)
        pet.policyNumber = policyNumber
        pet.petName = petName
        pet.breedName = breedName
        pet.image = image
        pet.sex = sex
        pet.age = age
        pet.species = species
        pet.planDescription = planDescription
        pet.planType = planType
        pet.deductible = deductible
        pet.reimbursement = reimbursement
        
        if let pet = petExists(policyNumber) {
            pets.append(pet)
            completion(true, pets)
        } 
        save()
    }
    
    func deleteAllRecords() {
            //delete all data
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Pet")
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)

            do {
                try moc.execute(deleteRequest)
                try moc.save()
            } catch {
                print ("There was an error")
            }
        }
    
    private func save(completion: ((Bool) -> Void)? = nil) {
        var success: Bool
        do {
            try moc.save()
            success = true
        } catch let error {
            print("Saved error: \(error.localizedDescription)")
            moc.rollback()
            success = false
        }
        
        if let completion = completion {
            completion(success)
        }
    }
}


