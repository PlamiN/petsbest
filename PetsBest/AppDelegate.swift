//
//  AppDelegate.swift
//  PetsBest
//
//  Created by Plamena on 9.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit
import KeychainSwift
import CoreData
import BackgroundTasks
import Firebase
import UserNotifications
import FirebaseMessaging

extension Notification.Name {

    static let appTimeout = Notification.Name("appTimeout")

}

class AppDelegate : UIResponder, UIApplicationDelegate {
    var state: NSDictionary? = nil
    
    // Single Window Support (iOS ≤ 12)
    var window: UIWindow?
    
    lazy var coreDataStack = CoreDataStack()
    private let keychain = KeychainSwift()
    private let defaults = UserDefaults.standard
    
    private let testWorker = TestWorker()
    
    private var idleTimer: Timer?
    
    private var wentToBackground = false
    
    private var userId: String = ""
    private var channelId: String = ""
    
    var indicator = UIActivityIndicatorView()
    
    func registerPlugins() {
        
        // MCE Inbox Templates Plugins
        MCEInboxActionPlugin.register()
        MCEInboxDefaultTemplate.register()
        MCEInboxPostTemplate.register()
        
    }
    
    /// set orientations you want to be allowed in this property by default
    var orientationLock = UIInterfaceOrientationMask.portrait
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow:3) as Date)
        
//        let isAppUpdated = isUpdateAvailable()
        
        keychain.delete("FaceID")
        keychain.delete("foregroundFaceID")
        keychain.delete("PushNotifications")
        keychain.delete("firstPushNotifications")
        keychain.delete("isSubmitted")

        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        registerPlugins()
        
        UNUserNotificationCenter.current().delegate = NotificationDelegate.shared
        
        //Notificaiton Settings Support
        if #available(iOS 12.0, *) {
            MCESdk.shared.openSettingsForNotification = { notification in
                if let vc = MCESdk.shared.findCurrentViewController() {
                    let alert = UIAlertController(title: "Should show app settings for notifications", message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in

                    }))
                    vc.present(alert, animated: true, completion: {

                    })
                } else {
                    print("Should show app settings for notifications")
                }
            }
        }

//        self.acquireIdentification()
        self.inboxUpdate()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.inboxUpdate), name:
            MCENotificationName.InboxCountUpdate.rawValue, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.acquireIdentification), name:
                                                MCENotificationName.MCERegistered.rawValue, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.failure), name:
                                                MCENotificationName.eventFailure.rawValue, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.openInboxLink(_:)), name: Notification.Name.deepLinkTapped, object: nil)

        // This can be used to not present push notifications while app is running
        MCESdk.shared.presentNotification = {(userInfo) -> Bool in
            return true
        }

        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Pet")
        do {
            let results = try coreDataStack.objectContext.fetch(fetchRequest)
            if results.count == 0 {
                
            }
        } catch {
            fatalError("Error fetching data!")
        }
        
        UserDefaults.standard.set(nil, forKey: "chosenOption")
        
        StoreReviewHelper.incrementAppOpenedCount()
        
        
        self.startWithLoginFlow()
        
        return true
    }
    

    private func isUpdateAvailable() -> Bool {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                return false
        }
        
        let data = try? Data(contentsOf: url)
        guard let json = try? JSONSerialization.jsonObject(with: data ?? Data(), options: [.allowFragments]) as? [String: Any] else {
            return false
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            print("version in app store", version,currentVersion);
            
            return true
        }
        
        return false
    }
    
    //MARK: UIActivity Indicator
    func addIndicator(){
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.height)) as UIActivityIndicatorView
        //  indicator.hidesWhenStopped = true
        indicator.style = UIActivityIndicatorView.Style.whiteLarge
        indicator.backgroundColor = UIColor.black
        indicator.alpha = 0.75
    }
    
    func showIndicator(){
        //show the Indicator
        indicator.startAnimating()
        window?.rootViewController?.view .addSubview(indicator)
        
    }
    
    func hideIndicator(){
        //Hide the Indicator
        indicator.stopAnimating()
        indicator.removeFromSuperview()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("token: \(token)")
        MCESdk.shared.registerDeviceToken(deviceToken)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("Recived: \(userInfo)")
       //Parsing userinfo:

       if let info = userInfo["aps"] as? Dictionary<String, AnyObject>
                {
                    var alertMsg = info["subject"] as! String
                    var alert: UIAlertView!
                    alert = UIAlertView(title: "", message: alertMsg, delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                }
        
//        if let data = try? JSONSerialization.data(withJSONObject: userInfo, options: .prettyPrinted), let string = String(data: data, encoding: .utf8) {
//            print("Silent notification incoming: \(string)\n\n")
//        } else {
//            print("Silent notification incoming: <unknown format>")
//        }
    }
    
    @objc func openInboxLink(_ note: NSNotification) {
        
        let deepLinkTapped = defaults.string(forKey: "deepLink")
        
        if(deepLinkTapped == "petsbestapp://submitclaim"){
            self.keychain.set("true", forKey: "dynamicLinkSubmitClaim")
            let tabBarViewController = TabBarViewController.storyboardInstance()
            self.window?.rootViewController = tabBarViewController
        } else if (deepLinkTapped == "petsbestapp://moremenu"){
            self.keychain.set("true", forKey: "dynamicLinkMore")
            let tabBarViewController = TabBarViewController.storyboardInstance()
            self.window?.rootViewController = tabBarViewController
        } else if (deepLinkTapped == "petsbestapp://support"){
            self.keychain.set("true", forKey: "dynamicLinkSupport")
            let tabBarViewController = TabBarViewController.storyboardInstance()
            self.window?.rootViewController = tabBarViewController
        } else if (deepLinkTapped == "petsbestapp://home"){
            self.keychain.set("true", forKey: "dynamicLinkMyPets")
            let tabBarViewController = TabBarViewController.storyboardInstance()
            self.window?.rootViewController = tabBarViewController
        } else if (deepLinkTapped == "petsbestapp://addpet"){
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is TabBarViewController } ).first?.rootViewController as? TabBarViewController
            tabBarVC?.selectedIndex = 0 //you can select another tab if needed
            let vc = AddPetViewController()
            if let navController = tabBarVC?.viewControllers?[0] as? NavigationController {
               navController.pushViewController(vc, animated: true)
            }
        } else if (deepLinkTapped == "petsbestapp://inbox"){
            self.keychain.set("true", forKey: "isOpenedFromMore")

            DispatchQueue.main.async { // Change `2.0` to the desired number of seconds.
                let vc = InboxTableViewController()
                 if let tabBarController = self.window?.rootViewController as? UITabBarController,
                     let navController = tabBarController.selectedViewController as? UINavigationController {
                         navController.pushViewController(vc, animated: true)
                 }
            }
            
        } else if (deepLinkTapped == "petsbestapp://documentcenter"){
            DispatchQueue.main.async { // Change `2.0` to the desired number of seconds.
                let vc = MyPetsViewController()
                 if let tabBarController = self.window?.rootViewController as? UITabBarController,
                     let navController = tabBarController.selectedViewController as? UINavigationController {
                         navController.pushViewController(vc, animated: true)
                 }
            }
        } else if (deepLinkTapped == "petsbestapp://documentsandforms"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            if let url = try? APIRouter.get_documentCenter.asURLRequestCustomerPortal() {
                DispatchQueue.main.async {
                    print(self.window?.rootViewController)
                    let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                     if let tabBarController = self.window?.rootViewController as? UITabBarController,
                         let navController = tabBarController.selectedViewController as? UINavigationController {
                             vc.openedFromScreen = "MyPets"
                             navController.pushViewController(vc, animated: true)
                     }
                    
//                    let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is TabBarViewController } ).first?.rootViewController as? TabBarViewController
//                    tabBarVC?.selectedIndex = 3 //you can select another tab if needed
//                    let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
//                    vc.openedFromScreen = "MyPets"
//                    if let navController = tabBarVC?.viewControllers?[3] as? NavigationController {
//                       navController.pushViewController(vc, animated: true)
//                    }
                }
            }
        } else if (deepLinkTapped == "petsbestapp://viewmedicalrecords"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            if let url = try? APIRouter.get_medical_records.asURLRequestCustomerPortal() {
                DispatchQueue.main.async {
                    let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                     if let tabBarController = self.window?.rootViewController as? UITabBarController,
                         let navController = tabBarController.selectedViewController as? UINavigationController {
                             vc.openedFromScreen = "MyPets"
                             navController.pushViewController(vc, animated: true)
                     }
                }
            }
        } else if (deepLinkTapped == "petsbestapp://addmedicalrecords"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            if let url = try? APIRouter.add_medical_records.asURLRequestCustomerPortal() {
                DispatchQueue.main.async {
                    let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                     if let tabBarController = self.window?.rootViewController as? UITabBarController,
                         let navController = tabBarController.selectedViewController as? UINavigationController {
                             vc.openedFromScreen = "MyPets"
                             navController.pushViewController(vc, animated: true)
                     }
                }
            }
        } else if (deepLinkTapped == "petsbestapp://claimcenter"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            DispatchQueue.main.async { // Change `2.0` to the desired number of seconds.
                let vc = ClaimCenterViewController()
                 if let tabBarController = self.window?.rootViewController as? UITabBarController,
                     let navController = tabBarController.selectedViewController as? UINavigationController {
                         navController.pushViewController(vc, animated: true)
                 }
            }
        } else if (deepLinkTapped == "petsbestapp://viewclaims"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            if let url = try? APIRouter.get_all_claims.asURLRequestCustomerPortal() {
                DispatchQueue.main.async {
                    let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                     if let tabBarController = self.window?.rootViewController as? UITabBarController,
                         let navController = tabBarController.selectedViewController as? UINavigationController {
                             vc.openedFromScreen = "ClaimCenter"
                             navController.pushViewController(vc, animated: true)
                     }
                }
            }
            
        } else if (deepLinkTapped == "petsbestapp://updateclaim"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            if let url = try? APIRouter.get_update_claim.asURLRequestCustomerPortal() {
                DispatchQueue.main.async {
                    let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                     if let tabBarController = self.window?.rootViewController as? UITabBarController,
                         let navController = tabBarController.selectedViewController as? UINavigationController {
                             vc.openedFromScreen = "ClaimCenter"
                             navController.pushViewController(vc, animated: true)
                     }
                }
            }
        } else if (deepLinkTapped == "petsbestapp://accountsettings"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            
            let vc = AccountSettingsViewController()
            DispatchQueue.main.async {
                if let tabBarController = self.window?.rootViewController as? UITabBarController,
                    let navController = tabBarController.selectedViewController as? UINavigationController {
                        navController.pushViewController(vc, animated: true)
                }
            }
        } else if (deepLinkTapped == "petsbestapp://personaldetails"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            if let url = try? APIRouter.personal_info.asURLRequestCustomerPortal() {
                DispatchQueue.main.async {
                    let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                     if let tabBarController = self.window?.rootViewController as? UITabBarController,
                         let navController = tabBarController.selectedViewController as? UINavigationController {
                             vc.openedFromScreen = "AccountSettings"
                             navController.pushViewController(vc, animated: true)
                     }
                }
            }
        } else if (deepLinkTapped == "petsbestapp://paymentinformation"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            if let url = try? APIRouter.payment_information.asURLRequestCustomerPortal() {
                DispatchQueue.main.async {
                    let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                     if let tabBarController = self.window?.rootViewController as? UITabBarController,
                         let navController = tabBarController.selectedViewController as? UINavigationController {
                             vc.openedFromScreen = "AccountSettings"
                             navController.pushViewController(vc, animated: true)
                     }
                }
            }
        } else if (deepLinkTapped == "petsbestapp://appsettings"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            let vc = AppSettingsViewController()
            DispatchQueue.main.async {
                if let tabBarController = self.window?.rootViewController as? UITabBarController,
                    let navController = tabBarController.selectedViewController as? UINavigationController {
                        navController.pushViewController(vc, animated: true)
                }
            }
        } else if (deepLinkTapped == "petsbestapp://supportfaq"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            if UIDevice().userInterfaceIdiom == .pad {
                let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is TabBarViewController } ).first?.rootViewController as? TabBarViewController
                tabBarVC?.selectedIndex = 2 //you can select another tab if needed
                let vc = PetSupportPadViewController()
                if let navController = tabBarVC?.viewControllers?[2] as? NavigationController {
                   navController.pushViewController(vc, animated: true)
                }
            } else {
                let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is TabBarViewController } ).first?.rootViewController as? TabBarViewController
                tabBarVC?.selectedIndex = 2 //you can select another tab if needed
                let vc = PetSupportViewController()
                if let navController = tabBarVC?.viewControllers?[2] as? NavigationController {
                   navController.pushViewController(vc, animated: true)
                }
            }
            
        } else if (deepLinkTapped == "petsbestapp://underwriting"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            
            if let url = try? APIRouter.get_underwriting.asURLRequestCustomerPortal() {
                let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                 if let tabBarController = self.window?.rootViewController as? UITabBarController,
                     let navController = tabBarController.selectedViewController as? UINavigationController {
                         vc.openedFromScreen = "Underwriting"
                         navController.pushViewController(vc, animated: true)
                 }
            }
        }
        
        
        self.window?.makeKeyAndVisible()
    }
    
    func application(_ application: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any] = [:] ) -> Bool {

        print("url \(url)")
        print("url host :\(url.host!)")
        print("url path :\(url.path)")
        
        let urlHost : String = url.host as String? ?? ""

        if(urlHost == "submitclaim"){
            self.keychain.set("true", forKey: "dynamicLinkSubmitClaim")
            let tabBarViewController = TabBarViewController.storyboardInstance()
            self.window?.rootViewController = tabBarViewController
        } else if (urlHost == "moremenu"){
            self.keychain.set("true", forKey: "dynamicLinkMore")
            let tabBarViewController = TabBarViewController.storyboardInstance()
            self.window?.rootViewController = tabBarViewController
        } else if (urlHost == "support"){
            self.keychain.set("true", forKey: "dynamicLinkSupport")
            let tabBarViewController = TabBarViewController.storyboardInstance()
            self.window?.rootViewController = tabBarViewController
        } else if (urlHost == "home"){
            self.keychain.set("true", forKey: "dynamicLinkMyPets")
            let tabBarViewController = TabBarViewController.storyboardInstance()
            self.window?.rootViewController = tabBarViewController
        } else if (urlHost == "addpet"){
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 0 //you can select another tab if needed
            let vc = AddPetViewController()
            if let navController = tabBarVC?.viewControllers?[0] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        } else if (urlHost == "inbox"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 3 //you can select another tab if needed
            let vc = InboxTableViewController()
            if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        } else if (urlHost == "documentcenter"){
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 3 //you can select another tab if needed
            let vc = MyPetsViewController()
            if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        } else if (urlHost == "documentsandforms"){
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 3 //you can select another tab if needed
            let vc = MyPetsViewController()
            if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
                if let url = try? APIRouter.get_documentCenter.asURLRequestCustomerPortal() {
                   let vc1 = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                    navController.pushViewController(vc1, animated: true)
                }
            }
        } else if (urlHost == "viewmedicalrecords"){
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 3 //you can select another tab if needed
            let vc = MyPetsViewController()
            if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
                if let url = try? APIRouter.get_medical_records.asURLRequestCustomerPortal() {
                    let vc1 = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                    navController.pushViewController(vc1, animated: true)
                }
            }
        } else if (urlHost == "addmedicalrecords"){
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 3 //you can select another tab if needed
            let vc = MyPetsViewController()
            if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
                if let url = try? APIRouter.add_medical_records.asURLRequestCustomerPortal() {
                    let vc1 = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                    navController.pushViewController(vc1, animated: true)
                }
            }
        } else if (urlHost == "claimcenter"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 3 //you can select another tab if needed
            let vc = ClaimCenterViewController()
            if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        } else if (urlHost == "viewclaims"){
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 3 //you can select another tab if needed
            let vc = ClaimCenterViewController()
            if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
                if let url = try? APIRouter.get_all_claims.asURLRequestCustomerPortal() {
                    let vc1 = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                    navController.pushViewController(vc1, animated: true)
                }
            }
        } else if (urlHost == "updateclaim"){
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 3 //you can select another tab if needed
            let vc = ClaimCenterViewController()
            if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
                if let url = try? APIRouter.get_update_claim.asURLRequestCustomerPortal() {
                    let vc1 = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.documentCenter)
                    navController.pushViewController(vc1, animated: true)
                }
            }
        } else if (urlHost == "accountsettings"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 3 //you can select another tab if needed
            let vc = AccountSettingsViewController()
            if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        } else if (urlHost == "personaldetails"){
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 3 //you can select another tab if needed
            let vc = AccountSettingsViewController()
            if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
                if let url = try? APIRouter.personal_info.asURLRequestCustomerPortal() {
                    let vc1 = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.personlaInfo)
                    navController.pushViewController(vc1, animated: true)
                }
            }
        } else if (urlHost == "paymentinformation"){
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 3 //you can select another tab if needed
            let vc = AccountSettingsViewController()
            if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
                if let url = try? APIRouter.payment_information.asURLRequestCustomerPortal() {
                    let vc1 = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.paymentInformation)
                    navController.pushViewController(vc1, animated: true)
                }
            }
        } else if (urlHost == "appsettings"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
            tabBarVC?.selectedIndex = 3 //you can select another tab if needed
            let vc = AppSettingsViewController()
            if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
               navController.pushViewController(vc, animated: true)
            }
        } else if (urlHost == "supportfaq"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            if UIDevice().userInterfaceIdiom == .pad {
                let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
                tabBarVC?.selectedIndex = 3 //you can select another tab if needed
                let vc = PetSupportPadViewController()
                if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
                   navController.pushViewController(vc, animated: true)
                }
            } else {
                let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
                tabBarVC?.selectedIndex = 3 //you can select another tab if needed
                let vc = PetSupportViewController()
                if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
                   navController.pushViewController(vc, animated: true)
                }
            }
            
        } else if (urlHost == "underwriting"){
            self.keychain.set("true", forKey: "isOpenedFromMore")
            if let url = try? APIRouter.get_underwriting.asURLRequestCustomerPortal() {
                let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
                tabBarVC?.selectedIndex = 3 //you can select another tab if needed
                let vc = AccountWebViewViewController(withUrl: url, withTitle: Constants.titlesScreens.underwriting)
                if let navController = tabBarVC?.viewControllers?[3] as? UINavigationController {
                   navController.pushViewController(vc, animated: true)
                }
            }
        }
        
        
        self.window?.makeKeyAndVisible()
        
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
            return self.orientationLock
    }
    
    
    func isExampleCategory(userInfo: NSDictionary) -> Bool {
        if let aps = userInfo["aps"] as? NSDictionary, let category = aps["category"] as? String, category == "example" {
            return true
        }
        return false
    }
    
    @objc func inboxUpdate() {
        DispatchQueue.main.async {
            UIApplication.shared.applicationIconBadgeNumber = Int(MCEInboxDatabase.shared.unreadMessageCount())
        }
    }
    
    @objc func failure() {
    }
    
    @objc func acquireIdentification() {
//        print(MCERegistrationDetails.shared.apsRegistered)
//        print(MCERegistrationDetails.shared.mceRegistered)
//        print(MCERegistrationDetails.shared.userId)
//        print(MCERegistrationDetails.shared.channelId)
//        print(MCERegistrationDetails.shared.appKey)
        
        
        guard let userID = MCERegistrationDetails.shared.userId,
              let channelID = MCERegistrationDetails.shared.channelId else {
                  return
              }
        
        userId = userID
        channelId = channelID
        self.keychain.set(userID, forKey: "mobileUserId")
        self.keychain.set(channelID, forKey: "channelId")
        DispatchQueue.main.async {
            let rootVC = self.window?.rootViewController as? LoginViewController
            rootVC?.userID = userID
            rootVC?.channelID = channelID
        }
        
//        true
//        true
//        Optional("WikrJJsG1oAlwGFg")
//        Optional("mug3k41R")
//        Optional("apwptJIeDV")
        
//        true
//        true
//        Optional("Ltssk1ANn7A022Ym")
//        Optional("8HSNJ6k0")
//        Optional("apwptJIeDV")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if wentToBackground == true {
            timeHasExceeded()
            wentToBackground = false
        }
        UserDefaults.standard.set(nil, forKey: "chosenOption")
    }
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        self.idleTimer = Timer.scheduledTimer(timeInterval: 20 * 60, target: self, selector: #selector(self.timeHasExceeded), userInfo: nil, repeats: true)
        wentToBackground = true
    }
    
    // if the timer reaches the limit as defined in timeoutInSeconds, post this notification
    @objc private func timeHasExceeded() {
        if idleTimer?.fireDate != nil {
            let fireDate = (idleTimer?.fireDate)!
            let nowDate = Date()
            let remainingTimeInterval = nowDate.timeIntervalSince(fireDate)
            //print(remainingTimeInterval)
            if remainingTimeInterval >= 0 {
                idleTimer?.invalidate()
                idleTimer = nil
                DispatchQueue.main.async {
                    let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
                    tabBarVC?.selectedIndex = 0 //you can select another tab if needed
                    let vc = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: true)
                    if let navController = tabBarVC?.viewControllers?[0] as? UINavigationController {
                       navController.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        UserDefaults.standard.set("true", forKey: "isAppKilled")
        keychain.delete("isSubmitted")

        self.coreDataStack.saveToMainContext()
    }
    
    private func startWithLoginFlow() {
        
        let storedToken = UserDefaults.standard.string(forKey: "appToken")
        let keychain = KeychainSwift()
        var appToken: String?
        if (storedToken != nil) {
            appToken = keychain.get("appToken")
        }
        

        if appToken != nil {
            if UserDefaults.standard.string(forKey: "isAppLogout") != nil {
                if UserDefaults.standard.string(forKey: "isAppKilled") != nil && UserDefaults.standard.string(forKey: "isAppLogout") != nil {
                    if ReachabilityClass.isConnectedToNetwork() {
                        DispatchQueue.main.async {
                            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
                            tabBarVC?.selectedIndex = 0 //you can select another tab if needed
                            let vc = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: true)
                            if let navController = tabBarVC?.viewControllers?[0] as? UINavigationController {
                               navController.pushViewController(vc, animated: true)
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: Constants.errorMessages.noInternetError, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: Constants.errorMessages.ok, style: .cancel) { _ in
                                DispatchQueue.main.async {
                                    let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
                                    tabBarVC?.selectedIndex = 0 //you can select another tab if needed
                                    let vc = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: true)
                                    if let navController = tabBarVC?.viewControllers?[0] as? UINavigationController {
                                       navController.pushViewController(vc, animated: true)
                                    }
                                }
                             })
                            self.window?.makeKeyAndVisible()
                            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        }
                    }
                } else {
                    if ReachabilityClass.isConnectedToNetwork() {
                        DispatchQueue.main.async {
                            let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
                            tabBarVC?.selectedIndex = 0 //you can select another tab if needed
                            let vc = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: true)
                            if let navController = tabBarVC?.viewControllers?[0] as? UINavigationController {
                               navController.pushViewController(vc, animated: true)
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: Constants.errorMessages.noInternetError, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: Constants.errorMessages.ok, style: .cancel) { _ in
                                DispatchQueue.main.async {
                                    let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
                                    tabBarVC?.selectedIndex = 0 //you can select another tab if needed
                                    let vc = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: true)
                                    if let navController = tabBarVC?.viewControllers?[0] as? UINavigationController {
                                       navController.pushViewController(vc, animated: true)
                                    }
                                }
                             })
                            self.window?.makeKeyAndVisible()
                            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
            if UserDefaults.standard.string(forKey: "isAppKilled") == nil && UserDefaults.standard.string(forKey: "isAppLogout") == nil {
                if ReachabilityClass.isConnectedToNetwork() {
                    DispatchQueue.main.async {
                        let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
                        tabBarVC?.selectedIndex = 0 //you can select another tab if needed
                        let vc = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: true)
                        if let navController = tabBarVC?.viewControllers?[0] as? UINavigationController {
                           navController.pushViewController(vc, animated: true)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "", message: Constants.errorMessages.noInternetError, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: Constants.errorMessages.ok, style: .cancel) { _ in
                            DispatchQueue.main.async {
                                let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
                                tabBarVC?.selectedIndex = 0 //you can select another tab if needed
                                let vc = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: true)
                                if let navController = tabBarVC?.viewControllers?[0] as? UINavigationController {
                                   navController.pushViewController(vc, animated: true)
                                }
                            }
                         })
                        self.window?.makeKeyAndVisible()
                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
            }
            
        } else {
            keychain.delete("appToken")
            keychain.delete("isFirstLaunch")
            keychain.delete("FaceID")
            keychain.delete("foregroundFaceID")
            keychain.delete("PushNotifications")
            keychain.delete("firstPushNotifications")
            keychain.clear()
            
            DispatchQueue.main.async {
                keychain.set("enabled", forKey: "isFirstLaunch")
                let tabBarVC = UIApplication.shared.windows.filter( {$0.rootViewController is UITabBarController } ).first?.rootViewController as? UITabBarController
                tabBarVC?.selectedIndex = 0 //you can select another tab if needed
                let vc = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: true)
                if let navController = tabBarVC?.viewControllers?[0] as? UINavigationController {
                   navController.pushViewController(vc, animated: true)
                }
            }
            
//            DispatchQueue.main.async {
//                keychain.set("enabled", forKey: "isFirstLaunch")
//                self.window = UIWindow(frame: UIScreen.main.bounds)
//                let loginViewController = LoginViewController(withCoreDataStack: CoreDataStack(), withAutoLogout: false)
//                self.window?.rootViewController = loginViewController
//                self.window?.makeKeyAndVisible()
//            }
        }
    }
}

// Firebase addition
extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")

        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        // If necessary, send a token to the application server.
    }
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }

    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      Messaging.messaging().apnsToken = deviceToken
    }
}

class NotificationDelegate: NSObject, UNUserNotificationCenterDelegate {
    static let shared = NotificationDelegate()

    // This method processes the notification tap
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        guard response.notification.request.content.userInfo["notification-action"] == nil else {
            MCENotificationDelegate.shared.userNotificationCenter(center, didReceive: response, withCompletionHandler: completionHandler)
            return
        }
        // handle other types of notifications here
    }

    // This method is used to determine if the notification should be shown to the user when the app is running
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        guard notification.request.content.userInfo["notification-action"] == nil else {
            MCENotificationDelegate.shared.userNotificationCenter(center, willPresent: notification, withCompletionHandler: completionHandler)
            return
        }
        // handle other types of notifications here, typically via
        completionHandler([.alert, .sound, .badge])
    }

    // This method is used to open the settings screen in the app for push notification preferences, only implement if you provide one
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        // show settings screen if available
    }
}

