//
//  CustomWebView.swift
//  PetsBest
//
//  Created by Plamena on 23.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import WebKit
import KeychainSwift

class CustomWebView: WKWebView {
    
    private let keychain = KeychainSwift()
    
    override func load(_ request: URLRequest) -> WKNavigation? {
        guard let mutableRequest: NSMutableURLRequest = request as? NSMutableURLRequest else {
            return super.load(request)
        }
        
        let appToken = keychain.get("appToken")
        
        let token = appToken?.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil)
        //print(token)
        mutableRequest.setValue("AuthToken", forHTTPHeaderField: token!)
        return super.load(mutableRequest as URLRequest)
    }
}
