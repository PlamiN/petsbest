//
//  Spinner.swift
//  PetsBest
//
//  Created by Plamena on 13.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

open class Spinner {
    
    internal static var spinner: UIActivityIndicatorView?
    static var style: UIActivityIndicatorView.Style = UIActivityIndicatorView.Style.large
    static var baseBackColor = UIColor.black.withAlphaComponent(0.5)
    static var baseColor = UIColor.init(hex: Constants.Colors.main)
    
    static func start(style: UIActivityIndicatorView.Style = style, backColor: UIColor = baseBackColor, baseColor: UIColor = baseColor!) {
        NotificationCenter.default.addObserver(self, selector: #selector(update), name: UIDevice.orientationDidChangeNotification, object: nil)
        if spinner == nil, let window = UIApplication.shared.keyWindow {
            let frame = UIScreen.main.bounds
            spinner = UIActivityIndicatorView(frame: frame)
            spinner?.backgroundColor = backColor
            spinner?.style = style
            spinner?.color = baseColor
            if spinner != nil {
                window.addSubview(spinner!)
                DispatchQueue.main.async {
                    spinner?.startAnimating()
                }
            }
        }
    }
    
    static func stop() {
        if spinner != nil {
            DispatchQueue.main.async {
                spinner?.stopAnimating()
                spinner?.removeFromSuperview()
                spinner = nil
            }
        }
    }
    
    @objc static func update() {
        if spinner != nil {
            stop()
            start()
        }
    }
}
