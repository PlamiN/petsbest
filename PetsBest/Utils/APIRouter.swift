//
//  APIRouter.swift
//  PetsBest
//
//  Created by Plamena on 9.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

enum APIRouter {
    
    case loginUser
    case allClaims
    case policy
    case get_contact_options
    case get_policy_holder
    case contact
    case get_claim_details
    case get_claim_conditions
    case get_documents
    case get_update_claim_details
    case update_claim
    case start_claim
    case get_fraud_warning
    case forgot_password
    case register
    case check_eligibility
    case all_statuses
    case test
    case addPet
    case personal_info
    case payment_information
    case get_documentCenter
    case get_medical_records
    case add_medical_records
    case get_underwriting
    case get_all_claims
    case get_update_claim
    case saved_vets
    case search_vets
    case FAQ
    
    private var baseURL: String {
        //https://www.petsbest.com/mobileApi
        //https://stagingnew.petsbest.com/mobileApi
        //https://qa.petsbest.com/mobileApi
        return "https://www.petsbest.com/mobileApi"
    }
    
    private var baseCustomerPortal: String {
        //https://stagingnew.petsbest.com
        //https://qa.petsbest.com
        //https://www.petsbest.com
        return "https://www.petsbest.com"
    }
    
    private var path: String {
        switch self {
        case .loginUser:
            return "/Authorize"
        case .allClaims:
            return "/Claims"
        case .policy:
            return "/Policy"
        case .get_contact_options:
            return "/Contact/BusinessHours"
        case .get_policy_holder:
            return "/PolicyHolder"
        case .contact:
            return "/Contact"
        case .get_claim_details:
            return "/Claims/"
        case .all_statuses:
            return "/Claims/GetAllStatuses"
        case .get_documents:
            return "/Documents"
        case .get_update_claim_details:
            return "/Claims/%1$s"
        case .get_claim_conditions:
            return "/Claims/ClaimConditionDetailsDict"
        case .update_claim:
            return "/Claims/UpdateClaim"
        case .start_claim:
            return "/Claims"
        case .get_fraud_warning:
            return "/Claims/FraudWarning"
        case .forgot_password:
            return "/customerportal/account/ResetPassword"
        case .register:
            return "/customerportal/account/RegisterAccount"
        case .check_eligibility:
            return "/Claims/CheckEligibility"
        case .test:
            return "/Test"
        case .personal_info:
            return "/customerportal/Account/PersonalInfo"
        case .payment_information:
            return "/customerportal/Account/PaymentInformation"
        case .get_documentCenter:
            return "/customerportal/Account/DownloadDocuments"
        case .get_all_claims:
            return "/customerportal/Claim/ViewClaims"
        case .get_update_claim:
            return "/customerportal/Claim/UpdateClaim"
        case .get_medical_records:
            return "/customerportal/MedicalRecords"
        case .add_medical_records:
            return "/customerportal/MedicalRecords/Submit"
        case .addPet:
            return "/customerportal/addpet"
        case .get_underwriting:
            return "/customer-portal/underwriters-licensing"
        case .saved_vets:
            return "/Vet/SavedVets"
        case .search_vets:
            return "/Vet/SavedVets"
        case .FAQ:
            return "/faq"
        @unknown default:
            return ""
        }
    }
    
    private var method: HTTPMethod {
        switch self {
        case .loginUser:
            return .post
        case .allClaims:
            return .get
        case .policy:
            return .get
        case .get_contact_options:
            return .get
        case .get_policy_holder:
            return .get
        case .contact:
            return .post
        case .get_claim_details:
            return .get
        case .get_claim_conditions:
            return .get
        case .all_statuses:
            return .get
        case .get_documents:
            return .get
        case .get_update_claim_details:
            return .get
        case .update_claim:
            return .post
        case .start_claim:
            return .post
        case .get_fraud_warning:
            return .get
        case .forgot_password:
            return .post
        case .check_eligibility:
            return .post
        case .test:
            return .get
        case .personal_info:
            return .get
        case .payment_information:
            return .get
        case .get_documentCenter:
            return .get
        case .addPet:
            return .get
        case .get_underwriting:
            return .get
        case .saved_vets:
            return .get
        @unknown default:
            print("All other cases")
            return .get
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        guard let wholeURL = URL(string: baseURL + path) else { throw AppError.wrongURL }
        guard let encodedURL = URL(string: wholeURL.absoluteString.removingPercentEncoding!) else { throw AppError.wrongURL }
        
        var request = URLRequest(url: encodedURL)
        request.httpMethod = method.rawValue
        
        return request
    }
    
    func asURLRequestCustomerPortal() throws -> NSMutableURLRequest {
        guard let wholeURL = URL(string: baseCustomerPortal + path) else { throw AppError.wrongURL }
        guard let encodedURL = URL(string: wholeURL.absoluteString.removingPercentEncoding!) else { throw AppError.wrongURL }
        
        let request = NSMutableURLRequest(url: encodedURL)
        
        return request
    }
}

enum AppError: Error {
    case wrongURL
}

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

enum Result<Value> {
    case success(Value)
    case failure(Error)
}
