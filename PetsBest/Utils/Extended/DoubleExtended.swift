//
//  DoubleExtended.swift
//  PetsBest
//
//  Created by Plamena on 16.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import Foundation

extension Double {
    func decimalCount() -> Int {
        if self == Double(Int(self)) {
            return 0
        }

        let integerString = String(Int(self))
        let doubleString = String(Double(self))
        let decimalCount = doubleString.count - integerString.count - 1

        return decimalCount
    }
}
