//
//  UISegmentedControlExtended.swift
//  PetsBest
//
//  Created by Plamena on 23.09.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

extension UISegmentedControl{
    
    private struct Holder {
        static var _underlineFinalXPosition: CGFloat?
    }
    
    var underlineFinalXPositionProperty :CGFloat {
        get {
            return Holder._underlineFinalXPosition ?? 0.0
        }
        set(newValue) {
            Holder._underlineFinalXPosition = newValue
        }
    }
    
    func removeBorder(){
        let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor, andSize: self.frame.size)
        self.setBackgroundImage(backgroundImage, for: .normal, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .selected, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)

        let deviderImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor, andSize: CGSize(width: 1.0, height: self.bounds.size.height))
        self.setDividerImage(deviderImage, forLeftSegmentState: .selected, rightSegmentState: .normal, barMetrics: .default)
        self.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.gray], for: .normal)
        self.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(hex: Constants.Colors.main)!], for: .selected)
    }

    func addUnderlineForSelectedSegment(){
        removeBorder()
        if UIDevice().userInterfaceIdiom == .pad {
            let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(Double(self.numberOfSegments)*1.2)
            let underlineHeight: CGFloat = 4.0
            let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.9))
            //print(underlineXPosition)
            let underLineYPosition = self.bounds.size.height - 2.0
            let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
            let underline = UIView(frame: underlineFrame)
            underline.backgroundColor = UIColor.init(hex: Constants.Colors.main)
            underline.tag = 1
            self.addSubview(underline)
            
            let underlineWidthSecond: CGFloat = self.bounds.size.width / CGFloat(Double(self.numberOfSegments)*0.96)
            let underlineHeightSecond: CGFloat = 4.0
            let underlineXPositionSecond = CGFloat(1 * Int(underlineWidth * 1.9))
            //print(underlineXPosition)
            let underLineYPositionSecond = self.bounds.size.height - 2.0
            let underlineFrameSecond = CGRect(x: underlineXPositionSecond, y: underLineYPositionSecond, width: underlineWidthSecond, height: underlineHeightSecond)
            let underlineSecond = UIView(frame: underlineFrameSecond)
            underlineSecond.backgroundColor = UIColor.init(hex: Constants.Colors.main)
            underlineSecond.tag = 2
            self.addSubview(underlineSecond)
            underlineSecond.isHidden = true
            
            let underlineWidthThird: CGFloat = self.bounds.size.width / CGFloat(Double(self.numberOfSegments)*0.7)
            let underlineHeightThird: CGFloat = 4.0
            let underlineXPositionThird = CGFloat(2 * Int(underlineWidthThird * 2.4))
            //print(underlineXPosition)
            let underLineYPositionThird = self.bounds.size.height - 2.0
            let underlineFrameThird = CGRect(x: underlineXPositionThird, y: underLineYPositionThird, width: underlineWidthThird, height: underlineHeightThird)
            let underlineThird = UIView(frame: underlineFrameThird)
            underlineThird.backgroundColor = UIColor.init(hex: Constants.Colors.main)
            underlineThird.tag = 3
            self.addSubview(underlineThird)
            underlineThird.isHidden = true
        } else {
            let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments*2)
            let underlineHeight: CGFloat = 4.0
            let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.9))
            //print(underlineXPosition)
            let underLineYPosition = self.bounds.size.height - 2.0
            let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
            let underline = UIView(frame: underlineFrame)
            underline.backgroundColor = UIColor.init(hex: Constants.Colors.main)
            underline.tag = 1
            self.addSubview(underline)
            
            let underlineWidthSecond: CGFloat = self.bounds.size.width / CGFloat(Double(self.numberOfSegments)*1.73)
            let underlineHeightSecond: CGFloat = 4.0
            let underlineXPositionSecond = CGFloat(1 * Int(underlineWidth * 1.9))
            //print(underlineXPosition)
            let underLineYPositionSecond = self.bounds.size.height - 2.0
            let underlineFrameSecond = CGRect(x: underlineXPositionSecond, y: underLineYPositionSecond, width: underlineWidthSecond, height: underlineHeightSecond)
            let underlineSecond = UIView(frame: underlineFrameSecond)
            underlineSecond.backgroundColor = UIColor.init(hex: Constants.Colors.main)
            underlineSecond.tag = 2
            self.addSubview(underlineSecond)
            underlineSecond.isHidden = true
            
            let underlineWidthThird: CGFloat = self.bounds.size.width / CGFloat(Double(self.numberOfSegments)*1.19)
            let underlineHeightThird: CGFloat = 4.0
            let underlineXPositionThird = CGFloat(2 * Int(underlineWidthThird * 1.4))
            print(underlineXPosition)
            let underLineYPositionThird = self.bounds.size.height - 2.0
            let underlineFrameThird = CGRect(x: underlineXPositionThird, y: underLineYPositionThird, width: underlineWidthThird, height: underlineHeightThird)
            let underlineThird = UIView(frame: underlineFrameThird)
            underlineThird.backgroundColor = UIColor.init(hex: Constants.Colors.main)
            underlineThird.tag = 3
            self.addSubview(underlineThird)
            underlineThird.isHidden = true
        }
    }

    func changeUnderlinePosition(){
        guard let underline = self.viewWithTag(1) else {return}
        guard let underlineSecond = self.viewWithTag(2) else {return}
        guard let underlineThird = self.viewWithTag(3) else {return}
        underline.isHidden = true
        underlineSecond.isHidden = true
        underlineThird.isHidden = true
        var underlineFinalXPosition = ((self.bounds.width) / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        let isSwipe = UserDefaults.standard.string(forKey: "isSwipe")
        if isSwipe == "true" {
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1334:
                    print("iPhone 6/6S/7/8, SE 2nd Generation")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.25))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        (underlineFinalXPosition) = 35.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineSecond.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 1.19
                    }
                    if selectedSegmentIndex == 2 {
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 1.02
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 1792:
                    print("iPhone XR/11")

                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        (underlineFinalXPosition) = 40.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineSecond.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 1.16
                    }
                    if selectedSegmentIndex == 2 {
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 0.99
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 1920, 2208:
                    print("iPhone 6+/6S+/7+/8+")

                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        (underlineFinalXPosition) = 40.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineSecond.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 1.16
                    }
                    if selectedSegmentIndex == 2 {
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 0.99
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2436:
                    print("iPhone X/XS/11 Pro,iPhone 12 Mini")

                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.40))
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        (underlineFinalXPosition) = 33.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineSecond.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 1.06
                    }
                    if selectedSegmentIndex == 2 {
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 0.91
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2532:
                    print("iPhone 12, iPhone 12 Pro")

                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.35))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        (underlineFinalXPosition) = 37.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineSecond.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 1.15
                    }
                    if selectedSegmentIndex == 2 {
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 0.99
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2688:
                    print("iPhone XS Max/11 Pro Max")

                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        (underlineFinalXPosition) = 40.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineSecond.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 1.16
                    }
                    if selectedSegmentIndex == 2 {
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 0.99
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2778:
                    print("iPhone 12 Pro Max")

                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        (underlineFinalXPosition) = 42.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineSecond.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 1.20
                    }
                    if selectedSegmentIndex == 2 {
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 1.03
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                default: break
                }
            }
            if UIDevice().userInterfaceIdiom == .pad {
                switch UIScreen.main.nativeBounds.height {
                case 1024:
                    print("iPad 2, iPad 1st gen, iPad mini")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = true
                        (underlineFinalXPosition) = 42.0
                    }
                    if selectedSegmentIndex == 1 {
                        underline.isHidden = true
                        underlineSecond.isHidden = false
                        underlineThird.isHidden = true
                        underlineFinalXPosition = underlineFinalXPosition * 1.20
                    }
                    if selectedSegmentIndex == 2 {
                        underline.isHidden = true
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 1.03
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2048:
                    print("iPad 3rd gen, iPad 4th gen, iPad Air,iPad Air 2, iPad mini 3, iPad mini 2, iPad mini 4,iPad Pro (1st gen 9.7”), iPad 5th gen, iPad 6th gen, iPad Mini (5th gen)")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = true
                        (underlineFinalXPosition) = 79.0
                    }
                    if selectedSegmentIndex == 1 {
                        underline.isHidden = true
                        underlineSecond.isHidden = false
                        underlineThird.isHidden = true
                        underlineFinalXPosition = underlineFinalXPosition * 2.2
                    }
                    if selectedSegmentIndex == 2 {
                        underline.isHidden = true
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 1.90
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2160:
                    print("iPad 7th gen")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = true
                        (underlineFinalXPosition) = 88.0
                    }
                    if selectedSegmentIndex == 1 {
                        underline.isHidden = true
                        underlineSecond.isHidden = false
                        underlineThird.isHidden = true
                        underlineFinalXPosition = underlineFinalXPosition * 2.38
                    }
                    if selectedSegmentIndex == 2 {
                        underline.isHidden = true
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 2.03
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2224:
                    print("iPad Pro (2nd gen 10.5), iPad Air (3rd gen)")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = true
                        (underlineFinalXPosition) = 88.0
                    }
                    if selectedSegmentIndex == 1 {
                        underline.isHidden = true
                        underlineSecond.isHidden = false
                        underlineThird.isHidden = true
                        underlineFinalXPosition = underlineFinalXPosition * 2.40
                    }
                    if selectedSegmentIndex == 2 {
                        underline.isHidden = true
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 2.05
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2360:
                    print("iPad Air (4th generation)")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = true
                        (underlineFinalXPosition) = 88.0
                    }
                    if selectedSegmentIndex == 1 {
                        underline.isHidden = true
                        underlineSecond.isHidden = false
                        underlineThird.isHidden = true
                        underlineFinalXPosition = underlineFinalXPosition * 2.40
                    }
                    if selectedSegmentIndex == 2 {
                        underline.isHidden = true
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 2.05
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2388:
                    print("iPad Pro (3rd gen 11),iPad Pro (4th gen 11)")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = true
                        (underlineFinalXPosition) = 90.0
                    }
                    if selectedSegmentIndex == 1 {
                        underline.isHidden = true
                        underlineSecond.isHidden = false
                        underlineThird.isHidden = true
                        underlineFinalXPosition = underlineFinalXPosition * 2.45
                    }
                    if selectedSegmentIndex == 2 {
                        underline.isHidden = true
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 2.1
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2732:
                    print("iPad Pro (1st gen 12.9),iPad Pro (2nd gen 12.9),iPad Pro (3rd gen 12.9),iPad Pro (4th gen 12.9)")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        underline.isHidden = false
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = true
                        (underlineFinalXPosition) = 125.0
                    }
                    if selectedSegmentIndex == 1 {
                        underline.isHidden = true
                        underlineSecond.isHidden = false
                        underlineThird.isHidden = true
                        underlineFinalXPosition = underlineFinalXPosition * 3.08
                    }
                    if selectedSegmentIndex == 2 {
                        underline.isHidden = true
                        underlineSecond.isHidden = true
                        underlineThird.isHidden = false
                        underlineFinalXPosition = underlineFinalXPosition * 2.63
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                default:
                    print("Unknown")
                }
            }
            if self.selectedSegmentIndex == 0 {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.0, animations: {
                        underline.frame.origin.x = underlineFinalXPosition
                    })
                }
            }
            if self.selectedSegmentIndex == 1 {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.0, animations: {
                        underlineSecond.frame.origin.x = underlineFinalXPosition
                    })
                }
            }
            if self.selectedSegmentIndex == 2 {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.0, animations: {
                        underlineThird.frame.origin.x = underlineFinalXPosition
                    })
                }
            }
        } else {
            
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1334:
                    print("iPhone 6/6S/7/8, SE 2nd Generation")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.10))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 35.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 1.15
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.98
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 1792:
                    print("iPhone XR/11")
                    
                    underlineFinalXPosition = underlineFinalXPosition * 1.15
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 40.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 1.11
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.95
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                    
                case 1920, 2208:
                    print("iPhone 6+/6S+/7+/8+")

                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.15))
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 40.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 1.11
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.95
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2436:
                    print("iPhone X/XS/11 Pro,iPhone 12 Mini")

                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.3))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 33.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.96
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.83
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2532:
                    print("iPhone 12, iPhone 12 Pro")

                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.12))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 37.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 1.13
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.97
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2688:
                    print("iPhone XS Max/11 Pro Max")

                    underlineFinalXPosition = underlineFinalXPosition * 1.15
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 40.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 1.11
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.95
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2778:
                    print("iPhone 12 Pro Max")

                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.15))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 42.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 1.11
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.96
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                default: break
                }
            }
            if UIDevice().userInterfaceIdiom == .pad {
                switch UIScreen.main.nativeBounds.height {
                case 1024:
                    print("iPad 2, iPad 1st gen, iPad mini")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 42.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 1.20
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 1.03
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2048:
                    print("iPad 3rd gen, iPad 4th gen, iPad Air,iPad Air 2, iPad mini 3, iPad mini 2, iPad mini 4,iPad Pro (1st gen 9.7”), iPad 5th gen, iPad 6th gen, iPad Mini (5th gen)")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 79.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.89
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.77
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2160:
                    print("iPad 7th gen")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 88.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.9
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.77
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2224:
                    print("iPad Pro (2nd gen 10.5), iPad Air (3rd gen)")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 88.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.9
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.77
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2360:
                    print("iPad Air (4th generation)")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 88.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.9
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.77
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2388:
                    print("iPad Pro (3rd gen 11),iPad Pro (4th gen 11)")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 90.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.9
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.77
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                case 2732:
                    print("iPad Pro (1st gen 12.9),iPad Pro (2nd gen 12.9),iPad Pro (3rd gen 12.9),iPad Pro (4th gen 12.9)")
                    let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
                    underlineFinalXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth * 1.45))
                    //print(underlineFinalXPosition)
                    if (underlineFinalXPosition) == 0.0 {
                        (underlineFinalXPosition) = 125.0
                    }
                    if selectedSegmentIndex == 1 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.93
                    }
                    if selectedSegmentIndex == 2 {
                        underlineFinalXPosition = underlineFinalXPosition * 0.79
                    }
                    underlineFinalXPositionProperty = underlineFinalXPosition
                default:
                    print("Unknown")
                }
            }
            if self.selectedSegmentIndex == 0 {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.0, animations: {
                        underline.frame.origin.x = self.underlineFinalXPositionProperty
                        underline.isHidden = false
                    })
                }
            }
            if self.selectedSegmentIndex == 1 {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.0, animations: {
                        underlineSecond.frame.origin.x = self.underlineFinalXPositionProperty
                        underlineSecond.isHidden = false
                    })
                }
            }
            if self.selectedSegmentIndex == 2 {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.0, animations: {
                        underlineThird.frame.origin.x = self.underlineFinalXPositionProperty
                        underlineThird.isHidden = false
                    })
                }
            }
        }
 
    }
    
    func lastUnderlinePosition(){
        guard let underline = self.viewWithTag(1) else {return}
        guard let underlineSecond = self.viewWithTag(2) else {return}
        guard let underlineThird = self.viewWithTag(3) else {return}
        underline.isHidden = true
        underlineSecond.isHidden = true
        underlineThird.isHidden = true
        if selectedSegmentIndex == 0 {
            underline.isHidden = false
            DispatchQueue.main.async {
                underline.frame.origin.x = self.underlineFinalXPositionProperty
            }
        }
        if selectedSegmentIndex == 1 {
            underlineSecond.isHidden = false
            DispatchQueue.main.async {
                underlineSecond.frame.origin.x = self.underlineFinalXPositionProperty
            }
        }
        if selectedSegmentIndex == 2 {
            underlineThird.isHidden = false
            DispatchQueue.main.async {
                underlineThird.frame.origin.x = self.underlineFinalXPositionProperty
            }
        }
    }
}

extension UIImage{

    class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        print(rectangle)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rectangleImage!
    }
}
