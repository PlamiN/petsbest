//
//  UIViewExtended.swift
//  PetsBest
//
//  Created by Plamena on 19.11.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as? UIViewController
            }
        }
        return nil
    }
}
