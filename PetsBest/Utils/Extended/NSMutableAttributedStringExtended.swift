//
//  NSMutableAttributedStringExtended.swift
//  PetsBest
//
//  Created by Plamena on 6.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {

    public func setAsLink(textToFind:String, linkURL:String) -> Bool {

        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}
