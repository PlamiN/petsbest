//
//  UICollectionViewExtended.swift
//  PetsBest
//
//  Created by Plamena on 20.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

extension UICollectionView {
    // In case you have a cell called `SmallClipTableCell` you can use this method as
    // tableView.registerCell(SmallClipTableCell)
    func registerCellNib<T>(_ cellType: T.Type) where T: ReusableView, T: UICollectionViewCell {
        self.register(T.nib, forCellWithReuseIdentifier: T.identifier)
    }
    
    // In case you have a cell called `SmallClipTableCell` you can use this method as
    // tableView.registerCell(SmallClipTableCell)
    func registerCellClass<T>(_ cellType: T.Type) where T: Identifiable, T: UICollectionViewCell {
        self.register(T.self, forCellWithReuseIdentifier: T.identifier)
    }
    
    // In case you have a cell called `SmallClipTableCell` you can use this method as
    // let cell: SmallClipTableCell = tableView.dequeueCell()
    func dequeueCell<T>(at indexPath: IndexPath) -> T where T: UICollectionViewCell, T: ReusableView {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as? T else { fatalError("No \(T.identifier) available") }
        return cell
    }
    
    func scrollToNearestVisibleCollectionViewCell() {
        self.decelerationRate = UIScrollView.DecelerationRate.fast
        let visibleCenterPositionOfScrollView = Float(self.contentOffset.x + (self.bounds.size.width / 2))
        var closestCellIndex = -1
        var closestDistance: Float = .greatestFiniteMagnitude
        for i in 0..<self.visibleCells.count {
            let cell = self.visibleCells[i]
            let cellWidth = cell.bounds.size.width
            let cellCenter = Float(cell.frame.origin.x + cellWidth / 2)

            // Now calculate closest cell
            let distance: Float = fabsf(visibleCenterPositionOfScrollView - cellCenter)
            if distance < closestDistance {
                closestDistance = distance
                closestCellIndex = self.indexPath(for: cell)!.row
            }
        }
        if closestCellIndex != -1 {
            self.scrollToItem(at: IndexPath(row: closestCellIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
}
