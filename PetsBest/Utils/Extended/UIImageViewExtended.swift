//
//  UIImageViewExtended.swift
//  PetsBest
//
//  Created by Plamena on 20.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

extension UIImageView {
    public func maskCircle(anyImage: UIImage) {
        self.contentMode = UIView.ContentMode.scaleAspectFit
        if UIDevice().userInterfaceIdiom == .pad {
            self.layer.cornerRadius = 430 / 2
        } else {
            if UIScreen.main.nativeBounds.height == 1334 {
                self.layer.cornerRadius = 200 / 2
            } else {
                self.layer.cornerRadius = self.frame.height / 2
            }
        }
        self.layer.masksToBounds = false
        self.clipsToBounds = true
        
        self.layer.borderWidth = 5.0
        let opacity:CGFloat = 1.0
        let borderColor = UIColor.init(hex: Constants.Colors.borderImage)
        self.layer.borderColor = borderColor!.withAlphaComponent(opacity).cgColor
        // make square(* must to make circle),
        // resize(reduce the kilobyte) and
        // fix rotation.
        self.image = anyImage
    }
    
    public func maskCircleSubmission(anyImage: UIImage) {
        self.contentMode = UIView.ContentMode.scaleAspectFit
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
        
        self.layer.borderWidth = 2.0
        let opacity:CGFloat = 1.0
        let borderColor = UIColor.init(hex: Constants.Colors.borderImage)
        self.layer.borderColor = borderColor!.withAlphaComponent(opacity).cgColor
        // make square(* must to make circle),
        // resize(reduce the kilobyte) and
        // fix rotation.
        self.image = anyImage
    }
}
