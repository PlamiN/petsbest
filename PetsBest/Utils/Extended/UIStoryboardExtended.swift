//
//  UIStoryboardExtended.swift
//  PetsBest
//
//  Created by Plamena on 20.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

struct Storyboard {
    
    static let main = "Main"
}

extension UIStoryboard {
    
    class var main: UIStoryboard {
        return UIStoryboard(name: Storyboard.main, bundle: nil)
    }
    
    func controllerWithID(_ id: StoryboardID) -> UIViewController {
        return self.instantiateViewController(withIdentifier: id.rawValue)
    }
}

enum StoryboardID: String {
    
    case tabBarViewController
    
    var name: String {
        return self.rawValue
    }
}
