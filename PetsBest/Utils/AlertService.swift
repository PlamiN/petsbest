//
//  AlertService.swift
//  PetsBest
//
//  Created by Plamena on 31.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(
        withTitle title: String = "",
        message: String = "",
        buttonTitle: String = "",
        preferredStyle: UIAlertController.Style = .alert, completion: (() -> Void)? = nil ) {
        
        let alerController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        let okAction = UIAlertAction(title: buttonTitle, style: .default) { _ in
            completion?()
            alerController.dismiss(animated: true, completion: nil)
        }
        alerController.addAction(okAction)
        
        self.present(alerController, animated: true, completion: nil)
    }
    
    func showActionSheetWithCancel(title: [String], action: @escaping (String) -> ()) {
        
        let actionSheet:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        for value in title {
            actionSheet.addAction(UIAlertAction(title: value, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
               // action
                action(value)

            }))
        }
        let alertAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            action("Cancel") // or for cancel call it here
        }
        actionSheet.addAction(alertAction)
        actionSheet.popoverPresentationController?.sourceView = self.view
        actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        self.present(actionSheet, animated: true, completion: nil)
      }
}
