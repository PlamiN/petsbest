//
//  Validator.swift
//  PetsBest
//
//  Created by Plamena on 9.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class Validator {
    static let EMAIL_ADDRESS =
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+"

    // Validator e-mail from string
    static func isValidEmail(_ value: String) -> Bool {
        let string = value.trimmingCharacters(in: .whitespacesAndNewlines)
        let predicate = NSPredicate(format: "SELF MATCHES %@", Validator.EMAIL_ADDRESS)
        return predicate.evaluate(with: string) || string.isEmpty
    }
    
    static func isEmpty(field: UITextField, value: String) -> Bool {
     let string = value.trimmingCharacters(in: .whitespacesAndNewlines)
        if string.count > 0 {
             return false
        }
        else {
            return true
        }
    }
}
