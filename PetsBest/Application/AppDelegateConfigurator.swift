//
//  AppDelegateConfigurator.swift
//  PetsBest
//
//  Created by Plamena on 9.07.20.
//  Copyright © 2020 Plamena. All rights reserved.
//

import UIKit

class AppDelegateConfigurator {

    // MARK: - Singleton
    static var shared = AppDelegateConfigurator()
    
    private init() {}

    // MARK: - Setup
    func configure() {

        // Apply App Theme
        //ThemeManager.shared.applyTheme()
    }
}

