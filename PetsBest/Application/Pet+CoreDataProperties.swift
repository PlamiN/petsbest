//
//  Pet+CoreDataProperties.swift
//  PetsBest
//
//  Created by Plamena on 21.08.20.
//  Copyright © 2020 Plamena. All rights reserved.
//
//

import Foundation
import CoreData


extension Pet {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Pet> {
        return NSFetchRequest<Pet>(entityName: "Pet")
    }

    @NSManaged public var policyNumber: String
    @NSManaged public var petName: String?
    @NSManaged public var breedName: String?
    @NSManaged public var image: Data?
    @NSManaged public var sex: String?
    @NSManaged public var age: String?
    @NSManaged public var species: String?
    @NSManaged public var planDescription: String?
    @NSManaged public var planType: String?
    @NSManaged public var deductible: String
    @NSManaged public var reimbursement: String

}
